/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util.setting.Open;

import com.je.common.base.service.rpc.SystemSettingRpcService;

public class OpenWeChat{

    private String corpId;

    private String corpSecret;

    private String secret;

    private String agentId;

    public OpenWeChat build(SystemSettingRpcService systemSettingRpcService) {
        corpId = systemSettingRpcService.findSettingValue("OPEN_WX_CORPID");
        corpSecret = systemSettingRpcService.findSettingValue("OPEN_WX_CORPSECRET");
        secret = systemSettingRpcService.findSettingValue("OPEN_WX_SECRET");
        agentId = systemSettingRpcService.findSettingValue("OPEN_WX_AGENTID");
        return this;
    }

    public OpenWeChat() {
    }

    public OpenWeChat(String corpId, String corpSecret, String secret, String agentId) {
        this.corpId = corpId;
        this.corpSecret = corpSecret;
        this.secret = secret;
        this.agentId = agentId;
    }

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getCorpSecret() {
        return corpSecret;
    }

    public void setCorpSecret(String corpSecret) {
        this.corpSecret = corpSecret;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }
}
