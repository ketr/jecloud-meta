/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util.setting.push;


import com.je.common.base.service.rpc.SystemSettingRpcService;

public class NoteAliyun {

    private String signName;

    private String template;

    private String accessKeyId;

    private String accessKeySecret;

    private String url;

    public NoteAliyun build(SystemSettingRpcService systemSettingRpcService) {
        signName = systemSettingRpcService.findSettingValue("NOTE_ALIYUN_SIGNNAME");
        template = systemSettingRpcService.findSettingValue("NOTE_ALIYUN_TEMPLATE");
        accessKeyId = systemSettingRpcService.findSettingValue("NOTE_ALIYUN_ACCESSKEY");
        accessKeySecret = systemSettingRpcService.findSettingValue("NOTE_ALIYUN_SECRET");
        url = systemSettingRpcService.findSettingValue("NOTE_ALIYUN_URL");
        return this;
    }

    public NoteAliyun() {
    }

    public NoteAliyun(String signName, String template, String accessKeyId, String accessKeySecret, String url) {
        this.signName = signName;
        this.template = template;
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        this.url = url;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public void setAccessKeySecret(String accessKeySecret) {
        this.accessKeySecret = accessKeySecret;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
