/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util.setting.document;

import com.je.common.base.service.rpc.SystemSettingRpcService;

public class DocumentWps{

    private String appId;

    private String editHost;

    private String appSecret;
    /**
     * 预览地址
     */
    private String host;

    /**
     * 回调地址
     */
    private String callHost;

    public DocumentWps() {

    }

    public DocumentWps(String appId, String editHost, String appSecret, String host, String callHost) {
        this.appId = appId;
        this.editHost = editHost;
        this.appSecret = appSecret;
        this.host = host;
        this.callHost = callHost;
    }

    public DocumentWps build(SystemSettingRpcService systemSettingRpcService) {
        host = systemSettingRpcService.findSettingValue("DOCUMENT_WPS_READHOST");
        callHost = systemSettingRpcService.findSettingValue("DOCUMENT_WPS_CALLHOST");
        appId = systemSettingRpcService.findSettingValue("DOCUMENT_WPS_APPID");
        editHost = systemSettingRpcService.findSettingValue("DOCUMENT_WPS_EDITHOST");
        appSecret = systemSettingRpcService.findSettingValue("DOCUMENT_WPS_APPSECRET");
        return this;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getEditHost() {
        return editHost;
    }

    public void setEditHost(String editHost) {
        this.editHost = editHost;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getCallHost() {
        return callHost;
    }

    public void setCallHost(String callHost) {
        this.callHost = callHost;
    }
}
