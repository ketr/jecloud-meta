/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util.setting.push;

import com.je.common.base.service.rpc.SystemSettingRpcService;

public class NoteWj {

    private String uid;

    private String key;

    private String keyMd5;

    private String signName;

    private String url;

    public NoteWj build(SystemSettingRpcService systemSettingRpcService) {
        uid = systemSettingRpcService.findSettingValue("NOTE_SYS_UID");
        key = systemSettingRpcService.findSettingValue("NOTE_SYS_KEY");
        keyMd5 = systemSettingRpcService.findSettingValue("NOTE_SYS_MD5");
        signName = systemSettingRpcService.findSettingValue("NOTE_SYS_SIGNNAME");
        url = systemSettingRpcService.findSettingValue("NOTE_SYS_URL");
        return this;
    }

    public NoteWj() {
    }

    public NoteWj(String uid, String key, String keyMd5, String signName, String url) {
        this.uid = uid;
        this.key = key;
        this.keyMd5 = keyMd5;
        this.signName = signName;
        this.url = url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKeyMd5() {
        return keyMd5;
    }

    public void setKeyMd5(String keyMd5) {
        this.keyMd5 = keyMd5;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
