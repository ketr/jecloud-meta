/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.util.database;

import java.util.Locale;
import java.util.StringTokenizer;

/**
 * DDL的SQL语句格式化(摘自hibernate)
 *
 * @author tang
 */
public class DDLFormatterUtils {

  public static String format(String sql) {
    if (null == sql || 0 == sql.length()) {
      return sql;
    }
    if (sql.toLowerCase(Locale.ROOT).startsWith("create table")) {
      return formatCreateTable(sql);
    } else if (sql.toLowerCase(Locale.ROOT).startsWith("alter table")) {
      return formatAlterTable(sql);
    } else if (sql.toLowerCase(Locale.ROOT).startsWith("comment on")) {
      return formatCommentOn(sql);
    } else {
      return "\n    " + sql;
    }
  }

  private static String formatCommentOn(String sql) {
    final StringBuilder result = new StringBuilder(60).append("    ");
    final StringTokenizer tokens = new StringTokenizer(sql, " '[]\"", true);

    boolean quoted = false;
    while (tokens.hasMoreTokens()) {
      final String token = tokens.nextToken();
      result.append(token);
      if (isQuote(token)) {
        quoted = !quoted;
      } else if (!quoted) {
        if ("is".equals(token)) {
          result.append("\n       ");
        }
      }
    }

    return result.toString();
  }

  private static String formatAlterTable(String sql) {
    final StringBuilder result = new StringBuilder(60).append("    ");
    final StringTokenizer tokens = new StringTokenizer(sql, " (,)'[]\"", true);

    boolean quoted = false;
    while (tokens.hasMoreTokens()) {
      final String token = tokens.nextToken();
      if (isQuote(token)) {
        quoted = !quoted;
      } else if (!quoted) {
        if (isBreak(token)) {
          result.append("\n        ");
        }
      }
      result.append(token);
    }

    return result.toString();
  }

  private static String formatCreateTable(String sql) {
    final StringBuilder result = new StringBuilder(60).append("    ");
    final StringTokenizer tokens = new StringTokenizer(sql, "(,)'[]\"", true);

    int depth = 0;
    boolean quoted = false;
    while (tokens.hasMoreTokens()) {
      final String token = tokens.nextToken();
      if (isQuote(token)) {
        quoted = !quoted;
        result.append(token);
      } else if (quoted) {
        result.append(token);
      } else {
        if (")".equals(token)) {
          depth--;
          if (depth == 0) {
            result.append("\n    ");
          }
        }
        result.append(token);
        if (",".equals(token) && depth == 1) {
          result.append("\n       ");
        }
        if ("(".equals(token)) {
          depth++;
          if (depth == 1) {
            result.append("\n        ");
          }
        }
      }
    }

    return result.toString();
  }

  private static boolean isBreak(String token) {
    return "drop".equals(token) ||
        "add".equals(token) ||
        "references".equals(token) ||
        "foreign".equals(token) ||
        "on".equals(token);
  }

  private static boolean isQuote(String tok) {
    return "\"".equals(tok) ||
        "`".equals(tok) ||
        "]".equals(tok) ||
        "[".equals(tok) ||
        "'".equals(tok);
  }

}
