/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.table.ColumnType;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.spring.SpringContextHolder;

import java.util.List;

public class SqlUtil {

	public static final String IN = "in";
	public static final String NOT_IN = "not in";
	public static final String LIKE ="like";
	
	public static String escape(String fieldValue) {
		return fieldValue.replaceAll("'", "''");
	}

	/**
	 * 获取字符串过长的字段code
	 * @return
	 */
	public static String getStringLong(DynaBean dynaBean){
		BeanService beanService = SpringContextHolder.getBean(BeanService.class);
		DynaBean resourceTable = beanService.getResourceTable(dynaBean.getStr(BeanService.KEY_TABLE_CODE));
		List<DynaBean> columns = resourceTable.getDynaBeanList(BeanService.KEY_TABLE_COLUMNS);
		String errors="";
		for(DynaBean column:columns){
			String type=column.getStr("TABLECOLUMN_TYPE");
			String code=column.getStr("TABLECOLUMN_CODE");
			//排除字段
			if(ColumnType.NUMBER.equals(type) || ColumnType.CLOB.equals(type)|| ColumnType.BIGCLOB.equals(type) || ColumnType.FLOAT.equals(type) || ColumnType.CUSTOM.equals(type)){
				continue;
			}
			Integer length=255;
			if(ColumnType.ID.equals(type) || ColumnType.FOREIGNKEY.equals(type) || ColumnType.DATETIME.equals(type)){
				length=50;
			}else if(ColumnType.VARCHAR100.equals(type)){
				length=100;
			}else if(ColumnType.VARCHAR50.equals(type)){
				length=50;
			}else if(ColumnType.VARCHAR30.equals(type)){
				length=30;
			}else if(ColumnType.VARCHAR767.equals(type)){
				length=767;
			}else if(ColumnType.VARCHAR1000.equals(type)){
				length=1000;
			}else if(ColumnType.VARCHAR2000.equals(type)){
				length=2000;
			}else if(ColumnType.VARCHAR4000.equals(type)){
				length=4000;
			}else if(ColumnType.VARCHAR.equals(type)){
				length=Integer.parseInt(column.getStr("TABLECOLUMN_LENGTH","0"));
			}else if(ColumnType.VARCHAR4000.equals(type)){
				length=4000;
			}else if(ColumnType.YESORNO.equals(type)){
				length=4;
			}
			if(dynaBean.containsKey(code)){
				String value=dynaBean.getStr(code);
				if(value.length()>length){
					errors=column.getStr("TABLECOLUMN_NAME")+"【"+column.getStr("TABLECOLUMN_CODE")+"】字段过长，请修改字段类型!";
					break;
				}
			}
		}
		return errors;
	}
}
