/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.model.database.type;

import java.util.Arrays;

/**
 * 数据库产品类型的枚举定义
 *
 * @author Tang
 */
public enum ProductTypeEnum {
  /**
   * 未知数据库类型
   */
  UNKNOWN(0),

  /**
   * MySQL数据库类型
   */
  MYSQL(1),

  /**
   * Oracle数据库类型
   */
  ORACLE(2),

  /**
   * SQLServer 2000数据库类型
   */
  SQLSERVER2000(3),

  /**
   * SQLServer数据库类型
   */
  SQLSERVER(4),

  /**
   * PostgreSQL数据库类型
   */
  POSTGRESQL(5),

  /**
   * Greenplum数据库类型
   */
  GREENPLUM(6),

  /**
   * MariaDB数据库类型
   */
  MARIADB(7),

  /**
   * DB2数据库类型
   */
  DB2(8),

  /**
   * [国产]达梦数据库类型
   */
  DM(9),

  /**
   * [国产]人大金仓数据库类型
   */
  KINGBASE(10),

  /**
   * [国产]神通数据库
   */
  OSCAR(11),

  /**
   * [国产]南大通用GBase8a数据库
   */
  GBASE8A(12),

  /**
   * HIVE数据库
   */
  HIVE(13),

  /**
   * SQLite数据库
   */
  SQLITE3(14),

  /**
   * Sybase数据库类型
   */
  SYBASE(15),
  ;

  private int index;

  ProductTypeEnum(int idx) {
    this.index = idx;
  }

  public int getIndex() {
    return index;
  }

  public boolean noCommentStatement() {
    return Arrays.asList(
            ProductTypeEnum.MYSQL,
            ProductTypeEnum.MARIADB,
            ProductTypeEnum.GBASE8A,
            ProductTypeEnum.HIVE,
            ProductTypeEnum.SQLITE3,
            ProductTypeEnum.SYBASE
    ).contains(this);
  }

}
