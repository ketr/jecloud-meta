/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model;

public class SQLTreeNodeType {
    /**
     * ID
     */
    public static final String ID = "idField";
    /**
     * NAME
     */
    public static final String NAME = "nameField";
    /**
     * CODE
     */
    public static final String CODE = "codeField";

    /**
     * 路径
     */
    public static final String PATH = "pathField";

    /**
     * 字典类型
     */
    public static final String NODETYPE = "nodeTypeField";
    /**
     * 树形排序字段
     */
    public static final String TREEORDERINDEX = "treeOrderIndexField";
    /**
     * 父节点
     */
    public static final String PARENT = "parentField";

    /**
     * 是否禁用
     */
    public static final String DISABLED = "disabledField";

    /**
     * 图表样式
     */
    public static final String ICONCLS = "iconClsField";

    /**
     * 字体样式
     */
    public static final String FONTCLS = "fontClsField";

    /**
     * 字体背景颜色
     */
    public static final String FONTBACKGROUNDCLS = "fontBackgroundClsField";

}
