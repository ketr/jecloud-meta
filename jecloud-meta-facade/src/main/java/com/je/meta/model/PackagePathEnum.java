/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model;

import org.slf4j.helpers.MessageFormatter;

/**
 * 项目升级包文件路径枚举
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/9/25
 */
public enum PackagePathEnum {

    /**
     * 资源表模块数据
     */
    TABLE_MODULES("/tables/modules.json"),
    /**
     * 资源表和视图数据
     */
    TABLE_TABLES("/tables/tables.json"),
    /**
     * 资源表和视图列
     * 参数:tableCode
     */
    TABLE_COLUMNS("/tables/{}/columns.json"),
    /**
     * 资源表和视图键
     * 参数:tableCode
     */
    TABLE_KEYS("/tables/{}/keys.json"),
    /**
     * 资源表索引
     * 参数:tableCode
     */
    TABLE_INDEXS("/tables/{}/indexs.json"),
    /**
     * 视图创建语句
     * 参数:tableCode,DBNAME
     */
    TABLE_VIEW_SQL("/tables/{}/view_{}.json"),

    /**
     * 打包功能父级的子系统和模块
     */
    FUNC_SUB_SYSTEM("/funcInfo/subSystem.json"),
    /**
     * 打包的功能
     */
    FUNC_INFO("/funcInfo/funcInfo.json"),
    /**
     * 功能列表的列信息
     * 参数:funcCode
     */
    FUNC_COLUMNS("/funcInfo/{}/columns.json"),
    /**
     * 功能表单的字段信息
     * 参数:funcCode
     */
    FUNC_FIELDS("/funcInfo/{}/fields.json"),
    /**
     * 查询策略
     * 参数:funcCode
     */
    FUNC_QUERY("/funcInfo/{}/querys.json"),
    /**
     * 公共高级查询策略
     * 参数:funcCode
     */
    FUNC_GROUP_QUERY("/funcInfo/{}/groupquerys.json"),
    /**
     * 数据权限
     * 参数:funcCode
     */
    FUNC_PERMS("/funcInfo/{}/funcperms.json"),
    /**
     * 功能按钮
     * 参数:funcCode
     */
    FUNC_BUTTONS("/funcInfo/{}/buttons.json"),
    /**
     * 数据流转
     * 参数:funcCode
     */
    FUNC_DATAFLOW("/funcInfo/{}/dataflow.json"),
    /**
     * 功能按钮
     * 参数:funcCode
     */
    FUNC_CHILDREN("/funcInfo/{}/children.json"),

    /**
     * 流程
     */
    PROCESS("/process/process.json"),

    /**
     * 首页规划
     */
    PORTAL("/portal/portals.json"),
    /**
     * 首页规划数据
     * 参数:首页规划PkValue
     */
    PORTAL_DATA("/portal/datas/{}.json"),

    /**
     * 平台首页
     */
    HOME("/home/homes.json"),

    /**
     * 菜单
     */
    MENUS("/menus/menus.json"),

    /**
     * 数据源
     */
    DATASOURCE("/dataSources/dataSources.json"),
    /**
     * 数据源模块
     */
    DATASOURCE_MODULES("/dataSources/modules.json"),
    /**
     * 数据源配置数据
     * 参数:数据源PkValue
     */
    DATASOURCE_DATA("/dataSources/datas/{}.json"),

    /**
     * 图表
     */
    CHARTS("/charts/charts.json"),
    /**
     * 图表模块
     */
    CHARTS_MODULES("/charts/modules.json"),
    /**
     * 图表属性配置
     * 参数:图表PkValue
     */
    CHARTS_CONFIG_ATTR("/charts/datas/{}_ATTRCONFIG.json"),
    /**
     * 图表数据源配置
     * 参数:图表PkValue
     */
    CHARTS_CONFIG_DS("/charts/datas/{}_DSCONFIG.json"),

    /**
     * 报表
     */
    REPORTS("/reports/reports.json"),
    /**
     * 报表模块
     */
    REPORTS_MODULES("/reports/modules.json"),
    /**
     * 报表数据源配置
     * 参数:图表PkValue
     */
    REPORTS_CONFIG_DS("/reports/datas/{}_DSCONFIG.json"),

    /**
     * 定时任务
     */
    TASKS("/timedTasks/tasks.json"),

    /**
     * 系统变量
     */
    CONFIGS("/configs/datas.json"),

    /**
     * 业务数据
     */
    DATAS("/datas/datas.json"),
    /**
     * 业务数据
     * 参数:tableCode
     */
    DATAS_TABLES("/datas/{}.json"),

    /**
     * sqlServer
     */
    SQL_SQLSERVER("/sql/upgrade.sql"),
    /**
     * MYSQL
     */
    SQL_MYSQL("/sql/upgrade_mysql.sql"),
    /**
     * TIDB
     */
    SQL_TIDB("/sql/upgrade_tidb.sql"),
    /**
     * sqlServer
     */
    SQL_ORACLE("/sql/upgrade_oracle.sql"),
    /**
     * 神通数据库
     * oscar
     */
    SQL_OSCAR("/sql/upgrade_oscar.sql"),
    /**
     * 人大金仓
     * kingbase8
     */
    SQL_KINGBASEES("/sql/upgrade_kingbasees.sql"),
    /**
     * 达梦
     * kingbase8
     */
    SQL_DM("/sql/upgrade_dm.sql"),

    /**
     * 字典主表
     */
    DIC_DICTIONARY("/dictionary/dictionarys.json"),
    /**
     * 字典项
     * 参数:字典CODE
     */
    DIC_ITEM("/dictionary/{}_ITEMS.json"),

    /**
     * EXCEL导入模板
     */
    EXCEL_EXCELS("/excel/excels.json"),
    /**
     * EXCEL导入模板表格配置
     * 参数:EXCELGROUP_ID
     */
    EXCEL_SHEETS("/excel/{}_SHEETS.json"),
    /**
     * EXCEL导入模板表格配置
     * 参数:EXCELGROUP_ID
     */
    EXCEL_ITEMS("/excel/{}_ITEMS.json"),
    /**
     * EXCEL模板文件信息
     */
    EXCEL_FILES_MSG("/excel/files.json"),
    /**
     * EXCEL模板文件
     * 参数: 文件名，fileKey + . + 后缀
     */
    EXCEL_FILES("/excel/files/{}"),

    /**
     * 角色
     */
    ROLE("/role/roles.json"),

    /**
     * 手机APK表数据
     */
    PHONE_APKS("/phone/apks.json"),
    /**
     * 手机APP表数据
     */
    PHONE_APPS("/phone/apps.json"),
    /**
     * 手机APP功能字段
     * 参数:appId
     */
    PHONE_FIELDS("/phone/fields/{}.json"),
    /**
     * 手机插件
     */
    PHONE_PLUGINS("/phone/plugins.json"),
    /**
     * 手机插件数据
     */
    PHONE_PLUGIN_INFO("/phone/plugins/{}.json"),
    /**
     * 手机功能
     */
    PHONE_APPFUNCS("/phone/appfuncs.json"),
    /**
     * 手机功能数据
     */
    PHONE_APPFUNC_INFO("/phone/appfuncs/{}.json"),
    /**
     * app_versions表数据
     */
    PHONE_VERSIONS("/phone/appversions.json"),
    /**
     * 手机附件文件信息
     */
    PHONE_FILES_MSG("/phone/files.json"),
    /**
     * 手机附件文件
     * 参数: 文件名，fileKey
     */
    PHONE_FILES("/phone/files/{}"),

    /**
     * 升级记录
     */
    UPGRADE("/upgrade.json");

    String path;

    /**
     * 私有化构造器不允许外部新增
     *
     * @param path 路径字符串
     */
    private PackagePathEnum(String path) {
        this.path = path;
    }

    /**
     * 获取格式化后的路径
     *
     * @param params 格式化参数
     * @return java.lang.String
     */
    public String getPath(String... params) {
        //格式化字符串,params数组依次替换占位符{}
        return MessageFormatter.arrayFormat(path, params).getMessage();
    }
}
