/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model.database;

import com.je.common.base.util.StringUtil;
import com.je.meta.model.database.type.ImportedKeyCascadeTypeEnum;
import com.je.meta.model.database.type.ImportedKeyTypeEnum;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 外键
 */
public class ImportedKeyData {

    /**
     * 外键类型
     */
    private ImportedKeyTypeEnum type;
    /**
     * 级联类型
     */
    private ImportedKeyCascadeTypeEnum cascadeType;
    /**
     * 关联表
     */
    private String linkTable;
    /**
     * 关联字段
     */
    private String lineColumnCode;
    /**
     * 字段编码
     */
    private String columnCode;
    /**
     * 键编码
     */
    private String keyCode;

    public static ImportedKeyData buildDataByResult(ResultSet foreignKey, SchemaTableMeta tableMeta) {
        ImportedKeyData importedKeyData = new ImportedKeyData();
        try {
            String linkType = foreignKey.getString("DELETE_RULE");
            if ("0".equals(linkType) || "3".equals(linkType)) {
                importedKeyData.setCascadeType(ImportedKeyCascadeTypeEnum.CASCADE);
            } else if ("1".equals(linkType)) {
                importedKeyData.setCascadeType(ImportedKeyCascadeTypeEnum.NOACTION);
            } else if ("2".equals(linkType)) {
                importedKeyData.setCascadeType(ImportedKeyCascadeTypeEnum.SETNULL);
            }
            importedKeyData.setLinkTable(foreignKey.getString("PKTABLE_NAME"));
            importedKeyData.setLineColumnCode(foreignKey.getString("PKCOLUMN_NAME"));
            importedKeyData.setColumnCode(foreignKey.getString("FKCOLUMN_NAME"));
            importedKeyData.setKeyCode(foreignKey.getString("FK_NAME"));
            String linkTable = importedKeyData.getLinkTable();
            if (StringUtil.isEmpty(linkTable)) {
                tableMeta.setParentTableCodes("");
            } else {
                if ((linkTable).equals(tableMeta.getTableCode())) {
                    importedKeyData.setType(ImportedKeyTypeEnum.Inline);
                } else {
                    importedKeyData.setType(ImportedKeyTypeEnum.Foreign);
                    tableMeta.setParentTableCodes(linkTable);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return importedKeyData;
    }

    public ImportedKeyTypeEnum getType() {
        return type;
    }

    public void setType(ImportedKeyTypeEnum type) {
        this.type = type;
    }

    public ImportedKeyCascadeTypeEnum getCascadeType() {
        return cascadeType;
    }

    public void setCascadeType(ImportedKeyCascadeTypeEnum cascadeType) {
        this.cascadeType = cascadeType;
    }

    public String getLinkTable() {
        return linkTable;
    }

    public void setLinkTable(String linkTable) {
        this.linkTable = linkTable;
    }

    public String getLineColumnCode() {
        return lineColumnCode;
    }

    public void setLineColumnCode(String lineColumnCode) {
        this.lineColumnCode = lineColumnCode;
    }

    public String getColumnCode() {
        return columnCode;
    }

    public void setColumnCode(String columnCode) {
        this.columnCode = columnCode;
    }

    public String getKeyCode() {
        return keyCode;
    }

    public void setKeyCode(String keyCode) {
        this.keyCode = keyCode;
    }
}
