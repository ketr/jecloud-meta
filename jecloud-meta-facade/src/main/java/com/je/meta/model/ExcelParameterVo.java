/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.model;

import com.je.common.base.DynaBean;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.List;

public class ExcelParameterVo implements Serializable {

    private String dotype;

    private DynaBean group;

    private DynaBean sheet;

    private List<DynaBean> datas;

    private HttpServletRequest request;

    /**
     * sheet处理数据前后逻辑
     */
    public ExcelParameterVo(String dotype, DynaBean group, DynaBean sheet, List<DynaBean> datas, HttpServletRequest request) {
        this.dotype = dotype;
        this.group = group;
        this.sheet = sheet;
        this.datas = datas;
        this.request = request;
    }

    /**
     * sheet处理数据
     * @param dotype
     * @param group
     * @param sheet
     * @param datas
     */
    public ExcelParameterVo(String dotype, DynaBean group, DynaBean sheet, List<DynaBean> datas) {
        this.dotype = dotype;
        this.group = group;
        this.sheet = sheet;
        this.datas = datas;
    }


    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public String getDotype() {
        return dotype;
    }

    public void setDotype(String dotype) {
        this.dotype = dotype;
    }

    public DynaBean getGroup() {
        return group;
    }

    public void setGroup(DynaBean group) {
        this.group = group;
    }

    public DynaBean getSheet() {
        return sheet;
    }

    public void setSheet(DynaBean sheet) {
        this.sheet = sheet;
    }

    public List<DynaBean> getDatas() {
        return datas;
    }

    public void setDatas(List<DynaBean> datas) {
        this.datas = datas;
    }
}
