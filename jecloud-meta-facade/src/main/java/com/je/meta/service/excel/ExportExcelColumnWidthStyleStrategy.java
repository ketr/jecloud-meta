/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.excel;

import com.alibaba.excel.enums.CellDataTypeEnum;
import com.alibaba.excel.metadata.Head;
import com.alibaba.excel.metadata.data.CellData;
import com.alibaba.excel.metadata.data.WriteCellData;
import com.alibaba.excel.write.handler.context.CellWriteHandlerContext;
import com.alibaba.excel.write.metadata.holder.WriteSheetHolder;
import com.alibaba.excel.write.style.column.AbstractColumnWidthStyleStrategy;
import org.apache.poi.ss.usermodel.Cell;

import java.util.List;

/**
 * 列宽
 */
public class ExportExcelColumnWidthStyleStrategy extends AbstractColumnWidthStyleStrategy {

    @Override
    public void setColumnWidth(CellWriteHandlerContext context) {
        this.setColumnWidth(context.getWriteSheetHolder(), context.getCellDataList(), context.getCell(), context.getHeadData(), context.getRelativeRowIndex(), context.getHead());
    }

    @Override
    protected void setColumnWidth(WriteSheetHolder writeSheetHolder, List<WriteCellData<?>> cellDataList, Cell cell, Head head,
                                  Integer relativeRowIndex, Boolean isHead) {
        Integer width = columnWidth(cellDataList, cell.getColumnIndex());
        if (width != null) {
            width = width * 256;
            if (writeSheetHolder.getSheet().getColumnWidth(cell.getColumnIndex()) < width) {
                if (width > 65280) {
                    writeSheetHolder.getSheet().setColumnWidth(cell.getColumnIndex(), 60000);
                }else {
                    writeSheetHolder.getSheet().setColumnWidth(cell.getColumnIndex(), width);
                }
            }
        }
    }

    private Integer columnWidth(List cellDataList, Integer integer) {
        CellData cellData = (CellData) cellDataList.get(0);
        CellDataTypeEnum type = cellData.getType();
        if (type == null) {
            return -1;
        } else {

            switch (type) {

                case STRING:
                    return cellData.getStringValue().getBytes().length + 2;

                case BOOLEAN:

                    return cellData.getBooleanValue().toString().getBytes().length;

                case NUMBER:

                    return cellData.getNumberValue().toString().getBytes().length;

                default:

                    return -1;

            }
        }
    }

}
