/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.excel.converter;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;

import java.text.DecimalFormat;
import java.util.Map;

public class ExcelExportNumberConverter extends AbstractExcelExportConverter {


    @Override
    public Object converter(String value, DynaBean formColumn, Map<String, Map<String, String>> ddInfos) {
        String configInfo = formColumn.getStr("RESOURCEFIELD_CONFIGINFO");
        int precision = 0;
        if (!Strings.isNullOrEmpty(configInfo)) {
            try {
                precision = Integer.parseInt(configInfo);
            } catch (Exception e) {
                precision = 0;
            }
        }
        return formatNumber(value, precision);
    }

    public static String formatNumber(String number, int precision) {
        double num = 0;
        try {
            num = Double.parseDouble(number);
        } catch (NumberFormatException e) {
            return number;
        }
        String pattern = "#.";
        for (int i = 0; i < precision; i++) {
            pattern += "#";
        }
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        String resultString = decimalFormat.format(num);
        if (precision == 0) {
            return resultString.replace(".", "");
        }
        return resultString;
    }

}


