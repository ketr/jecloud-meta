/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.excel.ExcelErrorCode;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.MetaService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.ReflectionUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.model.ExcelParamVo;
import com.je.meta.model.ExcelParameterVo;
import com.je.meta.model.ExcelReturnVo;
import com.je.meta.service.WayImportExcelHandleDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class WayImportExcelHandleDataServiceImpl implements WayImportExcelHandleDataService {
    @Autowired
    private MetaService metaService;

    @Override
    public ExcelReturnVo handleDataBySql(String sql, Map<String, Object> ddMap) {
        ExcelReturnVo excelReturnVo = new ExcelReturnVo();
       /* Map<String, Object> beanMap = new HashMap<>();
        if (paramVo.getDynaBean() != null) {
            beanMap = paramVo.getDynaBean().clone().getValues();
        }*/
        if(StringUtil.isNotEmpty(sql)){
            sql = StringUtil.parseKeyWord(sql, ddMap);
            //sql = StringUtil.parseKeyWord(sql, beanMap);
            metaService.executeSql(sql);
        }
        return excelReturnVo;
    }

    @Override
    public ExcelReturnVo handleDataByBean(String beanName, String beanMethod,  ExcelParameterVo paramVo) {
        ExcelReturnVo returnVo = new ExcelReturnVo(1, "成功");
        if (StringUtil.isNotEmpty(beanName) && StringUtil.isNotEmpty(beanMethod)) {
            //处理前执行类
            Object bean = SpringContextHolder.getBean(beanName);
            returnVo = (ExcelReturnVo) ReflectionUtils.getInstance().invokeMethod(bean, beanMethod, new Object[]{paramVo});
            if (ExcelErrorCode.STOPERROR == returnVo.getCode()) {
                throw new PlatformException(returnVo.getMsg(), PlatformExceptionEnum.JE_CORE_EXCEL_IMPDATA_ERROR, new Object[]{returnVo});
            }
        }
        return returnVo;
    }
}
