/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.je.common.base.DynaBean;
import com.je.meta.service.SheetImporExceltExecutionService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ReadExcelListener extends AnalysisEventListener<Map<Integer,Object>> {

    private static final int BATCH_COUNT = 5;
    List<Map<Integer, Object>> list = new ArrayList<>();

    private DynaBean group;
    private DynaBean sheet;
    private SheetImporExceltExecutionService sheetImporExceltExecutionService;
    private HttpServletRequest request;
    private int startRow;
    private int endRow;

    public ReadExcelListener(DynaBean group, DynaBean sheet, SheetImporExceltExecutionService sheetImporExceltExecutionService,HttpServletRequest request){
        this.group =group;
        this.sheet=sheet;
        this.sheetImporExceltExecutionService=sheetImporExceltExecutionService;
        this.request = request;
        this.startRow =sheet.getInt("STARTROWS",0);
        this.endRow =sheet.getInt("EXCELSHEET_JSX",0);
    }

    @Override
    public void onException(Exception exception, AnalysisContext context) throws Exception {
        System.out.println("读取excel异常");
    }

    @Override
    public void invoke(Map<Integer, Object> integerObjectMap, AnalysisContext analysisContext) {
        list.add(integerObjectMap);
        Integer rowIndex = analysisContext.readRowHolder().getRowIndex();
        if(list.size()>=BATCH_COUNT  ||rowIndex.equals(analysisContext.readSheetHolder().getApproximateTotalRowNumber()-1)){
            sheetImporExceltExecutionService.handleData(group,sheet,list,request);
            list.clear();
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        System.out.println("读取excel完成");
    }

    /**
     * 是否往下读
     * @param context
     * @return
     */
    @Override
    public boolean hasNext(AnalysisContext context) {
        if(endRow>startRow&&context.readRowHolder().getRowIndex()==endRow){
            return false;
        }
        return true;
    }
}
