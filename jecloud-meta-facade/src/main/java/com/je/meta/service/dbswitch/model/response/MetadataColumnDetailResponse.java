/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dbswitch.model.response;

import io.swagger.annotations.ApiModel;

@ApiModel("字段元数据详情")
public class MetadataColumnDetailResponse {

    private String fieldName;
    private String typeName;
    private String typeClassName;
    private String fieldType;
    private String displaySize;
    private String scaleSize;
    private String precisionSize;
    private String isPrimaryKey;
    private String isAutoIncrement;
    private String isNullable;
    private String remarks;

    public static MetadataColumnDetailResponse builder() {
        return new MetadataColumnDetailResponse();
    }

    public String getFieldName() {
        return fieldName;
    }

    public MetadataColumnDetailResponse fieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public String getTypeName() {
        return typeName;
    }

    public MetadataColumnDetailResponse typeName(String typeName) {
        this.typeName = typeName;
        return this;
    }

    public String getTypeClassName() {
        return typeClassName;
    }

    public MetadataColumnDetailResponse typeClassName(String typeClassName) {
        this.typeClassName = typeClassName;
        return this;
    }

    public String getFieldType() {
        return fieldType;
    }

    public MetadataColumnDetailResponse fieldType(String fieldType) {
        this.fieldType = fieldType;
        return this;
    }

    public String getDisplaySize() {
        return displaySize;
    }

    public MetadataColumnDetailResponse displaySize(String displaySize) {
        this.displaySize = displaySize;
        return this;
    }

    public String getScaleSize() {
        return scaleSize;
    }

    public MetadataColumnDetailResponse scaleSize(String scaleSize) {
        this.scaleSize = scaleSize;
        return this;
    }

    public String getPrecisionSize() {
        return precisionSize;
    }

    public MetadataColumnDetailResponse precisionSize(String precisionSize) {
        this.precisionSize = precisionSize;
        return this;
    }

    public String getIsPrimaryKey() {
        return isPrimaryKey;
    }

    public MetadataColumnDetailResponse isPrimaryKey(String isPrimaryKey) {
        this.isPrimaryKey = isPrimaryKey;
        return this;
    }

    public String getIsAutoIncrement() {
        return isAutoIncrement;
    }

    public MetadataColumnDetailResponse isAutoIncrement(String isAutoIncrement) {
        this.isAutoIncrement = isAutoIncrement;
        return this;
    }

    public String getIsNullable() {
        return isNullable;
    }

    public MetadataColumnDetailResponse isNullable(String isNullable) {
        this.isNullable = isNullable;
        return this;
    }

    public String getRemarks() {
        return remarks;
    }

    public MetadataColumnDetailResponse remarks(String remarks) {
        this.remarks = remarks;
        return this;
    }
}
