/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dbswitch.model.response;

import io.swagger.annotations.ApiModel;

@ApiModel("表元数据信息")
public class MetadataTableInfoResponse {

    private String tableCode;
    private String schemaName;
    private String tableName;
    private String type;
    private String typeName;
    private String metaIsCreate;
    private String keyGeneratorName;
    private String keyGeneratorType;

    public static MetadataTableInfoResponse builder() {
        return new MetadataTableInfoResponse();
    }

    public String getTableCode() {
        return tableCode;
    }

    public MetadataTableInfoResponse tableName(String tableName) {
        this.tableCode = tableName;
        return this;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public MetadataTableInfoResponse schemaName(String schemaName) {
        this.schemaName = schemaName;
        return this;
    }

    public String getTableName() {
        return tableName;
    }

    public MetadataTableInfoResponse remarks(String remarks) {
        this.tableName = remarks;
        return this;
    }

    public String getType() {
        return type;
    }

    public MetadataTableInfoResponse type(String type) {
        this.type = type;
        if(type.equals("PT")){
            this.typeName="表";
        }else {
            this.typeName="视图";
        }
        return this;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getMetaIsCreate() {
        return metaIsCreate;
    }

    public MetadataTableInfoResponse metaIsCreate(String metaIsCreate) {
        this.metaIsCreate = metaIsCreate;
        return this;
    }

    public String getKeyGeneratorName() {
        return keyGeneratorName;
    }

    public MetadataTableInfoResponse keyGeneratorName(String keyGeneratorName) {
        this.keyGeneratorName = keyGeneratorName;
        return this;
    }

    public String getKeyGeneratorType() {
        return keyGeneratorType;
    }

    public MetadataTableInfoResponse keyGeneratorType(String keyGeneratorType) {
        this.keyGeneratorType = keyGeneratorType;
        return this;
    }
}
