/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaResourceService;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.ExcelDataHandleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service("previewExcelDataServiceImpl")
public class PreviewExcelDataServiceImpl implements ExcelDataHandleService {
    @Autowired
    private MetaResourceService metaResourceService;
    @Override
    public void handleData(String fileKey, String groupTemId, DynaBean group, HttpServletRequest request) {
        NativeQuery nativeQuery = NativeQuery.build().eq("JE_CORE_EXCELGROUP_ID",group.getStr("JE_CORE_EXCELGROUP_ID"));
        if(StringUtil.isNotEmpty(groupTemId)){
            nativeQuery.ne("JE_EXCEL_GROUPTEM_ID",groupTemId);
        }
        metaResourceService.deleteByTableCodeAndNativeQuery("JE_EXCEL_GROUPTEM",nativeQuery);


    }
}
