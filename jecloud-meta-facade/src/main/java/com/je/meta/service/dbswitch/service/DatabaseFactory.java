/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
// Copyright tang.  All rights reserved.
// https://gitee.com/inrgihc/dbswitch
//
// Use of this source code is governed by a BSD-style license
//
// Author: tang (inrgihc@126.com)
// Date : 2020/1/2
// Location: beijing , china
/////////////////////////////////////////////////////////////
package com.je.meta.service.dbswitch.service;

import com.je.meta.model.database.type.ProductTypeEnum;
import com.je.meta.service.dbswitch.service.impl.database.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * 数据库实例构建工厂类
 *
 * @author tang
 */
public final class DatabaseFactory {

    private static final Map<ProductTypeEnum, Callable<AbstractDatabase>> DATABASE_MAPPER
            = new HashMap<ProductTypeEnum, Callable<AbstractDatabase>>() {

        private static final long serialVersionUID = 9202705534880971997L;

        {
            put(ProductTypeEnum.MYSQL, DatabaseMysqlImpl::new);
            put(ProductTypeEnum.MARIADB, DatabaseMariaDBImpl::new);
            put(ProductTypeEnum.ORACLE, DatabaseOracleImpl::new);
            put(ProductTypeEnum.SQLSERVER2000, DatabaseSqlserver2000Impl::new);
            put(ProductTypeEnum.SQLSERVER, DatabaseSqlserverImpl::new);
            put(ProductTypeEnum.POSTGRESQL, DatabasePostgresImpl::new);
            put(ProductTypeEnum.GREENPLUM, DatabaseGreenplumImpl::new);
            put(ProductTypeEnum.DB2, DatabaseDB2Impl::new);
            put(ProductTypeEnum.DM, DatabaseDmImpl::new);
            put(ProductTypeEnum.KINGBASE, DatabaseKingbaseImpl::new);
            put(ProductTypeEnum.OSCAR, DatabaseOscarImpl::new);
            put(ProductTypeEnum.GBASE8A, DatabaseGbase8aImpl::new);
            put(ProductTypeEnum.HIVE, DatabaseHiveImpl::new);
            put(ProductTypeEnum.SQLITE3, DatabaseSqliteImpl::new);
        }
    };

    public static AbstractDatabase getDatabaseInstance(ProductTypeEnum type) {
        Callable<AbstractDatabase> callable = DATABASE_MAPPER.get(type);
        if (null != callable) {
            try {
                return callable.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        throw new UnsupportedOperationException(
                String.format("Unknown database type (%s)", type.name()));
    }

    private DatabaseFactory() {
        throw new IllegalStateException();
    }

}
