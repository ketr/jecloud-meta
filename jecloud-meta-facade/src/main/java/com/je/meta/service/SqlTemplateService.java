/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service;

import net.sf.jsqlparser.JSQLParserException;

import java.util.List;
import java.util.Map;

/**
 * SQL模板
 */
public interface SqlTemplateService {

    default int insert(String entryDesc, String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException {
        return insertAndInit(null, null, entryDesc, projectCode, templateCode, params);
    }

    default int insert(String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException {
        return insertAndInit(null, null, null, projectCode, templateCode, params);
    }

    /**
     * 按照模板插入
     *
     * @param templateCode
     * @return
     */
    int insert(String entryType, String entryName, String entryDesc, String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException;

    default int insertAndInit(String entryDesc, String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException {
        return insertAndInit(null, null, entryDesc, projectCode, templateCode, params);
    }

    default int insertAndInit(String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException {
        return insertAndInit(null, null, null, projectCode, templateCode, params);
    }

    /**
     * 按照模板插入，并自动填充系统默认字段
     *
     * @param templateCode
     * @param params
     * @return
     */
    int insertAndInit(String entryType, String entryName, String entryDesc, String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException;

    default int update(String entryDesc, String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException {
        return update(null, null, entryDesc, projectCode, templateCode, params);
    }

    default int update(String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException {
        return update(null, null, null, projectCode, templateCode, params);
    }

    /**
     * 按照模板更新
     *
     * @param templateCode
     * @return
     */
    int update(String entryType, String entryName, String entryDesc, String projectCode, String templateCode, Map<String, Object> params) throws JSQLParserException;

    default int delete(String entryDesc, String projectCode, String template, Map<String, Object> params) throws JSQLParserException {
        return delete(null, null, entryDesc, projectCode, template, params);
    }

    default int delete(String projectCode, String template, Map<String, Object> params) throws JSQLParserException {
        return delete(null, null, null, projectCode, template, params);
    }

    /**
     * 按照模板删除
     *
     * @param template
     * @return
     */
    int delete(String entryType, String entryName, String entryDesc, String projectCode, String template, Map<String, Object> params) throws JSQLParserException;

    default List<Map<String, Object>> select(String entryDesc, String projectCode, String template, Map<String, Object> params) throws JSQLParserException {
        return select(entryDesc, projectCode, template, params);
    }

    default List<Map<String, Object>> select(String projectCode, String template, Map<String, Object> params) throws JSQLParserException {
        return select(projectCode, template, params);
    }

    /**
     * 按照模板查询
     *
     * @param template
     * @return
     */
    List<Map<String, Object>> select(String entryType, String entryName, String entryDesc, String projectCode, String template, Map<String, Object> params) throws JSQLParserException;

    /**
     * 模板日志
     *
     * @param success
     * @param templateId
     * @param entryCode
     * @param entryName
     * @param entryDesc
     * @param outputSql
     */
    void log(boolean success, String templateId, String entryCode, String entryName, String entryDesc, String outputSql);
}
