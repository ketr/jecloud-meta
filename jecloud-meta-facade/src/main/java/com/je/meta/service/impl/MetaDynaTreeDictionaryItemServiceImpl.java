/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.dd.DDType;
import com.je.common.base.entity.QueryInfo;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.service.rpc.SystemVariableRpcService;
import com.je.common.base.util.StringUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.service.MetaDictionaryItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-17 13:44
 * @description: 树形外部字典项服务实现
 */
@Service("metaDynaTreeDictionaryItemService")
public class MetaDynaTreeDictionaryItemServiceImpl implements MetaDictionaryItemService {

    @Autowired
    private SystemVariableRpcService systemVariableRpcService;
    @Lazy
    @Autowired
    private CommonService commonService;
    @Autowired
    private BeanService beanService;

    @Override
    public List<JSONTreeNode> getDdItems(DynaBean dictionary, Map<String, String> params, QueryInfo queryInfo, Boolean en) {
        if(dictionary == null){
            throw new PlatformException("获取字典为空！",PlatformExceptionEnum.UNKOWN_ERROR);
        }
        //非列表字典抛出异常
        if(!DDType.DYNA_TREE.equals(dictionary.getStr("DICTIONARY_DDTYPE"))){
            throw new PlatformException("获取字典项异常，非法的字典类型" + dictionary.getStr("DICTIONARY_DDTYPE"),PlatformExceptionEnum.UNKOWN_ERROR);
        }

        //声明变量集合，用于解析whereSql的通配符
        Map<String,Object> ddMap = systemVariableRpcService.formatCurrentUserAndCachedVariables();
        if (params != null) {
            ddMap.putAll(params);
        }

        String querySql = StringUtil.getDefaultValue(queryInfo.getWhereSql(), "");
        String ddWhereSql = dictionary.getStr("DICTIONARY_WHERESQL", "");
        if (StringUtil.isNotEmpty(ddWhereSql)) {
            querySql += ddWhereSql;
        }
        if (StringUtil.isNotEmpty(querySql)) {
            querySql = StringUtil.parseKeyWord(querySql, ddMap);
        }
        queryInfo.setWhereSql(querySql);

        String tableName = dictionary.getStr("DICTIONARY_CLASSNAME");
        DynaBean table = beanService.getResourceTable(tableName);
        List<DynaBean> columns = (List<DynaBean>) table.get(BeanService.KEY_TABLE_COLUMNS);
        JSONTreeNode template = beanService.buildJSONTreeNodeTemplate(columns);
        String rootId = ConstantVars.TREE_ROOT;
        List<JSONTreeNode> list = commonService.getLocalJsonTreeNodeList(rootId, tableName, template, queryInfo);
        return list;
    }

}
