/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.google.common.base.Strings;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.service.ContractRpcService;
import org.apache.servicecomb.registry.DiscoveryManager;
import org.apache.servicecomb.registry.consumer.MicroserviceManager;
import org.apache.servicecomb.registry.consumer.MicroserviceVersions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/meta/rpcContract")
public class ContractRpcController {

    @Autowired
    @Qualifier("contractRpcService")
    private ContractRpcService contractRpcService;

    @RequestMapping(value = "/load", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult getContract() {
        Page page = new Page(0, 30);
        List list = contractRpcService.getContract();
        page.setRecords(list);
        page.setTotal(list.size());
        return BaseRespResult.successResultPage(page.getRecords(), (long) page.getTotal());
    }

    @RequestMapping(value = {"/remove"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8",
            name = "清理契约缓存")
    public void remove(String microKey) {
        Map<String, MicroserviceManager> map = DiscoveryManager.INSTANCE.getAppManager().getApps();
        for (String microservice : map.keySet()) {
            MicroserviceManager microserviceManager = map.get(microservice);
            if (Strings.isNullOrEmpty(microKey) || microKey.equals("all")) {//清理所有
                Map<String, MicroserviceVersions> microserviceVersionsMap = microserviceManager.getVersionsByName();
                for (String microserviceVersionKey : microserviceVersionsMap.keySet()) {
                    microserviceManager.getVersionsByName().remove(microserviceVersionKey);
                }
                return;
            }
            //清理指定的
            for (String k : microKey.split(",")) {
                microserviceManager.getVersionsByName().remove(k);
            }
        }
    }

}
