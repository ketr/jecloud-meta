/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.ArrayUtils;
import com.je.common.base.util.PingYinUtil;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/je/meta/office")
public class OfficeController extends AbstractPlatformController {
    @RequestMapping(value = "/doImplField", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doImplField(BaseMethodArgument param, HttpServletRequest request){
        String pkValue=param.getPkValue();
        List<DynaBean> resFields=metaService.select("JE_CORE_RESOURCEFIELD", ConditionsWrapper.builder().
                in("JE_CORE_RESOURCEFIELD_ID", param.getIds().split(",")).orderByAsc("SY_ORDERINDEX"));
        long count=metaService.selectCount("JE_OFFICE_FIELD"," AND JE_OFFICE_DATA_ID={0}",pkValue);
        for(DynaBean resField:resFields){
            String fieldCode=resField.getStr("TABLECOLUMN_CODE");
            DynaBean field=metaService.selectOne("JE_OFFICE_FIELD",ConditionsWrapper.builder().eq("JE_OFFICE_FIELD_ID",pkValue)
                    .eq("FIELD_CODE",fieldCode));
            if(field!=null){
                continue;
            }
            field=new DynaBean("JE_OFFICE_FIELD",true);
            commonService.buildFuncDefaultValues("JE_OFFICE_FIELD",field);
            field.set("FIELD_NAME",resField.getStr("RESOURCEFIELD_NAME"));
            field.set("FIELD_CODE",resField.getStr("RESOURCEFIELD_CODE"));
            field.set("JE_OFFICE_DATA_ID",pkValue);
            field.set("SY_ORDERINDEX",count+1);
            if(ArrayUtils.contains(new String[]{"numberfield"},resField.getStr("RESOURCEFIELD_XTYPE"))){
                if(StringUtil.isNotEmpty(resField.getStr("RESOURCEFIELD_CONFIGINFO"))) {
                    field.set("FIELD_TYPE", "FLOAT");
                }else{
                    field.set("FIELD_TYPE", "NUMBER");
                }
            }else if(ArrayUtils.contains(new String[]{"datetimefield"},resField.getStr("RESOURCEFIELD_XTYPE"))){
                field.set("FIELD_TYPE", "DATETIME");
            }else if(ArrayUtils.contains(new String[]{"datemonthdatefiled","datefield","datemonthfield"},resField.getStr("RESOURCEFIELD_XTYPE"))){
                field.set("FIELD_TYPE", "DATE");
            }else if(ArrayUtils.contains(new String[]{"rgroup","cgroup","cbbfield","treessfield","treessareafield"},resField.getStr("RESOURCEFIELD_XTYPE"))) {
                field.set("FIELD_TYPE","VARCHAR");
                field.set("FIELD_CONFIGINFO",resField.getStr("RESOURCEFIELD_CONFIGINFO"));
                field.set("FIELD_DZCONFIG",resField.getStr("RESOURCEFIELD_CODE")+"~code");
                field.set("EXCEL_CBBFIELD","text");
            }else if(ArrayUtils.contains(new String[]{"gridssfield","gridssareafield","searchfield","queryuserfield","queryuserareafield"},resField.getStr("RESOURCEFIELD_XTYPE"))){
                field.set("FIELD_TYPE","VARCHAR");
                field.set("FIELD_CONFIGINFO",resField.getStr("RESOURCEFIELD_CONFIGINFO"));
            }else{
                field.set("FIELD_TYPE","VARCHAR");
            }
            commonService.buildModelCreateInfo(field);
            metaService.insert(field);
            count++;
        }
        return BaseRespResult.successResult("成功");
    }
    @RequestMapping(value = "/doImplWfField", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doImplWfField(BaseMethodArgument param, HttpServletRequest request){
        String pkValue=param.getPkValue();
        String nodeNames=request.getParameter("nodeNames");
        String[] types=new String[]{"_task","_user","_seal","_start","_time","_date","_comment"};
        String[] typeNames=new String[]{"节点","人员","签字","接受时间","审批时间","审批日期","意见"};
        long count=metaService.selectCount("JE_OFFICE_FIELD"," AND JE_OFFICE_DATA_ID={0}",pkValue);
        for(String nodeName:nodeNames.split(",")){
            if(StringUtil.isEmpty(nodeName))continue;
            for(int i=0;i<types.length;i++) {
                String type=types[i];
                String typeName=typeNames[i];
                String key=nodeName+type;
                String fieldCode=PingYinUtil.getInstance().getFirstSpell(nodeName).toUpperCase()+type.toUpperCase();
                DynaBean field = metaService.selectOne("JE_OFFICE_FIELD", ConditionsWrapper.builder().eq("FIELD_CODE",fieldCode).eq("JE_OFFICE_DATA_ID",pkValue).eq("FIELD_TYPE","WFDATA"));
                if (field != null) {
                    continue;
                }
                field = new DynaBean("JE_OFFICE_FIELD", true);
                commonService.buildFuncDefaultValues("JE_OFFICE_FIELD", field);
                field.set("FIELD_NAME", nodeName+typeName);
                field.set("FIELD_CODE", fieldCode);
                field.set("JE_OFFICE_DATA_ID", pkValue);
                field.set("SY_ORDERINDEX", count + 1);
                field.set("FIELD_TYPE", "WFDATA");
                field.set("FIELD_CONFIGINFO", key);
                if("_seal".equals(type)){
                    field.set("FIELD_OTHERCONFIG","50*23");
                }
                commonService.buildModelCreateInfo(field);
                metaService.insert(field);
                count++;
            }
        }
        return BaseRespResult.successResult("成功");
    }
}
