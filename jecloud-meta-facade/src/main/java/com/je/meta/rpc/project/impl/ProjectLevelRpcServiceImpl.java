package com.je.meta.rpc.project.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.project.ProjectLevelRpcService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RpcSchema(schemaId = "projectLevelRpcService")
public class ProjectLevelRpcServiceImpl implements ProjectLevelRpcService {

    @Autowired
    private MetaService metaService;

    @Override
    public List<DynaBean> findAllWithOrder(String tableCode, String orderField, String sql) {
        return metaService.select(tableCode, ConditionsWrapper.builder().apply(sql).orderByAsc(orderField));
    }

}
