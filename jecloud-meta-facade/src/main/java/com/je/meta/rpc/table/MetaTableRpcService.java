/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.alibaba.fastjson2.JSONArray;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.extjs.DbModel;
import com.je.common.base.exception.APIWarnException;
import com.je.table.vo.TableDataView;
import java.util.List;
import java.util.Map;

/**
 * 资源表元数据对外部模块的服务定义
 */
public interface MetaTableRpcService {

    DynaBean changePrimaryKeyPolicy(String resourceId, String tableCode, String type, String incrementerName, String generatorSql);

    List<String> clearTableCache(String tableCode);

    List<String> generateChangeTablePrimaryKeyDDL(String tableCode, String oldPk, String newPK);

    /**
     * 查找资源表名称
     *
     * @param idList
     * @return
     */
    List<DynaBean> findTableNames(List<String> idList);

    //----------------------------创建开始----------------------------

    /**
     * 获取DDL创建语句
     *
     * @param resourceTableId 资源表元数据ID
     * @return 创建物理表的DDL语句
     */
    List<String> generateCreateDDL(String resourceTableId);

    /**
     * 数据库创建操作
     *
     * @param resourceTableId 资源表元数据ID
     * @return 是否成功
     */
    boolean createTable(String resourceTableId);

    /**
     * 本地创建完表之后更新资源表元数据状态
     *
     * @param resourceTableId 资源表元数据ID
     */
    DynaBean afterTableCreated(String resourceTableId);

    //----------------------------创建完成----------------------------

    //----------------------------更新开始----------------------------

    /**
     * 获取DDL创建语句
     *
     * @param resourceTableId 资源表元数据ID
     * @param dbModels        本地实体表模型，此模型通过pcDataService获取
     * @return 物理表更新的DDL语句集合
     */
    List<String> generateUpdateDDL(String resourceTableId, List<DbModel> dbModels);

    /**
     * 本地创建完表之后更新资源表元数据状态
     *
     * @param resourceTableId
     */
    void afterTableUpdated(String resourceTableId);
    //----------------------------更新完成----------------------------

    //----------------------------删除开始----------------------------

    /**
     * 获取删除物理表DDL
     *
     * @param ids
     * @return
     */
    List<String> generateRemoveDDL(String ids);

    //----------------------------删除开始----------------------------

    //----------------------------导入开始----------------------------

    /**
     * 通过表名,把数据量中的表导入到资源表中管理
     *
     * @param resourceTable 需要导入的资源表
     * @return
     */
    DynaBean impTable(DynaBean resourceTable);

    //----------------------------导入开始----------------------------

    /**
     * 同步表结构
     *
     * @param table    资源表信息
     * @param dbModels 从数据库拽出来的列信息
     * @return
     */
    DynaBean syncTable(DynaBean table, List<DbModel> dbModels, boolean isDdl);


    boolean updateTable(String resourceTableId, Boolean isFuncs);
    boolean updateFunTable(String resourceTableId);
    /**
     * 更新树形表数据(路径 层次 顺序 排序字段)
     *
     * @param tableCode 表编码
     * @param pkCode    主键code
     * @param preFix    preFix
     */
    void syncTreePath(String tableCode, String pkCode, String preFix);

    /**
     * 查询sql列表
     *
     * @param columnList
     * @param whereList
     * @param orderList
     * @return
     */
    List<Map<String, Object>> selectSqlTable(JSONArray columnList, JSONArray whereList, JSONArray orderList, String tableCode, String page);

    /**
     * 生成插入sql
     *
     * @param columnList
     * @param list
     * @param tableCode
     * @return
     */
    List<String> generateInsertSQL(JSONArray columnList, List<Map<String, Object>> list, String tableCode, boolean status);

    /**
     * 获取父表信息
     *
     * @param infoById
     * @return
     */
    List<TableDataView> selectParentTable(DynaBean infoById);

    /**
     * 获取子表信息
     *
     * @param infoById
     * @return
     */
    List<TableDataView> selectChildTable(DynaBean infoById);

    /**
     * 获取该表下所有视图信息
     *
     * @param dynaBean
     * @return
     */
    List<TableDataView> selectTableByView(DynaBean dynaBean);

    /**
     * 获取该表下所有主子功能
     *
     * @param infoById
     * @return
     */
    List<TableDataView> selectTableByFunc(DynaBean infoById);

    /**
     * 获取该表下所有菜单
     *
     * @return
     */
    List<TableDataView> selectTableByMenus(List<String> list);

    /**
     * @param dynaBean
     * @return
     */
    DynaBean doSave(DynaBean dynaBean);

    /**
     * 粘贴表
     *
     * @param newTableName
     * @param newTableCode
     * @param dynaBean
     * @param useNewName
     * @return
     */
    DynaBean pasteTable(String newTableName, String newTableCode, DynaBean dynaBean, String useNewName);

    /**
     * 修改表
     *
     * @param dynaBean
     * @return
     */
    DynaBean doUpdate(DynaBean dynaBean);

    /**
     * 表转移
     *
     * @param resourceId
     * @param toResourceId
     * @param place
     * @return
     */
    DynaBean move(String resourceId, String toResourceId, String place);

    /**
     * 根据表id获取表类型
     *
     * @param parentId
     * @return
     */
    String getTableType(String parentId);

    /**
     * 查询关系视图
     *
     * @param pkValue
     * @return
     */
    DynaBean selectRelation(String pkValue);

    /**
     * 查询历史留痕
     *
     * @param resourceId
     * @return
     */
    List<Map<String, Object>> loadHistoricalTraces(String resourceId);

    /**
     * 同步资源表字段
     *
     * @param pkValue
     * @param ckPkValue
     */
    void syncTableField(String pkValue, String ckPkValue);

    /**
     * 设置主键
     *
     * @param pkValue
     * @param newCode
     * @return
     */
    String changeTablePrimaryKey(String pkValue, String newCode) throws APIWarnException;

    /**
     * 在资源表中单独根据id查询
     *
     * @param resourceTable
     * @param resourceId
     * @return
     */
    DynaBean selectOneByPk(String resourceTable, String resourceId);


    /**
     * DDL完之后修改元数据  删除物理表状态
     *
     * @param ids
     */
    void removeTableMetaToMetaNoDDL(String ids);

    /**
     * 根据表编码和指定类型获取表信息
     *
     * @param tableCode
     * @return
     */
    DynaBean selectOneByCodeToType(String tableCode);

    /**
     * 根据资源表id查询实体表信息
     *
     * @param tableCode
     * @param resourcetableId
     * @return
     */
    List<DynaBean> selectTableByResourceTablePk(String tableCode, String resourcetableId);

    /**
     * 根据资源表id查询实体表信息
     *
     * @param je_core_tablecolumn
     * @param je_core_tablecolumn_id
     * @param split
     * @return
     */
    List<DynaBean> selectTableInIds(String je_core_tablecolumn, String je_core_tablecolumn_id, String[] split);

    /**
     * 根据资源表id查询实体表
     *
     * @param tablecolumnId
     * @return
     */
    List<DynaBean> selectColmListInIds(String tablecolumnId);

    /**
     * 修改时校验code
     *
     * @param table
     * @return
     */
    Boolean checkTableCodeExcludeCode(DynaBean table);

    /**
     * 获取删除表的DDL
     *
     * @param ids
     * @return
     */
    public String getDeleteTableDDlByIds(String ids);

    /**
     * 删除资源表元数据前校验
     *
     * @param ids
     */
    public String checkBeforeDeletion(String ids);

    /**
     * 导入表，添加元数据信息
     *
     * @param table
     */
    DynaBean afterImportMetaInfo(DynaBean table, List<Map<String, Object>> lists);

    /**
     * 导入表，之前的操作，初始化信息
     *
     * @param table
     * @return
     */
    DynaBean beforeImportBuildDynabeanInfo(DynaBean table);

    /**
     * 根据表编码获取表信息
     *
     * @param tableCode
     * @return
     */
    List<DynaBean> selectByColumnCode(String tableCode);

    /**
     * 应用表之前的校验
     */
    String checkTableForApply(String tableId);

}
