/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.func;

import com.je.common.base.DynaBean;

import java.util.List;
import java.util.Map;

/**
 * 功能RPC服务定义
 */
public interface MetaFuncRpcService {

    /**
     * 根据功能编码集合获取功能基础信息
     * @param funcCodeList
     * @return
     */
    List<DynaBean> findFuncAnsButtonBaseInformationByCode(List<String> funcCodeList);

    /**
     * 查找功能、子功能及所有功能按钮
     * @param funcCodeList
     * @return
     */
    Map<String, List<DynaBean>> findPermedFuncsAndChildFuncsWithButtonsByFuncCodes(List<String> funcCodeList);

    /**
     * 根据微应用编码获取微应用自定义按钮
     * @param microAppCodeList
     * @return
     */
    Map<String, List<DynaBean>> findMicroAppButtonsByMicroAppCodes(List<String> microAppCodeList);

    /**
     * 根据微应用编码获取微应用
     * @param microAppCodeList
     * @return
     */
    Map<String,DynaBean> findMicroAppByMicroAppCodes(List<String> microAppCodeList);

    /**
     * 查找功能、子功能及所有功能按钮
     * @param funcIdList
     * @return
     */
    Map<String, List<DynaBean>> findPermedFuncsAndChildFuncsWithButtonsByFuncIds(List<String> funcIdList);

    /**
     * 通过子功能找到所有功能信息
     * @param subFuncIdList
     * @return Map<String,DynaBean> key为子功能主键，bean为父功能bean信息
     */
    Map<String,DynaBean> findSubFuncParentFuncInfos(List<String> subFuncIdList);

    /**
     * 查找功能的产品
     * @param funcCodeList
     * @return
     */
    Map<String,String> findFuncProductMap(List<String> funcCodeList);

    /**
     * 禁用功能表单字段
     * @param funcCode
     * @param columnCode
     */
    void disableFormField(String funcCode,String columnCode);

    /**
     * 启用功能表单字段
     * @param funcCode
     * @param columnCode
     */
    void enableFormField(String funcCode,String columnCode);
}
