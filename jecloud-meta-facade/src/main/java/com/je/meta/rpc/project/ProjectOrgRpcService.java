package com.je.meta.rpc.project;

import com.je.common.base.DynaBean;

import java.util.List;

public interface ProjectOrgRpcService {

    List<DynaBean> findProjectOrgWithOrder(String tableCode, String projectIdColumn, String projectId, String orderColumn, String orgProjectGlSql);

    /**
     * 获取人员所在项目信息
     *
     * @return
     */
    List<DynaBean> findProjectInformationPersonnel(String userTable, String accDeptId,
                                                   String accDeptUserIdFiledCode, String userOrgIdFiled,
                                                   String orgTable, String orgTablePkField, String orgOrderByCodeFiled,
                                                   String userGlSql, String orgProjectGlSql);
}
