package com.je.meta.rpc.project;

import com.je.common.base.DynaBean;

import java.util.List;

public interface ProjectInfoRpcService {

    /**
     * 查找所有项目信息,使用创建时间排序
     *
     * @param tableCode
     * @return
     */
    List<DynaBean> findAllWithOrder(String tableCode, String infoLevelGlSql);

    /**
     * 根据项目主键查找项目信息
     *
     * @param tableCode
     * @param projectId
     * @return
     */
    DynaBean findProjectByPk(String tableCode, String projectId, String orgTableCode,
                             String orgId, String userTableCode, String systemDeptUserIdColumnCode
            , String deptUserId, String userGlSql, String userForeignKeyOrgIdField);
}
