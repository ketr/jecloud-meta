/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.excel.impl;

import com.je.common.base.entity.QueryInfo;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.rpc.dictionary.DictionaryRpcService;
import com.je.meta.rpc.excel.ExcelAssistRpcService;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ExcelAssistRpcServiceImpl implements ExcelAssistRpcService {

    @RpcReference(microserviceName = "meta",schemaId = "dictionaryRpcService")
    private DictionaryRpcService dictionaryRpcService;


    @Override
    public List<JSONTreeNode> getAllTyepDdListItems(String ddCode, Map<String, String> requestParams, QueryInfo queryInfo, Boolean en) {
        return dictionaryRpcService.getAllTyepDdListItems(ddCode, requestParams, new QueryInfo(), false);
    }
}
