package com.je.meta.rpc.project;

import com.je.common.base.DynaBean;

import java.util.List;

public interface ProjectUserRpcService {

    DynaBean findUserProjectInfo(String projectTableCode, String projectTablePkColumnCode,
                                 String userTableCode, String userTableProjectFkColumnCode,
                                 String systemDeptUserIdColumnCode, String deptUserId, String userForeignKeyOrgIdField, String orgId,
                                 String projectOrgTableCode, String projectOrgPkColumnCode, String userGlSql);

    List<DynaBean> findProjectUserWithOrder(String taleCode, String projectIdColumn, String projectId, String orderColumn, String glSql);


    List<DynaBean> findProjectUserByDepartment(String tableCode, String departmentFiledCode, List<String> departmentIds, String userGlSql);
}
