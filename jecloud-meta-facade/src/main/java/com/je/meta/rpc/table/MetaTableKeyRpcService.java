/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.APIWarnException;
import com.je.common.base.result.BaseRespResult;

import java.util.List;

/**
 * 资源表键服务
 */
public interface MetaTableKeyRpcService {

    /**
     * 初始化表格键信息
     *
     * @param resourceTable 修改表的地方
     * @param isTree        是否根节点
     */
    void initKeys(DynaBean resourceTable, Boolean isTree);

    /**
     * 对键进行检测  主要检测键为空和键重复
     *
     * @param keys 键列表数据
     * @return
     */
    String checkKeys(List<DynaBean> keys);

    /**
     * 删除键
     * @param dynaBean 自定义动态类
     * @param ids ids
     * @param ddl 是否执行ddl
     * @return
     */
    Integer removeKey(DynaBean dynaBean, String ids, String ddl);

    /**
     * 物理删除指定表的键
     *
     * @param tableCode 表名字
     * @param keys      删除的键信息
     * @param ddl       是否删除
     */
    void deleteKey(String tableCode, List<DynaBean> keys, String ddl);

    /**
     * 删除指定表键的元数据
     * @param tableCode
     * @param keys
     */
    void deleteKeyMeta(String tableCode, List<DynaBean> keys);

    /**
     * 删除指定表键的元数据
     * @param tableCode
     * @param keys
     */
    String requireDeletePhysicalKeyDDL(String tableCode, List<DynaBean> keys);

    /**
     * 修改表格键
     * @param dynaBean
     * @param strData
     * @return
     */
    int doUpdateList(DynaBean dynaBean, String strData) throws APIWarnException;

    /**
     * 获取删除键DDL
     * @param dynaBean
     * @param keys
     * @param isDeleteDdl
     * @return
     */
    String generateDeleteKeyDDL(DynaBean dynaBean, List<DynaBean> keys, String isDeleteDdl);


    /**
     * 根据键ID获取键数据
     * @param ids
     * @return
     */
    List<DynaBean> selectTableKeyByKeyId(String ids);


    public List<DynaBean> selectTableIndexByIndexId(String ids);

    /**
     * 删除键
     * @param dynaBean
     * @param keys
     * @param isDeleteDdl
     * @return
     */
    boolean doRemoveKey(DynaBean dynaBean, List<DynaBean> keys, String isDeleteDdl,String tableCode);


}
