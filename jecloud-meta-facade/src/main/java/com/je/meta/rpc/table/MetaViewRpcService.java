/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.alibaba.fastjson2.JSONArray;
import com.je.common.base.DynaBean;
import com.je.meta.model.view.ViewColumn;
import com.je.meta.model.view.ViewOuput;
import java.util.List;

/**
 * 元数据视图服务定义
 */
public interface MetaViewRpcService {


    /**
     * 创建视图
     *
     * @param dynaBean 创建视图信息
     * @return
     */
    DynaBean createView(DynaBean dynaBean);


    /**
     * 持久视图信息
     *
     * @param table  表信息
     * @param fields 字段
     * @return
     */
    DynaBean saveViewInfo(DynaBean table, String fields);

    /**
     * 更新视图级联信息
     * @param table
     * @param cascade
     * @return
     */
    DynaBean updateViewCascadeInfo(DynaBean table, String cascade);

    /**
     * 创建表和视图的关系
     * @param tableInfoList
     * @param resourcetableTablecode
     */
    void saveTableView(JSONArray tableInfoList, String resourcetableTablecode);

    /**
     * 修改视图
     *
     * @param table 修改视图的信息
     * @return
     */
    DynaBean updateView(DynaBean table);

    /**
     * 持久视图信息
     *
     * @param table    视图信息
     * @param fields   需要的列
     * @param syncView 是否同步视图，是则不修改功能信息和键信息   只同步列信息
     * @return
     */
    DynaBean updateViewInfo(DynaBean table, String fields, Boolean syncView);

    /**
     * 生成视图ddl
     * @param viewColumnList
     * @param viewOuputList
     * @param viewCode
     * @param masterTable
     * @return
     */
    StringBuilder generateViewDdl(List<ViewColumn> viewColumnList, List<ViewOuput> viewOuputList, String viewCode, String masterTable);


    /**
     * 创建资源表中视图表数据
     * @param dynaBean
     * @return
     */
    int saveViewDdl(DynaBean dynaBean);

    /**
     * 执行DDL 视图生成
     * @param dynaBean
     * @param strData
     * @return
     */
    DynaBean saveTableByView(DynaBean dynaBean, String strData);

    /**
     * 封装修改视图
     * @param dynaBean
     * @param strData
     * @return
     */
    DynaBean updateViews(DynaBean dynaBean, String strData);

    /**
     * 更新视图联级信息 新传参版本
     * @param dynaBean
     * @param viewColumnList
     * @return
     */
    DynaBean updateViewCascadeInfos(DynaBean dynaBean, List<ViewColumn> viewColumnList);

    /**
     * 持久化视图信息 新传参版本
     * @param dynaBean
     * @param viewOuputList
     * @return
     */
    DynaBean saveViewInfos(DynaBean dynaBean, List<ViewOuput> viewOuputList,DynaBean table);

    /**
     * 创建表和视图关系
     * @param tableInfoList
     * @param tableCode
     * @param b
     */
    void saveTableViews(List<String> tableInfoList, String tableCode, boolean b);

    /**
     * 修改视图表关系
     * @param table
     * @param viewOuputList
     * @param b
     * @return
     */
    DynaBean updateViewInfos(DynaBean table, List<ViewOuput> viewOuputList, boolean b,DynaBean recourceTable);

    /**
     * 创建视图
     * @param strData
     * @param dynaBean
     * @return
     */
    DynaBean selectTableByView(String strData, DynaBean dynaBean);

    /**
     * 修改视图
     * @param strData
     * @param dynaBean
     * @return
     */
    DynaBean updateTableByView(String strData, DynaBean dynaBean);

    /**
     * 根据表编码获取表信息
     * @param dynaBean
     * @return
     */
    DynaBean selectOneByCode(DynaBean dynaBean);
}
