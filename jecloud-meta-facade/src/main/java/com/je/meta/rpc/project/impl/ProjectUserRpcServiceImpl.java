package com.je.meta.rpc.project.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.project.ProjectUserRpcService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RpcSchema(schemaId = "projectUserRpcService")
public class ProjectUserRpcServiceImpl implements ProjectUserRpcService {

    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean findUserProjectInfo(String projectTableCode, String projectTablePkColumnCode,
                                        String userTableCode, String userTableProjectFkColumnCode,
                                        String systemDeptUserIdColumnCode, String deptUserId,
                                        String userForeignKeyOrgIdField, String orgId, String projectOrgTableCode,
                                        String projectOrgPkColumnCode, String userGlSql) {
        List<DynaBean> projectUserBeanList = metaService.select(userTableCode, ConditionsWrapper.builder()
                .eq(systemDeptUserIdColumnCode, deptUserId).apply(userGlSql).orderByAsc("SY_CREATETIME"));
        if (projectUserBeanList == null || projectUserBeanList.isEmpty()) {
            return null;
        }
        DynaBean user = null;
        if (projectUserBeanList.size() > 1 && !Strings.isNullOrEmpty(orgId)) {
            for (DynaBean u : projectUserBeanList) {
                if (u.getStr(userForeignKeyOrgIdField).equals(orgId)) {
                    user = u;
                    break;
                }
            }
        } else {
            user = projectUserBeanList.get(0);
            orgId = user.getStr(userForeignKeyOrgIdField);
        }

        DynaBean orgBean = metaService.selectOne(projectOrgTableCode, ConditionsWrapper.builder().eq(projectOrgPkColumnCode, orgId));
        if (orgBean == null) {
            return null;
        }
        DynaBean project = metaService.selectOne(projectTableCode, ConditionsWrapper.builder().eq(projectTablePkColumnCode, user.getStr(userTableProjectFkColumnCode)));
        if (project == null) {
            return null;
        }
        project.set("orgBean", orgBean.getValues());
        return project;
    }

    @Override
    public List<DynaBean> findProjectUserWithOrder(String taleCode, String projectIdColumn, String projectId, String orderColumn, String glSql) {
        return metaService.select(taleCode, ConditionsWrapper.builder().eq(projectIdColumn, projectId).apply(glSql).orderByAsc(orderColumn));
    }

    @Override
    public List<DynaBean> findProjectUserByDepartment(String tableCode, String departmentFiledCode, List<String> departmentIds, String glSql) {
        return metaService.select(tableCode, ConditionsWrapper.builder().in(departmentFiledCode, departmentIds).apply(glSql));
    }

}
