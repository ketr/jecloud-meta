/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.je.common.base.DynaBean;

import java.util.List;

/**
 * 资源表索引服务
 */
public interface MetaTableIndexRpcService {

    /**
     * 保存创建索引的逻辑数据
     */
    DynaBean saveLogicData(DynaBean dynaBean);
    /**
     * 获取创建索引的ddl
     * dynaBean
     * @return
     */
    String getDDL4AddIndex(DynaBean dynaBean);

    /**
     * 初始化表格键信息
     *
     * @param resourceTable 修改表的地方
     * @param isTree        是否根节点
     */
    void initIndexs(DynaBean resourceTable, Boolean isTree);


    /**
     * 删除索引
     *
     * @param funcId     功能id
     * @param columnCode 列编码
     * @param columnId   列ID
     * @return
     */
    String removeIndexByColumn(String funcId, String columnCode, String columnId);

    /**
     * 对索引进行检测，
     * 检测:索引重复、索引为空、索引所属表是否为视图
     * @param indexs 检索的索引
     * @return
     */
    String checkIndexs(List<DynaBean> indexs);

    /**
     * 删除索引
     * @param dynaBean 自定义动态类
     * @param ids 索引ids
     * @return
     */
    Integer removeIndex(DynaBean dynaBean, String ids);

    /**
     * 物理删除指定表的索引
     *
     * @param tableCode 表名
     * @param indexs    索引
     */
    void deleteIndex(String tableCode, List<DynaBean> indexs);

    /**
     * 删除指定表的索引元数据
     */
    String deleteIndexMeta(DynaBean dynaBean);

    /**
     * 删除指定表的索引元数据
     */
    String requireDeletePhysicalIndexDDL(DynaBean dynaBean);

    String getDelIndexDdl(String ids);
}
