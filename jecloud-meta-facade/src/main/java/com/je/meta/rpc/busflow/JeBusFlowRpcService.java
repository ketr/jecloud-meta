package com.je.meta.rpc.busflow;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;

import java.util.List;

public interface JeBusFlowRpcService {
    /**
     * 获取流程定义
     * @param busFlowCode
     * @param refresh
     * @return
     */
    public DynaBean getBusFlow(String busFlowCode, String refresh);

    /**
     * 获取流程节点信息,与业务数据进行拼接
     * @param busFlowCode
     * @param mainId
     * @return
     */
    public List<DynaBean> loadFlowNodes(String busFlowCode, String mainId);
    /**
     * 初始化流程节点信息
     * @param busFlowCode
     * @param mainId
     */
    public void doInitFlowNodes(String busFlowCode,String mainId);

    /**
     * 执行刷新节点的数字、人员、执行时间
     * @param busFlowCode
     * @param mainId
     */
    public void doRenewFlowNodes(String busFlowCode,String mainId);

    /**
     * 执行单节点计算
     * @param busFlowCode
     * @param mainId
     * @param nodeCode
     */
    public void doRenewNode(String busFlowCode,String mainId,String nodeCode);
    /**
     * 执行单节点更新状态
     * @param busFlowCode
     * @param mainId
     * @param nodeCode
     */
    public void doRenewNodeInfo(String busFlowCode, String mainId, String nodeCode, JSONObject infoObj);

    /**
     * 清理缓存
     * @param busFlowCode
     */
    public void doClearCache(String busFlowCode);
}
