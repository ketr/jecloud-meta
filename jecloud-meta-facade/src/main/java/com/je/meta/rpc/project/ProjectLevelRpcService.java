package com.je.meta.rpc.project;

import com.je.common.base.DynaBean;

import java.util.List;

public interface ProjectLevelRpcService {

    List<DynaBean> findAllWithOrder(String tableCode,String orderField,String sql);

}
