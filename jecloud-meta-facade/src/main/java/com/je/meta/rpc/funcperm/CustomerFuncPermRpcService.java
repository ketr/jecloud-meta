package com.je.meta.rpc.funcperm;

import com.je.common.base.func.funcPerm.fastAuth.CustomerFuncPerm;
import com.je.common.base.func.funcPerm.fastAuth.FuncQuickPermission;
import com.je.common.base.func.funcPerm.roleSqlAuth.RoleSqlAuthVo;

public interface CustomerFuncPermRpcService {

    /**
     * 根据自定义功能编码获取快速授权配置
     *
     * @param code
     * @return
     */
    FuncQuickPermission getCustomerFuncQuickPermission(String code);

    /**
     * 根据自定义功能编码获取角色sql配置
     *
     * @param code
     * @return
     */
    RoleSqlAuthVo getCustomerFuncRoleSqlPermission(String code);

    /**
     * 根据自定义功能编码获取自定义sql配置
     *
     * @param code
     * @return
     */
    String getCustomerFuncSqlPermission(String code);

    /**
     * 获取自定义功能授权（全量）
     *
     * @param code
     * @return
     */
    CustomerFuncPerm getCustomerFuncAllPermByCode(String code);

}
