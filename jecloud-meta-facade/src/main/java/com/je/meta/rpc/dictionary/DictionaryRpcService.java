/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.dictionary;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.QueryInfo;
import com.je.common.base.mapper.query.Query;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.model.dd.DictionaryItemVo;
import java.util.List;
import java.util.Map;

public interface DictionaryRpcService {

    /**
     * 获取所有字典项
     * @param ddCode
     * @return
     */
    List<DynaBean> getAllDicItems(String ddCode);

    /**
     * 构建字典项
     *
     * @param dic 字典元数据
     * @param query 查询对象
     * @param itemCode 字典项编码
     */
    List<DictionaryItemVo> buildChildrenList(DynaBean dic, Boolean en, Query query, String itemCode);

    /**
     * 获取指定列表字典的字典项
     * @return
     */
    JSONObject getAllListDicItem();

    /**
     * 获取指定列表字典的字典项
     *
     * @param whereSql 查询sql
     * @param params 参数列表
     * @return
     */
    JSONObject getAllListDicItemWithWhere(String whereSql,Object... params);

    /**
     * 缓存所有列表字典
     */
    void doCacheAllListTypeDicItem();

    /**
     * 缓存所有字典信息
     */
    void doCacheAllDicInfo();

    /**
     * 执行重新缓存，指定字典编码；如果没有指定编码或编码为空，则重新缓存所有字典
     */
    void reCacheDicInfo(String dicCodes);

    /**
     * 得到列表字典信息
     *
     * @param ddCode 字典编码
     * @param query 查询对象
     * @param en    语言类型
     * @return
     */
    List<DictionaryItemVo> getDicList(String ddCode, Query query, Boolean en);

    /**
     * 根据字典项Code获取字典Name值
     *
     * @param dicItems 字典项集合
     * @param itemCode 字典项编码
     * @return 字典text
     */
    String getItemNameByCode(List<DictionaryItemVo> dicItems, String itemCode);

    /**
     * 获取字典所有项  支持所有字典
     *
     * @param ddCode    字典编码
     * @param params    字典参数，用于构建query对象sql参数系统定义参数赋值
     * @param queryInfo 查询对象
     * @param en        是否英文
     * @return 返回字典项树形结构
     */
    List<JSONTreeNode> getAllTyepDdListItems(String ddCode, Map<String, String> params, QueryInfo queryInfo, Boolean en);

    /**
     * 更新省市县的分类信息
     */
    void syncSsxDic();

}
