package com.je.meta.rpc.project;

import com.je.common.base.DynaBean;

import java.util.List;

public interface ProjectWorkflowRpcService {


    /**
     * 查询项目流程信息
     *
     * @param tableCode
     * @param projectId
     * @return
     */
    List<DynaBean> findProjectWorkflow(String projectId,String projectFiledCode, String tableCode,
                                   String funcCode, String funcFiledCode);
}
