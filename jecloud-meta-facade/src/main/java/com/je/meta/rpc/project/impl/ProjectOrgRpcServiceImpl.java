package com.je.meta.rpc.project.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.project.ProjectOrgRpcService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

@RpcSchema(schemaId = "projectOrgRpcService")
public class ProjectOrgRpcServiceImpl implements ProjectOrgRpcService {

    @Autowired
    private MetaService metaService;

    @Override
    public List<DynaBean> findProjectOrgWithOrder(String tableCode, String projectIdColumn, String projectId, String orderColumn, String orgProjectGlSql) {
        return metaService.select(tableCode, ConditionsWrapper.builder().eq(projectIdColumn, projectId).apply(orgProjectGlSql).orderByAsc(orderColumn));
    }

    @Override
    public List<DynaBean> findProjectInformationPersonnel(String userTable, String accDeptId,
                                                          String accDeptUserIdFiledCode, String userOrgIdFiled,
                                                          String orgTable, String orgTablePkField, String orgOrderByCodeFiled, String userGlSql, String orgProjectGlSql) {
        //查询人员表中的项目id
        List<DynaBean> list = metaService.select(userTable, ConditionsWrapper.builder().eq(accDeptUserIdFiledCode, accDeptId).apply(userGlSql));
        List<String> findOrgIds = new ArrayList<>();
        for (DynaBean dynaBean : list) {
            findOrgIds.add(dynaBean.getStr(userOrgIdFiled));
        }
        //根据项目id，查询项目信息
        return metaService.select(orgTable, ConditionsWrapper.builder().in(orgTablePkField, findOrgIds).apply(orgProjectGlSql).orderByAsc(orgOrderByCodeFiled));
    }

}
