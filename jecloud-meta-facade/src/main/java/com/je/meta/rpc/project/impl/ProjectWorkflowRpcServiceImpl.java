package com.je.meta.rpc.project.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.project.ProjectWorkflowRpcService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RpcSchema(schemaId = "projectWorkflowRpcService")
public class ProjectWorkflowRpcServiceImpl implements ProjectWorkflowRpcService {

    @Autowired
    private MetaService metaService;

    @Override
    public List<DynaBean> findProjectWorkflow(String projectId, String projectFiledCode, String tableCode,
                                          String funcCode, String funcFiledCode) {
        List<DynaBean> list = metaService.select(tableCode, ConditionsWrapper.builder()
                .eq(projectFiledCode, projectId)
                .eq(funcFiledCode, funcCode)
        );
        return list;
    }
}
