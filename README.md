# 元数据服务项目

> [了解更多](https://gitee.com/ketr/jecloud.git)

## 项目简介

JECloud元数据管理模块，用于管理平台模型元数据，包括资源表元数据、功能元数据、列表元数据、表单元数据等。 依赖于元数据管理模型，用户可以以更低成本，快速完成业务系统的构建与应用。
其主要包括如下功能：

- 服务管理
  - 平台类服务管理
  - 方案类服务管理
- 系统设置管理
  - 平台设置
  - 系统设置
  - 缓存管理
  - 服务契约管理
  - 系统变量管理
  - 存储桶管理
  - 流程分类维护管理
- 资源表管理模块
  - 普通表管理
  - 树形表管理
  - 视图管理
  - 索引管理
  - 约束管理
- 数据字典模块。
  - 列表字典管理
  - SQL列表字典管理
  - 树形字典管理
  - SQL树形字典管理
  - 外部树形字典管理
  - 自定义字典管理
  - 字典回收站
- 应用中心管理模块
  - 功能管理
  - 列表管理及在线设计器
  - 表单管理及在线设计器
- 微应用管理
- 业务编号管理
- 日历管理模块
- 升级管理模块
  - 构建平台升级包管理
  - 安装平台升级包管理
- 开发展板

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。


  > Maven安装 (http://maven.apache.org/download.cgi)

  > Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- aggregate-tomcat： SpringBoot Tomcat聚合模块
- jecloud-meta-facade: 接口门面模块
- jecloud-meta-sdk: 消费者模块
- jecloud-meta-service: 提供者模块

## 编译部署

使用maven生成对应环境的war包，例: 生产环境打包
``` shell
//开发环境打包
mvn clean package -Pdev -Dmaven.test.skip=true
//本地开发环境打包
mvn clean package -Plocal -Dmaven.test.skip=true
//测试环境打包
mvn clean package -Ptest -Dmaven.test.skip=true
//生产环境打包
mvn clean package -Pprod -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)
