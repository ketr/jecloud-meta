## 3.0.6 (2024-12-24)
### Features
- feat : 数据源执行排序不生效问题修复
- feat : 组织管理树添加过滤sql
- feat : 添加子功能时，将菜单添加校验去掉
- feat : 项目映射，添加过滤条件
- feat : 添加项目部门变量
- feat : 项目添加组织部门
- feat : 解决sql注入问题
- feat : 接口兼容
- feat : 添加禁用数据越权环境变量
- feat : 打包业务数据时，自动根据父子表关系，进行排序。如果是树形表，自动添加排序
- feat : sql树形字典，去掉nodeinfo拼到id字段上，这个逻辑已经去掉了
- feat : 自定义sql字典，添加bean信息
- feat : 添加rpc接口，项目模式下，流程会根据项目关系，进行过滤
- feat : 系统变量鉴权产品，添加所有的业务产品，子功能集合，校验权限改成根据主功能校验
- feat : 系统变量鉴权产品，添加所有的业务产品
- feat : 请求获取平台版本时，如果url请求不到，直接返回空信息
- feat : 升级版本，如果查询失败，直接返回空，避免前端白屏

### Bug Fixes
- fix : 解决加载参数，系统白屏问题
- fix : 修复使用快速创建功能菜单进行授权时，导致顶部菜单权限失效问题

## 3.0.5 (2024-10-30)
### Features
- feat : 添加远程打包接口
- feat : 添加业务级别系统变量
- feat : 添加接口，获取JE_META_GLOBAL_FILE数据
- feat : 修改升级批量执行逻辑
- -feature: 修复拼接SQL的问题
- -feature: 增加自定义功能数据授权配置
- feat : 添加接口，获取全局代码库自用数据
- feat : 业务流修改业务数据获取方式
- feat : 删除jecloud-meta.ipr 和jecloud-meta.iws文件

### Bug Fixes
- fix : 从自增id设置成uuid时，元数据信息未改变问题修复

## 3.0.4 (2024-09-19)
### Features
- feat : 添加接口，自定义功能权限解析，输出sql
- feat : 将自定义功能权限类CustomerFuncPerm，移到common项目中
- feat : 添加自定义功能数据授权
- feat : 添加获取自定义功能数据权限接口
- -feature: 增加自定义功能数据授权配置
- -feature: 增加数据源监控，用户自己可以控制数据源是否开启
- feat : 添加接口，根据微应用编码获取微应用自定义按钮信息
- feat : 升级附件时，只删除历史元数据信息，不删除物理文件
- feat : 添加数据垂直越权校验
- feat : 升级资源表时，判断是否存在，如果存在则不修改列的创建时间了
- feat : 用户设置个性化时，添加方案分类，可以根据不通的方案展示不同步的个性化设置

## 3.0.3 (2024-07-26)
### Features
- feat : 批量打包，全局打包数据去重
- feat : 打增量包，判断是否勾选了打包
- -feature: 增加平台版版本升级包列表
- feat : 批量打包，添加附件
- feat : 升级资源表时，判断重复的依据是id or tableCode
- feat : 添加方案类打包实现
- feat : 打包升级，添加角色资源信息
- feat : 添加icon资源安装
- feat : 插件打包单独提取
- feat : 产品打包，添加插件资源打包
- feat : 批量打包，解决数据排序问题
- feat : 批量打包，添加打包排序
- feat : 安装升级包，判断是否存在安装资源表，如果不存在，不做升级资源保存，兼容老版本
- feat : 批量打包,查询安装包的数据时，添加判断，勾选了打包的数据才打包
- feat : 全量打包，去除重复数据
- feat : 批量打包，去除重复数据
- feat : 打包升级包，支持批量打包
- feat : 打包升级包，支持打包文件
- feat : 打包内部自用包时，根据产品名称和版本号生成zip的名称
- feat : 解决升级图标、报表、数据源找不到父级的问题

### Bug Fixes
- fix : 修复安装升级包时，如果附件是pdf，预览时一直下载问题

## 3.0.2 (2024-06-25)
### Features
- feat : 打包文件如果有附件，附件名称乱码问题
- feat : 打包升级包，支持打包文件
- feat : 打包业务数据，支持附件打包
- feat : 升级office数据，子数据用主键进行判断重复
- feat : 支持子数据打包
- -feature: 增加实时日志输出
- feat : 打包资源，添加文档套件
- feat : 添加脚本套件打包方式
- feat : 打包资源，添加升级门户、升级图表、升级报表、升级SQL模版
- feat : 应用表如果发生异常，弹出异常提醒
- feat : 1.功能运行修改主键编码 2.升级安装包添加日志打印
- feat (pom) : 添加inspector引用

### Bug Fixes
- fix : 打包升级包,office组件缺少数据bug修复
- fix : 升级历史升级包报错问题修复
- fix : 获取数据字典rpc接口异常修复
- fix (sql模版) : 执行sql模版，sql格式化错误处理
- fix (资源表应用) : 使用表生成的外键，应用不生效问题修复

## 3.0.1 (2024-06-01)
### Features
- feat : 添加方案类变量-登录页底部自定义html
- feat : 发送短信添加模版编码

### Bug Fixes
- fix : 导出bug修复，异步导出rpc调用时，请求文没有设置导致用户信息获取异常问题修复

## 3.0.0 (2024-04-25)
### Features
- -feature: 修复构建数据权限and异常的问题
- -feature:修复数据权限
- -feature: 增加调整区县rpc接口
- feature : sql视图查询数据，将null的也展示出来
- feature : 添加功能高级查询字段
- -feature: 增加证书版本控制，调整版本至3.0.0
- --feature:增加证书机制，调整版本至3.0.0
- --feature:抽离nacos
- feature (文件移除) : 移除idea下的文件git记录
- feature  : 临时修改配置文件
- feature :调整je_ibatise版本

### Bug Fixes
- fix : （资源表），资源表使用字典添加字段，如果是sql列表字典，则设置code存储config
- fix : （多列头导出），多列头导出数据混乱问题修复
- fix : （资源表导入），如果是mysql，导入的tableCode默认大写
- fix : （资源表导出），资源表结构导出异常修复
- fix : 获取图标配置异常修复
- fixBug (变量) : 获取后端变量bug修复
- -bugfix:替换fastjson为fastjson2

## 2.2.3 (2024-02-23)
### Features
- --feature:抽离nacos
- --feature:适配kingbase
- feature (表单字段) : 字段最大长度如果为空则为0，去掉这个逻辑
- feature (yml) : 添加rest配置
- --feature:适配nacos
- feat (oracle) : oracle适配

### Bug Fixes
- fix (视图) : 生成视图dll异常修复
