/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.func.funcPerm.ControlFieldAuthVo;
import com.je.common.base.func.funcPerm.FieldAuth.FieldAuthDataVo;
import com.je.common.base.func.funcPerm.FieldAuth.FieldAuthScopeVo;
import com.je.common.base.func.funcPerm.FieldAuth.FieldAuthVo;
import com.je.common.base.func.funcPerm.dictionaryAuth.DictCanCelAuthScopeVo;
import com.je.common.base.func.funcPerm.dictionaryAuth.DictionaryAuthVo;
import com.je.common.base.func.funcPerm.dictionaryAuth.DictionaryVo;
import com.je.common.base.func.funcPerm.fastAuth.FastAuthScopeVo;
import com.je.common.base.func.funcPerm.fastAuth.FastAuthVo;
import com.je.common.base.func.funcPerm.fastAuth.FuncQuickPermission;
import com.je.common.base.func.funcPerm.roleSqlAuth.RoleSqlAuthScopeVo;
import com.je.common.base.func.funcPerm.roleSqlAuth.RoleSqlAuthVo;
import com.je.common.base.util.StringUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataPermUtils {

    public static FuncQuickPermission toFuncQuickPermission(FastAuthVo fastAuthVo, ControlFieldAuthVo controlFieldAuthVo) {
        FuncQuickPermission funcQuickPermission = new FuncQuickPermission();
        Class clazz = funcQuickPermission.getClass();
        try {
            String selectStr = fastAuthVo.getSelectPermStr();
            if (StringUtil.isNotEmpty(selectStr)) {
                selectStr += ",";
            }
            FastAuthScopeVo deptControlVo = fastAuthVo.getDeptControl();
            selectStr += (deptControlVo == null ? "" : (deptControlVo.getConfigPerm() + ","));
            FastAuthScopeVo orgControlVo = fastAuthVo.getOrgControl();
            selectStr += (orgControlVo == null ? "" : (orgControlVo.getConfigPerm() + ","));
            FastAuthScopeVo roleControlVo = fastAuthVo.getRoleControl();
            selectStr += (roleControlVo == null ? "" : (roleControlVo.getConfigPerm() + ","));
            FastAuthScopeVo userControlVo = fastAuthVo.getUserControl();
            selectStr += (userControlVo == null ? "" : userControlVo.getConfigPerm());
            List<String> list = Arrays.asList(selectStr.split(","));
            for (String str : list) {
                if (StringUtil.isNotEmpty(str)) {
                    Field field = clazz.getDeclaredField(str);
                    field.setAccessible(true);
                    field.setBoolean(funcQuickPermission, Boolean.TRUE);
                }

            }
            funcQuickPermission.setActive("1".equals(fastAuthVo.getAuthOnOff()));
            if (fastAuthVo.getDeptControl() != null && StringUtil.isNotEmpty(fastAuthVo.getDeptControl().getIds())) {
                funcQuickPermission.setSeeDeptIdList(getValue(fastAuthVo.getDeptControl().getIds()));
            }

            if (fastAuthVo.getOrgControl() != null && StringUtil.isNotEmpty(fastAuthVo.getOrgControl().getIds())) {
                funcQuickPermission.setSeeOrgIdList(getValue(fastAuthVo.getOrgControl().getIds()));
            }

            if (fastAuthVo.getRoleControl() != null && StringUtil.isNotEmpty(fastAuthVo.getRoleControl().getIds())) {
                funcQuickPermission.setSeeRoleIdList(getValue(fastAuthVo.getRoleControl().getIds()));
            }

            if (fastAuthVo.getUserControl() != null && StringUtil.isNotEmpty(fastAuthVo.getUserControl().getIds())) {
                funcQuickPermission.setSeeUserIdList(getValue(fastAuthVo.getUserControl().getIds()));
            }
            //替换控制字段的值
            if (controlFieldAuthVo != null) {
                if (StringUtil.isNotEmpty(controlFieldAuthVo.getUserIdsFieldCode())) {
                    funcQuickPermission.setCreateUserIdField(controlFieldAuthVo.getUserIdsFieldCode());
                }
                if (StringUtil.isNotEmpty(controlFieldAuthVo.getDeptIdsFieldCode())) {
                    funcQuickPermission.setCreateDeptIdField(controlFieldAuthVo.getDeptIdsFieldCode());
                }
                if(StringUtil.isNotEmpty(controlFieldAuthVo.getCompanyIdFieldCode())){
                    funcQuickPermission.setCompanyIdFieldCode(controlFieldAuthVo.getCompanyIdFieldCode());
                }
                if(StringUtil.isNotEmpty(controlFieldAuthVo.getGroupCompanyIdFieldCode())){
                    funcQuickPermission.setGroupCompanyIdFieldCode(controlFieldAuthVo.getGroupCompanyIdFieldCode());
                }
            }
            //赋值本人修改本人、本人删除本人
            funcQuickPermission.setMyselfEdit(true);
            funcQuickPermission.setMyselfDelete(true);
            return funcQuickPermission;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<String> getValue(String ids) {
        if (Strings.isNullOrEmpty(ids)) {
            return null;
        }
        return Splitter.on(",").splitToList(ids);
    }

    public static DictionaryAuthVo toDictionaryAuthVo(String funcpermOnOff, List<DynaBean> childs) {
        if (childs != null && childs.size() > 0) {
            DictionaryAuthVo dictionaryAuthVo = new DictionaryAuthVo();
            dictionaryAuthVo.setAuthOnOff(funcpermOnOff);
            List<DictCanCelAuthScopeVo> deptDict = new ArrayList();
            dictionaryAuthVo.setDictCanCelAuthDeptList(deptDict);
            List<DictCanCelAuthScopeVo> orgDict = new ArrayList();
            dictionaryAuthVo.setDictCanCelAuthOrgList(orgDict);
            List<DictCanCelAuthScopeVo> roleDict = new ArrayList();
            dictionaryAuthVo.setDictCanCelAuthRoleList(roleDict);
            for (DynaBean item : childs) {
                String dataType = item.getStr("CONFIG_TYPE");
                String dataId = item.getStr("CONFIG_DATA_ID");
                String configInfo = item.getStr("CONFIG_INFO");
                DictCanCelAuthScopeVo dictCanCelAuthScopeVo = new DictCanCelAuthScopeVo();
                dictCanCelAuthScopeVo.setId(dataId);
                List<DictionaryVo> list = JSON.parseArray(configInfo, DictionaryVo.class);
                dictCanCelAuthScopeVo.setDictList(list);
                if ("org".equals(dataType)) {
                    orgDict.add(dictCanCelAuthScopeVo);
                }
                if ("dept".equals(dataType)) {
                    deptDict.add(dictCanCelAuthScopeVo);
                }
                if ("role".equals(dataType)) {
                    roleDict.add(dictCanCelAuthScopeVo);
                }
            }
            return dictionaryAuthVo;
        }
        return null;
    }

    public static FieldAuthVo toFieldAuthVo(String funcpermOnOff, List<DynaBean> childs) {
        if (childs != null && childs.size() > 0) {
            FieldAuthVo fieldAuthVo = new FieldAuthVo();
            fieldAuthVo.setAuthOnOff(funcpermOnOff);
            FieldAuthScopeVo roleAuthFieldVo = new FieldAuthScopeVo();
            List<FieldAuthDataVo> roleDatas = new ArrayList<>();
            roleAuthFieldVo.setDatas(roleDatas);
            fieldAuthVo.setRoleAuthFieldVo(roleAuthFieldVo);
            FieldAuthScopeVo deptAuthFieldVo = new FieldAuthScopeVo();
            List<FieldAuthDataVo> deptDatas = new ArrayList<>();
            deptAuthFieldVo.setDatas(deptDatas);
            fieldAuthVo.setDeptAuthFieldVo(deptAuthFieldVo);
            FieldAuthScopeVo orgAuthFieldVo = new FieldAuthScopeVo();
            List<FieldAuthDataVo> orgDatas = new ArrayList<>();
            orgAuthFieldVo.setDatas(orgDatas);
            fieldAuthVo.setOrgAuthFieldVo(orgAuthFieldVo);
            for (DynaBean item : childs) {
                String dataType = item.getStr("CONFIG_TYPE");
                String dataId = item.getStr("CONFIG_DATA_ID");
                String configInfo = item.getStr("CONFIG_INFO");
                String CONFIG_CONTROL_SCOPE = item.getStr("CONFIG_CONTROL_SCOPE");
                String CONFIG_SELECT_SCOPE = item.getStr("CONFIG_SELECT_SCOPE");

                if ("org".equals(dataType)) {
                    FieldAuthDataVo fieldAuthDataVo = JSON.parseObject(configInfo, FieldAuthDataVo.class);
                    orgDatas.add(fieldAuthDataVo);
                    orgAuthFieldVo.setControlScope(CONFIG_CONTROL_SCOPE);
                    orgAuthFieldVo.setSelectControl(CONFIG_SELECT_SCOPE);
                }
                if ("dept".equals(dataType)) {
                    FieldAuthDataVo fieldAuthDataVo = JSON.parseObject(configInfo, FieldAuthDataVo.class);
                    deptDatas.add(fieldAuthDataVo);
                    deptAuthFieldVo.setControlScope(CONFIG_CONTROL_SCOPE);
                    deptAuthFieldVo.setSelectControl(CONFIG_SELECT_SCOPE);
                }
                if ("role".equals(dataType)) {
                    FieldAuthDataVo fieldAuthDataVo = JSON.parseObject(configInfo, FieldAuthDataVo.class);
                    roleDatas.add(fieldAuthDataVo);
                    roleAuthFieldVo.setSelectControl(CONFIG_SELECT_SCOPE);
                    roleAuthFieldVo.setControlScope(CONFIG_CONTROL_SCOPE);
                }
            }
            return fieldAuthVo;
        }
        return null;
    }

    public static RoleSqlAuthVo toRoleSqlAuth(String funcpermOnOff, List<DynaBean> childs) {
        if (childs != null && childs.size() > 0) {
            RoleSqlAuthVo roleSqlAuthVo = new RoleSqlAuthVo();
            roleSqlAuthVo.setAuthOnOff(funcpermOnOff);
            List<RoleSqlAuthScopeVo> deptRoleSql = new ArrayList<>();
            roleSqlAuthVo.setRoleSqlAuthDeptList(deptRoleSql);
            List<RoleSqlAuthScopeVo> orgRoleSql = new ArrayList<>();
            roleSqlAuthVo.setRoleSqlAuthOrgList(orgRoleSql);
            List<RoleSqlAuthScopeVo> roleRoleSql = new ArrayList<>();
            roleSqlAuthVo.setRoleSqlAuthRoleList(roleRoleSql);
            for (DynaBean item : childs) {
                String dataType = item.getStr("CONFIG_TYPE");
                String dataId = item.getStr("CONFIG_DATA_ID");
                String configInfo = item.getStr("CONFIG_INFO");
                RoleSqlAuthScopeVo roleSqlAuthScopeVo = new RoleSqlAuthScopeVo();
                roleSqlAuthScopeVo.setId(dataId);
                JSONObject jsonObject = JSON.parseObject(configInfo);
                roleSqlAuthScopeVo.setSql(jsonObject.getString("sql"));
                roleSqlAuthScopeVo.setCoverSql(jsonObject.getString("coverSql"));
                if ("org".equals(dataType)) {
                    orgRoleSql.add(roleSqlAuthScopeVo);
                }
                if ("dept".equals(dataType)) {
                    deptRoleSql.add(roleSqlAuthScopeVo);
                }
                if ("role".equals(dataType)) {
                    roleRoleSql.add(roleSqlAuthScopeVo);
                }
            }
            return roleSqlAuthVo;
        }
        return null;
    }

}
