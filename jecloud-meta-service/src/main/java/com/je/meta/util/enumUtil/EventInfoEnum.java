/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.util.enumUtil;

public enum EventInfoEnum {

    column(
            "JE_CORE_RESOURCECOLUMN_ID,RESOURCECOLUMN_JSLISTENER,RESOURCECOLUMN_NAME",
            "JE_CORE_RESOURCECOLUMN",
            "RESOURCECOLUMN_FUNCINFO_ID",
            " AND RESOURCECOLUMN_JSLISTENER IS NOT NULL"
            ),
    field(
            "JE_CORE_RESOURCEFIELD_ID,RESOURCEFIELD_NAME,RESOURCEFIELD_JSLISTENER,RESOURCEFIELD_XTYPE",
            "JE_CORE_RESOURCEFIELD",
            "RESOURCEFIELD_FUNCINFO_ID",
            " AND RESOURCEFIELD_JSLISTENER IS NOT NULL"
        ),
    action(
            "JE_CORE_RESOURCEBUTTON_ID,RESOURCEBUTTON_NAME,RESOURCEBUTTON_JSLISTENER",
            "JE_CORE_RESOURCEBUTTON",
            "RESOURCEBUTTON_FUNCINFO_ID",
             " AND RESOURCEBUTTON_JSLISTENER IS NOT NULL AND RESOURCEBUTTON_TYPE='ACTION'"
        ),
    button(
            "JE_CORE_RESOURCEBUTTON_ID,RESOURCEBUTTON_NAME,RESOURCEBUTTON_JSLISTENER",
            "JE_CORE_RESOURCEBUTTON",
            "RESOURCEBUTTON_FUNCINFO_ID",
            " AND RESOURCEBUTTON_JSLISTENER IS NOT NULL AND RESOURCEBUTTON_TYPE!='ACTION'"
            ),
    func(
            "JE_CORE_FUNCINFO_ID,FUNCINFO_GRIDJSLISTENER,FUNCINFO_GRIDROWTIP,FUNCINFO_FUNCNAME,FUNCINFO_FORMJSLISTENER",
            "JE_CORE_FUNCINFO",
            "JE_CORE_FUNCINFO_ID",
            ""
        );
    String queryField;
    String tableCode;
    String foreignKey;
    String whereSql;

    EventInfoEnum(String queryField,String tableCode,String foreignKey,String whereSql) {

        this.queryField = queryField;
        this.tableCode = tableCode;
        this.foreignKey = foreignKey;
        this.whereSql = whereSql;
    }

    public String getQueryField() {
        return this.queryField;
    }

    public String getTableCode() {
        return this.tableCode;
    }

    public String getForeignKey() {
        return this.foreignKey;
    }

    public String getWhereSql() {
        return this.whereSql;
    }
}
