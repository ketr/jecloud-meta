/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.framework.product;

import cn.hutool.core.date.DateUtil;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @ClassName
 * @Author wangchao
 * @Date 2022/7/14 0014 14:00
 * @Version V1.0
 */
@RestController
@RequestMapping("/je/meta/product/application")
public class ProductApplicationController extends AbstractPlatformController {
    /**
     * 发起申请
     */

    private final String DOBACK = "doback";
    private final String INITIATE = "initiate";
    private final String ACCEPT = "accept";
    private final String PROCESS = "process";
    private final String FINISH = "finish";
    /**
     * 资源类
     */
    private final String RESOURCE = "resource";
    /**
     * 部署类
     */
    private final String DEPLOY = "deploy";

    @Transactional
    @RequestMapping(value = "/process", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult process(BaseMethodArgument param, HttpServletRequest request) {
        String operateType = getStringParameter(request,"operateType");
        String type = getStringParameter(request,"type");
        String id = getStringParameter(request,"id");
        if (Strings.isNullOrEmpty(id)) {
            return BaseRespResult.errorResult("请先保存数据！");
        }
        DynaBean dynaBean = null;
        DynaBean flowLogDynaBean = new DynaBean("JE_PRODUCT_FLOWLOG", false);
        if (RESOURCE.equals(type)) {
            dynaBean = metaService.selectOne("PRODUCT_RESOURCE_APPLICATION", ConditionsWrapper.builder().eq("ID", id));
            if (null == dynaBean) {
                return BaseRespResult.errorResult("请检查数据是否正确！");
            }
            flowLogDynaBean.setStr("FLOWLOG_PRODUCT_CODE", dynaBean.getStr("APPLICATION_TYPE"));

        } else if (DEPLOY.equals(type)) {
            dynaBean = metaService.selectOne("PRODUCT_DEPLOYMENT", ConditionsWrapper.builder().eq("ID", id));
            if (null == dynaBean) {
                return BaseRespResult.errorResult("请检查数据是否正确！");
            }
            flowLogDynaBean.setStr("FLOWLOG_PRODUCT_CODE", dynaBean.getStr("DEPLOYMENT_TYPE"));
        }
        if (null == dynaBean) {
            return BaseRespResult.errorResult("请检查数据是否正确！");
        }
        flowLogDynaBean.setStr("FLOWLOG_PROJECT_ID", dynaBean.getStr("ID"));
        flowLogDynaBean.setStr("FLOWLOG_CZR", SecurityUserHolder.getCurrentAccountRealUserName());
        flowLogDynaBean.setStr("FLOWLOG_CZSJ", DateUtil.now());
        //产品资源申请、发起申请前判断明细表是否有数据
        if (INITIATE.equals(operateType)&&RESOURCE.equals(type)) {
            List<DynaBean> dynaBeanList = metaService.select("PRODUCT_RESOURCE_APPLICATION_DETAILS", ConditionsWrapper.builder().eq("PRODUCT_RESOURCE_APPLICATION_ID", id));
            if (null==dynaBeanList||dynaBeanList.isEmpty()) {
                return BaseRespResult.errorResult("请先填写资源申请明细表！");
            }
        }
        if (INITIATE.equals(operateType)) {
            dynaBean.set("PROCESSING_STATUS", "1");
            flowLogDynaBean.setStr("FLOWLOG_SPZT_CODE", "1");
        } else if (ACCEPT.equals(operateType)) {
            dynaBean.set("PROCESSING_STATUS", "2");
            flowLogDynaBean.setStr("FLOWLOG_SPZT_CODE", "2");
        }else if (PROCESS.equals(operateType)) {
            dynaBean.set("PROCESSING_STATUS", "3");
            flowLogDynaBean.setStr("FLOWLOG_SPZT_CODE", "3");
        }else if (FINISH.equals(operateType)) {
            dynaBean.set("PROCESSING_STATUS", "4");
            flowLogDynaBean.setStr("FLOWLOG_SPZT_CODE", "4");
        }else if (DOBACK.equals(operateType)) {
            dynaBean.set("PROCESSING_STATUS", "0");
            flowLogDynaBean.setStr("FLOWLOG_SPZT_CODE", "0");
        }
        metaService.update(dynaBean);
        commonService.buildModelCreateInfo(flowLogDynaBean);
        metaService.insert(flowLogDynaBean);
        return BaseRespResult.successResult("发起成功！");
    }
}
