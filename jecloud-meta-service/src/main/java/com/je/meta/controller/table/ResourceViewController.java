/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.table;

import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.table.ParseBuildingSqlFactory;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.model.view.ViewColumn;
import com.je.meta.model.view.ViewDdlVo;
import com.je.meta.model.view.ViewOuput;
import com.je.meta.service.table.column.MetaTableColumnService;
import com.je.meta.service.table.MetaTableService;
import com.je.meta.service.table.view.MetaViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLSyntaxErrorException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 资源表视图管理
 */
@RestController
@RequestMapping(value = "/je/meta/resourceTable/view")
public class ResourceViewController extends AbstractPlatformController {

    @Autowired
    private MetaTableService tableService;
    @Autowired
    private MetaViewService mateViewService;
    @Autowired
    private MetaTableColumnService metaTableColumnService;

    /**
     * 更新多条记录
     *
     * @param param
     */
    @Override
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/doUpdateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String strData = param.getStrData();
        if (StringUtil.isEmpty(strData)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.strData.canNotEmpty"));
        }
        int count = metaTableColumnService.doUpdateList(dynaBean, strData);
        return BaseRespResult.successResult(MessageUtils.getMessage("common.update.nums", count));
    }

    /**
     * 修改视图
     *
     * @param param
     * @param request
     * @return
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/updateView", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult updateView(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String resourcetableTablename = getStringParameter(request, "RESOURCETABLE_TABLENAME");
        String resourcetableTablecode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        String resourcetableSql = getStringParameter(request, "RESOURCETABLE_SQL");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        String strData = param.getStrData();

        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablename)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.name.canNotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablecode)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.code.canNotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableSql)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.ddl.canNotEmpty"));
        }
        if (StringUtil.isEmpty(strData)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.cascade.canNotEmpty"));
        }
        DynaBean table = mateViewService.updateViews(dynaBean, strData);
        return BaseRespResult.successResult(table);
    }

    /**
     * 同步视图
     *
     * @param param
     * @param request
     * @return
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/syncView", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult syncView(BaseMethodArgument param, HttpServletRequest request) {
        String id = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", id);
        table = mateViewService.updateViewInfo(table, "[]", true);
        //清空mybatis缓存
        metaService.clearMyBatisCache();
        return BaseRespResult.successResult(table);
    }

    /**
     * 生成视图DDL
     *
     * @param param
     * @param request
     * @return
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/generateViewDdl", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult generateViewDdl(BaseMethodArgument param, HttpServletRequest request) {
        ViewDdlVo viewDdlVo = JSONObject.parseObject(param.getStrData(), ViewDdlVo.class);
        List<ViewColumn> viewColumnList = viewDdlVo.getViewColumn();
        List<ViewOuput> viewOuputList = viewDdlVo.getViewOuput();
        String repeat = ViewDdlVo.checkRepeatField(viewOuputList);
        if (!Strings.isNullOrEmpty(repeat)) {
            return BaseRespResult.errorResult("1000","",MessageUtils.getMessage("table.view.output.field.repeat",repeat));
        }
        String viewCode = viewDdlVo.getViewCode();
        String masterTableId = viewDdlVo.getMasterTableId();
        if (null == viewColumnList) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.cascade.canNotEmpty"));
        }
        if (null == viewOuputList) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.columns.canNotEmpty"));
        }
        if (StringUtil.isEmpty(viewCode)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.code.canNotEmpty"));
        }
        if (StringUtil.isEmpty(masterTableId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.mainTable.canNotEmpty"));
        }
        StringBuilder ddl = mateViewService.generateViewDdl(viewColumnList, viewOuputList, viewCode, masterTableId);
        String sql = "";
        try {
            sql = ParseBuildingSqlFactory.build().beautifyCreateViewDdl(ddl.toString());
        } catch (SQLSyntaxErrorException e) {
            e.printStackTrace();
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.format.ddl.wrong"));
        }
        return BaseRespResult.successResult(sql);
    }

    /**
     * 执行DDl视图,点击确定保存ddl信息,创建视图
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/saveTableByView", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult saveTableByView(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean("JE_CORE_RESOURCETABLE", false);
        String resourcetableTablename = getStringParameter(request, "RESOURCETABLE_TABLENAME");
        String resourcetableTablecode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        String resourcetableSql = getStringParameter(request, "RESOURCETABLE_SQL");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        String syParent = getStringParameter(request, "SY_PARENT");
        String strData = param.getStrData();

        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablename)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.name.canotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablecode)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.code.canotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableSql)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.ddl.canNotEmpty "));
        }
        if (StringUtil.isEmpty(syParent)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.canotempty"));
        }
        if (StringUtil.isEmpty(strData)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.cascade.canNotEmpty"));
        }

        dynaBean.set("RESOURCETABLE_TABLENAME", resourcetableTablename);
        dynaBean.set("RESOURCETABLE_TABLECODE", resourcetableTablecode);
        dynaBean.set("RESOURCETABLE_SQL", resourcetableSql);
        dynaBean.set("SY_PARENT", syParent);
        dynaBean.put("RESOURCETABLE_COORDINATE", getStringParameter(request, "RESOURCETABLE_COORDINATE"));
        dynaBean.setStr("RESOURCETABLE_RELATION_DESC",getStringParameter(request,"RESOURCETABLE_RELATION_DESC"));

        if (tableService.checkTableCodeExcludeCode(dynaBean)) {
            throw new PlatformException(MessageUtils.getMessage("table.view.code.repeat"), PlatformExceptionEnum.JE_CORE_TABLE_CHECKCOLUMN_ERROR);
        }

        dynaBean = mateViewService.saveTableByView(dynaBean, strData);
        return BaseRespResult.successResult(dynaBean);
    }

    /**
     * 根据表编码获取DDL
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/showDdl", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult showDdl(BaseMethodArgument param, HttpServletRequest request) {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        if (StringUtil.isEmpty(resourceId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.id.canotEmpty"));
        }
        DynaBean infoById = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        String resourcetableSql = infoById.getStr("RESOURCETABLE_SQL");
        if (StringUtil.isEmpty(resourcetableSql)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.ddl.isEmpty"));
        }
        try {
            if("VIEW".equals(infoById.getStr("RESOURCETABLE_TYPE"))){
                resultMap.put("sql", resourcetableSql);
            }else{
                resultMap.put("sql", ParseBuildingSqlFactory.build().beautifyCreateViewDdl(resourcetableSql));
            }

        } catch (SQLSyntaxErrorException e) {
            e.printStackTrace();
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.format.ddl.wrong"));
        }
        return BaseRespResult.successResult(resultMap);
    }

}
