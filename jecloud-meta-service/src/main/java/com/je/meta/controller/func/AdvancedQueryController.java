/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.func.AdvancedQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 高级查询
 */
@RestController
@RequestMapping(value = "/je/meta/advancedQuery")
public class AdvancedQueryController extends AbstractPlatformController {

    @Autowired
    private AdvancedQueryService advancedQueryService;

    @RequestMapping(value = "/doUpdateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @Override
    public BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        if(StringUtil.isEmpty(param.getFuncId())){
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.params.canotEmpty","funcId"));
        }
        DynaBean dynaBean = new DynaBean();
        dynaBean.setStr("funcId",param.getFuncId());
        dynaBean.setStr("strData",param.getStrData());
        dynaBean.setStr("FUNCINFO_LAYOUT_TYPE",getStringParameter(request,"FUNCINFO_LAYOUT_TYPE"));
        dynaBean.setStr("FUNCINFO_ADD_QUERY_STRATEGY",getStringParameter(request,"FUNCINFO_ADD_QUERY_STRATEGY"));
        dynaBean.setStr("FUNCINFO_LAYOUT_COLUMN_COUNT",getStringParameter(request,"FUNCINFO_LAYOUT_COLUMN_COUNT"));
        dynaBean.setStr("FUNCINFO_GROUPQUERY_LABEL_WIDTH",getStringParameter(request,"FUNCINFO_GROUPQUERY_LABEL_WIDTH"));
        dynaBean.setStr("FUNCINFO_DISABLE_CHANGE",getStringParameter(request,"FUNCINFO_DISABLE_CHANGE"));
        dynaBean.setStr("FUNCINFO_GROUPQUERY_ENABEL_QUERY",getStringParameter(request,"FUNCINFO_GROUPQUERY_ENABEL_QUERY"));
        dynaBean.setStr("FUNCINFO_GROUPQUERY_START_EXPORT",getStringParameter(request,"FUNCINFO_GROUPQUERY_START_EXPORT"));
        dynaBean.setStr("FUNCINFO_GROUPQUERY_BJS",getStringParameter(request,"FUNCINFO_GROUPQUERY_BJS"));
        advancedQueryService.doUpdateList(dynaBean);
        return BaseRespResult.successResult(MessageUtils.getMessage("common.save.success"));
    }

    @RequestMapping(value = "/getData", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult getData(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean();
        dynaBean.setStr("funcId",param.getFuncId());
        dynaBean.setStr("sort",getStringParameter(request,"sort"));
        dynaBean.setStr("queryStr",getStringParameter(request,"queryStr"));
        Map<String,Object> data = advancedQueryService.getData(dynaBean);
        return BaseRespResult.successResult(data);
    }
}
