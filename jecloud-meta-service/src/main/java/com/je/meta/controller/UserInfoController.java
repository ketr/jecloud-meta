/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Array;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-07-26 12:34
 * @description:
 */
@RestController
@RequestMapping(value = "/je/meta/userInfo")
public class UserInfoController extends AbstractPlatformController {

    /**
     * 修改 保存用户态 配置数据
     *
     * @param configInfo 配置
     * @param type       类型
     */
    @RequestMapping(value = {"/editUserConfigInfo"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult editUserInfo(String configInfo, String type, String plan) {
        if (StringUtils.isBlank(type)) {
            throw new PlatformException(" USERINFO_TYPE 不能为空！", PlatformExceptionEnum.UNKOWN_ERROR);
        }
        //声明操作对象
        DynaBean bean;

        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder()
                .eq("USERINFO_USERID", SecurityUserHolder.getCurrentAccountId())
                .eq("USERINFO_TYPE", type);
        if (!Strings.isNullOrEmpty(plan)) {
            conditionsWrapper.eq("USERINFO_PLAN", plan);
        }

        List<DynaBean> list = metaService.select("JE_CORE_USERINFO", conditionsWrapper);
        //清除脏数据
        if (list.size() > 1) {
            for (int i = 1; i < list.size(); i++) {
                metaService.delete("JE_CORE_USERINFO", ConditionsWrapper.builder("JE_CORE_USERINFO_ID = {0}", list.get(i).getStr("JE_CORE_USERINFO_ID")));
            }
        }
        //初始化bean
        bean = list.isEmpty() ? new DynaBean() : list.get(0);

        //赋值
        bean.setStr("USERINFO_CONFIG", configInfo);
        bean.setStr("USERINFO_USERID", SecurityUserHolder.getCurrentAccountId());
        bean.setStr("USERINFO_USERCODE", SecurityUserHolder.getCurrentAccountCode());
        bean.setStr("USERINFO_TYPE", type);
        bean.setStr("USERINFO_PLAN", plan);

        //没有tableCode说明是新增的bean
        if (StringUtils.isBlank(bean.getTableCode())) {
            commonService.buildModelCreateInfo(bean);
            metaService.insert("JE_CORE_USERINFO", bean);
        } else {
            commonService.buildModelModifyInfo(bean);
            metaService.update(bean);
        }
        DynaBean result = new DynaBean();
        result.setStr("userId", bean.getStr("USERINFO_USERID"));
        result.setStr("type", bean.getStr("USERINFO_TYPE"));
        result.setStr("configInfo", bean.getStr("USERINFO_CONFIG"));
        return BaseRespResult.successResult(result.getValues());
    }

    /**
     * 读取用户态数据
     *
     * @param type 类型
     */
    @RequestMapping(value = {"/loadUserConfigInfo"}, method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadUserConfigInfo(String type, String plan) {
        //明操作对象
        List<DynaBean> bean = null;

        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
        conditionsWrapper.eq("USERINFO_USERID", SecurityUserHolder.getCurrentAccountId());

        if (!Strings.isNullOrEmpty(type)) {
            conditionsWrapper.eq("USERINFO_TYPE", type);
        }
        if (!Strings.isNullOrEmpty(plan)) {
            conditionsWrapper.eq("USERINFO_PLAN", plan);
        }

        bean = metaService.select("JE_CORE_USERINFO", conditionsWrapper);

        if (bean != null && bean.size() > 0) {
            Map<String, String> result = null;
            List<Map<String, String>> resultList = new LinkedList<>();
            for (DynaBean dynaBean : bean) {
                result = new LinkedHashMap<>();
                result.put("userId", dynaBean.getStr("USERINFO_USERID"));
                result.put("type", dynaBean.getStr("USERINFO_TYPE"));
                result.put("configInfo", dynaBean.getStr("USERINFO_CONFIG"));
                result.put("plan", dynaBean.getStr("USERINFO_PLAN"));
                resultList.add(result);
            }
            return BaseRespResult.successResult(resultList);
        } else {
            return BaseRespResult.successResult(new Array[]{});
        }

    }
}
