/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.table;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableSet;
import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.framework.product.ProductRpcService;
import com.je.meta.service.common.MetaDevelopLogService;
import com.je.meta.service.table.MetaTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

@RestController
@RequestMapping("/je/meta/resourceTable/module")
public class ResourceTableModuleController extends AbstractPlatformController {

    @Autowired
    private MetaDevelopLogService developLogService;
    @Autowired
    private MetaTableService tableService;
    @Autowired
    private ProductRpcService productRpcService;

    /**
     * 模块允许添加类型
     * MODULE 模块
     */
    private static final Set<String> TYPES = ImmutableSet.of("MODULE");

    @Override
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/doSave", method = RequestMethod.POST)
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {//RESOURCETABLE_TABLECODE
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String resourcetableTablename = getStringParameter(request, "RESOURCETABLE_TABLENAME");
        String resourcetableTablecode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        DynaBean product = productRpcService.getInfoById(productId);
        if (null == product) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.prodcut.notExits"));
        } else {
            dynaBean.setStr("SY_PRODUCT_CODE", product.getStr("PRODUCT_CODE"));
            dynaBean.setStr("SY_PRODUCT_NAME", product.getStr("PRODUCT_NAME"));
        }
        if (StringUtil.isEmpty(resourcetableTablename)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.name.cannotEmpty"));
        }
        if (resourcetableTablename.length() > 40) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.name.length"));
        }
        if (resourcetableTablecode.length() > 40) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.code.length"));
        }
        boolean matches = resourcetableTablecode.matches("^[A-Z]{1}[A-Z_0-9]{0,100}$");
        if (!matches) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.code.regrex.message"));
        }
        if (Strings.isNullOrEmpty(dynaBean.getStr("SY_PARENT"))) {
            dynaBean.setStr("SY_PARENT", "ROOT");
        }
        if (!dynaBean.getStr("SY_PARENT").equals("ROOT")) {
            DynaBean parentInfo = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", dynaBean.getStr("SY_PARENT"));
            if (null == parentInfo) {
                return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.notExits"));
            }
            if (!TYPES.contains(parentInfo.getStr("RESOURCETABLE_TYPE"))) {
                return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.create"));
            }
        }

        if (tableService.checkTableCodeExcludeCode(dynaBean)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("module.code.repeat"));
        }
        DynaBean inserted = tableService.doSave(dynaBean);
        return BaseRespResult.successResult(inserted, MessageUtils.getMessage("table.saveModule"));
    }


}