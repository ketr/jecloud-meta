/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.setting;

import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/meta/script")
public class GlobalScriptController extends AbstractPlatformController {

    /**
     * 保存全局样式和全局脚本
     *
     * @param param 传入信息
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_SETTINGS_*-show")
    @RequestMapping(value = "/saveCssFile", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult saveCssFile(BaseMethodArgument param, HttpServletRequest request) {
        String context = getStringParameter(request, "context");
        //查找全局程序数据
        DynaBean jsDynaBean = metaService.selectOne("JE_CORE_SETTING", ConditionsWrapper.builder().eq("CODE", "JE_SYS_CSS"));
        if (jsDynaBean == null) {
            jsDynaBean = new DynaBean("JE_CORE_SETTING", true);
            jsDynaBean.setStr("CODE", "JE_SYS_CSS");
            jsDynaBean.setStr("VALUE", context);
            commonService.buildModelCreateInfo(jsDynaBean);
            metaService.insert(jsDynaBean);
        } else {
            jsDynaBean.setStr("VALUE", context);
            commonService.buildModelCreateInfo(jsDynaBean);
            metaService.update(jsDynaBean);
        }
        return BaseRespResult.successResult(null, "全局脚本保存成功，请刷新缓存!");
    }

    /**
     * 保存全局样式和全局脚本
     *
     * @param param 传入信息
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_SETTINGS_*-show")
    @RequestMapping(value = "/saveJsFile", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult saveJsFile(BaseMethodArgument param, HttpServletRequest request) {
        String context = getStringParameter(request, "context");
        //查找全局程序数据
        DynaBean jsDynaBean = metaService.selectOne("JE_CORE_SETTING", ConditionsWrapper.builder().eq("CODE", "JE_SYS_JS"));
        if (jsDynaBean == null) {
            jsDynaBean = new DynaBean("JE_CORE_SETTING", true);
            jsDynaBean.setStr("CODE", "JE_SYS_JS");
            jsDynaBean.setStr("VALUE", context);
            commonService.buildModelCreateInfo(jsDynaBean);
            metaService.insert(jsDynaBean);
        } else {
            jsDynaBean.setStr("VALUE", context);
            commonService.buildModelCreateInfo(jsDynaBean);
            metaService.update(jsDynaBean);
        }
        return BaseRespResult.successResult(null, "全局脚本保存成功，请刷新缓存!");
    }

}
