/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.develop;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.extjs.Model;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.table.service.PCDataService;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.DynaModelRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping(value = "/je/meta/doAct/doAct")
public class DoActController extends AbstractPlatformController {

    /**底层数据库操作类   如获取指定SQL列*/
    @Autowired
    private PCDataService pcDataService;
    @Autowired
    private DynaModelRpcService dynaModelRpcService;

    /**
     * 得到EXTJS使用大的MODEL通过className
     * YUNFENGCHENG
     * 2011-12-30 下午01:06:39
     *
     * @throws IOException
     */
    @RequestMapping(value = "/getModelFields", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public JSONArray getModelFields(BaseMethodArgument param, HttpServletRequest request) {
        String modelName = param.getModelName();
        Boolean doTree = param.getDoTree();
        String tableName = getStringParameter(request,"tableName");
        String strData;
        String type = getStringParameter(request,"type");
        if ("procedure".equals(type) || "sql".equals(type) || "iditprocedure".equals(type)) {
            String funcId = getStringParameter(request,"funcId");
            List<DynaBean> columns = metaService.select("JE_CORE_RESOURCECOLUMN",
                    ConditionsWrapper.builder().apply("ESOURCECOLUMN_FUNCINFO_ID={0} AND SY_FLAG='1'", funcId));
            List<Model> models = new ArrayList<Model>();
            Model model;
            for (DynaBean column : columns) {
                String code = column.getStr("RESOURCECOLUMN_CODE");
                String columnType = column.getStr("RESOURCEFIELD_SYSMODE");
                model = new Model(code, columnType);
                models.add(model);
            }
            strData = JSON.toJSONString(models);
        } else if ("iditprocedure".equals(type)) {
            String funcId = getStringParameter(request,"funcId");
            DynaBean funcInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", funcId, "JE_CORE_FUNCINFO_ID,FUNCINFO_PKNAME");
            List<Model> models = new ArrayList<Model>();
            models.add(new Model(funcInfo.getStr("FUNCINFO_PKNAME"), "string"));
            strData = JSON.toJSONString(models);
        } else {
            strData = dynaModelRpcService.buildModel(tableName, modelName, doTree, param.getExcludes());
        }
        return JSONArray.parseArray(strData);
    }

    /**
     * 获取多个集合model集合
     */
    @RequestMapping(value = "/loadMultiModels", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult loadMultiModels(BaseMethodArgument param) {
        String strData = param.getStrData();
        JSONArray arrays = JSONArray.parseArray(strData);
        JSONObject returnObj = new JSONObject();
        for (Integer i = 0; i < arrays.size(); i++) {
            JSONObject obj = arrays.getJSONObject(i);
            String tableName = obj.getString("tableName");
            String modelName = obj.getString("modelName");
            Boolean doTree = obj.getBoolean("doTree");
            String modelStr = dynaModelRpcService.buildModel(tableName, modelName, doTree, param.getExcludes());
            if (StringUtil.isNotEmpty(modelStr)) {
                if (StringUtil.isNotEmpty(tableName)) {
                    if (doTree) {
                        returnObj.put("dynabean." + tableName + "_tree", JSONArray.parseArray(modelStr));
                    } else {
                        returnObj.put("dynabean." + tableName, JSONArray.parseArray(modelStr));
                    }
                } else {
                    returnObj.put(modelName, JSONArray.parseArray(modelStr));
                }
            }
        }
        return BaseRespResult.successResult(returnObj);
    }


}
