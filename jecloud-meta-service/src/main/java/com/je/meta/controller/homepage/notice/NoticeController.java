/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.homepage.notice;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName
 * @Author wangchao
 * @Date 2022/5/24 0024 9:33
 * @Version V1.0
 */
@RestController
@RequestMapping(value = "/je/meta/notice")
public class NoticeController extends AbstractPlatformController {

    /**
     * 首页新闻公告
     *
     * @param param 请求参数封装对象
     */
    @RequestMapping(value = "/load", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        ConditionsWrapper whereSqlWrapper = ConditionsWrapper.builder().table(dynaBean.getTableCode());
        String noticeType = getStringParameter(request, "noticeType");
        if (StringUtil.isNotEmpty(noticeType)) {
            whereSqlWrapper.apply("and NOTICE_QY = '1'  and NOTICE_TYPE_CODE = {0} order by SY_CREATETIME asc , SY_ORDERINDEX asc", noticeType);
        }
        int limit = param.getLimit();
        int pageNum = param.getPage();
        long count = 0L;
        int currentPage = 0;
        int pages = 0;
        List<DynaBean> list;
        if (limit == -1) {
            list = metaService.select(whereSqlWrapper);
            count = list.size();
        } else {
            Page page = new Page(pageNum, limit);
            list = metaService.select(dynaBean.getTableCode(), page, whereSqlWrapper);
            count = page.getTotal();
            currentPage = page.getCurrent();
            pages = page.getPages();
        }
        JSONObject returnObj = new JSONObject();
        List<HashMap> values = new ArrayList<>();
        for (DynaBean bean : list) {
            values.add(bean.getValues());
        }
        returnObj.put("rows", values);
        returnObj.put("totalCount", count);
        returnObj.put("currentPage", currentPage);
        returnObj.put("pages", pages);
        return BaseRespResult.successResult(returnObj);
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        //数据主键
        String pkId = getStringParameter(request, "JE_CORE_NOTICE_ID");

        DynaBean dynaBean = metaService.selectOne("JE_CORE_NOTICE", ConditionsWrapper.builder().eq("JE_CORE_NOTICE_ID", pkId));

        //阅读人Id
        String currentLookUserId = getStringParameter(request, "NOTICE_LOOKUSERIDS");
        //点赞用户Id
        String currentDzUserId = getStringParameter(request, "NOTICE_DZUSERIDS");

        if (StringUtil.isEmpty(currentLookUserId) && StringUtil.isEmpty(currentDzUserId)) {
            return BaseRespResult.errorResult("传入用户参数为空！");
        }
        if (StringUtil.isNotEmpty(currentLookUserId)) {
            String lookUserIds = dynaBean.getStr("NOTICE_LOOKUSERIDS");
            String readNums = dynaBean.getStr("NOTICE_YDL");
            if (!lookUserIds.contains(currentLookUserId)) {
                dynaBean.set("NOTICE_LOOKUSERIDS", lookUserIds + "," + currentLookUserId);
                dynaBean.set("NOTICE_YDL", Integer.parseInt(readNums) + 1);
            }
        }
        if (StringUtil.isNotEmpty(currentDzUserId)) {
            String dzUserIds = dynaBean.getStr("NOTICE_DZUSERIDS");
            String dzNums = dynaBean.getStr("NOTICE_DZL");
            if (!dzUserIds.contains(currentDzUserId)) {
                dynaBean.set("NOTICE_DZUSERIDS", dzUserIds + "," + currentDzUserId);
                dynaBean.set("NOTICE_DZL", Integer.parseInt(dzNums) + 1);
            }
        }
        int update = metaService.update(dynaBean);

        return BaseRespResult.successResult(dynaBean);

    }

}
