/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.framework.product;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.framework.product.ProductService;
import com.je.rbac.rpc.RoleRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/je/meta/product/platform/manage")
public class PlatformProductManageController extends AbstractPlatformController {

    @Autowired
    private ProductService productService;
    @Autowired
    private RoleRpcService roleRpcService;

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean bean = productService.doSave(param,request);
        return BaseRespResult.successResult(bean.getValues());
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean bean = productService.doUpdate(param,request);
        return BaseRespResult.successResult(bean.getValues());
    }

    @Override
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        String productId = getStringParameter(request,"ids");
        if(Strings.isNullOrEmpty(productId)){
            return BaseRespResult.errorResult("请选择要移除的产品！");
        }
        List<DynaBean> productRoleList = roleRpcService.findProductDevelopRoles(Splitter.on(",").splitToList(productId));
        if (productRoleList != null && !productRoleList.isEmpty()) {
            return BaseRespResult.errorResult("请先移除产品开发者角色！");
        }
        return super.doRemove(param, request);
    }

    @RequestMapping(value = "/beforeSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult beforeSave(BaseMethodArgument param, HttpServletRequest request) {
        String productCode = getStringParameter(request,"productCode");
        String pkValue = getStringParameter(request,"pkValue");
        if(Strings.isNullOrEmpty(productCode)){
            return BaseRespResult.errorResult("请先输入产品编码！");
        }
        List<DynaBean> productDynaBean=null;
        if (Strings.isNullOrEmpty(pkValue)) {
            productDynaBean = metaService.select("JE_PRODUCT_MANAGE", ConditionsWrapper.builder().eq("PRODUCT_CODE", productCode));
            if (!productDynaBean.isEmpty()){
                return BaseRespResult.errorResult("产品编码重复，请重新输入编码！");
            }
        }else {
            productDynaBean = metaService.select("JE_PRODUCT_MANAGE", ConditionsWrapper.builder().eq("PRODUCT_CODE", productCode));
            if (productDynaBean.size()>1) {
                return BaseRespResult.errorResult("产品编码重复，请重新输入编码！");
            }
        }
        return BaseRespResult.successResult("验证成功！");
    }
}
