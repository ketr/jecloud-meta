/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.global;

import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/je/meta/global")
public class GlobalController {

    @Autowired
    private MetaService metaService;

    /**
     * 全局脚本
     *
     * @param request
     * @return
     */
    @RequestMapping(value = {"/script"}, method = {RequestMethod.POST}, produces = {"application/json; charset=utf-8"})
    public BaseRespResult script(HttpServletRequest request) {
        String media = request.getParameter("media");
        ConditionsWrapper wrapper = ConditionsWrapper.builder()
                .table("JE_CORE_QJJBK")
                .eq("QJJBK_QY_CODE", "1");
        if ("app".equals(media)) {
            wrapper.in("QJJBK_ZYFW_CODE", "APP", "ALL");
        } else {
            wrapper.in("QJJBK_ZYFW_CODE", "PC", "ALL");
        }
        List<Map<String, Object>> result = metaService.selectSql(wrapper);
        if (result == null) {
            result = new ArrayList<>();
        }
        return BaseRespResult.successResult(result);
    }

    @RequestMapping(value = {"/css"}, method = {RequestMethod.POST}, produces = {"application/json; charset=utf-8"})
    public BaseRespResult css(HttpServletRequest request) {
        String media = request.getParameter("media");
        ConditionsWrapper wrapper = ConditionsWrapper.builder()
                .table("JE_CORE_QJCSS")
                .eq("QJCSS_QY_CODE", "1");
        if ("app".equals(media)) {
            wrapper.in("QJCSS_ZYFW_CODE", "APP", "ALL");
        } else {
            wrapper.in("QJCSS_ZYFW_CODE", "PC", "ALL");
        }
        List<Map<String, Object>> result = metaService.selectSql(wrapper);
        if (result == null) {
            result = new ArrayList<>();
        }
        return BaseRespResult.successResult(result);
    }

    @RequestMapping(value = {"/codes"}, method = {RequestMethod.POST}, produces = {"application/json; charset=utf-8"})
    public BaseRespResult codes(HttpServletRequest request) {
        ConditionsWrapper wrapper = ConditionsWrapper.builder().table("JE_META_CODES");
        try {
            List<Map<String, Object>> result = metaService.selectSql(wrapper);
            if (result == null) {
                result = new ArrayList<>();
            }
            return BaseRespResult.successResult(result);
        }catch (Exception e){
            return BaseRespResult.successResult(new ArrayList<>());
        }
    }

    @RequestMapping(value = {"/icon"}, method = {RequestMethod.POST}, produces = {"application/json; charset=utf-8"})
    public BaseRespResult icon(HttpServletRequest request) {
        try {
            List<Map<String, Object>> result = metaService.selectSql(ConditionsWrapper.builder()
                    .table("JE_META_GLOBAL_ICON")
                    .eq("ICON_ENABLE", "1").orderByAsc("SY_ORDERINDEX"));
            if (result == null) {
                result = new ArrayList<>();
            }
            return BaseRespResult.successResult(result);
        }catch (Exception e){
            return BaseRespResult.successResult(new ArrayList<>());
        }
    }

    @RequestMapping(value = {"/file"}, method = {RequestMethod.POST}, produces = {"application/json; charset=utf-8"})
    public BaseRespResult file(HttpServletRequest request) {
        try {
            List<Map<String, Object>> result = metaService.selectSql(ConditionsWrapper.builder()
                    .table("JE_META_GLOBAL_FILE")
                    .eq("FILE_QY", "1"));
            if (result == null) {
                result = new ArrayList<>();
            }
            return BaseRespResult.successResult(result);
        } catch (Exception e) {
            return BaseRespResult.successResult(new ArrayList<>());
        }
    }


    @RequestMapping(value = {"/stl"}, method = {RequestMethod.POST}, produces = {"application/json; charset=utf-8"})
    public BaseRespResult stl(HttpServletRequest request) {
        try {
            List<Map<String, Object>> result = metaService.selectSql(ConditionsWrapper.builder()
                    .table("JE_CORE_QJSQL")
                    .eq("QJSQL_QY_CODE", "1"));
            if (result == null) {
                result = new ArrayList<>();
            }
            return BaseRespResult.successResult(result);
        } catch (Exception e) {
            return BaseRespResult.successResult(new ArrayList<>());
        }

    }

}
