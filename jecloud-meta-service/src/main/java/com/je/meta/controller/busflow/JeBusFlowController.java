package com.je.meta.controller.busflow;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaBusService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.ReflectionUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.busflow.JeBusFlowService;
import com.je.servicecomb.RpcSchemaFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 业务流管理
 */
@RestController
@RequestMapping(value = "/je/meta/busflow/jeBusFlow")
public class JeBusFlowController  extends AbstractPlatformController {
    @Autowired
    private JeBusFlowService jeBusFlowService;
    /**
     * 添加流程节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/getBusFlow", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult getBusFlow(BaseMethodArgument param, HttpServletRequest request){
        String code=request.getParameter("code");
        String refresh=request.getParameter("refresh");
        if(StringUtil.isEmpty(refresh)){
            refresh="0";
        }
        DynaBean busFlow=jeBusFlowService.getBusFlow(code,refresh);
        return BaseRespResult.successResult(busFlow);
    }
    /**
     * 添加流程节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/loadFlowNodes", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadFlowNodes(BaseMethodArgument param, HttpServletRequest request){
        String code=request.getParameter("code");
        String mainId=request.getParameter("mainId");
        List<DynaBean> nodes=jeBusFlowService.loadFlowNodes(code,mainId);
        List<HashMap> values=new ArrayList<>();
        for(DynaBean node:nodes){
            values.add(node.getValues());
        }
        //获取主表信息
        DynaBean mainInfo=new DynaBean();
        DynaBean busFlow=jeBusFlowService.getBusFlow(code, "0");
        if(StringUtil.isNotEmpty(mainId) && StringUtil.isNotEmpty(busFlow.getStr("BUSFLOW_TABLECODE"))){
            String pd=busFlow.getStr("SY_PROJECT_CODE");
            if(StringUtil.isEmpty(pd)){
                pd="meta";
            }
            List<Map<String,Object>> mainDatas=new ArrayList<>();
            if("meta".equals(pd)){
                mainDatas=metaService.selectSql("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }else {
                MetaBusService metaBusService = RpcSchemaFactory.getRemoteProvierClazz(pd, "metaBusService", MetaBusService.class);
                mainDatas = metaBusService.selectMap("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }
            if(mainDatas!=null && mainDatas.size()>0){
                mainInfo=new DynaBean();
                Map<String,Object> infos=mainDatas.get(0);
                for(String field:infos.keySet()){
                    if(infos.get(field)==null || BeanService.KEY_TABLE_CODE.equals(field)){
                        continue;
                    }
                    mainInfo.set(field,infos.get(field));
                }
            }
        }
        BaseRespResult baseRespResult=BaseRespResult.successResultPage(values, new Long(nodes.size()));
        baseRespResult.setRows(values);
        baseRespResult.setData(mainInfo.getValues());
        return baseRespResult;
    }
    /**
     * 添加流程节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/loadFlowDiyNodes", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadFlowDiyNodes(BaseMethodArgument param, HttpServletRequest request){
        HashMap params = new HashMap<>();
        Map<String, String[]> paramInfos = request.getParameterMap();
        for (String key : paramInfos.keySet()) {
            params.put(key, request.getParameter(key));
        }
        JSONObject returnObj=new JSONObject();
        String code=request.getParameter("code");
        DynaBean busflow=jeBusFlowService.getBusFlow(code,"0");
        List<DynaBean> nodes=new ArrayList<>();
        String beanName = busflow.getStr("BUSFLOW_BEAN");
        String beanMethod = busflow.getStr("BUSFLOW_METHOD");
        if (StringUtil.isNotEmpty(beanName) && StringUtil.isNotEmpty(beanMethod)) {
            Object bean = SpringContextHolder.getBean(beanName);
            nodes = (List<DynaBean>) ReflectionUtils.getInstance().invokeMethod(bean, beanMethod, new Object[]{params,returnObj});
        }
        if(!returnObj.getBoolean("success")){
            return BaseRespResult.errorResult(returnObj.getString("obj"));
        }
        List<HashMap> values=new ArrayList<>();
        for(DynaBean node:nodes){
            values.add(node.getValues());
        }
        return BaseRespResult.successResultPage(values, new Long(nodes.size()));
    }
    /**
     * 添加流程节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/doInitFlowNodes", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doInitFlowNodes(BaseMethodArgument param, HttpServletRequest request){
        String code=request.getParameter("code");
        String mainId=request.getParameter("mainId");
        if(StringUtil.isEmpty(code) ||StringUtil.isEmpty(mainId)){
            return BaseRespResult.errorResult("传入的信息有误!");
        }
        jeBusFlowService.doInitFlowNodes(code,mainId);
        return BaseRespResult.successResult("初始化节点成功!");
    }
    /**
     * 添加流程节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/doRenewFlowNodes", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRenewFlowNodes(BaseMethodArgument param, HttpServletRequest request){
        String code=request.getParameter("code");
        String mainId=request.getParameter("mainId");
        if(StringUtil.isEmpty(code) ||StringUtil.isEmpty(mainId)){
            return BaseRespResult.errorResult("传入的信息有误!");
        }
        jeBusFlowService.doRenewFlowNodes(code,mainId);
        return BaseRespResult.successResult("重新计算成功");
    }
    /**
     * 添加流程节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/doRenewNode", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doRenewNode(BaseMethodArgument param, HttpServletRequest request){
        String code=request.getParameter("code");
        String nodeCode=request.getParameter("nodeCode");
        String mainId=request.getParameter("mainId");
        if(StringUtil.isEmpty(code) ||StringUtil.isEmpty(mainId)){
            return BaseRespResult.errorResult("传入的信息有误!");
        }
        jeBusFlowService.doRenewNode(code,mainId,nodeCode);
        return BaseRespResult.successResult("重新计算成功");
    }
}
