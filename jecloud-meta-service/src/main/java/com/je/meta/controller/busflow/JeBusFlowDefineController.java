package com.je.meta.controller.busflow;

import com.alibaba.fastjson2.JSONObject;
import com.je.auth.check.annotation.AuthCheckDataPermission;
import com.je.auth.check.annotation.CheckDataType;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.meta.service.busflow.JeBusFlowDefineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 业务流定义管理
 */
@RestController
@RequestMapping(value = "/je/meta/busflow/jeBusFlowDefine")
public class JeBusFlowDefineController extends AbstractPlatformController {
    @Autowired
    private JeBusFlowDefineService jeBusFlowDefineService;
    @RequestMapping(
            value = {"/doUpdate"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    @AuthCheckDataPermission(
            checkDataType = CheckDataType.UPDATE
    )
    @Override
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        return BaseRespResult.successResult(this.manager.doUpdate(param, request).getValues());
    }
    /**
     * 添加流程节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/doAddNode", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doAddNode(BaseMethodArgument param, HttpServletRequest request){
        HashMap<String, String> params = new HashMap<>();
        Map<String, String[]> paramInfos = request.getParameterMap();
        for (String key : paramInfos.keySet()) {
            params.put(key, request.getParameter(key));
        }
        JSONObject returnObj=new JSONObject();
        DynaBean node=jeBusFlowDefineService.doAddNode(params, returnObj);
        if(!returnObj.getBoolean("success")){
            return BaseRespResult.errorResult(returnObj.getString("obj"));
        }
        return BaseRespResult.successResult(node);
    }

    /**
     * 删除节点信息
     * @param param
     * @param request
     */
    @RequestMapping(value = "/doDelNode", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doDelNode(BaseMethodArgument param, HttpServletRequest request){
        HashMap<String, String> params = new HashMap<>();
        Map<String, String[]> paramInfos = request.getParameterMap();
        for (String key : paramInfos.keySet()) {
            params.put(key, request.getParameter(key));
        }
        JSONObject returnObj=new JSONObject();
        jeBusFlowDefineService.doDelNode(params, returnObj);
        if(!returnObj.getBoolean("success")){
            return BaseRespResult.errorResult(returnObj.getString("obj"));
        }
        return BaseRespResult.successResult(returnObj.getString("obj"));
    }

    /**
     * 初始化特殊图形的节点信息(有且只有一个固定的编码)
     * @param param
     * @param request
     */
    @RequestMapping(value = "/doInitDrawNode", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doInitDrawNode(BaseMethodArgument param, HttpServletRequest request){
        HashMap<String, String> params = new HashMap<>();
        Map<String, String[]> paramInfos = request.getParameterMap();
        for (String key : paramInfos.keySet()) {
            params.put(key, request.getParameter(key));
        }
        JSONObject returnObj=new JSONObject();
        DynaBean node=jeBusFlowDefineService.doInitDrawNode(params, returnObj);
        if(!returnObj.getBoolean("success")){
            return BaseRespResult.errorResult(returnObj.getString("obj"));
        }
        return BaseRespResult.successResult(node);
    }
}
