/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.setting;

import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.meta.service.setting.SytemSettingPlanService;
import com.je.meta.setting.SettingException;
import com.je.meta.setting.SystemSetting;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * 系统设置方案
 */
@RestController
@RequestMapping(value = "/je/meta/settingPlan")
public class SytemSettingPlanController extends AbstractPlatformController {

    private static final String SETTING_PLAN = "manage-plan";

    @Autowired
    private SytemSettingPlanService sytemSettingPlanService;

    /**
     * 加载方案，免登陆
     */
    @RequestMapping(value = "/loadPlanAndLoginConfig", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadPlans(HttpServletRequest request) {
        String code = getStringParameter(request, "code");
        try {
            DynaBean planList = sytemSettingPlanService.loadPlanAndLoginConfig(code);
            return BaseRespResult.successResult(planList);
        } catch (Throwable e) {
            e.printStackTrace();
            return BaseRespResult.errorResult("加载失败！" + e.getMessage());
        }
    }

    @ApiResponses({
            @ApiResponse(code = 200, response = File.class, message = ""),
    })
    @RequestMapping(value = "/getPicture", method = RequestMethod.GET, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<InputStream> getPicture(HttpServletRequest request) {
        String type = getStringParameter(request, "type");
        String code = getStringParameter(request, "code");
        InputStream inputStream = sytemSettingPlanService.getPicture(type, code);
        String fileName = "";
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, "image/jpeg")
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName)
                .body(inputStream);
    }


    /**
     * 查看所有方案
     */
    @RequestMapping(value = "/loadSettingPlans", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadSettingPlans(HttpServletRequest request) {
        List<DynaBean> planList = sytemSettingPlanService.loadSettingPlans();
        return BaseRespResult.successResult(planList);
    }

    /**
     * 查看某个方案信息
     */
    @RequestMapping(value = "/loadSettingPlanInfo", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult loadSettingPlanInfo(HttpServletRequest request) {
        String setplanId = getStringParameter(request, "JE_CORE_SETPLAN_ID");
        SystemSetting plan = sytemSettingPlanService.loadSettingPlanInfo(setplanId, SETTING_PLAN);
        return BaseRespResult.successResult(plan);
    }

    /**
     * 删除某个方案信息
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_SETTINGS_*-show")
    @RequestMapping(value = "/deleteSettingPlan", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult deleteSettingPlan(HttpServletRequest request) {
        String setplanId = getStringParameter(request, "JE_CORE_SETPLAN_ID");
        sytemSettingPlanService.deleteSettingPlan(setplanId);
        return BaseRespResult.successResult(null);
    }

    /**
     * 新添方案
     */
    @RequestMapping(value = "/insertSettingPlan", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult insertSettingPlan(HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        try {
            sytemSettingPlanService.insertSettingPlan(SETTING_PLAN, dynaBean);
        } catch (SettingException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult(null);
    }

    /**
     * 修改保存方案
     */
    @RequestMapping(value = "/saveSettingPlan", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult saveSettingPlan(HttpServletRequest request) {
        String setplanId = getStringParameter(request, "JE_CORE_SETPLAN_ID");
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        try {
            sytemSettingPlanService.saveSettingPlan(SETTING_PLAN, setplanId, dynaBean);
        } catch (SettingException e) {
            return BaseRespResult.errorResult(e.getMessage());
        }
        return BaseRespResult.successResult(null);
    }

}
