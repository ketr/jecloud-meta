/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.CommonCheckService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.ResFieldPrintPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/meta/resField/print/plan")
public class ResFieldPrintPlanController extends AbstractPlatformController {

    @Autowired
    private ResFieldPrintPlanService resFieldPrintPlanService;
    @Autowired
    private CommonCheckService commonCheckService;

    @Override
    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String[] key = {"tableCode", "PLAN_NAME", "PLAN_CODE", "PLAN_FUNCINFO_ID"};
        String result = StringUtil.checkEmpty(key, request);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        ConditionsWrapper select = ConditionsWrapper.builder().table(dynaBean.getTableCode()).eq("PLAN_FUNCINFO_ID", dynaBean.getStr("PLAN_FUNCINFO_ID")).eq("PLAN_CODE", dynaBean.getStr("PLAN_CODE"));
        if (commonCheckService.checkRepeat(select)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.code.repeat", dynaBean.getStr("PLAN_CODE")));
        }
        return super.doSave(param, request);
    }

    @RequestMapping(value = "/doCopy", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @Override
    public BaseRespResult doCopy(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String[] key = {"tableCode", "PLAN_NAME", "PLAN_CODE", "PLAN_FUNCINFO_ID", "fromPlanId"};
        String result = StringUtil.checkEmpty(key, request);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        ConditionsWrapper select = ConditionsWrapper.builder().table(dynaBean.getTableCode()).eq("PLAN_FUNCINFO_ID", dynaBean.getStr("PLAN_FUNCINFO_ID")).eq("PLAN_CODE", dynaBean.getStr("PLAN_CODE"));
        if (commonCheckService.checkRepeat(select)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.code.repeat", dynaBean.getStr("PLAN_CODE")));
        }
        dynaBean = resFieldPrintPlanService.doCopy(dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }

    @Override
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        ConditionsWrapper select = ConditionsWrapper.builder().table(dynaBean.getTableCode()).eq("PLAN_FUNCINFO_ID", dynaBean.getStr("PLAN_FUNCINFO_ID")).eq("PLAN_CODE", dynaBean.getStr("PLAN_CODE"));
        if (commonCheckService.checkRepeat(select)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.code.repeat", dynaBean.getStr("PLAN_CODE")));
        }
        return super.doUpdate(param, request);
    }

    @RequestMapping(value = "/changeDefaultPlan", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult changeDefaultPlan(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean("JE_CORE_RESOURCEFIELD_PRINT_PLAN", true);
        String[] key = {"JE_CORE_RESOURCEFIELD_PRINT_PLAN_ID"};
        String result = StringUtil.checkEmpty(key, request);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        dynaBean.setStr("JE_CORE_RESOURCEFIELD_PRINT_PLAN_ID", getStringParameter(request, "JE_CORE_RESOURCEFIELD_PRINT_PLAN_ID"));
        dynaBean = commonService.changeDefaultPlan(dynaBean, "PLAN_FUNCINFO_ID", "PLAN_DEFAULT");
        return BaseRespResult.successResult(dynaBean);
    }
}
