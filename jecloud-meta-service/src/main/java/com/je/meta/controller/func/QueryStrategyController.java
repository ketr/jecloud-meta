/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;

import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.Condition;
import com.je.common.base.mapper.query.Order;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.meta.service.func.MetaFuncRelyonService;
import com.je.meta.service.func.QueryStrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/meta/queryStrategy")
public class QueryStrategyController extends AbstractPlatformController {

    @Autowired
    private MetaFuncRelyonService funcRelyonService;
    @Autowired
    private QueryStrategyService queryStrategyService;

    /**
     * TODO未处理
     *
     * @param param
     */
    @RequestMapping(value = "/load", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean();
        List<Map<String, Object>> list = queryStrategyService.getDataList(param, dynaBean);
        return BaseRespResult.successResultPage(list, (long) list.size());
    }

    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {

        DynaBean dynaBean = new DynaBean("JE_CORE_QUERYSTRATEGY", true);
        dynaBean.put("QUERYSTRATEGY_FUNCINFO_ID", getStringParameter(request, "QUERYSTRATEGY_FUNCINFO_ID"));
        dynaBean.put("QUERYSTRATEGY_NAME", getStringParameter(request, "QUERYSTRATEGY_NAME"));
        dynaBean.put("QUERYSTRATEGY_FUNCCODE", getStringParameter(request, "QUERYSTRATEGY_FUNCCODE"));
        queryStrategyService.doSave(dynaBean);
        funcRelyonService.doSave("JE_CORE_QUERYSTRATEGY", dynaBean.getStr("QUERYSTRATEGY_FUNCINFO_ID"), dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }

    /**
     * TODO未处理
     *
     * @param param
     */
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        commonService.buildModelModifyInfo(dynaBean);
        metaService.update(dynaBean);
        funcRelyonService.doUpdate("JE_CORE_QUERYSTRATEGY", dynaBean.getStr("QUERYSTRATEGY_FUNCINFO_ID"), dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }

    /**
     * TODO未处理
     *
     * @param param
     */
    @RequestMapping(value = "/doUpdateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean("JE_CORE_QUERYSTRATEGY", true);
        String strData = param.getStrData();
        List<DynaBean> updateList = commonService.doUpdateList(dynaBean.getTableCode(), strData, null, null);
        funcRelyonService.doUpdateList("JE_CORE_QUERYSTRATEGY", "", updateList);
        return BaseRespResult.successResult(updateList.size() + "条记录被更新!");
    }

    /**
     * @param param
     */
    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    @Override
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        String ids = param.getIds();
        funcRelyonService.doRemove("JE_CORE_QUERYSTRATEGY", null, ids, null);
        int delNum = manager.doRemove(param, request);
        return BaseRespResult.successResult(delNum + "条记录被删除!");
    }

}
