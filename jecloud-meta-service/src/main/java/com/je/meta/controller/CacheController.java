/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.google.common.base.Strings;
import com.je.common.base.cache.CacheRegistry;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.meta.cache.dd.DicCache;
import com.je.meta.cache.dd.DicQuickCache;
import com.je.meta.cache.func.FuncConfigCache;
import com.je.meta.cache.func.FuncInfoCache;
import com.je.meta.cache.func.FuncStaticizeCache;
import com.je.meta.cache.table.DynaCache;
import com.je.meta.cache.table.TableCache;
import com.je.meta.rpc.dictionary.DictionaryRpcService;
import com.je.meta.service.variable.MetaSysVariablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.je.common.base.cache.AbstractRegistCache.CACHA_REGIST_KEY;

@RestController
@RequestMapping("/je/meta/cache")
public class CacheController extends AbstractPlatformController {

    @Autowired
    private FuncStaticizeCache funcStaticizeCache;
    @Autowired
    private FuncInfoCache funcInfoCache;
    @Autowired
    private FuncConfigCache funcConfigCache;
    @Autowired
    private DynaCache dynaCache;
    @Autowired
    private TableCache tableCache;
    @Autowired
    private DicCache dicCache;
    @Autowired
    private DicQuickCache dicQuickCache;
    @Autowired
    private DictionaryRpcService dictionaryRpcService;
    @Autowired
    private MetaSysVariablesService metaSysVariablesService;
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/caches", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult caches(HttpServletRequest request) {
        Map<String, Object> result = redisTemplate.opsForHash().entries(CACHA_REGIST_KEY);
        List<CacheRegistry> cacheRegistryList = new ArrayList<>();
        for (Map.Entry<String, Object> eachEntry : result.entrySet()) {
            if (!(eachEntry.getValue() == null)) {
                cacheRegistryList.add((CacheRegistry) eachEntry.getValue());
            }
        }
        Collections.sort(cacheRegistryList);
        return BaseRespResult.successResult(cacheRegistryList);
    }

    @RequestMapping(value = "/clearFunc", method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult clearFunc() {
        funcStaticizeCache.clear();
        funcInfoCache.clear();
        funcConfigCache.clear();
        dynaCache.clear();
        tableCache.clear();
        metaService.clearMyBatisCache();
        return BaseRespResult.successResult(null, "清除成功！");
    }

    @RequestMapping(value = "/clearDic", method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult clearDic(BaseMethodArgument param, HttpServletRequest request) {
        String dicCodes = getStringParameter(request, "dicCodes");
        if (Strings.isNullOrEmpty(dicCodes)) {
            metaSysVariablesService.reloadSystemVariables();
            dicCache.clear();
            dicQuickCache.clear();
            dictionaryRpcService.doCacheAllDicInfo();
        } else {
            dicCache.clear();
            dicQuickCache.clear();
            dictionaryRpcService.doCacheAllDicInfo();
        }
        return BaseRespResult.successResult(null, "清除成功！");
    }

    @RequestMapping(value = "/clearSysVar", method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult clearSysVar(BaseMethodArgument param) {
        metaSysVariablesService.reloadSystemVariables();
        return BaseRespResult.successResult(null, "清除成功！");
    }

}
