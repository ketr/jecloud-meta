/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;


import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.CommonCheckService;
import com.je.common.base.service.CommonService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.ResColumnPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/meta/resColumn/plan")
public class ResColumnPlanController extends AbstractPlatformController {

    @Autowired
    private ResColumnPlanService resColumnPlanService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private CommonCheckService commonCheckService;

    @RequestMapping(value = "/doSave", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @Override
    public BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String[] key = {"tableCode", "PLAN_NAME", "PLAN_CODE", "PLAN_FUNCINFO_ID"};
        String result = StringUtil.checkEmpty(key, request);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        ConditionsWrapper select = ConditionsWrapper.builder().table(dynaBean.getTableCode()).eq("PLAN_FUNCINFO_ID", dynaBean.getStr("PLAN_FUNCINFO_ID")).eq("PLAN_CODE", dynaBean.getStr("PLAN_CODE"));
        if (commonCheckService.checkRepeat(select)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.code.repeat", dynaBean.getStr("PLAN_CODE")));
        }
        dynaBean.setStr("columnId", getStringParameter(request, "columnId"));
        dynaBean.setStr("fromPlanId", getStringParameter(request, "fromPlanId"));
        dynaBean = resColumnPlanService.doSave(dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }

    @RequestMapping(value = "/saveUserPlan", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult saveUserPlan(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String[] key = {"tableCode", "PLAN_NAME", "PLAN_PARENT_ID", "PLAN_FUNCINFO_ID"};
        String result = StringUtil.checkEmpty(key, request);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        dynaBean.setStr("columnIds", getStringParameter(request, "columnIds"));
        dynaBean = resColumnPlanService.saveUserPlan(dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }

    @Override
    @RequestMapping(value = "/loadUserPlan", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult load(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean("JE_CORE_RESOURCECOLUMN_PLAN", true);
        /**
         * 系统方案ID
         */
        String JE_CORE_RESOURCECOLUMN_PLAN_ID = getStringParameter(request, "JE_CORE_RESOURCECOLUMN_PLAN_ID");
        if (StringUtil.isEmpty(JE_CORE_RESOURCECOLUMN_PLAN_ID)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.params.canotEmpty", "JE_CORE_RESOURCECOLUMN_PLAN_ID"));
        }
        dynaBean.setStr("JE_CORE_RESOURCECOLUMN_PLAN_ID", JE_CORE_RESOURCECOLUMN_PLAN_ID);
        List<DynaBean> list = resColumnPlanService.loadUserPlan(dynaBean);
        return BaseRespResult.successResult(list);
    }


    /**
     * @param param
     */
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @Override
    public BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        ConditionsWrapper select = ConditionsWrapper.builder().table(dynaBean.getTableCode()).eq("PLAN_FUNCINFO_ID", dynaBean.getStr("PLAN_FUNCINFO_ID")).eq("PLAN_CODE", dynaBean.getStr("PLAN_CODE"));
        if (commonCheckService.checkRepeat(select)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("common.code.repeat", dynaBean.getStr("PLAN_CODE")));
        }
        super.doUpdate(param, request);
        return BaseRespResult.successResult(dynaBean);
    }

    @RequestMapping(value = "/doRemove", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @Override
    public BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        resColumnPlanService.doRemove(dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }

    @RequestMapping(value = "/changeDefaultPlan", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult changeDefaultPlan(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean("JE_CORE_RESOURCECOLUMN_PLAN", true);
        String[] key = {"JE_CORE_RESOURCECOLUMN_PLAN_ID"};
        String result = StringUtil.checkEmpty(key, request);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        dynaBean.setStr("JE_CORE_RESOURCECOLUMN_PLAN_ID", getStringParameter(request, "JE_CORE_RESOURCECOLUMN_PLAN_ID"));
        dynaBean = commonService.changeDefaultPlan(dynaBean, "PLAN_FUNCINFO_ID", "PLAN_DEFAULT");
        return BaseRespResult.successResult(dynaBean);
    }
}
