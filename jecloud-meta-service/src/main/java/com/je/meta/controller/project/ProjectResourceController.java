package com.je.meta.controller.project;

import com.alibaba.fastjson2.JSONArray;
import com.google.common.base.Strings;
import com.je.common.base.result.BaseRespResult;
import com.je.meta.service.project.ProjectResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/je/meta/project/resource")
public class ProjectResourceController {

    @Autowired
    private ProjectResourceService projectResourceService;

    @RequestMapping(
            value = {"/requireResourceIds"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    public BaseRespResult requireResourceIds(HttpServletRequest request) {
        return BaseRespResult.successResult(projectResourceService.requireResourceIds());
    }

    @RequestMapping(
            value = {"/doImport"},
            method = {RequestMethod.POST},
            produces = {"application/json; charset=utf-8"}
    )
    public BaseRespResult doImport(HttpServletRequest request) {
        String data = request.getParameter("data");
        if(Strings.isNullOrEmpty(data)){
            return BaseRespResult.errorResult("导入数据为空");
        }
        JSONArray dataArray = JSONArray.parseArray(data);
        projectResourceService.doImport(dataArray);
        return BaseRespResult.successResult("导入成功");
    }

}
