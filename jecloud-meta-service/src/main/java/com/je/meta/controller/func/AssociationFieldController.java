/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller.func;

import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.func.AssociationFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-07-22 18:18
 * @description:
 */
@RestController
@RequestMapping(value = "/je/meta/associationField")
public class AssociationFieldController extends AbstractPlatformController {

    @Autowired
    private AssociationFieldService associationFieldService;

    /**
     * 数据更新
     *
     * @param param
     */
    @RequestMapping(value = "/doUpdateList", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @Override
    public BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request) {
        String strData = param.getStrData();
        Set<String> codes = new HashSet<>();
        String funcId = getStringParameter(request, "funcId");
        String assFieldId = "";
        String codeGenFieldInfo = param.getCodeGenFieldInfo();
        DynaBean dynaBean = associationFieldService.checkCode(strData);
         String errorSrr =  dynaBean.getStr("errorSrr");
        if(!StringUtil.isEmpty(errorSrr)){
            return BaseRespResult.errorResult(errorSrr+"编码重复!");
        }
        List<DynaBean> updateList = commonService.doUpdateList("JE_CORE_ASSOCIATIONFIELD", strData, null, null, codeGenFieldInfo);
        //处理分步加载自动勾选主功能字段
        for (DynaBean updated : updateList) {
            if (StringUtil.isNotEmpty(updated.getStr("ASSOCIATIONFIELD_PRIFIELDCODE"))) {
                codes.add(updated.getStr("ASSOCIATIONFIELD_PRIFIELDCODE"));
            }
            assFieldId = updated.getStr("JE_CORE_ASSOCIATIONFIELD_ID");
        }
        if (codes.size() > 0 && StringUtil.isNotEmpty(assFieldId)) {
            String columnTableCode = "JE_CORE_RESOURCECOLUMN";
            metaService.executeSql("UPDATE " + columnTableCode + " SET RESOURCECOLUMN_LAZYLOAD='1' WHERE RESOURCECOLUMN_CODE IN ({0}) " +
                    "AND RESOURCECOLUMN_FUNCINFO_ID={1}", codes, funcId);
        }
        return BaseRespResult.successResult(updateList.size() + "条记录被更新!");
    }

}
