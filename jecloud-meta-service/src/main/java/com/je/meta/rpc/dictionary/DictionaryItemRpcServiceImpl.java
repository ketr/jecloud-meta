/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.dictionary;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.dd.DDType;
import com.je.common.base.entity.QueryInfo;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.dd.DicInfoCache;
import com.je.meta.service.MetaDictionaryItemService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-17 18:58
 * @description: 字典项RPC服务实现
 */
@RpcSchema(schemaId = "dictionaryItemRpcService")
public class DictionaryItemRpcServiceImpl implements DictionaryItemRpcService {

    @Autowired
    private DicInfoCache dicInfoCache;
    @Autowired
    private MetaService metaService;
    @Autowired
    @Qualifier("metaCustomDictionaryItemService")
    private MetaDictionaryItemService metaCustomDictionaryItemService;
    @Autowired
    @Qualifier("metaDynaTreeDictionaryItemService")
    private MetaDictionaryItemService metaDynaTreeDictionaryItemService;
    @Autowired
    @Qualifier("metaListDictionaryItemService")
    private MetaDictionaryItemService metaListDictionaryItemService;
    @Autowired
    @Qualifier("metaSqlDictionaryItemService")
    private MetaDictionaryItemService metaSqlDictionaryItemService;
    @Autowired
    @Qualifier("metaSqlTreeDictionaryItemService")
    private MetaDictionaryItemService metaSqlTreeDictionaryItemService;
    @Autowired
    @Qualifier("metaTreeDictionaryItemService")
    private MetaDictionaryItemService metaTreeDictionaryItemService;

    @Override
    public List<JSONTreeNode> getDdItems(String code, Map<String, String> params, QueryInfo queryInfo, Boolean en) {
        DynaBean dictionary = dicInfoCache.getCacheValue(code);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", code)
                    .apply("and (SY_STATUS = '' or SY_STATUS = '1' or SY_STATUS is NULL)"));
        }
        if (dictionary == null){
            return new ArrayList<JSONTreeNode>();
        }
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        if (DDType.LIST.equals(ddType)) {
            return metaListDictionaryItemService.getDdItems(dictionary, params, queryInfo, en);
        } else if (DDType.TREE.equals(ddType)) {
            return metaTreeDictionaryItemService.getDdItems(dictionary, params, queryInfo, en);
        }
        if (DDType.DYNA_TREE.equals(ddType)) {
            return metaDynaTreeDictionaryItemService.getDdItems(dictionary, params, queryInfo, en);
        }
        if (DDType.SQL_TREE.equals(ddType)) {
            return metaSqlTreeDictionaryItemService.getDdItems(dictionary, params, queryInfo, en);
        }
        if (DDType.SQL.equals(ddType)) {
            return metaSqlDictionaryItemService.getDdItems(dictionary, params, queryInfo, en);
        }
        if (DDType.CUSTOM.equals(ddType)) {
            return metaCustomDictionaryItemService.getDdItems(dictionary, params, queryInfo, en);
        } else {
            throw new PlatformException("不合法的字典类型" + ddType, PlatformExceptionEnum.UNKOWN_ERROR);
        }
    }

}
