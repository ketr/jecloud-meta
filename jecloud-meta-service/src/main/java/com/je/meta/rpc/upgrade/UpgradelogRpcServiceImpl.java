/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.upgrade;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

@RpcSchema(schemaId = "upgradelogRpcService")
public class UpgradelogRpcServiceImpl implements UpgradelogRpcService{

    @Autowired
    private CommonService commonService;

    @Autowired
    private MetaService metaService;
    @Override
    public void saveUpgradelog(String upgradeInstallId, String upgradelogTypeCode, String upgradelogTypeName, String upgradelogContentName, String upgradelogContentCode) {
        DynaBean dynaBean = new DynaBean("JE_META_UPGRADELOG",true);
        dynaBean.setStr("JE_META_UPGRADEINSTALL_ID",upgradeInstallId);
        dynaBean.setStr("UPGRADELOG_TYPE_CODE",upgradelogTypeCode);
        dynaBean.setStr("UPGRADELOG_TYPE_NAME",upgradelogTypeName);
        dynaBean.setStr("UPGRADELOG_CONTENT_NAME",upgradelogContentName);
        dynaBean.setStr("UPGRADELOG_CONTENT_CODE",upgradelogContentCode);
        commonService.buildModelCreateInfo(dynaBean);
        metaService.insert(dynaBean);
    }
}
