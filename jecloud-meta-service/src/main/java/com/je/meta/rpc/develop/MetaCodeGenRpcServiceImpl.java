/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.develop;


import com.je.common.base.service.rpc.CodeGenService;
import com.je.meta.service.func.MetaCodeGenService;
import com.alibaba.fastjson2.JSONObject;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

@RpcSchema(schemaId = "metaCodeGenRpcService")
public class MetaCodeGenRpcServiceImpl implements CodeGenService {

    @Autowired
    private MetaCodeGenService metaCodeGenService;

    @Override
    public void cleanCodeGenerator() {
        metaCodeGenService.cleanCodeGenerator();
    }

    @Override
    public void cleanCodeValueByType(String emptyType) {
        metaCodeGenService.cleanCodeValueByType(emptyType);
    }

    @Override
    public String getSeq(JSONObject infos, String fieldName, String codeBase, String step, String cycle, Integer length) {
        System.out.println("-----");
        return metaCodeGenService.getSeq(infos,fieldName,codeBase,step,cycle,length);
    }

    @Override
    public String getSeqByJtgsId(JSONObject infos, String fieldName, String codeBase, String step, String cycle, Integer length, String jtgsId) {
        return metaCodeGenService.getSeq(infos,fieldName,codeBase,step,cycle,length,jtgsId);
    }
}
