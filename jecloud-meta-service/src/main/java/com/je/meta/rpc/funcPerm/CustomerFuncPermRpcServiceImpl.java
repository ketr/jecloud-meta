package com.je.meta.rpc.funcPerm;

import com.je.common.base.func.funcPerm.fastAuth.CustomerFuncPerm;
import com.je.common.base.func.funcPerm.fastAuth.FuncQuickPermission;
import com.je.common.base.func.funcPerm.roleSqlAuth.RoleSqlAuthVo;
import com.je.meta.rpc.funcperm.CustomerFuncPermRpcService;
import com.je.meta.service.func.CustomerFuncPermService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

@RpcSchema(schemaId = "customerFuncPermRpcService")
public class CustomerFuncPermRpcServiceImpl implements CustomerFuncPermRpcService {

    @Autowired
    private CustomerFuncPermService customerFuncPermService;

    @Override
    public FuncQuickPermission getCustomerFuncQuickPermission(String code) {
        return customerFuncPermService.getCustomerFuncQuickPermission(code);
    }

    @Override
    public RoleSqlAuthVo getCustomerFuncRoleSqlPermission(String code) {
        return customerFuncPermService.getCustomerFuncRoleSqlPermission(code);
    }

    @Override
    public String getCustomerFuncSqlPermission(String code) {
        return customerFuncPermService.getCustomerFuncSqlPermission(code);
    }

    @Override
    public CustomerFuncPerm getCustomerFuncAllPermByCode(String code) {
        CustomerFuncPerm customerFuncPerm = new CustomerFuncPerm();
        customerFuncPerm.setFuncQuickPermission(customerFuncPermService.getCustomerFuncQuickPermission(code));
        customerFuncPerm.setRoleSqlAuthVo(customerFuncPermService.getCustomerFuncRoleSqlPermission(code));
        customerFuncPerm.setSql(customerFuncPermService.getCustomerFuncSqlPermission(code));
        return customerFuncPerm;
    }
}
