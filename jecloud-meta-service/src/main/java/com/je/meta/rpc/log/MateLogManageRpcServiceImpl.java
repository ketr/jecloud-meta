/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.log;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MateLogManageService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.setting.MetaSystemSettingService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RpcSchema(schemaId = "mateLogManageRpcService")
public class MateLogManageRpcServiceImpl  implements MateLogManageService {

    @Autowired
    private MetaService metaService;

    @Autowired
    private CommonService commonService;

    @Override
    public void saveSlowSql(Date starTime, Date endTime, String methodName, String excuteSql) {
        //开关
        String logInfo = requireSettingValue("JE_SYS_LOGINFO");
        if(StringUtil.isEmpty(logInfo)){
            return;
        }

        List<String> list = Arrays.asList(logInfo.split(","));
        if(!list.contains("JE_CORE_SLOWSQL")){
            return;
        }
        //耗时  秒
        String takeTimeThreshold = requireSettingValue("JE_CORE_SLOWSQL_TAKETIME");
        if(StringUtil.isEmpty(takeTimeThreshold) || "0".equals(takeTimeThreshold)){
            //默认为5秒
            takeTimeThreshold="5";
        }

        String startTimeStr = DateUtils.formatDateTime(starTime);
        String endTimeStr = DateUtils.formatDateTime(endTime);

        long takeTime = DateUtils.getDatedifferenceMillisecond(startTimeStr,endTimeStr);
       if(takeTime<Long.parseLong(takeTimeThreshold)){
            return;
        }
        DynaBean dynaBean = new DynaBean();
        dynaBean.table("JE_CORE_SLOWSQLLOG");
        dynaBean.setStr("START_TIME", startTimeStr);
        dynaBean.setStr("END_TIME",endTimeStr);
        dynaBean.setLong("TAKE_TIME",takeTime);
        dynaBean.setStr("EXCUTE_METHOD_NAME",methodName);
        dynaBean.setStr("EXCUTE_SQL",excuteSql);
        commonService.buildModelCreateInfo(dynaBean);
        metaService.insert(dynaBean);
    }

    public String requireSettingValue(String code) {
        //该方法不能加上@CheckSlowSql，否则会出现死循环
        DynaBean valueBean = metaService.selectOne("JE_CORE_SETTING",
                ConditionsWrapper.builder()
                .eq("CODE", code),"VALUE");
        if (null == valueBean) {
            return null;
        }
        return valueBean.getStr("VALUE");

    }

}
