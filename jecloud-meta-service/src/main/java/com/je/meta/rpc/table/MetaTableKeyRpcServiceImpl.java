/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.APIWarnException;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.ArrayUtils;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.table.key.MetaTableKeyService;
import com.je.meta.service.table.MetaTableTraceService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 13:25
 * @description:
 */
@RpcSchema(schemaId = "metaTableKeyRpcService")
public class MetaTableKeyRpcServiceImpl implements MetaTableKeyRpcService {

    @Autowired
    private MetaTableKeyService metaTableKeyService;
    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaTableTraceService metaTableTraceService;

    @Override
    public void initKeys(DynaBean resourceTable, Boolean isTree) {
        metaTableKeyService.initKeys(resourceTable, isTree);
    }

    @Override
    public String checkKeys(List<DynaBean> keys) {
        return metaTableKeyService.checkKeys(keys);
    }

    @Override
    public Integer removeKey(DynaBean dynaBean, String ids, String ddl) {
        return metaTableKeyService.removeKey(dynaBean, ids, ddl);
    }

    @Override
    public void deleteKey(String tableCode, List<DynaBean> keys, String ddl) {
        metaTableKeyService.deleteKey(tableCode, keys, ddl);
    }

    @Override
    public void deleteKeyMeta(String tableCode, List<DynaBean> keys) {
        metaTableKeyService.deleteKeyMeta(tableCode, keys);
    }

    @Override
    public String requireDeletePhysicalKeyDDL(String tableCode, List<DynaBean> keys) {
        return metaTableKeyService.requireDeletePhysicalKeyDDL(tableCode, keys);
    }

    @Override
    public int doUpdateList(DynaBean dynaBean, String strData) throws APIWarnException {
        return metaTableKeyService.doUpdateList(dynaBean, strData);
    }

    @Override
    public String generateDeleteKeyDDL(DynaBean dynaBean, List<DynaBean> keys, String isDeleteDdl) {
        return metaTableKeyService.generateDeleteKeyDDL(dynaBean, keys, isDeleteDdl);
    }

    @Override
    public List<DynaBean> selectTableKeyByKeyId(String ids) {
        return metaTableKeyService.selectTableKeyByKeyId(ids);
    }

    @Override
    public List<DynaBean> selectTableIndexByIndexId(String ids) {
        String[] split = ids.split(ArrayUtils.SPLIT);
        List<DynaBean> indexs = metaService.select(ConditionsWrapper.builder()
                .table("JE_CORE_TABLEINDEX")
                .in("JE_CORE_TABLEINDEX_ID", split));
        for (DynaBean index : indexs) {
            metaTableTraceService.saveTableTrace("JE_CORE_TABLEINDEX", index, index, "DELETE", index.getStr("TABLEINDEX_RESOURCETABLE_ID"), index.getStr("TABLEINDEX_ISCREATE"));
        }
        return indexs;
    }

    @Override
    public boolean doRemoveKey(DynaBean dynaBean, List<DynaBean> keys, String isDeleteDdl, String tableCode) {
        return false;
    }

    /*@Override
    public String doRemoveIndex(String ids) {
        String[] split = ids.split(ArrayUtils.SPLIT);
        List<DynaBean> indexs = metaService.select(ConditionsWrapper.builder()
                .table("JE_CORE_TABLEINDEX").in("JE_CORE_TABLEINDEX_ID", split));
        for (DynaBean index : indexs) {
            metaTableTraceService.saveTableTrace("JE_CORE_TABLEINDEX", index, null, "DELETE", index.getStr("TABLEINDEX_RESOURCETABLE_ID"), index.getStr("TABLEINDEX_ISCREATE"));
        }
        String tableCode = "";
        if (indexs.size() > 0) {
            tableCode = indexs.get(0).getStr("TABLEINDEX_TABLECODE");
        }
        String ddl = metaTableColumnRpcService.deleteIndex(tableCode, indexs);
        tableCache.removeCache(tableCode);
        dynaCache.removeCache(tableCode);
        return ddl;
    }*/

}
