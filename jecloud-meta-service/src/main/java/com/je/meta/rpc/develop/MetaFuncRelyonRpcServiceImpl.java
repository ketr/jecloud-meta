/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.develop;

import com.je.common.base.DynaBean;
import com.je.common.base.service.rpc.FuncRelyonRpcService;
import com.je.meta.service.func.MetaFuncRelyonService;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@RpcSchema(schemaId = "metaFuncRelyonRpcService")
public class MetaFuncRelyonRpcServiceImpl implements FuncRelyonRpcService {

    @Autowired
    private MetaFuncRelyonService funcRelyonService;

    @Override
    public void saveRelyon(DynaBean funcInfo, String type, String funcId) {
        funcRelyonService.saveRelyon(funcInfo,type,funcId);
    }

    @Override
    public void doSave(String tableCode, String funcId, DynaBean dynaBean) {
        funcRelyonService.doSave(tableCode,funcId,dynaBean);
    }

    @Override
    public void doUpdate(String tableCode, String funcId, DynaBean dynaBean) {
        funcRelyonService.doUpdate(tableCode,funcId,dynaBean);
    }

    @Override
    public DynaBean getUpdateBean(String tableCode, DynaBean newBean, DynaBean oldBean) {
        return funcRelyonService.getUpdateBean(tableCode,newBean, oldBean);
    }

    @Override
    public void doUpdateList(String tableCode, String funcId, List<DynaBean> beans) {
        funcRelyonService.doUpdateList(tableCode,funcId,beans);
    }

    @Override
    public void doRemove(String tableCode, String funcId, String ids, List<DynaBean> beans) {
        funcRelyonService.doRemove(tableCode,funcId,ids,beans);
    }

    @Override
    public void doImpl(String funcId) {
        funcRelyonService.doImpl(funcId);
    }

    @Override
    public void doSyncColumn(String funcId) {
        funcRelyonService.doSyncColumn(funcId);
    }

    @Override
    public void doSyncField(String funcId) {
        funcRelyonService.doSyncField(funcId);
    }

    @Override
    public void removeTarget(String tableCode, DynaBean softFunc, List<DynaBean> beans) {
        funcRelyonService.removeTarget(tableCode,softFunc,beans);
    }

    @Override
    public void updateTarget(String tableCode, DynaBean softFunc, DynaBean newBean) {
        funcRelyonService.updateTarget(tableCode,softFunc,newBean);
    }

    @Override
    public void insertTarget(String tableCode, DynaBean softFunc, DynaBean dynaBean) {
        funcRelyonService.insertTarget(tableCode,softFunc,dynaBean);
    }

    @Override
    public List<DynaBean> getRelyons(String funcId) {
        return funcRelyonService.getRelyons(funcId);
    }
    
}
