/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.alibaba.fastjson2.JSONArray;
import com.je.common.base.DynaBean;
import com.je.meta.model.view.ViewColumn;
import com.je.meta.model.view.ViewOuput;
import com.je.meta.service.table.view.MetaViewService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 13:26
 * @description:
 */
@RpcSchema(schemaId = "metaViewRpcService")
public class MetaViewRpcServiceImpl implements MetaViewRpcService {

    @Autowired
    private MetaViewService metaViewService;


    @Override
    public DynaBean createView(DynaBean dynaBean) {
        return metaViewService.createView(dynaBean);
    }

    @Override
    public DynaBean saveViewInfo(DynaBean table, String fields) {
        return metaViewService.saveViewInfo(table,fields);
    }

    @Override
    public DynaBean updateViewCascadeInfo(DynaBean table, String cascade) {
        return metaViewService.updateViewCascadeInfo(table,cascade);
    }

    @Override
    public void saveTableView(JSONArray tableInfoList, String resourcetableTablecode) {
        metaViewService.saveTableView(tableInfoList,resourcetableTablecode);
    }

    @Override
    public DynaBean updateView(DynaBean table) {
        return metaViewService.updateView(table);
    }

    @Override
    public DynaBean updateViewInfo(DynaBean table, String fields, Boolean syncView) {
        return metaViewService.updateViewInfo(table,fields,syncView);
    }

    @Override
    public StringBuilder generateViewDdl(List<ViewColumn> viewColumnList, List<ViewOuput> viewOuputList, String viewCode, String masterTable) {
        return metaViewService.generateViewDdl(viewColumnList,viewOuputList,viewCode,masterTable);
    }

    @Override
    public int saveViewDdl(DynaBean dynaBean) {
        return metaViewService.saveViewDdl(dynaBean);
    }

    @Override
    public DynaBean saveTableByView(DynaBean dynaBean, String strData) {
        return metaViewService.saveTableByView(dynaBean,strData);
    }

    @Override
    public DynaBean updateViews(DynaBean dynaBean, String strData) {
        return metaViewService.updateViews(dynaBean,strData);
    }

    @Override
    public DynaBean updateViewCascadeInfos(DynaBean dynaBean, List<ViewColumn> viewColumnList) {
        return metaViewService.updateViewCascadeInfos(dynaBean,viewColumnList);
    }

    @Override
    public DynaBean saveViewInfos(DynaBean dynaBean, List<ViewOuput> viewOuputList,DynaBean table) {
        return metaViewService.saveViewInfos(dynaBean,viewOuputList,table);
    }

    @Override
    public void saveTableViews(List<String> tableInfoList, String tableCode, boolean b) {
        metaViewService.saveTableViews(tableInfoList,tableCode,b);
    }

    @Override
    public DynaBean updateViewInfos(DynaBean table, List<ViewOuput> viewOuputList, boolean b,DynaBean recourceTable) {
        return metaViewService.updateViewInfos(table,viewOuputList,b,recourceTable);
    }

    @Override
    public DynaBean selectTableByView(String strData, DynaBean dynaBean) {
        return null;
    }

    @Override
    public DynaBean updateTableByView(String strData, DynaBean dynaBean) {
        return null;
    }

    @Override
    public DynaBean selectOneByCode(DynaBean dynaBean) {
        return metaViewService.selectOneByCode(dynaBean);
    }
}
