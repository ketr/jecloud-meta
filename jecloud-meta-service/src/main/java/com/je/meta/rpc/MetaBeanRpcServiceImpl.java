/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc;

import com.je.common.base.DynaBean;
import com.je.common.base.entity.func.FuncRelationField;
import com.je.common.base.service.rpc.BeanService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.service.common.MetaBeanService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;
import java.util.Map;

@RpcSchema(schemaId = "metaBeanRpcService")
public class MetaBeanRpcServiceImpl implements BeanService {

    @Autowired
    private MetaBeanService metaBeanService;

    @Override
    public DynaBean getResourceTable(String tableCode) {
        return metaBeanService.getResourceTable(tableCode);
    }

    @Override
    public String getPKeyFieldNames(DynaBean dynaBean) {
        return metaBeanService.getPKeyFieldNames(dynaBean);
    }

    @Override
    public String getPKeyFieldNamesByTableCode(String tableCode) {
        return metaBeanService.getPKeyFieldNames(tableCode);
    }

    @Override
    public String getForeignKeyField(String tableCode, String parentTableCode, String parentPkCode, List<FuncRelationField> relatedFields) {
        return metaBeanService.getForeignKeyField(tableCode,parentTableCode,parentPkCode,relatedFields);
    }

    @Override
    public String[] getNames(DynaBean dynaBean) {
        return metaBeanService.getNames(dynaBean);
    }

    @Override
    public String getNames4Sql(DynaBean table) {
        return metaBeanService.getNames4Sql(table);
    }

    @Override
    public String getUpdateInfos4Sql(DynaBean resourceTable, Map<String,Object> values) {
        return metaBeanService.getUpdateInfos4Sql(resourceTable,values);
    }

    @Override
    public String getNames2DynaBean4Sql(DynaBean dynaBean) {
        return metaBeanService.getNames2DynaBean4Sql(dynaBean);
    }

    @Override
    public String getValues4Sql(DynaBean table) {
        return metaBeanService.getValues4Sql(table);
    }

    @Override
    public DynaBean getDynaBeanByResourceTable(DynaBean table) {
        return metaBeanService.getDynaBeanByResourceTable(table);
    }

    @Override
    public Object[] getValues(DynaBean dynaBean) {
        return metaBeanService.getValues(dynaBean);
    }

    @Override
    public List<DynaBean> buildUpdateList(String updateStr, String tableCode) {
        return metaBeanService.buildUpdateList(updateStr,tableCode);
    }

    @Override
    public String[] getDynaBeanIdArray(List<DynaBean> beans, String pkCode) {
        return metaBeanService.getDynaBeanIdArray(beans,pkCode);
    }

    @Override
    public DynaBean initSysTable(String tableCode) {
        return metaBeanService.initSysTable(tableCode);
    }

    @Override
    public String getFieldNames(DynaBean resourceTable, String[] excludes) {
        return metaBeanService.getFieldNames(resourceTable,excludes);
    }

    @Override
    public String getSysQueryFields(String tableCode) {
        return metaBeanService.getSysQueryFields(tableCode);
    }

    @Override
    public String getProQueryFields(String tableCode) {
        return metaBeanService.getProQueryFields(tableCode);
    }

    @Override
    public String getQueryFields(String tableCode, String[] excludes) {
        return metaBeanService.getQueryFields(tableCode,excludes);
    }

    @Override
    public String getNoClobQueryFields(String tableCode) {
        return metaBeanService.getNoClobQueryFields(tableCode);
    }

    @Override
    public JSONTreeNode getTreeTemplate(String tableCode) {
        return metaBeanService.getTreeTemplate(tableCode);
    }

    @Override
    public JSONTreeNode buildJSONTreeNodeTemplate(List<DynaBean> columns) {
        return metaBeanService.buildJSONTreeNodeTemplate(columns);
    }

}
