package com.je.meta.rpc.busflow;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.util.StringUtil;
import com.je.meta.cache.busflow.JeBusFlowCache;
import com.je.meta.service.busflow.JeBusFlowService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RpcSchema(schemaId = "jeBusFlowRpcService")
public class JeBusFlowRpcServiceImpl implements JeBusFlowRpcService{
    @Autowired
    private JeBusFlowService jeBusFlowService;
    @Autowired
    private JeBusFlowCache jeBusFlowCache;
    @Override
    public DynaBean getBusFlow(String busFlowCode, String refresh) {
        return jeBusFlowService.getBusFlow(busFlowCode,refresh);
    }

    @Override
    public List<DynaBean> loadFlowNodes(String busFlowCode, String mainId) {
        return jeBusFlowService.loadFlowNodes(busFlowCode,mainId);
    }

    @Override
    public void doInitFlowNodes(String busFlowCode, String mainId) {
        jeBusFlowService.doInitFlowNodes(busFlowCode,mainId);
    }

    @Override
    public void doRenewFlowNodes(String busFlowCode, String mainId) {
        jeBusFlowService.doRenewFlowNodes(busFlowCode,mainId);
    }

    @Override
    public void doRenewNode(String busFlowCode, String mainId, String nodeCode) {
        jeBusFlowService.doRenewNode(busFlowCode,mainId,nodeCode);
    }

    @Override
    public void doRenewNodeInfo(String busFlowCode, String mainId, String nodeCode, JSONObject infoObj) {
        jeBusFlowService.doRenewNodeInfo(busFlowCode,mainId,nodeCode,infoObj);
    }

    @Override
    public void doClearCache(String busFlowCode) {
        if(StringUtil.isNotEmpty(busFlowCode)){
            jeBusFlowCache.removeCache(busFlowCode);
        }else{
            jeBusFlowCache.clear();
        }
    }
}
