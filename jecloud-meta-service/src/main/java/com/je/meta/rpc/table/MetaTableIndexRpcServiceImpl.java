/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.table.BuildingSqlFactory;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.common.MetaBeanService;
import com.je.meta.service.table.index.MetaTableIndexService;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 13:24
 * @description:
 */
@RpcSchema(schemaId = "metaTableIndexRpcService")
public class MetaTableIndexRpcServiceImpl implements MetaTableIndexRpcService {

    @Autowired
    private MetaTableIndexService metaTableIndexService;

    @Autowired
    protected MetaService metaService;

    @Autowired
    private MetaBeanService metaBeanService;

    @Override
    public void initIndexs(DynaBean resourceTable, Boolean isTree) {
        metaTableIndexService.initIndexs(resourceTable,isTree);
    }


    @Override
    public String removeIndexByColumn(String funcId, String columnCode, String columnId) {
        return metaTableIndexService.removeIndexByColumn(funcId,columnCode,columnId);
    }

    @Override
    public String checkIndexs(List<DynaBean> indexs) {
        for(DynaBean dynaBean :indexs){
            String funcId = dynaBean.getStr("funcId");
            if(StringUtil.isNotEmpty(funcId)){
                DynaBean funcInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO",funcId);
                dynaBean.setStr("TABLEINDEX_TABLECODE",funcInfo.getStr("FUNCINFO_TABLENAME"));
            }
        }
        return metaTableIndexService.checkIndexs(indexs);
    }

    @Override
    public Integer removeIndex(DynaBean dynaBean, String ids) {
        return metaTableIndexService.removeIndex(dynaBean,ids);
    }

    @Override
    public void deleteIndex(String tableCode, List<DynaBean> indexs) {
        metaTableIndexService.deleteIndex(tableCode,indexs);
    }

    @Override
    public String deleteIndexMeta(DynaBean dynaBean) {
        String tableCode="";
        List<DynaBean> indexs = new ArrayList<DynaBean>();
        String ids = dynaBean.getStr("tableIndexIds");
        if(StringUtil.isNotEmpty(ids)){
            //资源表删除数据
            indexs = metaService.select("JE_CORE_TABLEINDEX",ConditionsWrapper.builder().in("JE_CORE_TABLEINDEX_ID", Arrays.asList(ids.split(","))));
            tableCode=indexs.get(0).getStr("TABLEINDEX_TABLECODE");
        }else{
            //功能删除逻辑表数据
            String funcId = dynaBean.getStr("funcId");
            DynaBean funcInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", funcId);
            tableCode = funcInfo.getStr("FUNCINFO_TABLENAME");
            DynaBean index = getIndexInfo(dynaBean,tableCode);
            /*if ("SYS".equals(index.getStr("TABLEINDEX_CLASSIFY"))) {
                return MessageUtils.getMessage("index.delete.sys");
            }*/
            indexs.add(index);
        }
        metaTableIndexService.deleteIndexMeta(tableCode,indexs);
        return  null;
    }

    private DynaBean getIndexInfo(DynaBean dynaBean,String tableCode) {
        String columnCode = dynaBean.getStr("TABLEINDEX_FIELDCODE");
        DynaBean resourceTable = metaBeanService.getResourceTable(tableCode);
        DynaBean index = metaService.selectOne("JE_CORE_TABLEINDEX",
                ConditionsWrapper.builder()
                        .eq("TABLEINDEX_FIELDCODE", columnCode)
                        .eq("TABLEINDEX_RESOURCETABLE_ID", resourceTable.getStr("JE_CORE_RESOURCETABLE_ID")));
        return  index;
    }
    @Override
    public String requireDeletePhysicalIndexDDL(DynaBean dynaBean) {
        String funcId = dynaBean.getStr("funcId");
        DynaBean funcInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", funcId);
        String tableCode = funcInfo.getStr("FUNCINFO_TABLENAME");
        DynaBean index = getIndexInfo(dynaBean,tableCode);
        List<DynaBean> indexs = new ArrayList<DynaBean>();
        indexs.add(index);
        return metaTableIndexService.requireDeletePhysicalIndexDDL(tableCode,indexs);
    }

    @Override
    public String getDelIndexDdl(String ids) {
       List<DynaBean> indexs = metaService.select("JE_CORE_TABLEINDEX",ConditionsWrapper.builder().in("JE_CORE_TABLEINDEX_ID", Arrays.asList(ids.split(","))));
       String tableCode="";
       if(indexs!=null&&indexs.size()>0){
           tableCode = indexs.get(0).getStr("TABLEINDEX_TABLECODE");
           return BuildingSqlFactory.getDdlService().getDeleteIndexSql(tableCode, indexs);
       }
       return null;
    }


    @Override
    public DynaBean saveLogicData(DynaBean dynaBean) {
        String funcId = dynaBean.getStr("funcId");
        DynaBean funcInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", funcId);
        String tableCode=funcInfo.getStr("FUNCINFO_TABLENAME");
        String columnCode = dynaBean.getStr("TABLEINDEX_FIELDCODE");
        String columnId = dynaBean.getStr("columnId");
        DynaBean resourceTable = metaBeanService.getResourceTable(tableCode);
        return metaTableIndexService.saveLogicData(funcInfo,resourceTable,columnCode,columnId);
    }

    @Override
    public String getDDL4AddIndex(DynaBean dynaBean){
        DynaBean resourceTable = metaBeanService.getResourceTable(dynaBean.getStr("TABLEINDEX_TABLECODE"));
        return metaTableIndexService.getDDL4AddIndex(dynaBean,resourceTable);
    }

}
