/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.listener;

import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.spring.SpringContextHolder;
import com.je.meta.rpc.dictionary.DictionaryRpcService;
import org.apache.servicecomb.core.BootListener;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-07-21 19:14
 * @description:
 */
public class DictionaryInitListener implements BootListener {

    @Override
    public int getOrder() {
        return 101;
    }

    @Override
    public void onAfterRegistry(BootEvent event) {
        DictionaryRpcService metaDictionaryService = SpringContextHolder.getBean(DictionaryRpcService.class);
        SystemSettingRpcService systemSettingRpcService = SpringContextHolder.getBean(SystemSettingRpcService.class);
        if ("1".equals(systemSettingRpcService.findSettingValue("JE_SYS_PRODICTIONARY"))) {
            metaDictionaryService.doCacheAllListTypeDicItem();
        }
        metaDictionaryService.doCacheAllDicInfo();
    }

}
