/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.system;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class PlanSystemConfig extends SystemSetting {

    public PlanSystemConfig() {
        super("manage-plan", "系统方案配置");
        this.addItem(new ConfigItem("JE_SYS_GRAY_MODEL","系统灰色模式","","","提示：系统整体调整为灰色，国家各类纪念日等使用"));
        this.addItem(new ConfigItem("JE_SYS_TOPMENU","顶部菜单位置","","","提示：顶部菜单的摆放位置"));
        this.addItem(new ConfigItem("JE_SYS_PATHSCHEME","路径方案","","","提示：访问者通过唯一的路径编码，完成系统不同的登录页面，以及初级产品的菜单过滤"));
        this.addItem(new ConfigItem("JE_SYS_PATHSCHEMENAME","路径方案名称","","","提示：访问者通过唯一的路径编码，完成系统不同的登录页面，以及初级产品的菜单过滤"));
        this.addItem(new ConfigItem("JE_SYS_AVAILABLEPRODUCTS","可使用产品ID","","","提示：访问者通过唯一的路径编码，完成系统不同的登录页面，以及初级产品的菜单过滤"));
        this.addItem(new ConfigItem("JE_SYS_AVAILABLEPRODUCTS_NAME","可使用产品NAME","","","提示：访问者通过唯一的路径编码，完成系统不同的登录页面，以及初级产品的菜单过滤"));
        this.addItem(new ConfigItem("JE_SYS_LOGINMETHOD","登录页方式","","",""));
        this.addItem(new ConfigItem("JE_SYS_LOGINCOLOR","登录页高亮元素颜色","","","提示：登录按钮，选中的超链接文字等原色高亮颜色"));
        this.addItem(new ConfigItem("JE_SYS_TITLELOGO","系统标题图（logo）","","","提示：系统会基于高度等比缩放，居中显示"));
        this.addItem(new ConfigItem("JE_SYS_BACKGROUNDLOGO","网页背景图（background）","","","提示：系统会基于高度等比缩放，居中显示（高出部分不显示），建议大小不超过1MB"));
        this.addItem(new ConfigItem("JE_SYS_LEFTIMG","左侧信息图（img）","","","提示1：方案二起效时，展示再左侧信息栏中\n" +
                "提示2：系统会基于宽带等比缩放，居中显示"));
        this.addItem(new ConfigItem("JE_SYS_SYSNAME","系统名称（sysname）","","","提示：以系统名义对外发送邮件，短时消息时使用"));
        this.addItem(new ConfigItem("JE_SYS_SYSURL","系统访问地址（url）","","","提示：系统对外提供的网络地址，http：//或https：//"));
        this.addItem(new ConfigItem("JE_SYS_ICON","浏览器显示图标（icon）","","","提示：140px*140px，ioc格式"));
        this.addItem(new ConfigItem("JE_SYS_WEBTITLE","浏览页页面标题（Title）","","","提示：网页标题，展示再浏览器标题栏中"));
        this.addItem(new ConfigItem("JE_SYS_WEBLOGO","系统顶部标题（Logo）","","","提示：推荐图形高度为60px的倍数，当高度为60px时宽度不超过400px"));
        this.addItem(new ConfigItem("JE_SYS_DEFAULT_HUE","系统默认色调（hue）","","",""));
        this.addItem(new ConfigItem("JE_SYS_LOGIN_FOOTER_HTML","登录页底部自定义html","","",""));
        this.addItem(new ConfigItem("JE_SYS_DEFAULT_MENUCOLOR","系统默认菜单色调","","",""));
        this.addItem(new ConfigItem("JE_SYS_FILING_NUMBER","备案号","","","提示：备案号将展示在登录页的底部位置"));
        this.addItem(new ConfigItem("SETPLAN_IS_DEFULT","是否默认方案","","",""));
        this.addItem(new ConfigItem("SETPLAN_YESORNOUSED","是否可用","","",""));
        this.addItem(new ConfigItem("SETPLAN_ASSOCIATE_TOP_MENU_ID","关联顶部菜单ID","","",""));
        this.addItem(new ConfigItem("SETPLAN_ASSOCIATE_TOP_MENU_NAME","关联顶部菜单名称","","",""));
        this.addItem(new ConfigItem("SETPLAN_ASSOCIATE_ALL_TOP_MENU","是否关联所有顶部菜单","","",""));

    }

}
