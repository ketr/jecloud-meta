/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting;

import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SystemSetting implements Serializable {

    /**
     * 配置类型编码
     */
    private final String code;
    /**
     * 配置类型名称
     */
    private final String text;

    private Set<ConfigItem> items;

    private Set<SystemSetting> childSettings;

    public SystemSetting(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public Set<ConfigItem> getSpecifyTypeItems(String type) {
        Set<ConfigItem> items = this.getItems();
        Set<ConfigItem> newItems = new HashSet<>();
        for (ConfigItem item : items) {
            if (Strings.isNullOrEmpty(item.getType()) || item.getType().equals(type)) {
                newItems.add(item);
            }
        }
        return newItems;
    }

    public String getCode() {
        return code;
    }

    public String getText() {
        return text;
    }

    public Set<ConfigItem> getItems() {
        return items;
    }

    public void setItems(Set<ConfigItem> items) {
        this.items = items;
    }

    public Set<SystemSetting> getChildSettings() {
        return childSettings;
    }

    public void setChildSettings(Set<SystemSetting> childSettings) {
        this.childSettings = childSettings;
    }

    public void addChild(SystemSetting systemSetting) {
        if (childSettings == null || childSettings.isEmpty()) {
            childSettings = new HashSet<>();
        }
        childSettings.add(systemSetting);
    }

    public void addItem(ConfigItem item) {
        if (items == null || items.isEmpty()) {
            items = new HashSet<>();
        }
        items.add(item);
    }

    public void addItem(String code, String text, String value, String remark) {
        ConfigItem findedItem = findItem(code);
        if (findedItem == null) {
            findedItem = new ConfigItem();
            addItem(findedItem);
        }
        findedItem.setCode(code);
        if (!Strings.isNullOrEmpty(text)) {
            findedItem.setText(text);
        }
        findedItem.setValue(value);
        if (!Strings.isNullOrEmpty(remark)) {
            findedItem.setRemark(remark);
        }
    }

    public List<String> getItemKeys() {
        List<String> keys = new ArrayList<>();
        if (null == items) {
            return keys;
        }
        items.forEach(each -> {
            keys.add(each.getCode());
        });
        return keys;
    }

    public boolean checkChildSetting(String settingCode) {
        return findChildSetting(settingCode, "") != null;
    }

    public SystemSetting findChildSetting(String settingCode, String type) {

        if (getCode().equals(settingCode)) {
            return this;
        }

        if (null == getChildSettings() || getChildSettings().isEmpty()) {
            return null;
        }

        SystemSetting findedSetting = null;
        for (SystemSetting node : getChildSettings()) {
            if (node.getCode().equals(settingCode)) {
                if (Strings.isNullOrEmpty(type)) {
                    return node;
                }
                node.setItems(node.getSpecifyTypeItems(type));
            }
            findedSetting = node.findChildSetting(settingCode, type);
            if (findedSetting != null) {
                break;
            }
        }

        return findedSetting;
    }

    public boolean checkItem(String code) {
        return findItem(code) != null;
    }

    public ConfigItem findItem(String code) {
        ConfigItem findedItem = null;
        if (null != getItems() && getItems().size() > 0) {
            for (ConfigItem item : getItems()) {
                if (item.getCode().equals(code)) {
                    findedItem = item;
                    break;
                }
            }
        }
        return findedItem;
    }

//    @Override
//    public String toString() {
//        return "SystemSetting{" +
//                "code='" + code + '\'' +
//                ", text='" + text + '\'' +
//                ", items=" + items +
//                ", childSettings=" + childSettings +
//                '}';
//    }
}
