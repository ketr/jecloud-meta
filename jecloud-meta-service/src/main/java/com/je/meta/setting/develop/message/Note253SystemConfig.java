/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.message;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * 253短信配置
 */
public class Note253SystemConfig extends SystemSetting {

    public Note253SystemConfig() {
        super("note-253", "253短信配置");
        this.addItem(new ConfigItem("NOTE_253_USERNAME","Account","","","提示：API账号，管理后台获取"));
        this.addItem(new ConfigItem("NOTE_253_PASSWORD","Password","","","提示：API密码，管理后台获取"));
        this.addItem(new ConfigItem("NOTE_253_REPORT","Report","","","提示：是否需要状态报告，默认false;如需状态报告则传：\"true\""));
        this.addItem(new ConfigItem("NOTE_253_SIGN","SignName","","","提示：在短信内容开始或者结尾展示，如：【JECloud】您可以登录创蓝云智短信后台配置"));
        this.addItem(new ConfigItem("NOTE_253_TEMPLATE","Template","","","提示：253后台设置的模板信息"));
        this.addItem(new ConfigItem("NOTE_253_SENDURL","接入网址","","","选择自定义时可以自行设置服务地址"));
    }

}
