/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.system.open;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * 开放平台设置
 */
public class OpenPlatformSystemConfig extends SystemSetting {

    public OpenPlatformSystemConfig() {
        super("open-config", "开放平台");
        this.addItem(new ConfigItem("JE_OPEN_WX","企业微信深度对接","","","用于企业微信消息提醒、组织挂接"));
        this.addItem(new ConfigItem("JE_OPEN_DD","钉钉深度对接","","","用于钉钉消息提醒、组织挂接"));
        this.addItem(new ConfigItem("JE_OPEN_FS","飞书深度对接","","","用于飞书消息提醒、组织挂接"));
        this.addItem(new ConfigItem("JE_OPEN_WELINK","WeLink深度对接","","","用于WeLink消息提醒、组织挂接"));

        this.addChild(new DingdingOpenPlatformSystemConfig());
        this.addChild(new FeishuOpenPlatformSystemConfig());
        this.addChild(new WechatOpenPlatformSystemConfig());
        this.addChild(new WelinkOpenPlatformSystemConfig());
    }

}
