/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.message;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class PushMessageSystemConfig extends SystemSetting {

    public PushMessageSystemConfig() {
        super("message-push", "消息推送");
        this.addItem(new ConfigItem("JE_SYS_NOTEDIY","短信服务商","","",""));

        //253短信
        this.addItem(new ConfigItem("NOTE_253_USERNAME","Account","","253","提示：API账号，管理后台获取"));
        this.addItem(new ConfigItem("NOTE_253_PASSWORD","Password","","253","提示：API密码，管理后台获取"));
        this.addItem(new ConfigItem("NOTE_253_REPORT","Report","","253","提示：是否需要状态报告，默认false;如需状态报告则传：\"true\""));
        this.addItem(new ConfigItem("NOTE_253_SIGNNAME","SignName","","253","提示：在短信内容开始或者结尾展示，如：【JECloud】您可以登录创蓝云智短信后台配置"));
        this.addItem(new ConfigItem("NOTE_253_TEMPLATE","Template","","253","提示：253后台设置的模板信息"));
        this.addItem(new ConfigItem("NOTE_253_SENDURL","接入网址","","253","选择自定义时可以自行设置服务地址"));

        //阿里云短信
        this.addItem(new ConfigItem("NOTE_ALIYUN_SIGNNAME","SignName","","ALIYUN","提示：您可以登录短信服务控制台，选择国内消息或国际/港澳台消息，在签名管理页面获取"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_TEMPLATE","TemplateCode","","ALIYUN","提示：短息模板Code，您可以登录短信服务控制台，选择国内消息或国际/港澳台消息，在页面查看模板CODE，必须是已添加，并通过审核的短信模板，且发送国际/港澳台消息时，请使用模板。"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_ACCESSKEY","Accesskey ID","","ALIYUN","提示：用于标识用户"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_SECRET","AccessKey Secret","","ALIYUN","提示：用户验证用户的密钥，AccessKey Secret必须保密"));
        this.addItem(new ConfigItem("NOTE_ALIYUN_URL","接入网址","","ALIYUN","选择自定义时可以设置服务地址"));

        //中国网建
        this.addItem(new ConfigItem("NOTE_SYS_UID","Uid","","SYS","提示：中国网建注册的用户名"));
        this.addItem(new ConfigItem("NOTE_SYS_KEY","Key","","SYS","提示：中国网建后台提供的接口密钥（可到用户平台修改接口密钥）"));
        this.addItem(new ConfigItem("NOTE_SYS_MD5","KeyMD5","","SYS","提示：中国网建后台提供的加密参数，KeyMD5=接口密钥32位MD5加密，大写"));
        this.addItem(new ConfigItem("NOTE_SYS_SIGNNAME","signName","","SYS","提示：在短信内容开始或者结尾展示，如【JECloud】，您可以登录网建后台，在短信签名中设置，长度为2-10个中英文字符，不能带标点符号"));
        this.addItem(new ConfigItem("NOTE_SYS_URL","接入网址","","SYS","提示：选择自定义时可以自行设置服务地址"));

        //自定义
        this.addItem(new ConfigItem("NOTE_DIY_CLASS","执行类及执行方法","","DIY","提示：自定义实现发送短信的类名及方法"));
        this.addItem(new ConfigItem("NOTE_DIY_URL","接入网址","","DIY","提示：选择自定义时可以自行设置服务地址"));
    }

}
