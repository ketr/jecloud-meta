/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.system;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class MailSystemConfig extends SystemSetting {

    public MailSystemConfig() {
        super("mail-config", "邮箱设置");
        this.addItem(new ConfigItem("JE_SYS_EMAIL_SENDTITLE","发送人名称","","","提示：发件人使用的名称"));
        this.addItem(new ConfigItem("JE_SYS_EMAIL_SERVERHOST","邮箱服务器（smtp）","","","提示：用户接收到的所有邮件将以本邮箱地址发出"));
        this.addItem(new ConfigItem("JE_SYS_EMAIL_SSL","安全协议（SSL）","","","提示：提示：SSL（Secure Sockets Layer 安全套接字协议）"));
        this.addItem(new ConfigItem("JE_SYS_EMAIL_SERVERPORT","邮箱服务端口","","","提示：邮箱服务端口，默认是465"));
        this.addItem(new ConfigItem("JE_SYS_EMAIL_AUTHCODE","邮箱授权码","","","提示：邮箱服授权码"));
        this.addItem(new ConfigItem("JE_SYS_EMAIL_USERNAME","邮箱账号（email）","","","提示：邮箱账号，所有邮件使用本账号发出"));
        this.addItem(new ConfigItem("JE_SYS_EMAIL_PASSWORD","邮箱密码（password）","","","提示：邮箱密码"));
        this.addItem(new ConfigItem("JE_SYS_EMAIL_SERVERVALIDATE","启用身份验证","","","提示：邮箱服务使用身份验证，默认开启"));
    }
}
