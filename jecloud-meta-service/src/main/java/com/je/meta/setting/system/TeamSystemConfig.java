/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.system;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

public class TeamSystemConfig extends SystemSetting {

    public TeamSystemConfig() {
        super("team-config", "开发团队");
        this.addItem(new ConfigItem("JE_SYS_DEVMANAGER","开发者管理员ID","","","提示：只有开发团队成员才可对系统进行二次开发（仅对用户进行功能菜单授权无效）"));
        this.addItem(new ConfigItem("JE_SYS_DEVMANAGER_NAME","开发者管理员NAME","","","提示：只有开发团队成员才可对系统进行二次开发（仅对用户进行功能菜单授权无效）"));
        this.addItem(new ConfigItem("JE_SYS_ADMIN","开发团队成员ID","","","提示：只有开发团队成员才可对系统进行二次开发（仅对用户进行功能菜单授权无效）"));
        this.addItem(new ConfigItem("JE_SYS_ADMIN_NAME","开发团队成员NAME","","","提示：只有开发团队成员才可对系统进行二次开发（仅对用户进行功能菜单授权无效）"));
    }

}
