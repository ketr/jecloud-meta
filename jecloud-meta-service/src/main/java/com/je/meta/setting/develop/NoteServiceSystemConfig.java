/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;
import com.je.meta.setting.develop.message.PushMessageSystemConfig;

public class NoteServiceSystemConfig extends SystemSetting {

    public static final String MESSAGE_TYPE = "message-service";

    public NoteServiceSystemConfig() {
        super(MESSAGE_TYPE, "消息服务");
        this.addItem(new ConfigItem("JE_PUSH_PLATFORM", "消息推送启用项", "", "", ""));
        this.addItem(new ConfigItem("JE_PUSH_WEB", "页面提醒", "", "WEB", "浏览器右下角弹出消息框",1));
        this.addItem(new ConfigItem("JE_PUSH_MAIL", "邮件提醒", "", "EMAIL", "邮件服务信息在系统设置-邮箱设置中维护",2));
        this.addItem(new ConfigItem("JE_PUSH_MSG", "短信提醒", "", "NOTE", "中国网建、创蓝253、阿里云短信服务、自定接口",3));
        this.addItem(new ConfigItem("JE_PUSH_APP", "APP消息推送", "", "APP", "个推参数配置请到JEAPP中设置"));
        this.addItem(new ConfigItem("JE_PUSH_WX", "企业微信", "", "WECHAT", "服务信息在系统设置-开放平台中维护",4));
        this.addItem(new ConfigItem("JE_PUSH_FS", "飞书提醒", "", "FLYBOOK", "服务信息在系统设置-开放平台中维护",6));
        this.addItem(new ConfigItem("JE_PUSH_DD", "钉钉提醒", "", "DINGTALK", "服务信息在系统设置-开放平台中维护",5));
        this.addItem(new ConfigItem("JE_PUSH_WELINK", "华为WeLink提醒", "", "WELINK", "服务信息在系统设置-开放平台中维护",7));
        this.addChild(new PushMessageSystemConfig());
    }
}
