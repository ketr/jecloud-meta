/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.login;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * Welink登录
 */
public class WelinkLoginSystemConfig extends SystemSetting {

    public WelinkLoginSystemConfig() {
        super("login-welink", "Welink登录");
        this.addItem(new ConfigItem("WL_LOGIN_CLIENTID","ClientID","","","提示：ClientID，可再开发者后台中查看"));
        this.addItem(new ConfigItem("WL_LOGIN_CLIENTSECRET","ClientSecret","","","提示：ClientSecret可在开发者后台中查看"));
        this.addItem(new ConfigItem("WL_LOGIN_REDIRECTURL","Redirecturi","","","提示：必须参数，授权后要回调得URL，接收Authorization Code的URL Redirecturi中不可带有query参数"));
    }
    
}
