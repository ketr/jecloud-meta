/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.setting.develop.login;

import com.je.meta.setting.ConfigItem;
import com.je.meta.setting.SystemSetting;

/**
 * QQ登录
 */
public class QqLoginSystemConfig extends SystemSetting {

    public QqLoginSystemConfig() {
        super("login-qq", "QQ登录");
        this.addItem(new ConfigItem("QQ_LOGIN_APPID","AppID","","","提示：应用ID（QQ开放平台->网站应用，申请地址：https://open.weixin.qq.com）"));
        this.addItem(new ConfigItem("QQ_LOGIN_APPKEY","AppSecret","","","提示：应用KEY（如果APP登录跟网站登录同步，则需要在开放平台将移动应用跟网站应用绑定）"));
        this.addItem(new ConfigItem("QQ_LOGIN_APPURL","访问域名","","","提示：系统访问域名，格式为：https://www.baidu.com"));
    }

}
