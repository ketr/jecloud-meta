/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func;


import com.alibaba.fastjson2.JSONObject;

public interface MetaCodeGenService {

    /**
     * 为编号执行周期归0操作
     */
    void cleanCodeGenerator();

    /**
     * 根据编号类型归0操作
     * @param emptyType
     */
    void cleanCodeValueByType(String emptyType);

    /**
     * 获取流水号
     * @param infos TODO未处理
     * @param fieldName TODO未处理
     * @param codeBase TODO未处理
     * @param step TODO未处理
     * @param cycle TODO未处理
     * @param length 长度
     * @return
     */
    String getSeq(JSONObject infos, String fieldName, String codeBase, String step, String cycle, Integer length);

    /**
     * 获取流水号
     * @param infos TODO未处理
     * @param fieldName 文件名称
     * @param codeBase TODO未处理
     * @param step TODO未处理
     * @param cycle TODO未处理
     * @param length 长度
     * @param jtgsId TODO未处理
     * @return
     */
    String getSeq(JSONObject infos, String fieldName, String codeBase, String step, String cycle, Integer length, String jtgsId);

}
