package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打包报表
 */
@Service("upgradeReportDate")
public class UpgradeReportDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeReportDate() {
        super(UpgradeResourcesEnum.REPORT);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> list = getSourceIds(upgradeBean);
        if (list == null || list.size() == 0) {
            return;
        }
        packageResult.setProductReports(packageResourcesOrderAsc(packageResult, list, "SY_TREEORDERINDEX"));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductReports();
    }


}
