/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.SecurityUserHolder;
import com.je.meta.service.common.MetaBeanService;
import com.je.meta.service.table.MetaTableTraceService;
import com.je.meta.service.table.TableTraceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 11:41
 * @description:
 */
@Service
public class MetaTableTraceServiceImpl implements MetaTableTraceService {

    @Autowired
    private MetaBeanService metaBeanService;
    @Autowired
    private MetaService metaService;

    @Override
    @Transactional
    public void saveTableTrace(String tableCode, DynaBean oldBean, DynaBean newBean, String oper, String tableId, String isCreate) {
        DynaBean tableInfo = metaService.selectOneByPk("JE_CORE_RESOURCETABLE",tableId);
        if(!"1".equals(tableInfo.getStr("RESOURCETABLE_ISCREATE"))){
                return;
        }
        DynaBean resourceTable = metaBeanService.getResourceTable(tableCode);
        List<DynaBean> columns = (List<DynaBean>) resourceTable.get(BeanService.KEY_TABLE_COLUMNS);
        DynaBean trace = buildBaseInfo(tableCode, tableId, isCreate);
        new TableTraceFactory().getTableTrace(tableCode).save(columns, trace, oldBean, newBean, oper);
    }

    @Override
    public void saveTableTrace(String tableCode, DynaBean dynaBean, String oper, String tableId, String isCreate) {
        saveTableTrace(tableCode, null, dynaBean, oper, tableId, isCreate);
    }

    private DynaBean buildBaseInfo(String tableCode, String tableId, String isCreate) {
        DynaBean trace = new DynaBean("JE_CORE_TABLETRACE", false);
        String nowTime = DateUtils.formatDateTime(new Date());
        trace.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLETRACE_ID");
        trace.set("TABLETRACE_CZRNAME", SecurityUserHolder.getCurrentAccountRealUserName());
        trace.set("TABLETRACE_CZRCODE", SecurityUserHolder.getCurrentAccountRealUserCode());
        trace.set("TABLETRACE_CZSJ", nowTime);
        trace.set("SY_CREATETIME", nowTime);
        trace.set("SY_CREATEUSERID", SecurityUserHolder.getCurrentAccountRealUserId());
        trace.set("SY_CREATEUSERNAME", SecurityUserHolder.getCurrentAccountRealUserName());
        trace.set("SY_CREATEORGID", SecurityUserHolder.getCurrentAccountRealOrgId());
        trace.set("SY_CREATEORGNAME", SecurityUserHolder.getCurrentAccountRealOrgName());
        trace.set("TABLETRACE_TABLECODE", tableCode);
        trace.set("TABLETRACE_RESOURCETABLE_ID", tableId);
        trace.set("TABLETRACE_SFYY", isCreate);
        return trace;
    }


}
