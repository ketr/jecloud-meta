/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dictionary.impl.item;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.CommonTreeService;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.service.dictionary.DictionaryItemService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DictionaryItemServiceImpl implements DictionaryItemService {


    @Autowired
    protected CommonService commonService;

    @Autowired
    private CommonTreeService commonTreeService;
    @Autowired
    private MetaService metaService;
    @Override
    public Map<String,Object> doUpdateList(String tableCode, String strData, String funcType, String funcCode, String codeGenFieldInfo) {
        List<DynaBean> list;
        //多根树或单根树处理
        if(commonService.checkFuncMoreRoot(funcCode)){
            list = commonTreeService.doMoreRootUpdateList(tableCode, strData, funcType, funcCode, codeGenFieldInfo);
        }else {
            list = commonTreeService.doSingleUpdateList(tableCode, strData, funcType, funcCode, codeGenFieldInfo);
        }
        List<Object> returnList = new ArrayList<>();
        List<JSONTreeNode> nodeList = new ArrayList<>();
        for (DynaBean dynaBean : list) {
            nodeList.add(commonTreeService.buildSingleTreeNode(dynaBean));
            returnList.add(dynaBean.getValues());
        }
        //更新主表的DICTIONARY_ITEMROOT_ID
        if(list!=null&&list.size()>0){
            DynaBean item =list.get(0);
            String nodeType = item.getStr("SY_NODETYPE");
            String itemRootId ="";
            if("ROOT".equals(nodeType)){
                 itemRootId  = item.getStr("JE_CORE_DICTIONARYITEM_ID");
            }
            if("LEAF".equals(nodeType)){
                itemRootId = item.getStr("SY_PARENT");
            }
            String dictionaryId =item.getStr("DICTIONARYITEM_DICTIONARY_ID");
            metaService.executeSql("UPDATE JE_CORE_DICTIONARY SET DICTIONARY_ITEMROOT_ID="+itemRootId +" WHERE JE_CORE_DICTIONARY_ID="+dictionaryId);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("dynaBeans", returnList);
        result.put("nodes", nodeList);
        return result;
    }
}
