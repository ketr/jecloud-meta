package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打包全局样式
 */
@Service("upgradeGlobalCssDate")
public class UpgradeGlobalCssDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeGlobalCssDate() {
        super(UpgradeResourcesEnum.GLOBAL_CSS);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> list = getSourceIds(upgradeBean);
        if (list == null || list.size() == 0) {
            return;
        }
        packageResult.setProductGlobalCss(packageResources(packageResult, list));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductGlobalCss();
    }

}
