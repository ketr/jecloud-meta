/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.common.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaRbacService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.SystemSettingRpcService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.common.MetaDevelopLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MetaDevelopLogServiceImpl implements MetaDevelopLogService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaRbacService metaRbacService;
    @Lazy
    @Autowired
    private CommonService commonService;
    @Autowired
    private SystemSettingRpcService systemSettingRpcService;

    @Override
    public long getMenuNum(String menuCode) {
        long count=0;
        //资源表
        if("SYS_TABLE".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_RESOURCETABLE WHERE SY_JECORE != {0} AND RESOURCETABLE_TYPE IN ('PT','VIEW','TREE')","1");
            //功能
        }else if("FUNCCFG_SUBSYSTEM".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_FUNCINFO WHERE SY_JECORE != {0} AND FUNCINFO_NODEINFOTYPE IN ('FUNC','FUNCFIELD')","1");
            //菜单
        }else if("SYS_MENU".equals(menuCode)){
            count = metaRbacService.countBySql("SELECT COUNT(*) FROM JE_CORE_MENU WHERE SY_JECORE != {0} AND MENU_NODEINFOTYPE!='MENU'","1");
            //工作流
        }else if("JE_CORE_PROCESSINFO".equals(menuCode)){
//            count = metaWorkflowService.countBySql("SELECT COUNT(*) FROM JE_CORE_PROCESSINFO WHERE PROCESSINFO_LASTVERSION = {0}","none");
            //数据字典
        }else if("JE_CORE_DICTIONARY".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_DICTIONARY WHERE SY_JECORE != {0}","1");
        }else if("JE_CORE_CHARTS".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_REPORT WHERE SY_DISABLED={0} AND SY_JECORE!={1}","0","1");
            long chartCount = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_CHARTS WHERE SY_DISABLED={0} AND SY_JECORE!={1}","0","1");
            count+=chartCount;
        }else if("JE_SYS_TIMEDTASK".equals(menuCode)){
            count = metaService.countBySql("SELECT COUNT(*) FROM JE_SYS_TIMEDTASK");
        }
        return count;
    }

    @Override
    @Transactional
    public void doDevelopLog(String act, String actName, String type, String typeName, String name, String code, String id,String productId) {
        String logFlag = systemSettingRpcService.findSettingValue("JE_SYS_LOGINFO");
        if(StringUtil.isEmpty(logFlag)){
            return;
        }
        if(!logFlag.contains("JE_SYS_DEVELOPLOG")) {
            return;
        }
        DynaBean log=new DynaBean("JE_CORE_DEVELOPLOG",true);
        log.set("DEVELOPLOG_USERNAME",SecurityUserHolder.getCurrentAccount().getName());
        log.set("DEVELOPLOG_USERID",SecurityUserHolder.getCurrentAccount().getId());
        log.set("DEVELOPLOG_TYPE_NAME",typeName);
        log.set("DEVELOPLOG_TYPE_CODE",type);
        log.set("DEVELOPLOG_ACT_NAME",actName);
        log.set("DEVELOPLOG_ACT_CODE",act);
        log.set("DEVELOPLOG_NAME",name);
        log.set("DEVELOPLOG_CODE",code);
        log.set("DEVELOPLOG_ID",id);
        log.set("SY_PRODUCT_ID",productId);
        commonService.buildModelCreateInfo(log);
        metaService.insert(log);
    }

}
