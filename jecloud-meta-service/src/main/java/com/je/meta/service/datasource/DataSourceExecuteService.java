/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.datasource;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.rpc.UpgradeModulePackageRpcService;
import com.je.servicecomb.RpcSchemaFactory;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface DataSourceExecuteService {

    default List<Map<String, Object>> routeExecuteSql(String productCode, String sql) {
        UpgradeModulePackageRpcService upgradeModulePackageRpcService = RpcSchemaFactory.getRemoteProvierClazz(productCode,
                "upgradeModulePackageRpcService", UpgradeModulePackageRpcService.class);
        List<Map<String, Object>> result = upgradeModulePackageRpcService.executeQuerySql(sql);
        if (result != null && result.size() > 0) {
            Map<String, Object> map = result.get(0);
            if (map.containsKey("message") && map.containsKey("success")) {
                throw new PlatformException(map.get("message").toString(), PlatformExceptionEnum.UNKOWN_ERROR);
            }
        }
        return result;
    }

    default List<Map<String, Object>> routeExecuteSql(String productCode, String sql, int limit) {
        UpgradeModulePackageRpcService upgradeModulePackageRpcService = RpcSchemaFactory.getRemoteProvierClazz(productCode,
                "upgradeModulePackageRpcService", UpgradeModulePackageRpcService.class);
        List<Map<String, Object>> result = upgradeModulePackageRpcService.executeQuerySqlByLimit(sql, limit);
        if (result != null && result.size() > 0) {
            Map<String, Object> map = result.get(0);
            if (map.containsKey("message") && map.containsKey("success")) {
                throw new PlatformException(map.get("message").toString(), PlatformExceptionEnum.UNKOWN_ERROR);
            }
        }
        return result;
    }

    default String buildSql(String sql, JSONObject jsonObject) {
        if (jsonObject == null) {
            return sql;
        }
        Set<String> set = jsonObject.keySet();
        for (String key : set) {
            String keyFlag = "{" + key + "}";
            if (sql.contains(keyFlag)) {
                String value = jsonObject.getString(key);
                String valueStr = "";
                if (value.contains(",")) {
                    keyFlag = "'{" + key + "}'";
                    String[] valueArr = value.split(",");
                    for (int i = 0; i < valueArr.length; i++) {
                        if (i == valueArr.length - 1) {
                            valueStr = valueStr + "\"" + valueArr[i] + "\"";
                        } else {
                            valueStr = valueStr + "\"" + valueArr[i] + "\"" + ",";
                        }
                    }
                } else {
                    valueStr = value;
                }
                sql = sql.replace(keyFlag, valueStr);
            }
        }
        return sql;
    }

    public List<Map<String, Object>> execute(DynaBean dataSource, String parameterStr, String limit);

    public List<DynaBean> executeForRpc(DynaBean dataSource, Map<String, Object> map, int limit);
}
