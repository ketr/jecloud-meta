package com.je.meta.service.busflow;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.auth.impl.RealOrganizationUser;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.tree.NodeType;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaBusService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.service.rpc.UpgradeModulePackageRpcService;
import com.je.common.base.util.*;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.busflow.JeBusFlowCache;
import com.je.servicecomb.RpcSchemaFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component("jeBusFlowService")
public class JeBusFlowServiceImpl implements JeBusFlowService {
    public Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private JeBusFlowCache jeBusFlowCache;
    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private BeanService beanService;
    @Override
    public DynaBean getBusFlow(String busFlowCode, String refresh) {
        DynaBean busFlow=jeBusFlowCache.getCacheValue(busFlowCode);
        if("1".equals(refresh)){
            busFlow=null;
        }
        if(busFlow==null){
            busFlow=metaService.selectOne("JE_META_BUSFLOW", ConditionsWrapper.builder().eq("BUSFLOW_CODE", busFlowCode));
            List<DynaBean> flowNodes=metaService.select("JE_META_BUSFLOW_NODE", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_ID", busFlow.getStr("JE_META_BUSFLOW_ID")).orderByAsc("SY_ORDERINDEX"));
            List<HashMap> nodes=new ArrayList<>();
            for(DynaBean node:flowNodes){
                nodes.add(node.getValues());
            }
            busFlow.set("_NODES_",nodes);
        }
        return busFlow;
    }

    @Override
    public List<DynaBean> loadFlowNodes(String busFlowCode, String mainId) {
        DynaBean busFlow=getBusFlow(busFlowCode, "0");
        List<DynaBean> nodes=metaService.select("JE_META_BUSFLOW_FNODE", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_ID",
                busFlow.getStr("JE_META_BUSFLOW_ID")).eq("MAIN_PKVALUE",mainId).orderByAsc("SY_TREEORDERINDEX"));
        return nodes;
    }

    @Override
    public void doInitFlowNodes(String busFlowCode, String mainId) {
        DynaBean busFlow=getBusFlow(busFlowCode, "0");
        List<DynaBean> haveNodes=metaService.select("JE_META_BUSFLOW_FNODE", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_ID",
                busFlow.getStr("JE_META_BUSFLOW_ID")).eq("MAIN_PKVALUE",mainId).orderByAsc("SY_TREEORDERINDEX"));
        Map<String,DynaBean> haveNodeInfos=new HashMap<>();
        for(DynaBean node:haveNodes){
            haveNodeInfos.put(node.getStr("JE_META_BUSFLOW_NODE_ID"),node);
        }
        DynaBean rootNode=metaService.selectOne("JE_META_BUSFLOW_FNODE", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_FNODE_ID",
                NodeType.ROOT));
        List<DynaBean> busNodes=metaService.select("JE_META_BUSFLOW_NODE", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_ID",
                busFlow.getStr("JE_META_BUSFLOW_ID")).orderByAsc("SY_TREEORDERINDEX"));
        DynaBean mainInfo=new DynaBean();
        if(StringUtil.isNotEmpty(mainId) && StringUtil.isNotEmpty(busFlow.getStr("BUSFLOW_TABLECODE"))){
            String pd=busFlow.getStr("SY_PROJECT_CODE");
            if(StringUtil.isEmpty(pd)){
                pd="meta";
            }
            UpgradeModulePackageRpcService upgradeModulePackageRpcService = RpcSchemaFactory.getRemoteProvierClazz(pd,
                    "upgradeModulePackageRpcService", UpgradeModulePackageRpcService.class);
//            List<Map<String, Object>> mainDatas = new ArrayList<>();
//            if (pd.equals("meta")) {
//                mainDatas = metaService.selectSql("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "= '" + mainId + "'");
//            } else {
//                mainDatas = upgradeModulePackageRpcService.executeQuerySql("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "= '" + mainId + "'");
//            }
            List<Map<String,Object>> mainDatas=new ArrayList<>();
            if("meta".equals(pd)){
                mainDatas=metaService.selectSql("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }else {
                MetaBusService metaBusService = RpcSchemaFactory.getRemoteProvierClazz(pd, "metaBusService", MetaBusService.class);
                mainDatas = metaBusService.selectMap("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }
//            MetaBusService metaBusService = RpcSchemaFactory.getRemoteProvierClazz(pd, "metaBusService", MetaBusService.class);
//            List<Map<String,Object>> mainDatas=metaBusService.selectMap("SELECT * FROM "+busFlow.getStr("BUSFLOW_TABLECODE")+" WHERE "+busFlow.getStr("BUSFLOW_PKCODE")+"={0}",mainId);
            if(mainDatas!=null && mainDatas.size()>0){
                mainInfo=new DynaBean();
                Map<String,Object> infos=mainDatas.get(0);
                for(String field:infos.keySet()){
                    if(infos.get(field)==null || BeanService.KEY_TABLE_CODE.equals(field)){
                        continue;
                    }
                    mainInfo.set(field,infos.get(field));
                }
            }
        }
        Set<Map.Entry<String,Object>> ddSet= new HashSet<>();
        ddSet.addAll(mainInfo.getValues().entrySet());
        doCascadeInitNode(rootNode,busNodes,haveNodeInfos,busFlow,mainId,ddSet);

    }
    private void doCascadeInitNode(DynaBean parentNode,List<DynaBean> busNodes,Map<String,DynaBean> haveNodeInfos,DynaBean busFlow,String mainId,Set<Map.Entry<String,Object>> ddSet){
        String pId=parentNode.getStr("JE_META_BUSFLOW_NODE_ID");
        if(StringUtil.isEmpty(pId) || NodeType.ROOT.equals(parentNode.getStr("JE_META_BUSFLOW_FNODE_ID"))){
            pId=NodeType.ROOT;
        }
        for(DynaBean busNode:busNodes){
            if(pId.equals(busNode.getStr("SY_PARENT"))){
                String nodeId=busNode.getStr("JE_META_BUSFLOW_NODE_ID");
                DynaBean node=haveNodeInfos.get(nodeId);
                Boolean isCreate=false;
                if(node==null){
                    node=new DynaBean("JE_META_BUSFLOW_FNODE",true);
                    node.set("JE_META_BUSFLOW_ID",busNode.getStr("JE_META_BUSFLOW_ID"));
                    node.set("JE_META_BUSFLOW_NODE_ID",nodeId);
                    node.set("FNODE_ZT_CODE","0");
                    node.set("FNODE_ZT_NAME","未执行");
                    node.set("JE_META_BUSFLOW_FNODE_ID", JEUUID.uuid());
                    node.set("MAIN_PKVALUE",mainId);
                    node.set("MAIN_TABLECODE",busFlow.getStr("BUSFLOW_TABLECODE"));
                    isCreate=true;
                }
                node.set("FNODE_NAME",busNode.getStr("NODE_NAME"));
                node.set("FNODE_CODE",busNode.getStr("NODE_CODE"));
                node.set("FNODE_NUM",0);
                node.set("SY_PARENT",parentNode.getStr("JE_META_BUSFLOW_FNODE_ID"));
                node.set("SY_PATH",parentNode.getStr("SY_PATH")+"/"+node.getStr("JE_META_BUSFLOW_FNODE_ID"));
                node.set("SY_NODETYPE",busNode.getStr("SY_NODETYPE"));
                node.set("SY_LAYER",busNode.getStr("SY_LAYER"));
                node.set("SY_DISABLED",busNode.getStr("SY_DISABLED"));
                node.set("SY_TREEORDERINDEX",busNode.getStr("SY_TREEORDERINDEX"));
                if(busNode.getStr("NODE_TABLECODE","").equals(busFlow.getStr("BUSFLOW_TABLECODE"))){
                    node.set("FNODE_PKVALUE",mainId);
                }
                String name="";
                if(StringUtil.isNotEmpty(busFlow.getStr("BUSFLOW_NAME_CFG"))){
                    name=StringUtil.parseKeyWordWithObject(busFlow.getStr("BUSFLOW_NAME_CFG"),ddSet);
                }
                String code="";
                if(StringUtil.isNotEmpty(busFlow.getStr("BUSFLOW_CODE_CFG"))){
                    code=StringUtil.parseKeyWordWithObject(busFlow.getStr("BUSFLOW_CODE_CFG"),ddSet);
                }
                String remark="";
                if(StringUtil.isNotEmpty(busFlow.getStr("BUSFLOW_BZ_CFG"))){
                    remark=StringUtil.parseKeyWordWithObject(busFlow.getStr("BUSFLOW_BZ_CFG"),ddSet);
                }
                node.set("MAIN_CODE",code);
                node.set("MAIN_NAME",name);
                node.set("FNODE_BZ",remark);
                commonService.buildModelCreateInfo(node);
                if(isCreate){
                    node=metaService.insertAndReturn(node);
                }else{
                    node=metaService.updateAndReturn(node);
                }
                doCascadeInitNode(node,busNodes,haveNodeInfos,busFlow,mainId,ddSet);
            }
        }
    }
    @Override
    public void doRenewFlowNodes(String busFlowCode, String mainId) {
        DynaBean busFlow=getBusFlow(busFlowCode, "0");
        List<DynaBean> busNodes=metaService.select("JE_META_BUSFLOW_FNODE_V", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_ID",
                busFlow.getStr("JE_META_BUSFLOW_ID")).eq("MAIN_PKVALUE",mainId).orderByAsc("SY_TREEORDERINDEX"));
        DynaBean mainInfo=new DynaBean();
        if(StringUtil.isNotEmpty(mainId) && StringUtil.isNotEmpty(busFlow.getStr("BUSFLOW_TABLECODE"))){
            String pd=busFlow.getStr("SY_PROJECT_CODE");
            if(StringUtil.isEmpty(pd)){
                pd="meta";
            }
            List<Map<String,Object>> mainDatas=new ArrayList<>();
            if("meta".equals(pd)){
                mainDatas=metaService.selectSql("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }else {
                MetaBusService metaBusService = RpcSchemaFactory.getRemoteProvierClazz(pd, "metaBusService", MetaBusService.class);
                mainDatas = metaBusService.selectMap("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }
            if (mainDatas != null && mainDatas.size() > 0) {
                mainInfo = new DynaBean();
                Map<String,Object> infos=mainDatas.get(0);
                for(String field:infos.keySet()){
                    if(infos.get(field)==null || BeanService.KEY_TABLE_CODE.equals(field)){
                        continue;
                    }
                    mainInfo.set(field,infos.get(field));
                }
            }
        }
        logger.info("1.重新结算节点信息，mainId："+mainId+"，主数据信息："+ JSONUtil.toJsonStr(mainInfo.getValues()));
        Set<Map.Entry<String,Object>> ddSet = new HashSet<>();
        ddSet.addAll(mainInfo.getValues().entrySet());
        for(DynaBean busNode:busNodes){
            JSONObject infoObj=doJsNode(busFlow,busNode,ddSet);
            doRenewNode(busNode,infoObj);
        }
    }

    @Override
    public void doRenewNode(String busFlowCode, String mainId, String nodeCode) {
        DynaBean busFlow=getBusFlow(busFlowCode, "0");
        DynaBean busNode=metaService.selectOne("JE_META_BUSFLOW_FNODE_V", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_ID",
                busFlow.getStr("JE_META_BUSFLOW_ID")).eq("NODE_CODE",nodeCode).eq("MAIN_PKVALUE",mainId));
        DynaBean mainInfo=new DynaBean();
        if(StringUtil.isNotEmpty(mainId) && StringUtil.isNotEmpty(busFlow.getStr("BUSFLOW_TABLECODE"))){
            String pd=busFlow.getStr("SY_PROJECT_CODE");
            if(StringUtil.isEmpty(pd)){
                pd="meta";
            }
            List<Map<String,Object>> mainDatas=new ArrayList<>();
            if("meta".equals(pd)){
                mainDatas=metaService.selectSql("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }else {
                MetaBusService metaBusService = RpcSchemaFactory.getRemoteProvierClazz(pd, "metaBusService", MetaBusService.class);
                mainDatas = metaBusService.selectMap("SELECT * FROM " + busFlow.getStr("BUSFLOW_TABLECODE") + " WHERE " + busFlow.getStr("BUSFLOW_PKCODE") + "={0}", mainId);
            }
            if(mainDatas!=null && mainDatas.size()>0){
                mainInfo=new DynaBean();
                Map<String,Object> infos=mainDatas.get(0);
                for(String field:infos.keySet()){
                    if(infos.get(field)==null || BeanService.KEY_TABLE_CODE.equals(field)){
                        continue;
                    }
                    mainInfo.set(field,infos.get(field));
                }
            }
        }
        Set<Map.Entry<String,Object>> ddSet = new HashSet<>();
        ddSet.addAll(mainInfo.getValues().entrySet());
        JSONObject infoObj=doJsNode(busFlow,busNode,ddSet);
        doRenewNode(busNode,infoObj);
    }
    private JSONObject doJsNode(DynaBean busFlow,DynaBean node,Set<Map.Entry<String,Object>> ddSet){
        JSONObject infoObj=new JSONObject();
        String jsTableCode=node.getStr("JS_TABLECODE");
        String jsWhereSql=node.getStr("JS_JQUERY","");
        if(StringUtil.isNotEmpty(jsWhereSql)){
            jsWhereSql=StringUtil.parseKeyWordWithObject(jsWhereSql,ddSet);
        }
        logger.info("2.开始计算节点["+node.getStr("NODE_NAME")+"]["+node.getStr("NODE_CODE")+"]，计算表名："+jsTableCode+"，计算条件："+ jsWhereSql);
        List<DynaBean> datas=new ArrayList<>();
        String pkValue="";
        if(StringUtil.isNotEmpty(jsTableCode)){
            String pd=busFlow.getStr("SY_PROJECT_CODE");
            if(StringUtil.isEmpty(pd)){
                pd="meta";
            }
            if(StringUtil.isNotEmpty(node.getStr("SY_PROJECT_CODE"))){
                pd=node.getStr("SY_PROJECT_CODE");
            }
            String pkCode=beanService.getPKeyFieldNamesByTableCode(jsTableCode);
            List<Map<String,Object>> jsDatas=new ArrayList<>();
            if("meta".equals(pd)){
                jsDatas=metaService.selectSql("SELECT * FROM "+jsTableCode+" WHERE 1=1 "+jsWhereSql);
            }else{
                MetaBusService metaBusService = RpcSchemaFactory.getRemoteProvierClazz(pd, "metaBusService", MetaBusService.class);
                jsDatas=metaBusService.selectMap("SELECT * FROM "+jsTableCode+" WHERE 1=1 "+jsWhereSql);
            }
            if(jsDatas!=null && jsDatas.size()>0){
                for(Map<String,Object> data:jsDatas) {
                    DynaBean info=new DynaBean();
                    for (String field : data.keySet()) {
                        if(data.get(field)==null || BeanService.KEY_TABLE_CODE.equals(field)){
                            continue;
                        }
                        info.set(field, data.get(field));
                    }
                    if(StringUtil.isNotEmpty(pkCode)){
                        pkValue=info.getStr(pkCode);
                    }
                    datas.add(info);
                }
            }
        }
        int num=datas.size();
        String ztCode="0";
        String ztName="未执行";
        if(datas.size()>0){
//            ztCode="2";
//            ztName="执行中";
            ztCode="1";
            ztName="已完成";
        }
        String jsConfigStr=node.getStr("JS_CONFIGINFO");
        logger.info("3.计算节点["+node.getStr("NODE_NAME")+"]["+node.getStr("NODE_CODE")+"]，获取数据："+num+"+条，计算规则："+ jsConfigStr);
        if(StringUtil.isNotEmpty(jsConfigStr)){
            JSONArray jsConfigs=JSONArray.parseArray(jsConfigStr);
            if(jsConfigs.size()>0){
                for(int i=0;i<jsConfigs.size();i++){
                    JSONObject jsConfig=jsConfigs.getJSONObject(i);
                    String jsName=jsConfig.getString("jsName");
                    String jsType=jsConfig.getString("jsType");
                    String fieldCode=jsConfig.getString("fieldCode");
                    String type=jsConfig.getString("type");
                    String dataType=jsConfig.getString("dataType");
                    String value=jsConfig.getString("value");
                    String resZtCode=jsConfig.getString("resZtCode");
                    String resZtName=jsConfig.getString("resZtName");
                    if(StringUtil.isNotEmpty(value)){
                        value=StringUtil.parseKeyWordWithObject(value,ddSet);
                    }
                    //数据小于等于0
                    if("00".equals(jsType)){
                        if(datas.size()<=0){
                            ztCode=resZtCode;
                            ztName=resZtName;
                        }
                    //数据大于0
                    }else if("01".equals(jsType)){
                        if(datas.size()>0){
                            ztCode=resZtCode;
                            ztName=resZtName;
                        }
                    //单条匹配成立 || 全部匹配成立
                    }else if("02".equals(jsType) || "03".equals(jsType)){
                        Boolean allFlag=true;
                        Boolean oneFlag=false;
                        for(int j=0;j<datas.size();j++){
                            DynaBean dynaBean=datas.get(j);
                            if(StringUtil.isEmpty(fieldCode)){
                                continue;
                            }
                            if(StringUtil.isEmpty(dynaBean.getStr(fieldCode))){
                                if("notEmpty".equals(type)){
                                   allFlag=false;
                                }
                                if("empty".equals(type)){
                                    oneFlag=true;
                                }
                            }
                            if("NUMBER".equals(dataType)){
                                int dVal=dynaBean.getInt(fieldCode);
                                int tVal=0;
                                if(StringUtil.isNotEmpty(value)){
                                    tVal=Integer.parseInt(value);
                                }
                                if("==".equals(type)){
                                    if(dVal!=tVal){
                                        allFlag=false;
                                        break;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if(">".equals(type)){
                                    if(dVal<=tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if(">=".equals(type)){
                                    if(dVal<tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if("<".equals(type)){
                                    if(dVal>=tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if("<=".equals(type)){
                                    if(dVal>tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }
                            }else if(ArrayUtils.contains(new String[]{"FLOAT","FLOAT2"},dataType)){
                                double dVal=dynaBean.getDouble(fieldCode);
                                double tVal=0;
                                if(StringUtil.isNotEmpty(value)){
                                    tVal=Double.parseDouble(value);
                                }
                                if("==".equals(type)){
                                    if(dVal!=tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if(">".equals(type)){
                                    if(dVal<=tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if(">=".equals(type)){
                                    if(dVal<tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if("<".equals(type)){
                                    if(dVal>=tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if("<=".equals(type)){
                                    if(dVal>tVal){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }
                            }else{
                                String dVal=dynaBean.getStr(fieldCode,"");
                                String tVal=value;
                                if("==".equals(type)){
                                    if(!dVal.equals(tVal)){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if(">".equals(type)){
                                    if(dVal.compareTo(tVal)<=0){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if(">=".equals(type)){
                                    if(dVal.compareTo(tVal)<0){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if("<".equals(type)){
                                    if(dVal.compareTo(tVal)>=0){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if("<=".equals(type)){
                                    if(dVal.compareTo(tVal)>0){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }else if("in".equals(type)){
                                    String[] tVals=tVal.split(",");
                                    String[] dVals=dVal.split(",");
                                    boolean have=false;
                                    for(String tV:tVals){
                                        if(StringUtil.isNotEmpty(tV) && ArrayUtils.contains(dVals,tV)){
                                            have=true;
                                            break;
                                        }
                                    }
                                    if(!have){
                                        allFlag=false;
                                    }else{
                                        oneFlag=true;
                                    }
                                }
                            }
                        }
                        if("02".equals(jsType) && oneFlag){
                            ztCode=resZtCode;
                            ztName=resZtName;
                        }else if("03".equals(jsType) && allFlag){
                            ztCode=resZtCode;
                            ztName=resZtName;
                        }
                    }
                }
            }
        }
        infoObj.put("ztCode",ztCode);
        infoObj.put("ztName",ztName);
        infoObj.put("num",num);
        infoObj.put("pkValue",pkValue);
        infoObj.put("user",node.getStr("FNODE_USER_NAME"));
        infoObj.put("userId",node.getStr("FNODE_USER_NAME"));
        RealOrganizationUser currentUser=SecurityUserHolder.getCurrentAccountRealUser();
        if(currentUser!=null){
            infoObj.put("user",currentUser.getName());
            infoObj.put("userId",currentUser.getId());
        }
        if("2".equals(ztCode)){//执行中
            infoObj.put("startTime",DateUtils.formatDateTime(new Date()));
        }else if("1".equals(ztCode)){//执行完毕
            infoObj.put("endTime",DateUtils.formatDateTime(new Date()));
        }
        logger.info("4.计算节点["+node.getStr("NODE_NAME")+"]["+node.getStr("NODE_CODE")+"]，结果："+infoObj.toString());
        return infoObj;
    }
    @Override
    public void doRenewNodeInfo(String busFlowCode, String mainId, String nodeCode, JSONObject infoObj) {
        DynaBean busFlow=getBusFlow(busFlowCode, "0");
        DynaBean busNode=metaService.selectOne("JE_META_BUSFLOW_FNODE", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_ID",
                busFlow.getStr("JE_META_BUSFLOW_ID")).eq("NODE_CODE",nodeCode).eq("MAIN_PKVALUE",mainId));
        doRenewNode(busNode,infoObj);
    }
    private void doRenewNode(DynaBean node, JSONObject infoObj){
        //状态
        node.set("FNODE_ZT_CODE",infoObj.getString("ztCode"));
        node.set("FNODE_ZT_NAME",infoObj.getString("ztName"));
        //数值
        if(infoObj.containsKey("num") && StringUtil.isNotEmpty(infoObj.getString("num"))){
            node.set("FNODE_NUM", infoObj.getIntValue("num"));//数值
        }
        //用户名
        if(infoObj.containsKey("user") && StringUtil.isNotEmpty(infoObj.getString("user"))){
            node.set("FNODE_USER_NAME",infoObj.getString("user"));
        }
        //用户ID
        if(infoObj.containsKey("userId") && StringUtil.isNotEmpty(infoObj.getString("userId"))){
            node.set("FNODE_USER_ID",infoObj.getString("userId"));
        }
        //开始时间
        if(infoObj.containsKey("startTime") && StringUtil.isNotEmpty(infoObj.getString("startTime"))){
            node.set("FNODE_STARTTIME",infoObj.getString("startTime"));
        }
        //结束时间
        if(infoObj.containsKey("endTime") && StringUtil.isNotEmpty(infoObj.getString("endTime"))){
            node.set("FNODE_ENDTIME",infoObj.getString("endTime"));
        }
        //意见
        if(infoObj.containsKey("comment") && StringUtil.isNotEmpty(infoObj.getString("comment"))){
            node.set("FNODE_COMMENT",infoObj.getString("comment"));
        }
        //PKVALUE
        if(infoObj.containsKey("pkValue") && StringUtil.isNotEmpty(infoObj.getString("pkValue"))){
            node.set("FNODE_PKVALUE",infoObj.getString("pkValue"));
        }
        if(StringUtil.isNotEmpty(node.getStr("FNODE_STARTTIME")) && StringUtil.isNotEmpty(node.getStr("FNODE_ENDTIME"))){
            Date startTime= DateUtils.getDate(node.getStr("FNODE_STARTTIME"),DateUtils.DAFAULT_DATETIME_FORMAT);
            if(startTime==null){
                startTime=DateUtils.getDate(node.getStr("FNODE_STARTTIME"),DateUtils.DAFAULT_DATE_FORMAT);
            }
            Date endTime= DateUtils.getDate(node.getStr("FNODE_ENDTIME"),DateUtils.DAFAULT_DATETIME_FORMAT);
            if(endTime==null){
                endTime=DateUtils.getDate(node.getStr("FNODE_ENDTIME"),DateUtils.DAFAULT_DATE_FORMAT);
            }
            if(startTime!=null && endTime!=null){
                node.set("FNODE_HS",endTime.getTime()-startTime.getTime());
                node.set("FNODE_HS_ZH",DateUtils.getTimeDescByMS(endTime.getTime()-startTime.getTime()));
            }
        }
        node.set(BeanService.KEY_TABLE_CODE,"JE_META_BUSFLOW_FNODE");
        metaService.update(node);
    }
}
