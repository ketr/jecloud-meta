/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.setting;

import com.je.common.base.DynaBean;
import com.je.meta.setting.SettingException;
import com.je.meta.setting.SystemSetting;

import java.io.InputStream;
import java.util.List;

public interface SytemSettingPlanService {

    /**
     *  查看所有方案
     */
    List<DynaBean> loadSettingPlans();

    /**
     *  修改保存方案
     */
    void saveSettingPlan(String settingPlanCode, String setplanId, DynaBean values) throws SettingException;

    /**
     *  查看某个方案信息
     */
    SystemSetting loadSettingPlanInfo(String setplanId, String settingPlanCode);

    /**
     *  新添方案
     */
    void insertSettingPlan(String settingPlanCode, DynaBean dynaBean) throws SettingException;

    /**
     *  删除某个方案信息
     */
    void deleteSettingPlan(String setplanId);

    DynaBean loadPlanAndLoginConfig(String code);

    InputStream getPicture(String type,String code);
}
