/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.view;

import com.je.common.base.expression.SpringElPermissionParserContext;
import com.je.meta.model.view.ViewColumn;
import com.je.meta.service.table.Entry;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.List;

public interface BuildAssociationDDLService {

    String createDdl(String template, List<ViewColumn> associationList);


     static final ExpressionParser PARSER = new SpelExpressionParser();
     static final ParserContext PARSER_CONTEXT = new SpringElPermissionParserContext();

     default String formatTemplate(String permissionTemplate, Entry... params) {
        EvaluationContext context = new StandardEvaluationContext();
        for (Entry eachEntry : params) {
            context.setVariable(eachEntry.getKey(), eachEntry.getValue());
        }
        return PARSER.parseExpression(permissionTemplate, PARSER_CONTEXT).getValue(context, String.class);
    }

}
