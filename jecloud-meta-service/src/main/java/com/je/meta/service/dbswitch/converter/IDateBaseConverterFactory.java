/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dbswitch.converter;

import com.je.meta.model.database.type.ProductTypeEnum;

import java.util.HashMap;
import java.util.Map;

public class IDateBaseConverterFactory {

    private static final Map<ProductTypeEnum, AbstractIDateBaseConverter> DATABASE_CONVERTER
            = new HashMap<ProductTypeEnum, AbstractIDateBaseConverter>();

    static {
        DATABASE_CONVERTER.put(ProductTypeEnum.MYSQL, new DatabaseMysqlConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.MARIADB, new DatabaseMariaDBConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.ORACLE, new DatabaseOracleConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.SQLSERVER2000, new DatabaseSqlserver2000Converter());

        DATABASE_CONVERTER.put(ProductTypeEnum.SQLSERVER, new DatabaseSqlserverConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.POSTGRESQL, new DatabasePostgresConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.GREENPLUM, new DatabaseGreenplumConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.DB2, new DatabaseDB2Converter());

        DATABASE_CONVERTER.put(ProductTypeEnum.DM, new DatabaseDmConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.KINGBASE, new DatabaseKingbaseConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.OSCAR, new DatabaseOscarConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.GBASE8A, new DatabaseGbase8aConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.HIVE, new DatabaseHiveConverter());

        DATABASE_CONVERTER.put(ProductTypeEnum.SQLITE3, new DatabaseSqliteConverter());
    }


    public static AbstractIDateBaseConverter getConverterByDBType(ProductTypeEnum type) {
        return DATABASE_CONVERTER.get(type);
    }


}

