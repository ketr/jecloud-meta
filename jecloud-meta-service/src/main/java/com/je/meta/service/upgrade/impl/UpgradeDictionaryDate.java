package com.je.meta.service.upgrade.impl;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 打包字典
 */
@Service("upgradeDictionaryDate")
public class UpgradeDictionaryDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeDictionaryDate() {
        super(UpgradeResourcesEnum.DICTIONARY);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> dictionaryIdList = getSourceIds(upgradeBean);
        if (dictionaryIdList == null || dictionaryIdList.size() == 0) {
            return;
        }
        packageResult.setProductDictionarys(packageDictionary(dictionaryIdList));
    }

    private List<DynaBean> packageDictionary(List<String> dictionaryIds) {
        List<DynaBean> dictionaryBeanList = metaService.select("JE_CORE_DICTIONARY", ConditionsWrapper.builder()
                .in("JE_CORE_DICTIONARY_ID", dictionaryIds));
        List<DynaBean> dictionaryItems = metaService.select("JE_CORE_DICTIONARYITEM", ConditionsWrapper.builder()
                .in("DICTIONARYITEM_DICTIONARY_ID", dictionaryIds).orderByAsc("SY_TREEORDERINDEX,SY_ORDERINDEX"));
        List<DynaBean> itemBeanList;
        for (DynaBean eachDictionaryBean : dictionaryBeanList) {
            itemBeanList = new ArrayList<>();
            for (DynaBean eachDictionaryItemBean : dictionaryItems) {
                if (eachDictionaryBean.getStr("JE_CORE_DICTIONARY_ID").equals(eachDictionaryItemBean.getStr("DICTIONARYITEM_DICTIONARY_ID"))) {
                    itemBeanList.add(eachDictionaryItemBean);
                }
            }
            eachDictionaryBean.set("items", itemBeanList);
        }
        return dictionaryBeanList;
    }

    @Override
    public void installUpgradeMetaDate(String upgradeInstallId, List<DynaBean> list, String productCode, Map<String, Object> customerParameter) {
        for (DynaBean dictionary : list) {
            String dictionaryId = dictionary.getStr("JE_CORE_DICTIONARY_ID");
            List<DynaBean> dictionaryList = metaService.select("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("JE_CORE_DICTIONARY_ID", dictionaryId));
            dictionary.table("JE_CORE_DICTIONARY");

            List<DynaBean> itemsDynaBean = new ArrayList<>();
            if (dictionaryList != null && dictionaryList.size() > 0) {
                commonService.buildModelModifyInfo(dictionary);
                metaService.update(dictionary);
                List<String> currentItemIds = getCurrentItemIds(dictionaryId);
                List<JSONObject> items = (List<JSONObject>) dictionary.get("items");
                for (JSONObject jsonObject : items) {
                    DynaBean dynaBean = new DynaBean();
                    dynaBean.setValues((Map<String, Object>) jsonObject.get("values"));
                    dynaBean.table("JE_CORE_DICTIONARYITEM");
                    String itemId = dynaBean.getStr("JE_CORE_DICTIONARYITEM_ID");
                    ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
                    conditionsWrapper.eq("JE_CORE_DICTIONARYITEM_ID", itemId);
                    List<DynaBean> itemsDynabean = metaService.select("JE_CORE_DICTIONARYITEM", conditionsWrapper);
                    if (itemsDynabean != null && itemsDynabean.size() > 0) {
                        commonService.buildModelModifyInfo(dynaBean);
                        metaService.update(dynaBean, ConditionsWrapper.builder().eq("JE_CORE_DICTIONARYITEM_ID", itemId));
                        currentItemIds.remove(itemId);
                    } else {
                        commonService.buildModelCreateInfo(dynaBean);
                        itemsDynaBean.add(dynaBean);
                    }
                }
                if (currentItemIds.size() > 0) {
                    metaService.delete("JE_CORE_DICTIONARYITEM", ConditionsWrapper.builder().in("JE_CORE_DICTIONARYITEM_ID", currentItemIds));
                }
            } else {
                commonService.buildModelCreateInfo(dictionary);
                metaService.insert(dictionary);
                List<JSONObject> items = (List<JSONObject>) dictionary.get("items");
                if (items != null && items.size() > 0) {
                    for (JSONObject jsonObject : items) {
                        DynaBean dynaBean = new DynaBean();
                        dynaBean.setValues((Map<String, Object>) jsonObject.get("values"));
                        dynaBean.table("JE_CORE_DICTIONARYITEM");
                        commonService.buildModelCreateInfo(dynaBean);
                        itemsDynaBean.add(dynaBean);
                    }
                }
            }
            if (itemsDynaBean.size() > 0) {
                metaService.insertBatch(itemsDynaBean);
            }
            //记录升级日志
            upgradelogRpcService.saveUpgradelog(upgradeInstallId,
                    UpgradeResourcesEnum.DICTIONARY.getType(), UpgradeResourcesEnum.DICTIONARY.getName(),
                    dictionary.getStr("DICTIONARY_DDNAME"), dictionary.getStr("DICTIONARY_DDCODE"));

        }
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductDictionarys();
    }

    private List<String> getCurrentItemIds(String dictionaryId) {
        List<DynaBean> list = metaService.select("JE_CORE_DICTIONARYITEM", ConditionsWrapper.builder()
                .eq("DICTIONARYITEM_DICTIONARY_ID", dictionaryId).ne("SY_NODETYPE", "ROOT"));
        List<String> ids = new ArrayList<>();
        for (DynaBean dynaBean : list) {
            ids.add(dynaBean.getStr("JE_CORE_DICTIONARYITEM_ID"));
        }

        return ids;
    }

}
