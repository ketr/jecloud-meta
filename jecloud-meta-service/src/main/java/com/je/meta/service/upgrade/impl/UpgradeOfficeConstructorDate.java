package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageFile;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.upgrade.InstallResourceDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 打包文档构造器
 */
@Service("upgradeOfficeConstructorDate")
public class UpgradeOfficeConstructorDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeOfficeConstructorDate() {
        super(UpgradeResourcesEnum.OFFICE_CONSTRUCTOR);
    }

    //子数据 导入SHEET明细
    private static final String SHEET_CHILD_CODE = "sheetChild";
    //子数据 数据字段
    private static final String FIELD_CHILD_CODE = "fieldChild";

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> list = getSourceIds(upgradeBean);
        if (list == null || list.size() == 0) {
            return;
        }
        packageResult.setProductOfficeConstructor(packageResources(packageResult, list));
    }

    @Override
    public void installUpgradeMetaDate(String upgradeInstallId, List<DynaBean> list, String productCode, Map<String, Object> customerParameter) {
        //主数据
        installResources(buildAbstractDto(upgradeInstallId, list, productCode));
        //子数据 导入SHEET明细
        List<DynaBean> allSheetList = getChildList(list, SHEET_CHILD_CODE);
        installResources(new InstallResourceDTO.Builder()
                .upgradeInstallId(upgradeInstallId)
                .list(allSheetList)
                .productCode(productCode)
                .identifier("JE_CORE_EXCELSHEET_ID")
                .tableCode("JE_CORE_EXCELSHEET")
                .logAdd(false)
                .build());
        //子数据 数据字段
        List<DynaBean> allFieldList = getChildList(allSheetList, FIELD_CHILD_CODE);
        installResources(new InstallResourceDTO.Builder()
                .upgradeInstallId(upgradeInstallId)
                .list(allFieldList)
                .productCode(productCode)
                .identifier("JE_CORE_EXCELFIELD_ID")
                .tableCode("JE_CORE_EXCELFIELD")
                .logAdd(false)
                .build());
    }

    @Override
    public List<DynaBean> packageResources(PackageResult packageResult, List<String> resourceIds) {
        List<DynaBean> beanList = metaService.select(upgradeResourcesEnum.getTableCode(), ConditionsWrapper.builder()
                .in(upgradeResourcesEnum.getContentCode(), resourceIds));
        addSheetInfo(beanList, resourceIds);
        List<PackageFile> packageFileList = getFiles(beanList);
        if (packageFileList.size() > 0) {
            packageResult.getFiles().addAll(packageFileList);
        }
        return beanList;
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductOfficeConstructor();
    }

    //添加sheet页明细
    public void addSheetInfo(List<DynaBean> beanList, List<String> resourceIds) {
        List<DynaBean> list = metaService.select("JE_CORE_EXCELSHEET",
                ConditionsWrapper.builder().in("JE_CORE_EXCELGROUP_ID", resourceIds));
        Map<String, List<DynaBean>> childSheetInfoList = new HashMap<>();

        //sheetIds
        List<String> sheetIds = new ArrayList<>();
        for (DynaBean dynaBean : list) {
            sheetIds.add(dynaBean.getPkValue());
        }

        Map<String, List<DynaBean>> excelFieldList = getFieldInfo(sheetIds);

        for (DynaBean dynaBean : list) {
            if (excelFieldList.get(dynaBean.getPkValue()) != null) {
                dynaBean.set(FIELD_CHILD_CODE, excelFieldList.get(dynaBean.getPkValue()));
            }
            String groupId = dynaBean.getStr("JE_CORE_EXCELGROUP_ID");
            if (childSheetInfoList.get(groupId) == null) {
                childSheetInfoList.put(groupId, new ArrayList<>());
            }
            childSheetInfoList.get(groupId).add(dynaBean);
        }

        for (DynaBean dynaBean : beanList) {
            String key = dynaBean.getPkValue();
            dynaBean.put(SHEET_CHILD_CODE, childSheetInfoList.get(key));
        }

    }

    //添加字段配置
    public Map<String, List<DynaBean>> getFieldInfo(List<String> sheetIds) {
        Map<String, List<DynaBean>> map = new HashMap<>();
        List<DynaBean> excelFields = metaService.select("JE_CORE_EXCELFIELD",
                ConditionsWrapper.builder().in("JE_CORE_EXCELSHEET_ID", sheetIds));
        for (DynaBean dynaBean : excelFields) {
            String sheetId = dynaBean.getStr("JE_CORE_EXCELSHEET_ID");
            if (map.get(sheetId) == null) {
                map.put(sheetId, new ArrayList<>());
            }
            map.get(sheetId).add(dynaBean);
        }
        return map;
    }

}
