/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.func.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.Enable;
import com.je.common.base.constants.ExtFieldType;
import com.je.common.base.constants.db.DbFieldType;
import com.je.common.base.db.DbFieldVo;
import com.je.common.base.db.FieldChineseName;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.db.JoinColumnName;
import com.je.common.base.entity.BaseEntity;
import com.je.common.base.entity.TreeBaseEntity;
import com.je.common.base.entity.extjs.Model;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.table.service.PCDataService;
import com.je.common.base.util.*;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.MetaFuncRelyonService;
import com.je.meta.service.table.func.MetaResColumnService;
import com.je.meta.service.variable.MetaSysVariablesService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.Entity;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.*;

@Service
public class MetaResColumnServiceImpl implements MetaResColumnService {

    @Autowired
    private MetaService metaService;
    @Lazy
    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaSysVariablesService sysVariablesService;
    @Autowired
    private PCDataService pcDataService;
    @Lazy
    @Autowired
    private MetaFuncRelyonService funcRelyonService;

    @Override
    public void buildDefault(DynaBean dynaBean) {
        String xtype = dynaBean.getStr("RESOURCECOLUMN_XTYPE");
        if ("morecolumn".equalsIgnoreCase(xtype) || "actioncolumn".equalsIgnoreCase(xtype) || "displayfield".equals(xtype)) {
            String orderSql = " order by (CONVERT(int,replace(RESOURCECOLUMN_CODE,'" + xtype + "_',''))) ASC ";
            if (ConstantVars.STR_ORACLE.equals(JEDatabase.getCurrentDatabase())
                    || ConstantVars.STR_SHENTONG.equals(JEDatabase.getCurrentDatabase())
                    || ConstantVars.STR_KINGBASEES.equals(JEDatabase.getCurrentDatabase())
                    || ConstantVars.STR_DM.equals(JEDatabase.getCurrentDatabase())) {
                orderSql = " order by (to_number(replace(RESOURCECOLUMN_CODE,'" + xtype + "_',''))) ASC ";
            } else if (ConstantVars.STR_MYSQL.equals(JEDatabase.getCurrentDatabase())
                    || ConstantVars.STR_TIDB.equals(JEDatabase.getCurrentDatabase())) {
                orderSql = " order by (convert(replace(RESOURCECOLUMN_CODE,'" + xtype + "_',''),SIGNED)) ASC ";
            }

            List<DynaBean> columns = metaService.select("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder().apply(" AND RESOURCECOLUMN_FUNCINFO_ID='" + dynaBean.getStr("RESOURCECOLUMN_FUNCINFO_ID") + "' AND RESOURCECOLUMN_XTYPE='" + xtype + "'" + orderSql));
            Integer index = 1;
            if (columns.size() > 0) {
                for (Integer i = 0; i < columns.size(); i++) {
                    if (!(xtype + "_" + (i + 1)).equals(columns.get(i).getStr("RESOURCECOLUMN_CODE"))) {
                        index = i + 1;
                        break;
                    }
                }
                if (index == 1 && (xtype + "_" + columns.size()).equals(columns.get(columns.size() - 1).getStr("RESOURCECOLUMN_CODE"))) {
                    index = columns.size() + 1;
                }
            }
            dynaBean.set("RESOURCECOLUMN_CODE", xtype + "_" + index);
            dynaBean.set("RESOURCECOLUMN_NAME", dynaBean.getStr("RESOURCECOLUMN_NAME") + index);
        }
        dynaBean.set("SY_STATUS", Enable.TRUE);
        dynaBean.set("SY_CREATETIME", DateUtils.formatDate(new Date()));
        dynaBean.set("RESOURCECOLUMN_ALLOWEDIT", "0");
        dynaBean.set("SY_ORDERINDEX", 0);
        dynaBean.set("RESOURCECOLUMN_QUICKQUERY", "0");
        dynaBean.set("RESOURCECOLUMN_IFIMPL", "0");
        //设置默认宽度为80
        dynaBean.set("RESOURCECOLUMN_WIDTH", "80");
        //是否拥有转换器conversion
        dynaBean.set("RESOURCECOLUMN_CONVERSION", "0");
        //是否拥有转换器highlighting
        dynaBean.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
        //是否超链接hyperLink
        dynaBean.set("RESOURCECOLUMN_HYPERLINK", "0");
        //是否在字典中取
        dynaBean.set("RESOURCECOLUMN_ISDD", "0");
        dynaBean.set("RESOURCECOLUMN_QUERYTYPE", "no");
        //合并单元格      否
        dynaBean.set("RESOURCECOLUMN_MERGE", "0");
        //锁定列
        dynaBean.set("RESOURCECOLUMN_LOCKED", "");
        //多表头   是
        dynaBean.set("RESOURCECOLUMN_MORECOLUMN", "1");
        //是否隐藏
        dynaBean.set("RESOURCECOLUMN_HIDDEN", "0");
        //是否隐藏
        dynaBean.set("RESOURCECOLUMN_ORDER", "0");
        //是否隐藏
        dynaBean.set("RESOURCECOLUMN_INDEX", "0");
    }

    @Override
    @Transactional
    public void impl(String funId, Class c) throws InstantiationException, IllegalAccessException {
        //查询出已经导入的字段
        String columnSql = "select RESOURCECOLUMN_CODE from JE_CORE_RESOURCECOLUMN where RESOURCECOLUMN_FUNCINFO_ID = '" + funId + "' and RESOURCECOLUMN_IFIMPL = 1";
        List<Map<String, Object>> columnResult = metaService.selectSql(columnSql);
        List<String> coloumnList = Lists.newArrayList();
        columnResult.forEach(map -> {
            coloumnList.add((String) map.get("RESOURCECOLUMN_CODE"));
        });

        String fieldSql = "select RESOURCEFIELD_CODE from JE_CORE_RESOURCEFIELD where RESOURCEFIELD_FUNCINFO_ID = '" + funId + "' and RESOURCEFIELD_IFIMPL = 1";
        List<Map<String, Object>> fieldResult = metaService.selectSql(fieldSql);
        List<String> fieldList = Lists.newArrayList();
        fieldResult.forEach(map -> {
            fieldList.add((String) map.get("RESOURCEFIELD_CODE"));
        });

        //获得主键名称   首字母可能为大写
        String oldIdName = EntityUtils.getInstance().getEntityIdName((BaseEntity) c.newInstance());
        String newIdName = "";
        if (StringUtils.isNotEmpty(oldIdName)) {
            //将主键首字段转化为小写
            newIdName = oldIdName.substring(0, 1).toLowerCase() + oldIdName.substring(1);
        }

        DynaBean funInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", funId);
        //取得当前功能的sysmode  CP、CPKZ或XMKF
        String sysMode = funInfo.getStr("FUNCINFO_SYSMODE");
        //导入数据
        Field[] fields = ReflectionUtils.getInstance().getClassFields(c, false);
        for (Field f : fields) {
            //字段所在的类
            Class clazz = f.getDeclaringClass();
            //判断是否有FieldChineseName注解
            boolean hasAnnotation = f.isAnnotationPresent(FieldChineseName.class);
            //判断是否有JoinColumnName注解
            boolean isJoinColumn = f.isAnnotationPresent(JoinColumnName.class);
            //加有FieldChineseName注解和JoinColumnName注解的
            if (hasAnnotation || isJoinColumn) {
                String fieldName = "";
                if (isJoinColumn) {
                    //实体的字段名字取的是属性上注解的值
                    fieldName = f.getAnnotation(JoinColumnName.class).value();
                } else {
                    fieldName = f.getName();
                }
                if (!coloumnList.contains(fieldName)) {
                    DynaBean rc = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
                    rc.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCECOLUMN_ID");
                    //有JoinColumnName注解 （是外键）
                    if (isJoinColumn) {
                        rc.set("RESOURCECOLUMN_CODE", f.getAnnotation(JoinColumnName.class).value());
                        rc.set("RESOURCECOLUMN_NAME", f.getAnnotation(JoinColumnName.class).descr());
                        rc.set("RESOURCECOLUMN_WIDTH", "");
                        rc.set("RESOURCECOLUMN_XTYPE", "");
                        rc.set("RESOURCECOLUMN_DESCRIPTION", "");
                    } else {
                        rc.set("RESOURCECOLUMN_CODE", f.getName());
                        rc.set("RESOURCECOLUMN_NAME", f.getAnnotation(FieldChineseName.class).value());
                        rc.set("RESOURCECOLUMN_WIDTH", f.getAnnotation(FieldChineseName.class).width());
                        rc.set("RESOURCECOLUMN_XTYPE", f.getAnnotation(FieldChineseName.class).xtype());
                        rc.set("RESOURCECOLUMN_DESCRIPTION", f.getAnnotation(FieldChineseName.class).descr());
                    }
                    //将导入的字段标识为true
                    rc.set("RESOURCECOLUMN_IFIMPL", "1");
                    //是否快速查询 false
                    rc.set("RESOURCECOLUMN_QUICKQUERY", "0");
                    //是否可以为空  true
                    rc.set("RESOURCECOLUMN_ALLOWBLANK", "1");
                    //默认序号为0
                    rc.set("SY_ORDERINDEX", 0);
                    //设置默认宽度为80
                    rc.set("RESOURCECOLUMN_WIDTH", "80");
                    //是否拥有转换器conversion
                    rc.set("RESOURCECOLUMN_CONVERSION", "0");
                    //是否拥有转换器highlighting
                    rc.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
                    //是否超链接hyperLink
                    rc.set("RESOURCECOLUMN_HYPERLINK", "0");
                    //是否在字典中取
                    rc.set("RESOURCECOLUMN_ISDD", "0");
                    //合并单元格      否
                    rc.set("RESOURCECOLUMN_MERGE", "0");
                    //锁定列  否
                    rc.set("RESOURCECOLUMN_LOCKED", "");
                    //多表头   否
                    rc.set("RESOURCECOLUMN_MORECOLUMN", "0");
                    rc.set("RESOURCECOLUMN_ALIGN", "left");
                    rc.set("RESOURCECOLUMN_ALIGN_TITLE", "left");
                    //多表头   否
                    rc.set("RESOURCECOLUMN_ORDER", "0");
                    rc.set("RESOURCECOLUMN_INDEX", "left");
                    //快速查询
                    rc.set("RESOURCECOLUMN_QUERYTYPE", "no");
                    String hidden = "";
                    if (hasAnnotation) {
                        hidden = f.getAnnotation(FieldChineseName.class).hidden();
                    }

                    //根据当前系统所在阶段（研发/实施/运行）为sysMode设置默认值
                    rc.set("RESOURCECOLUMN_SYSMODE", sysMode);
                    //如果是来自父类的字段或者这个字段是外键 默认设置为隐藏
                    if (BaseEntity.class == clazz || TreeBaseEntity.class == clazz || isJoinColumn) {
                        rc.set("RESOURCECOLUMN_HIDDEN", "1");
                    } else {
                        //根据注解设置是否隐藏
                        if ("1".equals(hidden)) {
                            rc.set("RESOURCECOLUMN_HIDDEN", "1");
                        } else {
                            //没有注解默认为显示
                            rc.set("RESOURCECOLUMN_HIDDEN", "0");
                        }

                    }
                    //是否主键
                    if (oldIdName.equals(f.getName()) || newIdName.equals(f.getName())) {
                        rc.set("RESOURCECOLUMN_ISPK", "1");
                    } else {
                        rc.set("RESOURCECOLUMN_ISPK", "0");
                    }
                    rc.set("RESOURCECOLUMN_ENABLEICON", "0");
                    //字段类型  默认为无  不可编辑
                    rc.set("RESOURCECOLUMN_XTYPE", "no");
                    //列表编辑   默认为否
                    rc.set("RESOURCECOLUMN_ALLOWEDIT", "0");
                    buildModelCreateInfo(rc);
                    metaService.insert(rc);
                }
                //导入列表字段的同时，导入表单字段
                //实体的字段名字取的是属性上注解的值
                if (!fieldList.contains(fieldName)) {
                    DynaBean rf = new DynaBean("JE_CORE_RESOURCEFIELD", false);
                    rf.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
                    //有JoinColumnName注解 （是外键）
                    if (isJoinColumn) {
                        rf.set("RESOURCEFIELD_CODE", f.getAnnotation(JoinColumnName.class).value());
                        rf.set("RESOURCEFIELD_NAME", f.getAnnotation(JoinColumnName.class).descr());
                        rf.set("RESOURCEFIELD_WIDTH", "");
                        rf.set("RESOURCEFIELD_XTYPE", "");
                        rf.set("RESOURCEFIELD_DESCRIPTION", "");
                    } else {
                        rf.set("RESOURCEFIELD_CODE", f.getName());
                        rf.set("RESOURCEFIELD_NAME", f.getAnnotation(FieldChineseName.class).value());
                        rf.set("RESOURCEFIELD_WIDTH", f.getAnnotation(FieldChineseName.class).width());
                        rf.set("RESOURCEFIELD_XTYPE", f.getAnnotation(FieldChineseName.class).xtype());
                        rf.set("RESOURCEFIELD_DESCRIPTION", f.getAnnotation(FieldChineseName.class).descr());
                    }
                    //将导入的字段标识为true
                    rf.set("RESOURCEFIELD_IFIMPL", "1");
                    //是否可用  true
                    rf.set("RESOURCEFIELD_DISABLED", "1");
                    //是否可以为空  true
                    rf.set("RESOURCEFIELD_ALLOWBLANK", "1");
                    if ("SY_CREATEORGID".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        //是否只读
                        rf.set("RESOURCEFIELD_READONLY", "1");
                        rf.set("RESOURCEFIELD_VALUE", "@DEPT_ID@");
                        //字段类型   默认为文本框
                        rf.set("RESOURCEFIELD_XTYPE", "textfield");
                    } else if ("SY_CREATEORGNAME".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        //是否只读
                        rf.set("RESOURCEFIELD_READONLY", "1");
                        rf.set("RESOURCEFIELD_VALUE", "@DEPT_NAME@");
                        //字段类型   默认为文本框
                        rf.set("RESOURCEFIELD_XTYPE", "textfield");
                    } else if ("SY_CREATETIME".equals(rf.get("RESOURCEFIELD_CODE"))) {
                        //是否只读
                        rf.set("RESOURCEFIELD_READONLY", "1");
                        //默认   年月日 时分秒
                        rf.set("RESOURCEFIELD_VALUE", "@NOW_TIME@");
                        //字段类型  “日期时间”
                        rf.set("RESOURCEFIELD_XTYPE", "datefield");
                        //配置信息 为  Y-m-d
                        //rf.set("RESOURCEFIELD_CONFIGINFO","Y-m-d");
                        rf.setStr("RESOURCEFIELD_OTHERCONFIG", getDateOtherconfig("dateTime"));
                    } else if ("SY_CREATEUSERID".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        //是否只读
                        rf.set("RESOURCEFIELD_READONLY", "1");
                        rf.set("RESOURCEFIELD_VALUE", "@USER_ID@");
                        //字段类型   默认为文本框
                        rf.set("RESOURCEFIELD_XTYPE", "textfield");
                    } else if ("SY_CREATEUSERNAME".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        //是否只读
                        rf.set("RESOURCEFIELD_READONLY", "1");
                        rf.set("RESOURCEFIELD_VALUE", "@USER_NAME@");
                        //字段类型   默认为文本框
                        rf.set("RESOURCEFIELD_XTYPE", "textfield");
                    } else if ("SY_AUDFLAG".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        //是否只读
                        rf.set("RESOURCEFIELD_READONLY", "1");
                        rf.set("RESOURCEFIELD_VALUE", "NOSTATUS");
                        //字段类型   默认为文本框
                        rf.set("RESOURCEFIELD_XTYPE", "cbbfield");
                        //字段类型   默认为文本框
                        rf.set("RESOURCEFIELD_CONFIGINFO", "JE_AUDFLAG,SY_AUDFLAG,code,S");
                    } else {
                        //是否只读
                        rf.set("RESOURCEFIELD_READONLY", "0");
                        //字段类型   默认为文本框
                        rf.set("RESOURCEFIELD_XTYPE", "textfield");
                    }
                    if ("SY_WARNFLAG".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        rf.set("RESOURCEFIELD_XTYPE", "cbbfield");
                        rf.set("RESOURCEFIELD_CONFIGINFO", "JE_WARNFLAG");
                    }
                    //默认序号为0
                    rf.set("SY_ORDERINDEX", 0);
                    //所占行数
                    rf.set("RESOURCEFIELD_ROWSPAN", 1);
                    //所占列数
                    rf.set("RESOURCEFIELD_COLSPAN", 1);
                    //可选可编辑   默认否
                    rf.set("RESOURCEFIELD_EDITABLE", "0");
                    String hidden = "";
                    if (hasAnnotation) {
                        hidden = f.getAnnotation(FieldChineseName.class).hidden();
                    }

                    //根据当前系统所在阶段（研发/实施/运行）为sysMode设置默认值
                    rf.set("RESOURCEFIELD_SYSMODE", sysMode);

                    //如果是来自父类的字段或者是外键 默认设置为隐藏
                    if (BaseEntity.class == clazz || TreeBaseEntity.class == clazz || isJoinColumn) {
                        rf.set("RESOURCEFIELD_HIDDEN", "1");
                    } else {
                        //根据注解设置是否隐藏
                        if ("1".equals(hidden)) {
                            rf.set("RESOURCEFIELD_HIDDEN", "1");
                        } else {
                            //没有注解默认为显示
                            rf.set("RESOURCEFIELD_HIDDEN", "0");
                        }
                    }
                    //是否主键
                    if (oldIdName.equals(f.getName()) || newIdName.equals(f.getName())) {
                        rf.set("RESOURCEFIELD_ISPK", "1");
                    } else {
                        rf.set("RESOURCEFIELD_ISPK", "0");
                    }
                    if ("1".equals(rf.getStr("RESOURCEFIELD_HIDDEN"))) {
                        rf.set("RESOURCEFIELD_FIELDLAZY", "0");
                    } else {
                        rf.set("RESOURCEFIELD_FIELDLAZY", "1");
                    }
                    buildModelCreateInfo(rf);
                    metaService.insert(rf);
                }
            }
        }

        //默认给表单添加一个child 用于子功能  有且仅有一个
        DynaBean rField = metaService.selectOne("JE_CORE_RESOURCEFIELD", ConditionsWrapper.builder().eq("RESOURCEFIELD_FUNCINFO_ID", funId).eq("RESOURCEFIELD_IFIMPL", "1"));
        if (rField == null) {
            DynaBean rf = new DynaBean("JE_CORE_RESOURCEFIELD", false);
            rf.set("RESOURCEFIELD_FUNCINFO_ID", funId);
            rf.set("RESOURCEFIELD_XTYPE", "child");
            //将导入的字段标识为true
            rf.set("RESOURCEFIELD_IFIMPL", "1");
            //是否可用  true
            rf.set("RESOURCEFIELD_DISABLED", "1");
            //是否可以为空  true
            rf.set("RESOURCEFIELD_ALLOWBLANK", "1");
            //默认序号为0
            rf.set("SY_ORDERINDEX", 0);
            //所占行数
            rf.set("RESOURCEFIELD_ROWSPAN", 1);
            //所占列数
            rf.set("RESOURCEFIELD_COLSPAN", 1);
            //可选可编辑   默认否
            rf.set("RESOURCEFIELD_EDITABLE", "0");
            rf.set("RESOURCEFIELD_HIDDEN", "1");
            rf.set("RESOURCEFIELD_CODE", "__child");
            rf.set("RESOURCEFIELD_NAME", "子功能");
            rf.set("RESOURCEFIELD_NAME_EN", "Sub Func");
            //根据当前系统所在阶段（研发/实施/运行）为sysMode设置默认值
            rf.set("RESOURCEFIELD_SYSMODE", sysMode);
            metaService.insert(rf);
        }

    }

    private void buildModelCreateInfo(DynaBean model) {
        Date now = new Date();
        String nowDateTime = DateUtils.formatDateTime(now);
        model.setStr("SY_CREATEUSERID", SecurityUserHolder.getCurrentAccountRealUserId());
        model.setStr("SY_CREATEUSERNAME", SecurityUserHolder.getCurrentAccountRealUserName());
        model.setStr("SY_CREATEORGID", SecurityUserHolder.getCurrentAccountRealOrgId());
        model.setStr("SY_CREATEORGNAME", SecurityUserHolder.getCurrentAccountRealOrgName());
        if (StringUtil.isEmpty(model.getStr("SY_CREATETIME"))) {
            model.setStr("SY_CREATETIME", nowDateTime);
        }
    }

    @Override
    @Transactional
    public void implDyanBean(String funId, String tableCode) {
        //查询出已经导入的字段
        String columnSql = "select RESOURCECOLUMN_CODE from JE_CORE_RESOURCECOLUMN where RESOURCECOLUMN_FUNCINFO_ID = '" + funId + "' and RESOURCECOLUMN_IFIMPL = 1";
        List<Map<String, Object>> columnResult = metaService.selectSql(columnSql);
        List<String> columnList = Lists.newArrayList();
        columnResult.forEach(eachMap -> {
            columnList.add((String) eachMap.get("RESOURCECOLUMN_CODE"));
        });

        String fieldSql = "select RESOURCEFIELD_CODE from JE_CORE_RESOURCEFIELD where RESOURCEFIELD_FUNCINFO_ID = '" + funId + "' and RESOURCEFIELD_IFIMPL = 1";
        List<Map<String, Object>> fieldResult = metaService.selectSql(fieldSql);
        List<String> fieldList = Lists.newArrayList();
        fieldResult.forEach(eachMap -> {
            fieldList.add((String) eachMap.get("RESOURCEFIELD_CODE"));
        });

        //资源表中字段
        DynaBean table = metaService.selectOne("JE_CORE_RESOURCETABLE", ConditionsWrapper.builder().eq("RESOURCETABLE_TABLECODE", tableCode).ne("RESOURCETABLE_TYPE", "MODULE"));
        if (table == null) {
            return;
        }

        List<DynaBean> tcList = metaService.select("JE_CORE_TABLECOLUMN", ConditionsWrapper.builder()
                .eq("TABLECOLUMN_RESOURCETABLE_ID", table.getStr("JE_CORE_RESOURCETABLE_ID"))
                .eq("TABLECOLUMN_ISCREATE", "1")
                .orderByAsc("SY_ORDERINDEX"));
        List<DynaBean> indexList = metaService.select("JE_CORE_TABLEINDEX", ConditionsWrapper.builder()
                .eq("TABLEINDEX_RESOURCETABLE_ID", table.getStr("JE_CORE_RESOURCETABLE_ID"))
                .eq("TABLEINDEX_ISCREATE", "1"));
        Set<String> indexCodes = new HashSet<String>();
        for (DynaBean index : indexList) {
            indexCodes.add(index.getStr("TABLEINDEX_FIELDCODE"));
        }
        DynaBean funInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", funId);
        List<String> treeCodes = getTreeTableCodes(funInfo);
        //取得功能的sysmode  CP、CPKZ或XMKF
        String sysMode = funInfo.getStr("FUNCINFO_SYSMODE");
        List<String> lazyLoadSqls = new ArrayList<String>();
        int order = 1;
        int fieldOrder = 1;
        /**
         * 创建默认列表方案
         */
        String cPlanId = "";
        DynaBean dbcPlan = metaService.selectOne("JE_CORE_RESOURCECOLUMN_PLAN", ConditionsWrapper.builder()
                .selectColumns("JE_CORE_RESOURCECOLUMN_PLAN_ID")
                .eq("PLAN_FUNCINFO_ID", funId)
                .eq("PLAN_TYPE", "1"));

        if (dbcPlan == null) {
            DynaBean cPlan = new DynaBean("JE_CORE_RESOURCECOLUMN_PLAN", false);
            cPlanId = JEUUID.uuid();
            cPlan.setStr("JE_CORE_RESOURCECOLUMN_PLAN_ID", cPlanId);
            cPlan.setStr("PLAN_NAME", "系统方案");
            cPlan.setStr("PLAN_FUNCINFO_ID", funId);
            cPlan.setStr("PLAN_DEFAULT", "1");
            cPlan.setStr("PLAN_CODE", "DEFAULT");
            cPlan.setStr("PLAN_TYPE", "1");
            cPlan.setStr("PLAN_SY", "1");
            cPlan.setStr("SY_STATUS", "1");
            cPlan.setStr("SY_ORDERINDEX", "1");
            commonService.buildModelCreateInfo(cPlan);
            metaService.insert(cPlan);
        } else {
            cPlanId = dbcPlan.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID");
        }

        /**
         * 创建默认表单方案
         */
        String fPlanId = "";
        DynaBean dbfPlan = metaService.selectOne("JE_CORE_RESOURCEFIELD_PLAN", ConditionsWrapper.builder()
                .selectColumns("JE_CORE_RESOURCEFIELD_PLAN_ID")
                .eq("PLAN_FUNCINFO_ID", funId)
                .eq("PLAN_SY", "1"));
        if (dbfPlan == null) {
            DynaBean fPlan = new DynaBean("JE_CORE_RESOURCEFIELD_PLAN", false);
            fPlanId = JEUUID.uuid();
            fPlan.setStr("JE_CORE_RESOURCEFIELD_PLAN_ID", fPlanId);
            fPlan.setStr("PLAN_NAME", "系统方案");
            fPlan.setStr("PLAN_FUNCINFO_ID", funId);
            fPlan.setStr("PLAN_CODE", "DEFAULT");
            fPlan.setStr("PLAN_SY", "1");
            fPlan.setStr("PLAN_DEFAULT", "1");
            fPlan.setStr("SY_ORDERINDEX", "1");
            fPlan.setStr("SY_STATUS", "1");
            commonService.buildModelCreateInfo(fPlan);
            metaService.insert(fPlan);
        } else {
            fPlanId = dbfPlan.getStr("JE_CORE_RESOURCEFIELD_PLAN_ID");
        }
        for (DynaBean tc : tcList) {
            if ("SY__POSTIL".equals(tc.getStr("TABLECOLUMN_CODE"))) {
                continue;
            }
            //已经导入的字段中没有该字段
            if (!columnList.contains(tc.getStr("TABLECOLUMN_CODE"))) {
                DynaBean rc = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
                rc.setStr("RESOURCECOLUMN_PLAN_ID", cPlanId);
                rc.set("RESOURCECOLUMN_FUNCINFO_ID", funId);
                rc.set("RESOURCECOLUMN_CODE", tc.getStr("TABLECOLUMN_CODE"));
                rc.set("RESOURCECOLUMN_NAME", tc.getStr("TABLECOLUMN_NAME"));
                rc.set("RESOURCECOLUMN_NAME_EN", tc.getStr("TABLECOLUMN_NAME_EN"));
                //将导入的字段标识为true
                rc.set("RESOURCECOLUMN_IFIMPL", "1");
                //是否快速查询 false
                rc.set("RESOURCECOLUMN_QUICKQUERY", "0");
                //默认序号为0
                rc.set("SY_ORDERINDEX", order);
                //设置默认宽度为100
                rc.set("RESOURCECOLUMN_WIDTH", "100");
                //是否拥有转换器conversion
                rc.set("RESOURCECOLUMN_CONVERSION", "0");
                //是否拥有转换器highlighting
                rc.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
                //是否超链接hyperLink
                rc.set("RESOURCECOLUMN_HYPERLINK", "0");
                //是否在字典中取
                rc.set("RESOURCECOLUMN_ISDD", "0");
                //合并单元格      否
                rc.set("RESOURCECOLUMN_MERGE", "0");
                //锁定列  否
                rc.set("RESOURCECOLUMN_LOCKED", "");
                //多表头   否
                rc.set("RESOURCECOLUMN_MORECOLUMN", "0");
                //启用图标
                rc.set("RESOURCECOLUMN_ENABLEICON", "0");
                //启用分步加载
                rc.set("RESOURCECOLUMN_LAZYLOAD", "0");
                rc.set("RESOURCECOLUMN_ALIGN", "left");
                rc.set("RESOURCECOLUMN_ALIGN_TITLE", "left");
                if (indexCodes.contains(tc.getStr("TABLECOLUMN_CODE"))) {
                    //排序
                    rc.set("RESOURCECOLUMN_ORDER", "0");
                    //索引
                    rc.set("RESOURCECOLUMN_INDEX", "1");
                } else {
                    //排序
                    rc.set("RESOURCECOLUMN_ORDER", "0");
                    //索引
                    rc.set("RESOURCECOLUMN_INDEX", "0");
                }
                //是否主键
                if (table.getStr("RESOURCETABLE_PKCODE", "").equals(tc.getStr("TABLECOLUMN_CODE"))) {
                    rc.set("RESOURCECOLUMN_ISPK", "1");
                    rc.set("RESOURCECOLUMN_LAZYLOAD", "1");
                } else {
                    rc.set("RESOURCECOLUMN_ISPK", "0");
                }
                rc.set("RESOURCECOLUMN_SYSMODE", sysMode);
                /**---------------------如果有初始化信息则使用初始化信息存储------------------------------------*/
                //是否可以为空  true
                rc.set("RESOURCECOLUMN_ALLOWBLANK", "1");
                rc.set("RESOURCECOLUMN_QUERYTYPE", "");
                //改动：判断是否包含NAME和CODE 张帅鹏
                //系统字段  默认隐藏
                if (tc.getStr("TABLECOLUMN_CODE").startsWith("SY_") || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("NAME") > 0 || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("CODE") > 0 || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("ID") > 0) {
                    rc.set("RESOURCECOLUMN_HIDDEN", "1");
                    if (ArrayUtils.contains(new String[]{"SY_CREATEUSERNAME", "SY_CREATEUSERID", "SY_CREATEUSERID", "SY_ACKFLAG", "SY_FUNCEDIT"}, tc.getStr("TABLECOLUMN_CODE"))) {
                        rc.set("RESOURCECOLUMN_LAZYLOAD", "1");
                    }
                } else {
                    rc.set("RESOURCECOLUMN_HIDDEN", "0");
                    rc.set("RESOURCECOLUMN_LAZYLOAD", "1");
                }
                if (treeCodes.contains(tc.getStr("TABLECOLUMN_CODE"))) {
                    rc.set("RESOURCECOLUMN_LAZYLOAD", "1");
                }
                //字段类型  默认为无  不可编辑
                rc.set("RESOURCECOLUMN_XTYPE", "uxcolumn");
                //列表编辑   默认为否
                rc.set("RESOURCECOLUMN_ALLOWEDIT", "0");
                rc.set("RESOURCECOLUMN_DESCRIPTION", tc.getStr("TABLECOLUMN_REMARK"));
                buildModelCreateInfo(rc);
                metaService.insert(rc);
                order++;
            }
            //导入列表字段的同时，导入表单字段
            if (!fieldList.contains(tc.getStr("TABLECOLUMN_CODE"))) {
                DynaBean rf = new DynaBean("JE_CORE_RESOURCEFIELD", false);
                rf.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
                rf.setStr("RESOURCEFIELD_LABELALIGN", "right");
                rf.set("RESOURCEFIELD_HELP_TIP_W", 660);
                rf.set("RESOURCEFIELD_USE_SCOPE", "ALL");
                rf.set("RESOURCEFIELD_HELP_TIP_H", 400);
                rf.set("RESOURCEFIELD_PLAN_ID", fPlanId);
                rf.set("RESOURCEFIELD_FUNCINFO_ID", funId);
                rf.set("RESOURCEFIELD_CODE", tc.getStr("TABLECOLUMN_CODE"));
                rf.set("RESOURCEFIELD_NAME", tc.getStr("TABLECOLUMN_NAME"));
                rf.set("RESOURCEFIELD_NAME_EN", tc.getStr("TABLECOLUMN_NAME_EN"));
                rf.set("RESOURCEFIELD_LABELALIGN", "right");
                //将导入的字段标识为true
                rf.set("RESOURCEFIELD_IFIMPL", "1");
                //是否可用  true
                rf.set("RESOURCEFIELD_DISABLED", "1");
                //是否可以为空  true
                rf.set("RESOURCEFIELD_ALLOWBLANK", "1");
                //可选可编辑   默认否
                rf.set("RESOURCEFIELD_EDITABLE", "0");
                //所占列数
                rf.set("RESOURCEFIELD_COLSPAN", 1);
                //配置
                //rf.set("RESOURCEFIELD_OTHERCONFIG",getSelUserOtherconfig());
                String columnType = tc.getStr("TABLECOLUMN_TYPE");
                //数值型
                if ("NUMBER".equals(columnType) || "FLOAT".equals(columnType)) {
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "numberfield");
                    if ("FLOAT".equals(columnType) && StringUtil.isNotEmpty(tc.getStr("TABLECOLUMN_LENGTH"))) {
                        String lengthStr = tc.getStr("TABLECOLUMN_LENGTH");
                        if (lengthStr.indexOf(",") != -1) {
                            //默认小数精度
                            rf.set("RESOURCEFIELD_CONFIGINFO", lengthStr.split(ArrayUtils.SPLIT)[1]);
                        } else {
                            //默认小数精度
                            rf.set("RESOURCEFIELD_CONFIGINFO", lengthStr);
                        }
                    } else if ("NUMBER".equals(columnType)) {
                        //默认小数精度
                        rf.set("RESOURCEFIELD_CONFIGINFO", "0");
                    }
                } else if ("FLOAT2".equals(columnType)) {
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "numberfield");
                    rf.set("RESOURCEFIELD_CONFIGINFO", "2");
                } else if ("DATE".equals(columnType)) {
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "datefield");
                    rf.setStr("RESOURCEFIELD_OTHERCONFIG", getDateOtherconfig("date"));
                } else if ("DATETIME".equals(columnType)) {
                    //字段类型
                    rf.set("RESOURCEFIELD_XTYPE", "datefield");
                    rf.setStr("RESOURCEFIELD_OTHERCONFIG", getDateOtherconfig("dateTime"));
                } else {
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "textfield");
                }
                if ("SY_CREATEORGID".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "1");
                    rf.set("RESOURCEFIELD_VALUE", "@DEPT_ID@");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "textfield");
                    //是否只读
                } else if ("SY_CREATEORGNAME".equals(tc.getStr("TABLECOLUMN_CODE"))) {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "1");
                    rf.set("RESOURCEFIELD_VALUE", "@DEPT_NAME@");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "textfield");
                } else if ("SY_CREATETIME".equals(tc.getStr("TABLECOLUMN_CODE"))) {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "1");
                    //默认   年月日 时分秒
                    rf.set("RESOURCEFIELD_VALUE", "@NOW_TIME@");
                    //字段类型  “日期时间”
                    rf.set("RESOURCEFIELD_XTYPE", "datefield");
                    //配置信息 为  Y-m-d
                    rf.setStr("RESOURCEFIELD_OTHERCONFIG", getDateOtherconfig("dateTime"));
                } else if ("SY_CREATEUSERID".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "1");
                    rf.set("RESOURCEFIELD_VALUE", "@USER_ID@");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "textfield");
                } else if ("SY_CREATEUSERNAME".equals(tc.getStr("TABLECOLUMN_CODE"))) {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "1");
                    rf.set("RESOURCEFIELD_VALUE", "@USER_NAME@");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "textfield");
                } else if ("SY_ORDERINDEX".equals(tc.getStr("TABLECOLUMN_CODE"))) {
                    rf.set("RESOURCEFIELD_VALUE", "1");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "numberfield");
                } else if ("SY_AUDFLAG".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "1");
                    rf.set("RESOURCEFIELD_VALUE", "NOSTATUS");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "cbbfield");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_CONFIGINFO", "JE_AUDFLAG,SY_AUDFLAG,code,S");
                } else if ("SY_ACKFLAG".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "1");
                    rf.set("RESOURCEFIELD_VALUE", "0");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "rgroup");
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_CONFIGINFO", "SY_ACKFLAG,SY_ACKFLAG,code,S");
                } else {
                    //是否只读
                    rf.set("RESOURCEFIELD_READONLY", "0");
                    if (!Strings.isNullOrEmpty(tc.getStr("TABLECOLUMN_NAME"))
                            && (tc.getStr("TABLECOLUMN_NAME").endsWith("_CODE") || tc.getStr("TABLECOLUMN_NAME").endsWith("_NAME"))) {
                        rf.set("RESOURCEFIELD_READONLY", "1");
                    }
                }
                //系统字段  默认隐藏
                if (tc.getStr("TABLECOLUMN_CODE").startsWith("SY_") || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("NAME") > 0 || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("CODE") > 0 || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("ID") > 0) {
                    rf.set("RESOURCEFIELD_HIDDEN", "1");
                    if ("SY_ORDERINDEX".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        rf.set("RESOURCEFIELD_VALUE", "1");
                    }
                } else {
                    rf.set("RESOURCEFIELD_HIDDEN", "0");
                }
                //系统字段  默认隐藏
               /* if (tc.getStr("TABLECOLUMN_CODE").startsWith("SY_") || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("NAME") > 0 || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("CODE") > 0 || tc.getStr("TABLECOLUMN_NAME").lastIndexOf("ID") > 0) {
                    rf.set("RESOURCEFIELD_HIDDEN", "1");
                    if ("SY_ORDERINDEX".equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                        rf.set("RESOURCEFIELD_VALUE", "0");
                    }
                } else {
                    rf.set("RESOURCEFIELD_HIDDEN", "0");
                }*/
                String config = "";
                //根据列的添加方式 字典添加，表添加来快速初始化功能配置项
                if (StringUtil.isNotEmpty(tc.getStr("TABLECOLUMN_DICCONFIG"))) {
                    config = tc.getStr("TABLECOLUMN_DICCONFIG");
                    String dicOtherConfig = tc.getStr("TABLECOLUMN_DICQUERYFIELD");
                    if (StringUtil.isNotEmpty(dicOtherConfig)) {
                        // edit by zhangyandong for 增加级联字典报错
                        if (dicOtherConfig.indexOf("cascadeField") != -1) {
                            JSONObject dicConfig = JSON.parseObject(dicOtherConfig);
                            String where = dicConfig.getString("where");
                            String other = dicConfig.getString("other");
                            rf.set("RESOURCEFIELD_XTYPE", "cbbfield");
                            rf.set("RESOURCEFIELD_CONFIGINFO", tc.getStr("TABLECOLUMN_DICCONFIG"));
                            rf.set("RESOURCEFIELD_OTHERCONFIG", other);
                            rf.set("RESOURCEFIELD_WHERESQL", where);
                        } else {
                            rf.set("RESOURCEFIELD_XTYPE", "treessfield");
                            rf.set("RESOURCEFIELD_CONFIGINFO", tc.getStr("TABLECOLUMN_DICCONFIG"));
                            rf.set("RESOURCEFIELD_OTHERCONFIG", URLDecoder.decode("%7B%22queryField%22%3A%22" + tc.getStr("TABLECOLUMN_DICQUERYFIELD") + "%22%7D "));
                        }
                    } else {
                        rf.set("RESOURCEFIELD_XTYPE", "cbbfield");
                        rf.set("RESOURCEFIELD_CONFIGINFO", tc.getStr("TABLECOLUMN_DICCONFIG"));
                    }
                } else if (StringUtil.isNotEmpty(tc.getStr("TABLECOLUMN_QUERYCONFIG"))) {
                    config = tc.getStr("TABLECOLUMN_QUERYCONFIG");
                    if (tc.getStr("TABLECOLUMN_QUERYCONFIG").startsWith("JE_RBAC_VUSERQUERY,")) {
                        rf.set("RESOURCEFIELD_XTYPE", "vueuserfield");
                        rf.set("RESOURCEFIELD_CONFIGINFO", tc.getStr("TABLECOLUMN_QUERYCONFIG"));
                        rf.set("RESOURCEFIELD_OTHERCONFIG", getSelUserOtherconfig());
                    } else {
                        rf.set("RESOURCEFIELD_XTYPE", "gridssfield");
                        rf.set("RESOURCEFIELD_CONFIGINFO", tc.getStr("TABLECOLUMN_QUERYCONFIG"));
                    }
                }
                if (StringUtil.isNotEmpty(config) && config.split(ArrayUtils.SPLIT).length > 1) {
                    String fieldCodes = config.split(ArrayUtils.SPLIT)[1];
                    if (StringUtil.isNotEmpty(fieldCodes)) {
                        for (String fieldCode : fieldCodes.split("~")) {
                            if (!fieldCode.equals(rf.getStr("RESOURCEFIELD_CODE"))) {
                                lazyLoadSqls.add("UPDATE JE_CORE_RESOURCECOLUMN SET RESOURCECOLUMN_LAZYLOAD='1' WHERE RESOURCECOLUMN_CODE='" + fieldCode + "' AND RESOURCECOLUMN_FUNCINFO_ID='" + funId + "'");
                            }
                        }
                    }
                }
                if ("SY_WARNFLAG".equals(tc.getStr("TABLECOLUMN_CODE"))) {
                    rf.set("RESOURCEFIELD_XTYPE", "cbbfield");
                    rf.set("RESOURCEFIELD_CONFIGINFO", "JE_WARNFLAG");
                }
                //默认序号为0
                rf.set("SY_ORDERINDEX", fieldOrder);
                //所占行数
                rf.set("RESOURCEFIELD_ROWSPAN", 1);
                rf.set("RESOURCEFIELD_SYSMODE", sysMode);
                //是否主键
                if (table.getStr("RESOURCETABLE_PKCODE", "").equals(tc.getStr("TABLECOLUMN_CODE"))) {
                    rf.set("RESOURCEFIELD_ISPK", "1");
                } else {
                    rf.set("RESOURCEFIELD_ISPK", "0");
                }
                if ("1".equals(rf.getStr("RESOURCEFIELD_HIDDEN"))) {
                    rf.set("RESOURCEFIELD_FIELDLAZY", "0");
                } else {
                    rf.set("RESOURCEFIELD_FIELDLAZY", "1");
                }
                rf.set("RESOURCEFIELD_DESCRIPTION", tc.getStr("TABLECOLUMN_REMARK"));
                buildModelCreateInfo(rf);
                metaService.insert(rf);
                fieldOrder++;

            }
        }
        //处理分步加载打勾操作
        for (String lazyloadSql : lazyLoadSqls) {
            metaService.executeSql(lazyloadSql);
        }
        //默认给表单添加一个child 用于子功能  有且仅有一个
        DynaBean rField = metaService.selectOne("JE_CORE_RESOURCEFIELD", ConditionsWrapper.builder()
                .eq("RESOURCEFIELD_FUNCINFO_ID", funId)
                .eq("RESOURCEFIELD_IFIMPL", "1").eq("RESOURCEFIELD_CODE", "__child"));
        if (rField == null) {
            DynaBean rf = new DynaBean("JE_CORE_RESOURCEFIELD", false);
            rf.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
            rf.set("RESOURCEFIELD_USE_SCOPE", "ALL");
            rf.set("RESOURCEFIELD_XTYPE", "child");
            //将导入的字段标识为true
            rf.set("RESOURCEFIELD_IFIMPL", "1");
            //是否可用  true
            rf.set("RESOURCEFIELD_DISABLED", "1");
            //是否可以为空  true
            rf.set("RESOURCEFIELD_ALLOWBLANK", "1");
            //默认序号为0
            rf.set("SY_ORDERINDEX", fieldOrder);
            //所占行数
            rf.set("RESOURCEFIELD_ROWSPAN", 1);
            //所占列数
            rf.set("RESOURCEFIELD_COLSPAN", 1);
            //可选可编辑   默认否
            rf.set("RESOURCEFIELD_EDITABLE", "0");
            rf.set("RESOURCEFIELD_HIDDEN", "1");
            rf.set("RESOURCEFIELD_CODE", "__child");
            rf.set("RESOURCEFIELD_NAME", "子功能");
            rf.set("RESOURCEFIELD_NAME_EN", "Sub Func");
            rf.set("RESOURCEFIELD_FUNCINFO_ID", funId);
            rf.set("RESOURCEFIELD_SYSMODE", sysMode);
            buildModelCreateInfo(rf);
            metaService.insert(rf);
        }

        //增加流程审批字段
        addApprovalRecordFiled(fieldOrder, funId, sysMode);

        //默认给列表添加一个action列
        DynaBean actionColumn = metaService.selectOne("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder()
                .eq("RESOURCECOLUMN_FUNCINFO_ID", funId).eq("RESOURCECOLUMN_IFIMPL", "1").eq("RESOURCECOLUMN_XTYPE", "actioncolumn"));
        if (actionColumn == null) {
            actionColumn = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
            actionColumn.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
            actionColumn.set("RESOURCECOLUMN_FUNCINFO_ID", funId);
            actionColumn.set("RESOURCECOLUMN_CODE", "actioncolumn_1");
            actionColumn.set("RESOURCECOLUMN_NAME", "操作");
            actionColumn.set("RESOURCECOLUMN_NAME_EN", "Action");
            //将导入的字段标识为true
            actionColumn.set("RESOURCECOLUMN_IFIMPL", "1");
            //是否快速查询 false
            actionColumn.set("RESOURCECOLUMN_QUICKQUERY", "0");
            //默认序号为0
            actionColumn.set("SY_ORDERINDEX", order);
            //设置默认宽度为80
            actionColumn.set("RESOURCECOLUMN_WIDTH", "100");
            //是否拥有转换器conversion
            actionColumn.set("RESOURCECOLUMN_CONVERSION", "0");
            //是否拥有转换器highlighting
            actionColumn.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
            //是否超链接hyperLink
            actionColumn.set("RESOURCECOLUMN_HYPERLINK", "0");
            //是否在字典中取
            actionColumn.set("RESOURCECOLUMN_ISDD", "0");
            //合并单元格      否
            actionColumn.set("RESOURCECOLUMN_MERGE", "0");
            //锁定列
            actionColumn.set("RESOURCECOLUMN_LOCKED", "");
            //多表头   否
            actionColumn.set("RESOURCECOLUMN_MORECOLUMN", "0");
            //启用图标
            actionColumn.set("RESOURCECOLUMN_ENABLEICON", "0");
            actionColumn.set("RESOURCECOLUMN_ALIGN", "center");
            actionColumn.set("RESOURCECOLUMN_ISPK", "0");
            actionColumn.set("RESOURCECOLUMN_ORDER", "0");
            actionColumn.set("RESOURCECOLUMN_INDEX", "0");
            //是否可以为空  true
            actionColumn.set("RESOURCECOLUMN_ALLOWBLANK", "1");
            actionColumn.set("RESOURCECOLUMN_QUERYTYPE", "no");
            actionColumn.set("RESOURCECOLUMN_HIDDEN", "1");
            actionColumn.set("RESOURCECOLUMN_OTHERCONFIG", "{\"headerAlign\":\"center\"}");
            actionColumn.set("RESOURCECOLUMN_LAZYLOAD", "0");
            //字段类型  默认为无  不可编辑
            actionColumn.set("RESOURCECOLUMN_XTYPE", "actioncolumn");
            //列表编辑   默认为否
            actionColumn.set("RESOURCECOLUMN_ALLOWEDIT", "0");
            buildModelCreateInfo(actionColumn);
            metaService.insert(actionColumn);
        }
        //默认给列表添加一个序号列
        DynaBean rownumberer = metaService.selectOne("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder().eq("RESOURCECOLUMN_FUNCINFO_ID", funId).eq("RESOURCECOLUMN_IFIMPL", "1").eq("RESOURCECOLUMN_XTYPE", "rownumberer"));
        if (rownumberer == null) {
            rownumberer = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
            rownumberer.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
            rownumberer.set("RESOURCECOLUMN_FUNCINFO_ID", funId);
            rownumberer.set("RESOURCECOLUMN_CODE", "rownumberer_1");
            rownumberer.set("RESOURCECOLUMN_NAME", "No.");
            rownumberer.set("RESOURCECOLUMN_NAME_EN", "No.");
            //将导入的字段标识为true
            rownumberer.set("RESOURCECOLUMN_IFIMPL", "1");
            //是否快速查询 false
            rownumberer.set("RESOURCECOLUMN_QUICKQUERY", "0");
            //默认序号为0
            rownumberer.set("SY_ORDERINDEX", 0);
            //设置默认宽度为80
            rownumberer.set("RESOURCECOLUMN_WIDTH", "50");
            //是否拥有转换器conversion
            rownumberer.set("RESOURCECOLUMN_CONVERSION", "0");
            //是否拥有转换器highlighting
            rownumberer.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
            //是否超链接hyperLink
            rownumberer.set("RESOURCECOLUMN_HYPERLINK", "0");
            //是否在字典中取
            rownumberer.set("RESOURCECOLUMN_ISDD", "0");
            //合并单元格      否
            rownumberer.set("RESOURCECOLUMN_MERGE", "0");
            //锁定列
            rownumberer.set("RESOURCECOLUMN_LOCKED", "");
            //多表头   否
            rownumberer.set("RESOURCECOLUMN_MORECOLUMN", "0");
            //启用图标
            rownumberer.set("RESOURCECOLUMN_ENABLEICON", "0");
            rownumberer.set("RESOURCECOLUMN_ALIGN", "center");
            rownumberer.set("RESOURCECOLUMN_ALIGN_TITLE", "center");
            rownumberer.set("RESOURCECOLUMN_ISPK", "0");
            rownumberer.set("RESOURCECOLUMN_ORDER", "0");
            rownumberer.set("RESOURCECOLUMN_INDEX", "0");
            //是否可以为空  true
            rownumberer.set("RESOURCECOLUMN_ALLOWBLANK", "1");
            rownumberer.set("RESOURCECOLUMN_QUERYTYPE", "no");
            rownumberer.set("RESOURCECOLUMN_HIDDEN", "0");
            //字段类型  默认为无  不可编辑
            rownumberer.set("RESOURCECOLUMN_XTYPE", "rownumberer");
            //列表编辑   默认为否
            rownumberer.set("RESOURCECOLUMN_ALLOWEDIT", "0");
            actionColumn.set("RESOURCECOLUMN_LAZYLOAD", "0");
            buildModelCreateInfo(rownumberer);
            metaService.insert(rownumberer);
        }
    }

    /**
     * 添加审批记录字段
     */
    private void addApprovalRecordFiled(int fieldOrder, String funId, String sysMode) {
        List<DynaBean> list = metaService.select("JE_CORE_RESOURCEFIELD", ConditionsWrapper.builder()
                .eq("RESOURCEFIELD_CODE", "__workflow_history")
                .eq("RESOURCEFIELD_FUNCINFO_ID", funId));
        if (list.size() > 0) {
            return;
        }
        DynaBean workflow = new DynaBean("JE_CORE_RESOURCEFIELD", false);
        workflow.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
        workflow.set("RESOURCEFIELD_USE_SCOPE", "ALL");
        workflow.set("RESOURCEFIELD_XTYPE", "workflowhistory");
        //将导入的字段标识为true
        workflow.set("RESOURCEFIELD_IFIMPL", "1");
        //是否可用  true
        workflow.set("RESOURCEFIELD_DISABLED", "1");
        //是否可以为空  true
        workflow.set("RESOURCEFIELD_ALLOWBLANK", "1");
        //默认序号为0
        workflow.set("SY_ORDERINDEX", fieldOrder + 1);
        //所占行数
        workflow.set("RESOURCEFIELD_ROWSPAN", 1);
        //所占列数
        workflow.set("RESOURCEFIELD_COLSPAN", 1);
        //可选可编辑   默认否
        workflow.set("RESOURCEFIELD_EDITABLE", "0");
        workflow.set("RESOURCEFIELD_HIDDEN", "1");
        workflow.set("RESOURCEFIELD_CODE", "__workflow_history");
        workflow.set("RESOURCEFIELD_NAME", "审批记录");
        workflow.set("RESOURCEFIELD_NAME_EN", "");
        workflow.set("RESOURCEFIELD_FUNCINFO_ID", funId);
        workflow.set("RESOURCEFIELD_SYSMODE", sysMode);
        commonService.buildModelCreateInfo(workflow);
        metaService.insert(workflow);
    }

    private String getSelUserOtherconfig() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deptQuery", "");
        return jsonObject.toString();
    }

    private String getDateOtherconfig(String type) {
        JSONObject jsonObject = new JSONObject();
        if ("date".equals(type)) {
            jsonObject.put("dateType", "date");
        } else {
            jsonObject.put("dateType", "dateTime");
        }
        return jsonObject.toString();
    }

    @Override
    @Transactional
    public void implProcedure(DynaBean funcInfo) {
        String dataSource = funcInfo.getStr("FUNCINFO_DBNAME");
        String procedureName = funcInfo.getStr("FUNCINFO_PROCEDURE");
        String queryParamStr = funcInfo.getStr("FUNCINFO_QUERYPARAM");
        JSONArray arrays = JSON.parseArray(queryParamStr);
        List<DbFieldVo> fieldVos = new ArrayList<DbFieldVo>();
        for (int i = 0; i < arrays.size(); i++) {
            JSONObject obj = arrays.getJSONObject(i);
            String name = obj.getString("name");
            String fieldType = obj.getString("fieldType");
            String paramType = "1".equals(obj.getString("paramType")) ? "out" : "in";
            DbFieldVo fieldVo = new DbFieldVo(name, paramType, fieldType);
//			if(paramValues.containsKey(name)){
            if (DbFieldType.NOWPAGE.equals(fieldType)) {
                fieldVo.setValue(1);
            } else if (DbFieldType.LIMIT.equals(fieldType)) {
                fieldVo.setValue(30);
            } else if (DbFieldType.TOTALCOUNT.equals(fieldType)) {
            } else if (DbFieldType.CODE.equals(fieldType)) {
            } else if (DbFieldType.MSG.equals(fieldType)) {
            }
            String defaultValue = "";
            if (obj.containsKey("defaultValue")) {
                defaultValue = obj.getString("defaultValue");
                if (StringUtil.isNotEmpty(defaultValue)) {
                    String val = sysVariablesService.findNativeFrontVariable(defaultValue);
                    if (StringUtil.isNotEmpty(val)) {
                        defaultValue = val;
                        fieldVo.setValue(val);
                    }
                }
            }
            if ("out".equalsIgnoreCase(paramType)) {

            } else {
                if (StringUtil.isEmpty(defaultValue)) {
                    fieldVo.setValue(null);
                }
            }
//			}
            fieldVos.add(fieldVo);
        }

        List<Model> models = pcDataService.loadProcedure(dataSource, procedureName, fieldVos);
        implModels(funcInfo, models, fieldVos);
        for (DbFieldVo fieldVo : fieldVos) {
            String fieldType = fieldVo.getFieldType();
            if ("in".equals(fieldVo.getParamType()) && !ArrayUtils.contains(new String[]{DbFieldType.CURSOR, DbFieldType.MSG, DbFieldType.CODE, DbFieldType.NOWPAGE, DbFieldType.LIMIT, DbFieldType.TOTALCOUNT}, fieldType)) {
                saveQueryField("", fieldVo.getName(), funcInfo.getStr("JE_CORE_FUNCINFO_ID"));
            }
        }
    }

    @Override
    @Transactional
    public void implIditProcedure(DynaBean funcInfo) {
        String queryParamStr = funcInfo.getStr("FUNCINFO_QUERYPARAM");
        JSONArray arrays = JSON.parseArray(queryParamStr);
        List<DbFieldVo> fieldVos = new ArrayList<DbFieldVo>();
        for (int i = 0; i < arrays.size(); i++) {
            JSONObject obj = arrays.getJSONObject(i);
            String name = obj.getString("name");
            String fieldType = obj.getString("fieldType");
            String paramType = "1".equals(obj.getString("paramType")) ? "out" : "in";
            DbFieldVo fieldVo = new DbFieldVo(name, paramType, fieldType);
            if (!"in".equals(paramType)) {

            }
//			if(paramValues.containsKey(name)){
//				if(DbFieldType.NOWPAGE.equals(fieldType)){
//					fieldVo.setValue(1);
//				}else if(DbFieldType.LIMIT.equals(fieldType)){
//					fieldVo.setValue(30);
//				}else if(DbFieldType.TOTALCOUNT.equals(fieldType)){
//				}else if(DbFieldType.CODE.equals(fieldType)){
//				}else if(DbFieldType.MSG.equals(fieldType)){
//				}
//				String defaultValue="";
//				if(obj.containsKey("defaultValue")){
//					defaultValue=obj.getString("defaultValue");
//					if(StringUtil.isNotEmpty(defaultValue)){
//					String val=StringUtil.getVarDefaultValue(defaultValue);
//						if(StringUtil.isNotEmpty(val)){
//							defaultValue=val;
//							fieldVo.setValue(val);
//						}
//					}
//				}
//				if("out".equalsIgnoreCase(paramType)){
//
//				}else{
//					if(StringUtil.isEmpty(defaultValue)){
//						fieldVo.setValue(null);
//					}
//				}
////			}
            fieldVos.add(fieldVo);
//		}
//		Object[] params=null;
//		if(StringUtil.isNotEmpty(queryParamStr)){
//			params=new Object[queryParamStr.split(ArrayUtils.SPLIT).length];
        }
        List<Model> models = new ArrayList<Model>();
        models.add(new Model(funcInfo.getStr("FUNCINFO_PKNAME"), "string"));
        implModels(funcInfo, models, fieldVos);
        for (DbFieldVo fieldVo : fieldVos) {
            String fieldType = fieldVo.getFieldType();
            if ("in".equals(fieldVo.getParamType()) && !ArrayUtils.contains(new String[]{DbFieldType.CURSOR, DbFieldType.MSG, DbFieldType.CODE, DbFieldType.NOWPAGE, DbFieldType.LIMIT, DbFieldType.TOTALCOUNT}, fieldType)) {
                saveQueryField("", fieldVo.getName(), funcInfo.getStr("JE_CORE_FUNCINFO_ID"));
            }
        }
    }

    @Override
    @Transactional
    public void implSql(DynaBean funcInfo) {
        String dataSource = funcInfo.getStr("FUNCINFO_DBNAME");
        String sql = funcInfo.getStr("FUNCINFO_SQL");
        String queryParamStr = funcInfo.getStr("FUNCINFO_QUERYPARAM");
        JSONArray arrays = JSON.parseArray(queryParamStr);
        List<DbFieldVo> fieldVos = new ArrayList<DbFieldVo>();
        for (int i = 0; i < arrays.size(); i++) {
            JSONObject obj = arrays.getJSONObject(i);
            String name = obj.getString("name");
            String fieldType = obj.getString("fieldType");
            String paramType = "1".equals(obj.getString("paramType")) ? "out" : "in";
            DbFieldVo fieldVo = new DbFieldVo(name, paramType, fieldType);
//			if(paramValues.containsKey(name)){
            if (DbFieldType.NOWPAGE.equals(fieldType)) {
                fieldVo.setValue(1);
            } else if (DbFieldType.LIMIT.equals(fieldType)) {
                fieldVo.setValue(30);
            } else if (DbFieldType.TOTALCOUNT.equals(fieldType)) {
            } else if (DbFieldType.CODE.equals(fieldType)) {
            } else if (DbFieldType.MSG.equals(fieldType)) {
            }
            String defaultValue = "";
            if (obj.containsKey("defaultValue")) {
                defaultValue = obj.getString("defaultValue");
                if (StringUtil.isNotEmpty(defaultValue)) {
                    String val = sysVariablesService.findNativeSystemVariable(defaultValue);
                    if (StringUtil.isNotEmpty(val)) {
                        defaultValue = val;
                        fieldVo.setValue(val);
                    }
                }
            }
            if ("out".equalsIgnoreCase(paramType)) {

            } else {
                if (StringUtil.isEmpty(defaultValue)) {
                    fieldVo.setValue(null);
                }
            }
//			}
            fieldVos.add(fieldVo);
        }
        List<Model> models = pcDataService.loadSql(dataSource, sql, fieldVos);
        implModels(funcInfo, models, null);
    }

    @Override
    @Transactional
    public void doSync(DynaBean dynaBean) {
        ConditionsWrapper selectField = ConditionsWrapper.builder();
        selectField.eq("RESOURCEFIELD_FUNCINFO_ID", dynaBean.getStr("RESOURCECOLUMN_FUNCINFO_ID"));
        if (StringUtil.isNotEmpty(dynaBean.getStr("RESOURCEFIELD_PLAN_ID"))) {
            selectField.eq("RESOURCEFIELD_PLAN_ID", dynaBean.getStr("RESOURCEFIELD_PLAN_ID"));
        }
        List<DynaBean> fieldList = metaService.select("JE_CORE_RESOURCEFIELD", selectField);
        ConditionsWrapper selectColumn = ConditionsWrapper.builder();
        selectColumn.eq("RESOURCECOLUMN_FUNCINFO_ID", dynaBean.getStr("RESOURCECOLUMN_FUNCINFO_ID"));
        if (StringUtil.isNotEmpty(dynaBean.getStr("RESOURCECOLUMN_PLAN_ID"))) {
            selectColumn.eq("RESOURCECOLUMN_PLAN_ID", dynaBean.getStr("RESOURCECOLUMN_PLAN_ID"));
        }
        List<DynaBean> columnList = metaService.select("JE_CORE_RESOURCECOLUMN", selectColumn);
        if (fieldList.size() > 0 && columnList.size() > 0) {
            for (DynaBean rf : fieldList) {
                for (DynaBean rc : columnList) {
                    if (rf.get("RESOURCEFIELD_CODE").equals(rc.get("RESOURCECOLUMN_CODE"))) {
                        rc.set("SY_ORDERINDEX", rf.get("SY_ORDERINDEX"));
                        rc.set("RESOURCECOLUMN_HIDDEN", rf.getStr("RESOURCEFIELD_HIDDEN"));
                        rc.set("RESOURCECOLUMN_NAME", rf.get("RESOURCEFIELD_NAME"));
                        rc.set(BeanService.KEY_TABLE_CODE, "JE_CORE_RESOURCECOLUMN");
                        metaService.update(rc);
                    }
                }
            }
        }
    }

    @Override
    @Transactional
    public String impl(DynaBean funcInfo) {
        String tableName = funcInfo.getStr("FUNCINFO_TABLENAME");
        String funcType = funcInfo.getStr("FUNCINFO_FUNCTYPE");
        try {
            if ("procedure".equals(funcType)) {
                implProcedure(funcInfo);
            } else if ("iditprocedure".equals(funcType)) {
                implIditProcedure(funcInfo);
            } else if ("sql".equals(funcType)) {
                implSql(funcInfo);
                //通过实体方式
            } else if (StringUtils.countMatches(tableName, ".") > 1) {
                //判断所给实体是否真实存在
                Class c;
                try {
                    c = Class.forName(tableName);
                    Annotation entity = c.getAnnotation(Entity.class);
                    if (entity != null) {
                        impl(funcInfo.getStr("JE_CORE_FUNCINFO_ID"), c);
                    } else {
                        throw new PlatformException(MessageUtils.getMessage("table.imp.getModel"), PlatformExceptionEnum.JE_CORE_DYNABEAN_MODEL_ERROR, new Object[]{funcInfo});
//						return "{success:false,obj:'不是实体类'}";
                    }
                } catch (ClassNotFoundException e1) {
                    throw new PlatformException(MessageUtils.getMessage("table.imp.getModel"), PlatformExceptionEnum.JE_CORE_DYNABEAN_MODEL_ERROR, new Object[]{funcInfo}, e1);
//					return "{success:false,obj:'不是实体类'}";
                }
            } else {  //DynaBean方式
                implDyanBean(funcInfo.getStr("JE_CORE_FUNCINFO_ID"), tableName);
            }
        } catch (Exception e) {
            throw new PlatformException(MessageUtils.getMessage("table.imp.getModel"), PlatformExceptionEnum.JE_CORE_DYNABEAN_MODEL_ERROR, new Object[]{funcInfo}, e);
//			return "{success:false,obj:'"+e.getMessage()+"'}";
        }
        return "";
    }

    @Override
    @Transactional
    public void saveQueryField(String name, String code, String funId) {
        //		String funId=dynaBean.getStr("RESOURCECOLUMN_FUNCINFO_ID");
//		String name=dynaBean.getStr("RESOURCECOLUMN_NAME");
//		String code=dynaBean.getStr("RESOURCECOLUMN_CODE");
        long count = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_RESOURCECOLUMN WHERE RESOURCECOLUMN_FUNCINFO_ID={0} and RESOURCECOLUMN_CODE={1}", funId, code);
        if (count > 0) {
            return;
        }

        DynaBean rc = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
        rc.set("RESOURCECOLUMN_FUNCINFO_ID", funId);
        rc.set("RESOURCECOLUMN_CODE", code);
        rc.set("RESOURCECOLUMN_NAME", name);
        rc.set("RESOURCECOLUMN_NAME_EN", "");
        //将导入的字段标识为true
        rc.set("RESOURCECOLUMN_IFIMPL", "1");
        //是否快速查询 false
        rc.set("RESOURCECOLUMN_QUICKQUERY", "0");
        //默认序号为0
        rc.set("SY_ORDERINDEX", 0);
        //设置默认宽度为80
        rc.set("RESOURCECOLUMN_WIDTH", "80");
        //是否拥有转换器conversion
        rc.set("RESOURCECOLUMN_CONVERSION", "0");
        //是否拥有转换器highlighting
        rc.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
        //是否超链接hyperLink
        rc.set("RESOURCECOLUMN_HYPERLINK", "0");
        rc.set("RESOURCECOLUMN_QUERYTYPE", "group");
        //是否在字典中取
        rc.set("RESOURCECOLUMN_ISDD", "0");
        //合并单元格      否
        rc.set("RESOURCECOLUMN_MERGE", "0");
        //锁定列  否
        rc.set("RESOURCECOLUMN_LOCKED", "");
        //多表头   否
        rc.set("RESOURCECOLUMN_MORECOLUMN", "0");
        //启用图标
        rc.set("RESOURCECOLUMN_ENABLEICON", "0");
        //启用分步加载
        rc.set("RESOURCECOLUMN_LAZYLOAD", "0");
        rc.set("RESOURCECOLUMN_ALIGN", "left");
        //排序
        rc.set("RESOURCECOLUMN_ORDER", "0");
        //索引
        rc.set("RESOURCECOLUMN_INDEX", "0");
        //是否主键
        rc.set("RESOURCECOLUMN_ISPK", "0");
        rc.set("RESOURCECOLUMN_SYSMODE", "CP");
        /**---------------------如果有初始化信息则使用初始化信息存储------------------------------------*/
        //是否可以为空  true
        rc.set("RESOURCECOLUMN_ALLOWBLANK", "1");
        //改动：判断是否包含NAME和CODE 张帅鹏
        rc.set("RESOURCECOLUMN_HIDDEN", "0");
        rc.set("RESOURCECOLUMN_LAZYLOAD", "1");
        //字段类型  默认为无  不可编辑
        rc.set("RESOURCECOLUMN_XTYPE", "uxcolumn");
        //列表编辑   默认为否
        rc.set("RESOURCECOLUMN_ALLOWEDIT", "0");
        rc.set("RESOURCECOLUMN_DESCRIPTION", "");
        rc.set("SY_FLAG", "1");
        commonService.buildModelCreateInfo(rc);
        metaService.insert(rc);

        long fieldCount = metaService.countBySql("SELECT COUNT(*) FROM JE_CORE_RESOURCEFIELD WHERE RESOURCEFIELD_FUNCINFO_ID={0} and RESOURCEFIELD_CODE={1}", funId, code);
        if (fieldCount <= 0) {
            DynaBean rf = new DynaBean("JE_CORE_RESOURCEFIELD", false);
            rf.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
            rf.set("RESOURCEFIELD_FUNCINFO_ID", funId);
            rf.set("RESOURCEFIELD_CODE", code);
            rf.set("RESOURCEFIELD_NAME", name);
            rf.set("RESOURCEFIELD_NAME_EN", "");
            //将导入的字段标识为true
            rf.set("RESOURCEFIELD_IFIMPL", "1");
            //是否可用  true
            rf.set("RESOURCEFIELD_DISABLED", "1");
            //是否可以为空  true
            rf.set("RESOURCEFIELD_ALLOWBLANK", "1");
            //可选可编辑   默认否
            rf.set("RESOURCEFIELD_EDITABLE", "0");
            //所占列数
            rf.set("RESOURCEFIELD_COLSPAN", 1);
            //数值型
            //字段类型   默认为文本框
            rf.set("RESOURCEFIELD_XTYPE", "textfield");
            //是否只读
            rf.set("RESOURCEFIELD_READONLY", "1");
            rf.set("RESOURCEFIELD_HIDDEN", "0");
            //根据列的添加方式 字典添加，表添加来快速初始化功能配置项
            //默认序号为0
            rf.set("SY_ORDERINDEX", 0);
            //所占行数
            rf.set("RESOURCEFIELD_ROWSPAN", 1);
            rf.set("RESOURCEFIELD_SYSMODE", "CP");
            rf.set("RESOURCEFIELD_ISPK", "0");
            rf.set("RESOURCEFIELD_FIELDLAZY", "0");
            rf.set("RESOURCEFIELD_DESCRIPTION", "");
            rf.set("SY_FLAG", "1");
            commonService.buildModelCreateInfo(rf);
            metaService.insert(rf);
        }

    }

    @Override
    public List<String> getTreeTableCodes(DynaBean funcInfo) {
        List<String> treeCodes = new ArrayList<String>();
//		DynaBean funcInfo=pcDynaServiceTemplate.selectOneByPk("JE_CORE_FUNCINFO", funcId,"FUNCINFO_TABLENAME,FUNCINFO_PKNAME,JE_CORE_FUNCINFO_ID");
//		String tableCode=funcInfo.getStr("FUNCINFO_TABLENAME");
//		DynaBean tableInfo=BeanUtils.getInstance().getResourceTable(tableCode);
//		List<DynaBean> columns=(List<DynaBean>) tableInfo.get(BeanUtils.KEY_TABLE_COLUMNS);
//		if(TableType.TREETABLE.equals(tableInfo.getStr("RESOURCETABLE_TYPE"))){
//			for(DynaBean column:columns){
//				String treeType=column.getStr("TABLECOLUMN_TREETYPE","");
//				if(StringUtil.isNotEmpty(treeType) && !"NORMAL".equalsIgnoreCase(treeType)){
//					treeCodes.add(column.getStr("TABLECOLUMN_CODE"));
//				}
//			}
//		}
        String dicConfig = funcInfo.getStr("FUNCINFO_FUNCDICCONFIG", "{}");
        if (StringUtil.isEmpty(dicConfig)) {
            dicConfig = "{}";
        }
        if (dicConfig.startsWith("{")) {
            JSONObject treeConfig = JSON.parseObject(dicConfig);
            for (Object keyObj : treeConfig.keySet()) {
                String key = keyObj + "";
                if (!"moreRoot".equals(key)) {
                    treeCodes.add(treeConfig.getString(key));
                }
            }
        }
        if (dicConfig.startsWith("[")) {
            JSONArray jsonArray = JSON.parseArray(dicConfig);
            if (jsonArray != null && jsonArray.size() > 0) {
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    treeCodes.add(jsonObject.getString("value"));
                }
            }

        }

        return treeCodes;
    }

    @Override
    public int doRemove(String ids) {
        List<DynaBean> beans = metaService.select("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder().apply(" AND JE_CORE_RESOURCECOLUMN_ID IN (" + StringUtil.buildArrayToString(ids.split(",")) + ")"));
        if (beans != null && beans.size() > 0) {
            String RESOURCECOLUMN_FUNCINFO_ID = beans.get(0).getStr("RESOURCECOLUMN_FUNCINFO_ID");
            for (DynaBean item : beans) {
                String RESOURCECOLUMN_XTYPE = item.getStr("RESOURCECOLUMN_XTYPE");
                if ("morecolumn".equals(RESOURCECOLUMN_XTYPE)) {
                    String RESOURCECOLUMN_CODE = item.getStr("RESOURCECOLUMN_CODE");
                    metaService.executeSql("update JE_CORE_RESOURCECOLUMN set  RESOURCECOLUMN_MORECOLUMNNAME=null where RESOURCECOLUMN_FUNCINFO_ID='" + RESOURCECOLUMN_FUNCINFO_ID + "' and RESOURCECOLUMN_MORECOLUMNNAME='" + RESOURCECOLUMN_CODE + "'");
                }
                metaService.delete("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder().eq("JE_CORE_RESOURCECOLUMN_ID", item.get("JE_CORE_RESOURCECOLUMN_ID")));
            }
            return beans.size();
        }
        return 0;
    }

    /**
     * 规则：
     * 先根据 字段code查找，如果找不到，再根据字段name查找，如果找不到，在进行模糊匹配
     * 模糊匹配：由于字段再创建的时候，字段编码会用“_”连接，第一段通常为表编码的后缀，所以去掉参考字段
     * 和目标字段的第一段，再进行匹配
     *
     * @param dynaBean
     * @return
     */
    @Override
    public Map<String, Object> getRelevanceField(DynaBean dynaBean) {
        String consultFields = dynaBean.getStr("consultFields");
        List<DynaBean> fields = metaService.select("JE_CORE_RESOURCEFIELD", ConditionsWrapper.builder()
                .eq("RESOURCEFIELD_FUNCINFO_ID", dynaBean.getStr("targetFuncId"))
                .selectColumns("RESOURCEFIELD_CODE,RESOURCEFIELD_NAME"));
        String[] consultFieldArr = consultFields.split(",");
        //去重
        Set<String> set = new HashSet<>();
        for (String str : consultFieldArr) {
            set.add(str);
        }
        Map<String, String> codeMap = new HashMap<>();
        Map<String, String> nameMap = new HashMap<>();
        Map<String, String> brokenCodeMap = new HashMap<>();
        out:
        for (String str : set) {
            String[] arrStr = str.split("~");
            String code = arrStr[0];
            String text = arrStr[1];
            for (DynaBean field : fields) {
                String RESOURCEFIELD_CODE = field.getStr("RESOURCEFIELD_CODE");
                String RESOURCEFIELD_NAME = field.getStr("RESOURCEFIELD_NAME");
                if (code.equals(RESOURCEFIELD_CODE)) {
                    codeMap.put(str, RESOURCEFIELD_CODE + "~" + RESOURCEFIELD_NAME);
                    continue out;
                }
            }
            for (DynaBean field : fields) {
                String RESOURCEFIELD_CODE = field.getStr("RESOURCEFIELD_CODE");
                String RESOURCEFIELD_NAME = field.getStr("RESOURCEFIELD_NAME");
                if (text.equals(RESOURCEFIELD_NAME)) {
                    nameMap.put(str, RESOURCEFIELD_CODE + "~" + RESOURCEFIELD_NAME);
                    continue out;
                }
            }
            /**
             * 去掉除去表名：第一段编码
             */
            for (DynaBean field : fields) {
                String RESOURCEFIELD_NAME = field.getStr("RESOURCEFIELD_NAME");
                String RESOURCEFIELD_CODE = field.getStr("RESOURCEFIELD_CODE");
                String[] brokenCodeFiled = RESOURCEFIELD_CODE.split("_", 2);
                String[] brokenCodePar = code.split("_", 2);
                if (brokenCodeFiled.length != 2 && brokenCodeFiled.length != brokenCodePar.length) {
                    continue out;
                }
                if (brokenCodePar[1].equals(brokenCodeFiled[1])) {
                    brokenCodeMap.put(str, RESOURCEFIELD_CODE + "~" + RESOURCEFIELD_NAME);
                    continue out;
                }
            }
        }
        List<String> currentFields = new ArrayList<>();
        List<String> targetFields = new ArrayList<>();
        if (!codeMap.isEmpty()) {
            for (Map.Entry<String, String> entry : codeMap.entrySet()) {
                currentFields.add(entry.getKey());
                targetFields.add(entry.getValue());
            }
        }
        if (!nameMap.isEmpty()) {
            for (Map.Entry<String, String> entry : nameMap.entrySet()) {
                currentFields.add(entry.getKey());
                targetFields.add(entry.getValue());
            }
        }
        if (!brokenCodeMap.isEmpty()) {
            for (Map.Entry<String, String> entry : brokenCodeMap.entrySet()) {
                currentFields.add(entry.getKey());
                targetFields.add(entry.getValue());
            }
        }
        if (currentFields.size() != consultFieldArr.length) {
            for (String str : consultFieldArr) {
                if (!currentFields.contains(str)) {
                    currentFields.add(str);
                }
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("current", currentFields);
        result.put("target", targetFields);
        return result;
    }

    @Override
    public int doUpdateList(String strData) {
        JSONArray array = JSON.parseArray(strData);
        if (array != null && array.size() > 0) {
            for (int i = 0; i < array.size(); i++) {
                JSONObject bean = array.getJSONObject(i);
                String index = bean.getString("RESOURCECOLUMN_QUERYINDEX");
                // RESOURCECOLUMN_QUERYINDEX 如果为空则不保存
                if (StringUtil.isEmpty(index)) {
                    bean.remove("RESOURCECOLUMN_QUERYINDEX");
                }
            }
        }
        strData = array.toJSONString();
        List<DynaBean> updateList = commonService.doUpdateList("JE_CORE_RESOURCECOLUMN", strData, null, null);
        funcRelyonService.doUpdateList("JE_CORE_RESOURCECOLUMN", "", updateList);
        return updateList.size();
    }

    /**
     * 批量调整功能列信息
     * 1._ID 字段，自动勾选 加载项
     * 2.关联查询列的，要给勾选上加载项
     *
     * @param funcId
     */
    @Override
    public void batchSortOutColumnInfoByFuncId(String funcId) {
        if (StringUtil.isEmpty(funcId)) {
            return;
        }
        List<DynaBean> columns = metaService.select("JE_CORE_RESOURCECOLUMN",
                ConditionsWrapper.builder().eq("RESOURCECOLUMN_FUNCINFO_ID", funcId));
        for (DynaBean dynaBean : columns) {
            boolean isUpdate = false;
            String code = dynaBean.getStr("RESOURCECOLUMN_CODE");
            //关联查询列
            String quickquery = dynaBean.getStr("RESOURCECOLUMN_QUICKQUERY");
            //加载项
            String lazyload = dynaBean.getStr("RESOURCECOLUMN_LAZYLOAD");
            if (code.lastIndexOf("_ID") != -1 && !"1".equals(lazyload)) {
                isUpdate = true;
                dynaBean.setStr("RESOURCECOLUMN_LAZYLOAD", "1");
            }
            if ("1".equals(quickquery) && !"1".equals(lazyload)) {
                isUpdate = true;
                dynaBean.setStr("RESOURCECOLUMN_LAZYLOAD", "1");
            }
            if (isUpdate) {
                metaService.update(dynaBean);
            }
        }
    }

    @Transactional
    public void implModels(DynaBean funcInfo, List<Model> models, List<DbFieldVo> fieldVos) {
        String funId = funcInfo.getStr("JE_CORE_FUNCINFO_ID");
        String columnSql = "select RESOURCECOLUMN_CODE from JE_CORE_RESOURCECOLUMN where RESOURCECOLUMN_FUNCINFO_ID = '" + funId + "' and RESOURCECOLUMN_IFIMPL = 1";
        List<Map<String, Object>> columnResult = metaService.selectSql(columnSql);
        List<String> columnList = Lists.newArrayList();
        columnResult.forEach(eachMap -> {
            columnList.add((String) eachMap.get("RESOURCECOLUMN_CODE"));
        });

        String fieldSql = "select RESOURCEFIELD_CODE from JE_CORE_RESOURCEFIELD where RESOURCEFIELD_FUNCINFO_ID = '" + funId + "' and RESOURCEFIELD_IFIMPL = 1";
        List<Map<String, Object>> fieldResult = metaService.selectSql(fieldSql);
        List<String> fieldList = Lists.newArrayList();
        fieldResult.forEach(eachMap -> {
            fieldList.add((String) eachMap.get("RESOURCEFIELD_CODE"));
        });

        int order = 1;
        int fieldOrder = 1;
        List<String> newModelCodes = new ArrayList<String>();
        for (Model model : models) {
            //已经导入的字段中没有该字段
            if (!columnList.contains(model.getName())) {
                DynaBean rc = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
                rc.set("RESOURCECOLUMN_FUNCINFO_ID", funId);
                rc.set("RESOURCECOLUMN_CODE", model.getName());
                rc.set("RESOURCECOLUMN_NAME", model.getName());
                rc.set("RESOURCECOLUMN_NAME_EN", "");
                //将导入的字段标识为true
                rc.set("RESOURCECOLUMN_IFIMPL", "1");
                //是否快速查询 false
                rc.set("RESOURCECOLUMN_QUICKQUERY", "0");
                //默认序号为0
                rc.set("SY_ORDERINDEX", order);
                //设置默认宽度为80
                rc.set("RESOURCECOLUMN_WIDTH", "80");
                //是否拥有转换器conversion
                rc.set("RESOURCECOLUMN_CONVERSION", "0");
                //是否拥有转换器highlighting
                rc.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
                //是否超链接hyperLink
                rc.set("RESOURCECOLUMN_HYPERLINK", "0");
                //是否在字典中取
                rc.set("RESOURCECOLUMN_ISDD", "0");
                rc.set("RESOURCECOLUMN_QUERYTYPE", "no");
                if (fieldVos != null) {
                    for (DbFieldVo fieldVo : fieldVos) {
                        if (model.getName().equals(fieldVo.getName()) && "in".equals(fieldVo.getParamType())) {
                            rc.set("RESOURCECOLUMN_QUERYTYPE", "group");
                        }
                    }
                }
                //合并单元格      否
                rc.set("RESOURCECOLUMN_MERGE", "0");
                //锁定列  否
                rc.set("RESOURCECOLUMN_LOCKED", "");
                //多表头   否
                rc.set("RESOURCECOLUMN_MORECOLUMN", "0");
                //启用图标
                rc.set("RESOURCECOLUMN_ENABLEICON", "0");
                //启用分步加载
                rc.set("RESOURCECOLUMN_LAZYLOAD", "0");
                rc.set("RESOURCECOLUMN_ALIGN", "left");
                //排序
                rc.set("RESOURCECOLUMN_ORDER", "0");
                //索引
                rc.set("RESOURCECOLUMN_INDEX", "0");
                rc.set("RESOURCECOLUMN_HIDDEN", "0");
                //是否主键
                if (funcInfo.getStr("FUNCINFO_PKNAME", "").equals(model.getName())) {
                    rc.set("RESOURCECOLUMN_ISPK", "1");
                    rc.set("RESOURCECOLUMN_NAME", "主键ID");
                    rc.set("RESOURCECOLUMN_LAZYLOAD", "1");
                    rc.set("RESOURCECOLUMN_HIDDEN", "1");
                } else {
                    rc.set("RESOURCECOLUMN_ISPK", "0");
                }
                rc.set("RESOURCECOLUMN_SYSMODE", "CP");
                /**---------------------如果有初始化信息则使用初始化信息存储------------------------------------*/
                //是否可以为空  true
                rc.set("RESOURCECOLUMN_ALLOWBLANK", "1");
                //改动：判断是否包含NAME和CODE 张帅鹏
                rc.set("RESOURCECOLUMN_LAZYLOAD", "1");
                //字段类型  默认为无  不可编辑
                rc.set("RESOURCECOLUMN_XTYPE", "uxcolumn");
                //列表编辑   默认为否
                rc.set("RESOURCECOLUMN_ALLOWEDIT", "0");
                rc.set("RESOURCECOLUMN_DESCRIPTION", "");
                rc.set("SY_FLAG", "1");
                rc.set("RESOURCECOLUMN_SYSMODE", model.getType());
                buildModelCreateInfo(rc);
                metaService.insert(rc);
                order++;
            }
            //导入列表字段的同时，导入表单字段
            if (!fieldList.contains(model.getName())) {
//	    		ExtFieldType.INT
                DynaBean rf = new DynaBean("JE_CORE_RESOURCEFIELD", false);
                rf.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
                rf.set("RESOURCEFIELD_FUNCINFO_ID", funId);
                rf.set("RESOURCEFIELD_CODE", model.getName());
                rf.set("RESOURCEFIELD_NAME", model.getName());
                rf.set("RESOURCEFIELD_NAME_EN", "");
                //将导入的字段标识为true
                rf.set("RESOURCEFIELD_IFIMPL", "1");
                //是否可用  true
                rf.set("RESOURCEFIELD_DISABLED", "1");
                //是否可以为空  true
                rf.set("RESOURCEFIELD_ALLOWBLANK", "1");
                //可选可编辑   默认否
                rf.set("RESOURCEFIELD_EDITABLE", "0");
                //所占列数
                rf.set("RESOURCEFIELD_COLSPAN", 1);
                String columnType = model.getType();
                //数值型
                if (ExtFieldType.INT.equals(columnType) || ExtFieldType.FLOAT.equals(columnType)) {
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "numberfield");
                    if (ExtFieldType.FLOAT.equals(columnType)) {
                        //默认小数精度
                        rf.set("RESOURCEFIELD_CONFIGINFO", "2");
                    } else if ("NUMBER".equals(columnType)) {
                        //默认小数精度
                        rf.set("RESOURCEFIELD_CONFIGINFO", "0");
                    }
                } else {
                    //字段类型   默认为文本框
                    rf.set("RESOURCEFIELD_XTYPE", "textfield");
                }
                //是否只读
                rf.set("RESOURCEFIELD_READONLY", "1");
                rf.set("RESOURCEFIELD_HIDDEN", "0");
                //根据列的添加方式 字典添加，表添加来快速初始化功能配置项
                //默认序号为0
                rf.set("SY_ORDERINDEX", fieldOrder);
                //所占行数
                rf.set("RESOURCEFIELD_ROWSPAN", 1);
                rf.set("RESOURCEFIELD_SYSMODE", "CP");
                //是否主键
                if (funcInfo.getStr("FUNCINFO_PKNAME", "").equals(model.getName())) {
                    rf.set("RESOURCEFIELD_ISPK", "1");
                } else {
                    rf.set("RESOURCEFIELD_ISPK", "0");
                }
                if ("1".equals(rf.getStr("RESOURCEFIELD_HIDDEN"))) {
                    rf.set("RESOURCEFIELD_FIELDLAZY", "0");
                } else {
                    rf.set("RESOURCEFIELD_FIELDLAZY", "1");
                }
                rf.set("RESOURCEFIELD_DESCRIPTION", "");
                rf.set("SY_FLAG", "1");
                rf.set("RESOURCEFIELD_SYSMODE", model.getType());
                buildModelCreateInfo(rf);
                metaService.insert(rf);
                fieldOrder++;
            }
            newModelCodes.add(model.getName());
        }
        metaService.executeSql(" DELETE FROM JE_CORE_RESOURCECOLUMN WHERE RESOURCECOLUMN_FUNCINFO_ID='" + funId + "' AND SY_FLAG='1' AND RESOURCECOLUMN_CODE NOT IN (" + StringUtil.buildArrayToString(ArrayUtils.getArray(newModelCodes)) + ")");
        metaService.executeSql(" DELETE FROM JE_CORE_RESOURCEFIELD WHERE RESOURCEFIELD_FUNCINFO_ID='" + funId + "' AND SY_FLAG='1' AND RESOURCEFIELD_CODE NOT IN (" + StringUtil.buildArrayToString(ArrayUtils.getArray(newModelCodes)) + ")");
        DynaBean rField = metaService.selectOne("JE_CORE_RESOURCEFIELD", ConditionsWrapper.builder()
                .eq("RESOURCEFIELD_FUNCINFO_ID", funId)
                .eq("RESOURCEFIELD_IFIMPL", "1")
                .eq("RESOURCEFIELD_CODE", "__child"));
        if (rField == null) {
            DynaBean rf = new DynaBean("JE_CORE_RESOURCEFIELD", false);
            rf.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
            rf.set("RESOURCEFIELD_XTYPE", "child");
            //将导入的字段标识为true
            rf.set("RESOURCEFIELD_IFIMPL", "1");
            //是否可用  true
            rf.set("RESOURCEFIELD_DISABLED", "1");
            //是否可以为空  true
            rf.set("RESOURCEFIELD_ALLOWBLANK", "1");
            //默认序号为0
            rf.set("SY_ORDERINDEX", fieldOrder);
            //所占行数
            rf.set("RESOURCEFIELD_ROWSPAN", 1);
            //所占列数
            rf.set("RESOURCEFIELD_COLSPAN", 1);
            //可选可编辑   默认否
            rf.set("RESOURCEFIELD_EDITABLE", "0");
            rf.set("RESOURCEFIELD_HIDDEN", "1");
            rf.set("RESOURCEFIELD_CODE", "__child");
            rf.set("RESOURCEFIELD_NAME", "子功能");
            rf.set("RESOURCEFIELD_NAME_EN", "Sub Func");
            rf.set("RESOURCEFIELD_FUNCINFO_ID", funId);
            rf.set("RESOURCEFIELD_SYSMODE", "CP");
            buildModelCreateInfo(rf);
            metaService.insert(rf);
        }
        //默认给列表添加一个action列
        DynaBean actionColumn = metaService.selectOne("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder().eq("RESOURCECOLUMN_FUNCINFO_ID", funId).eq("RESOURCECOLUMN_IFIMPL", "1").eq("RESOURCECOLUMN_XTYPE", "actioncolumn"));
        if (actionColumn == null) {
            actionColumn = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
            actionColumn.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
            actionColumn.set("RESOURCECOLUMN_FUNCINFO_ID", funId);
            actionColumn.set("RESOURCECOLUMN_CODE", "actioncolumn_1");
            actionColumn.set("RESOURCECOLUMN_NAME", "操作");
            actionColumn.set("RESOURCECOLUMN_NAME_EN", "Action");
            //将导入的字段标识为true
            actionColumn.set("RESOURCECOLUMN_IFIMPL", "1");
            //是否快速查询 false
            actionColumn.set("RESOURCECOLUMN_QUICKQUERY", "0");
            //默认序号为0
            actionColumn.set("SY_ORDERINDEX", order);
            //设置默认宽度为80
            actionColumn.set("RESOURCECOLUMN_WIDTH", "100");
            //是否拥有转换器conversion
            actionColumn.set("RESOURCECOLUMN_CONVERSION", "0");
            //是否拥有转换器highlighting
            actionColumn.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
            //是否超链接hyperLink
            actionColumn.set("RESOURCECOLUMN_HYPERLINK", "0");
            //是否在字典中取
            actionColumn.set("RESOURCECOLUMN_ISDD", "0");
            //合并单元格      否
            actionColumn.set("RESOURCECOLUMN_MERGE", "0");
            //锁定列  否
            actionColumn.set("RESOURCECOLUMN_LOCKED", "");
            //多表头   否
            actionColumn.set("RESOURCECOLUMN_MORECOLUMN", "0");
            //启用图标
            actionColumn.set("RESOURCECOLUMN_ENABLEICON", "0");
            actionColumn.set("RESOURCECOLUMN_ALIGN", "center");
            actionColumn.set("RESOURCECOLUMN_ISPK", "0");
            actionColumn.set("RESOURCECOLUMN_ORDER", "0");
            actionColumn.set("RESOURCECOLUMN_INDEX", "0");
            //是否可以为空  true
            actionColumn.set("RESOURCECOLUMN_ALLOWBLANK", "1");
            actionColumn.set("RESOURCECOLUMN_QUERYTYPE", "no");
            actionColumn.set("RESOURCECOLUMN_HIDDEN", "1");
            actionColumn.set("RESOURCECOLUMN_OTHERCONFIG", "{\"headerAlign\":\"center\"}");
            actionColumn.set("RESOURCECOLUMN_LAZYLOAD", "0");
            //字段类型  默认为无  不可编辑
            actionColumn.set("RESOURCECOLUMN_XTYPE", "actioncolumn");
            //列表编辑   默认为否
            actionColumn.set("RESOURCECOLUMN_ALLOWEDIT", "0");
            buildModelCreateInfo(actionColumn);
            metaService.insert(actionColumn);
        }
        //默认给列表添加一个序号列
        DynaBean rownumberer = metaService.selectOne("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder()
                .eq("RESOURCECOLUMN_FUNCINFO_ID", funId)
                .eq("RESOURCECOLUMN_IFIMPL", "1").eq("RESOURCECOLUMN_XTYPE", "rownumberer"));
        if (rownumberer == null) {
            rownumberer = new DynaBean("JE_CORE_RESOURCECOLUMN", false);
            rownumberer.set(BeanService.KEY_PK_CODE, "JE_CORE_RESOURCEFIELD_ID");
            rownumberer.set("RESOURCECOLUMN_FUNCINFO_ID", funId);
            rownumberer.set("RESOURCECOLUMN_CODE", "rownumberer_1");
            rownumberer.set("RESOURCECOLUMN_NAME", "No.");
            rownumberer.set("RESOURCECOLUMN_NAME_EN", "No.");
            //将导入的字段标识为true
            rownumberer.set("RESOURCECOLUMN_IFIMPL", "1");
            //是否快速查询 false
            rownumberer.set("RESOURCECOLUMN_QUICKQUERY", "0");
            //默认序号为0
            rownumberer.set("SY_ORDERINDEX", 0);
            //设置默认宽度为80
            rownumberer.set("RESOURCECOLUMN_WIDTH", "50");
            //是否拥有转换器conversion
            rownumberer.set("RESOURCECOLUMN_CONVERSION", "0");
            //是否拥有转换器highlighting
            rownumberer.set("RESOURCECOLUMN_HIGHLIGHTING", "0");
            //是否超链接hyperLink
            rownumberer.set("RESOURCECOLUMN_HYPERLINK", "0");
            //是否在字典中取
            rownumberer.set("RESOURCECOLUMN_ISDD", "0");
            //合并单元格      否
            rownumberer.set("RESOURCECOLUMN_MERGE", "0");
            //锁定列  否
            rownumberer.set("RESOURCECOLUMN_LOCKED", "");
            //多表头   否
            rownumberer.set("RESOURCECOLUMN_MORECOLUMN", "0");
            //启用图标
            rownumberer.set("RESOURCECOLUMN_ENABLEICON", "0");
            rownumberer.set("RESOURCECOLUMN_ALIGN", "center");
            rownumberer.set("RESOURCECOLUMN_ISPK", "0");
            rownumberer.set("RESOURCECOLUMN_ORDER", "0");
            rownumberer.set("RESOURCECOLUMN_INDEX", "0");
            //是否可以为空  true
            rownumberer.set("RESOURCECOLUMN_ALLOWBLANK", "1");
            rownumberer.set("RESOURCECOLUMN_QUERYTYPE", "no");
            rownumberer.set("RESOURCECOLUMN_HIDDEN", "0");
            //字段类型  默认为无  不可编辑
            rownumberer.set("RESOURCECOLUMN_XTYPE", "rownumberer");
            //列表编辑   默认为否
            rownumberer.set("RESOURCECOLUMN_ALLOWEDIT", "0");
            actionColumn.set("RESOURCECOLUMN_LAZYLOAD", "0");
            buildModelCreateInfo(rownumberer);
            metaService.insert(rownumberer);
        }
    }

}
