/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func;

import com.je.common.base.DynaBean;

import java.util.List;

/**
 * 功能依赖软连接处理
 * @author zhangshuaipeng
 *
 */
public interface MetaFuncRelyonService {

    /**
     * 保存依赖关系
     * @param funcInfo 目标功能对象
     * @param type 	         类型     母体/字体
     * @param funcId   功能外键
     */
    void saveRelyon(DynaBean funcInfo, String type, String funcId);

    /**
     * 监控保存时候触发软连接操作
     * @param tableCode 表编码
     * @param funcId TODO未处理
     * @param dynaBean TODO未处理
     */
    void doSave(String tableCode, String funcId, DynaBean dynaBean);

    /**
     * 监控修改时触发软连接操作
     * @param tableCode 表编码
     * @param funcId TODO未处理
     * @param dynaBean TODO未处理
     */
    void doUpdate(String tableCode, String funcId, DynaBean dynaBean);

    /**
     * 得到修改的值，完成局部修改
     * @param tableCode 表编码
     * @param newBean 新数据
     * @param oldBean 老数据
     * @return
     */
    DynaBean getUpdateBean(String tableCode, DynaBean newBean, DynaBean oldBean);

    /**
     * 监控列表更新触发软连接操作
     * @param tableCode 表编码
     * @param funcId  TODO未处理
     * @param beans TODO未处理
     */
    void doUpdateList(String tableCode, String funcId, List<DynaBean> beans);

    /**
     * 监控删除时触发软连接操作
     * @param tableCode 变编码
     * @param funcId TODO未处理
     * @param ids ID集合
     * @param beans TODO未处理
     */
    void doRemove(String tableCode, String funcId, String ids, List<DynaBean> beans);

    /**
     * 监控字段导入时触发软连接操作
     * @param funcId  TODO未处理
     */
    void doImpl(String funcId);

    /**
     * 监控与列表同步时触发软连接操作
     * @param funcId  TODO未处理
     */
    void doSyncColumn(String funcId);

    /**
     * 监控与表单同步时触发软连接操作
     * @param funcId  TODO未处理
     */
    void doSyncField(String funcId);

    /**
     * 移除依赖目标
     * @param tableCode 表编码
     * @param softFunc TODO未处理
     * @param beans TODO未处理
     */
    void removeTarget(String tableCode, DynaBean softFunc, List<DynaBean> beans);

    /**
     * 更新依赖目标
     * @param tableCode 变编码
     * @param softFunc TODO未处理
     * @param newBean TODO未处理
     */
    void updateTarget(String tableCode, DynaBean softFunc, DynaBean newBean);

    /**
     * 添加依赖目标
     * @param tableCode 变编码
     * @param softFunc TODO未处理
     * @param dynaBean TODO未处理
     */
    void insertTarget(String tableCode, DynaBean softFunc, DynaBean dynaBean);

    /**
     * 获取依赖的子体功能对象
     * @param funcId TODO未处理
     * @return
     */
    List<DynaBean> getRelyons(String funcId);

    DynaBean checkCode(String strData);
}
