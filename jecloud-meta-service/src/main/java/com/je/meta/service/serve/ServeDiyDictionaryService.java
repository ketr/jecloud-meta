package com.je.meta.service.serve;

import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.model.dd.DicInfoVo;

public interface ServeDiyDictionaryService {

    /**
     * 获取不同类型的服务，平台/方案类服务
     * @param dicInfoVo
     * @return
     */
    JSONTreeNode getTypedTreeServices(DicInfoVo dicInfoVo);

}
