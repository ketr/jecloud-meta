/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dictionary.impl;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.dd.DDType;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.dd.DicInfoCache;
import com.je.meta.service.dictionary.MetaDictionaryTreeService;
import com.je.meta.service.dictionary.MetaTreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MetaTreeServiceImpl implements MetaTreeService {

    @Autowired
    @Qualifier("customDictionaryTreeService")
    private MetaDictionaryTreeService customDictionaryTreeService;
    @Autowired
    @Qualifier("dynaTreeDictionaryTreeService")
    private MetaDictionaryTreeService dynaTreeDictionaryTreeService;
    @Autowired
    @Qualifier("sqlDictionaryTreeService")
    private MetaDictionaryTreeService sqlDictionaryTreeService;
    @Autowired
    @Qualifier("sqlTreeDictionaryTreeService")
    private MetaDictionaryTreeService sqlTreeDictionaryTreeService;
    @Autowired
    @Qualifier("metaListAndTreeDictionaryTreeService")
    private MetaDictionaryTreeService metaListAndTreeDictionaryTreeService;
    @Autowired
    private MetaService metaService;
    @Autowired
    private DicInfoCache dicInfoCache;

    @Override
    public List<JSONTreeNode> loadAsynTree(JSONObject obj, boolean en, boolean onlyItem) {
        String ddCode = obj.getString("ddCode");
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode).apply("and (SY_STATUS = '' or SY_STATUS = '1' or SY_STATUS is NULL)"));
        }
        if (dictionary == null){
            return new ArrayList<JSONTreeNode>();
        }
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        String productCode = dictionary.getStr("SY_PRODUCT_CODE");
        List<JSONTreeNode> result = null;
        if (DDType.LIST.equalsIgnoreCase(ddType) || DDType.TREE.equalsIgnoreCase(ddType)) {
            result = metaListAndTreeDictionaryTreeService.loadAsynTree(obj, productCode, en, onlyItem);
        } else if (DDType.CUSTOM.equalsIgnoreCase(ddType)) {
            result = customDictionaryTreeService.loadAsynTree(obj, productCode, en, onlyItem);
        } else if (DDType.SQL.equalsIgnoreCase(ddType)) {
            result = sqlDictionaryTreeService.loadAsynTree(obj, productCode, en, onlyItem);
        } else if (DDType.SQL_TREE.equalsIgnoreCase(ddType)) {
            result = sqlTreeDictionaryTreeService.loadAsynTree(obj, productCode, en, onlyItem);
        } else if (DDType.DYNA_TREE.equalsIgnoreCase(ddType)) {
            result = dynaTreeDictionaryTreeService.loadAsynTree(obj, productCode, en, onlyItem);
        } else {
            throw new PlatformException("不存在的数据字典类型:" + ddType, PlatformExceptionEnum.UNKOWN_ERROR);
        }
        return result;
    }

    @Override
    public List<JSONTreeNode> loadSyncTree(JSONObject obj, boolean en, boolean onlyItem) {
        String ddCode = obj.getString("ddCode");
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder()
                    .eq("DICTIONARY_DDCODE", ddCode).apply("and (SY_STATUS = '' or SY_STATUS = '1' or SY_STATUS is NULL)"));
        }
        if(dictionary == null){
           return new ArrayList<JSONTreeNode>();
        }
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        String productCode = dictionary.getStr("SY_PRODUCT_CODE");
        List<JSONTreeNode> result;
        if (DDType.LIST.equalsIgnoreCase(ddType) || DDType.TREE.equalsIgnoreCase(ddType)) {
            result = metaListAndTreeDictionaryTreeService.loadSyncTree(obj, productCode, en, onlyItem);
        } else if (DDType.CUSTOM.equalsIgnoreCase(ddType)) {
            result = customDictionaryTreeService.loadSyncTree(obj, productCode, en, onlyItem);
        } else if (DDType.SQL.equalsIgnoreCase(ddType)) {
            result = sqlDictionaryTreeService.loadSyncTree(obj, productCode, en, onlyItem);
        } else if (DDType.SQL_TREE.equalsIgnoreCase(ddType)) {
            result = sqlTreeDictionaryTreeService.loadSyncTree(obj, productCode, en, onlyItem);
        } else if (DDType.DYNA_TREE.equalsIgnoreCase(ddType)) {
            result = dynaTreeDictionaryTreeService.loadSyncTree(obj, productCode, en, onlyItem);
        } else {
            throw new PlatformException("不存在的数据字典类型:" + ddType, PlatformExceptionEnum.UNKOWN_ERROR);
        }
        return result;
    }

    @Override
    public List<JSONTreeNode> findAsyncNodes(JSONObject obj, String queryType, String value, boolean en, boolean onlyItem) {
        String ddCode = obj.getString("ddCode");
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode).apply("and (SY_STATUS = '' or SY_STATUS = '1' or SY_STATUS is NULL)"));
        }
        if(dictionary == null){
            return new ArrayList<JSONTreeNode>();
        }
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");

        List<JSONTreeNode> result;
        if (DDType.LIST.equalsIgnoreCase(ddType) || DDType.TREE.equalsIgnoreCase(ddType)) {
            result = metaListAndTreeDictionaryTreeService.findAsyncNodes(dictionary, obj, queryType, value, en, onlyItem);
        } else if (DDType.CUSTOM.equalsIgnoreCase(ddType)) {
            result = customDictionaryTreeService.findAsyncNodes(dictionary, obj, queryType, value, en, onlyItem);
        } else if (DDType.SQL.equalsIgnoreCase(ddType)) {
            result = sqlDictionaryTreeService.findAsyncNodes(dictionary, obj, queryType, value, en, onlyItem);
        } else if (DDType.SQL_TREE.equalsIgnoreCase(ddType)) {
            result = sqlTreeDictionaryTreeService.findAsyncNodes(dictionary, obj, queryType, value, en, onlyItem);
        } else if (DDType.DYNA_TREE.equalsIgnoreCase(ddType)) {
            result = dynaTreeDictionaryTreeService.findAsyncNodes(dictionary, obj, queryType, value, en, onlyItem);
        } else {
            throw new PlatformException("不存在的数据字典类型:" + ddType, PlatformExceptionEnum.UNKOWN_ERROR);
        }
        return result;
    }

    @Override
    public List<JSONTreeNode> loadLinkTree(String ddCode, String parentId, String parentCode, String rootId, String paramStr, boolean en, Query query) {
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode).apply("and (SY_STATUS = '' or SY_STATUS = '1' or SY_STATUS is NULL)"));
        }
        if(dictionary == null){
            return new ArrayList<JSONTreeNode>();
        }
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        List<JSONTreeNode> result;
        if (DDType.LIST.equalsIgnoreCase(ddType) || DDType.TREE.equalsIgnoreCase(ddType)) {
            result = metaListAndTreeDictionaryTreeService.loadLinkTree(ddCode, parentId, parentCode, rootId, paramStr, en, query);
        } else if (DDType.CUSTOM.equalsIgnoreCase(ddType)) {
            result = customDictionaryTreeService.loadLinkTree(ddCode, parentId, parentCode, rootId, paramStr, en, query);
        } else if (DDType.SQL.equalsIgnoreCase(ddType)) {
            result = sqlDictionaryTreeService.loadLinkTree(ddCode, parentId, parentCode, rootId, paramStr, en, query);
        } else if (DDType.SQL_TREE.equalsIgnoreCase(ddType)) {
            result = sqlTreeDictionaryTreeService.loadLinkTree(ddCode, parentId, parentCode, rootId, paramStr, en, query);
        } else if (DDType.DYNA_TREE.equalsIgnoreCase(ddType)) {
            result = dynaTreeDictionaryTreeService.loadLinkTree(ddCode, parentId, parentCode, rootId, paramStr, en, query);
        } else {
            throw new PlatformException("不存在的数据字典类型:" + ddType, PlatformExceptionEnum.UNKOWN_ERROR);
        }
        return result;
    }

}
