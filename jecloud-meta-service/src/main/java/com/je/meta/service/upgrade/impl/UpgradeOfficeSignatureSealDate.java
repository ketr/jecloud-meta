package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打包文档签字盖章
 */
@Service("upgradeOfficeSignatureSealDate")
public class UpgradeOfficeSignatureSealDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeOfficeSignatureSealDate() {
        super(UpgradeResourcesEnum.OFFICE_SIGNATURE_SEAL);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> list = getSourceIds(upgradeBean);
        if (list == null || list.size() == 0) {
            return;
        }
        packageResult.setProductOfficeSignatureSeal(packageResources(packageResult, list));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductOfficeSignatureSeal();
    }

}
