package com.je.meta.service.project;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Joiner;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.QueryInfo;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.dictionary.DictionaryItemRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProjectResourceService {

    private static final Logger log = LoggerFactory.getLogger(ProjectResourceService.class);
    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private DictionaryItemRpcService dictionaryItemRpcService;

    public String requireResourceIds() {
        List<DynaBean> existBeanList = metaService.select("JE_RBAC_PROJECT_RESOURCE", ConditionsWrapper.builder());
        if (existBeanList == null || existBeanList.isEmpty()) {
            return null;
        }
        List<String> resourceIds = new ArrayList<>();
        for (DynaBean bean : existBeanList) {
            resourceIds.add(bean.get("RESOURCE_DATA_ID").toString());
        }
        return Joiner.on(",").join(resourceIds);
    }

    public void doImport(JSONArray dataArray) {
        if (dataArray == null || dataArray.isEmpty()) {
            return;
        }

        List<DynaBean> existBeanList = metaService.select("JE_RBAC_PROJECT_RESOURCE", ConditionsWrapper.builder());
        if (existBeanList != null) {
            List<JSONObject> list = new ArrayList<>();
            for (int i = 0; i < dataArray.size(); i++) {
                for (DynaBean eachBean : existBeanList) {
                    if (eachBean.get("RESOURCE_DATA_ID").equals(dataArray.getJSONObject(i).getString("id"))) {
                        list.add(dataArray.getJSONObject(i));
                    }
                }
            }
            dataArray.removeAll(list);
        }

        List<JSONTreeNode> menuTypeItemList = dictionaryItemRpcService.getDdItems("JE_MENU_TYPE", new HashMap<>(), new QueryInfo(), false);
        Map<String, String> menuTypeMap = new HashMap<>();
        for (JSONTreeNode menuTypeItem : menuTypeItemList) {
            menuTypeMap.put(menuTypeItem.getCode(), menuTypeItem.getText());
        }
        log.info("menuTypeMap:{}", JSON.toJSONString(menuTypeMap));
        DynaBean eachBean;
        for (int i = 0; i < dataArray.size(); i++) {
            eachBean = new DynaBean("JE_RBAC_PROJECT_RESOURCE", false);
            eachBean.set("RESOURCE_TYPE_CODE", dataArray.getJSONObject(i).getString("nodeInfoType"));
            eachBean.set("RESOURCE_TYPE_NAME", menuTypeMap.get(dataArray.getJSONObject(i).getString("nodeInfoType")));
            eachBean.set("RESOURCE_DATA_ID", dataArray.getJSONObject(i).getString("id"));
            eachBean.set("RESOURCE_DATA_NAME", dataArray.getJSONObject(i).getString("name"));
            commonService.buildModelCreateInfo(eachBean);
            metaService.insert(eachBean);
        }
    }

}
