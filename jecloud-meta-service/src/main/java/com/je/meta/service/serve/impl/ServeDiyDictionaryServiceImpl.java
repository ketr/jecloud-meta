package com.je.meta.service.serve.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.TreeUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.model.dd.DicInfoVo;
import com.je.meta.service.serve.ServeDiyDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("serveDiyDictionaryService")
public class ServeDiyDictionaryServiceImpl implements ServeDiyDictionaryService {

    @Autowired
    private MetaService metaService;

    @Override
    public JSONTreeNode getTypedTreeServices(DicInfoVo dicInfoVo) {
        JSONTreeNode rootNode = TreeUtil.buildRootNode();
        JSONTreeNode platformNode = TreeUtil.buildTreeNode("platform","平台服务","platform",null,"GENERAL",null,rootNode.getId());
        JSONTreeNode planNode = TreeUtil.buildTreeNode("plan","方案服务","plan",null,"GENERAL",null,rootNode.getId());

        List<DynaBean> serviceBeanList = metaService.select("JE_PRODUCT_MANAGE", ConditionsWrapper.builder());
        JSONTreeNode eachNode;
        for (int i = 0; serviceBeanList != null && i < serviceBeanList.size(); i++) {
            if("2".equals(serviceBeanList.get(i).getStr("PRODUCT_TYPE"))){
                eachNode = TreeUtil.buildTreeNode(serviceBeanList.get(i).getStr("JE_PRODUCT_MANAGE_ID"),
                        serviceBeanList.get(i).getStr("PRODUCT_NAME"),
                        serviceBeanList.get(i).getStr("PRODUCT_CODE"),
                        null,
                        "LEAF",
                        null,
                        platformNode.getId());
                platformNode.getChildren().add(eachNode);
            }else {
                eachNode = TreeUtil.buildTreeNode(serviceBeanList.get(i).getStr("JE_PRODUCT_MANAGE_ID"),
                        serviceBeanList.get(i).getStr("PRODUCT_NAME"),
                        serviceBeanList.get(i).getStr("PRODUCT_CODE"),
                        null,
                        "LEAF",
                        null,
                        planNode.getId());
                planNode.getChildren().add(eachNode);
            }

        }

        rootNode.getChildren().add(platformNode);
        rootNode.getChildren().add(planNode);
        return rootNode;
    }

}
