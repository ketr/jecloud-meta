/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.Condition;
import com.je.common.base.mapper.query.Order;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.QueryStrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class QueryStrategyServiceImpl implements QueryStrategyService {

    @Autowired
    protected MetaService metaService;
    @Autowired
    protected CommonService commonService;

    @Override
    public void doSave(DynaBean dynaBean) {
        List<DynaBean> list = new ArrayList<>();
        list = metaService.select("JE_CORE_QUERYSTRATEGY", ConditionsWrapper.builder().eq("QUERYSTRATEGY_FUNCINFO_ID", dynaBean.getStr("QUERYSTRATEGY_FUNCINFO_ID")));
        commonService.buildModelCreateInfo(dynaBean);
        dynaBean.put("SY_ORDERINDEX", list.size() + 1);
        metaService.insert(dynaBean);
    }

    @Override
    public List<Map<String, Object>> getDataList(BaseMethodArgument param, DynaBean dynaBean) {
        Query query = param.buildQuery();
        List<Order> orderList = query.getOrder();
        List<Condition> conditionList = query.getCustom();
        ConditionsWrapper select = ConditionsWrapper.builder();
        if (conditionList != null && conditionList.size() > 0) {
            for (Condition condition : conditionList) {
                String type = condition.getType();
                if ("=".equals(type)) {
                    select.eq(condition.getCode(), condition.getValue());
                }
                if ("isNull".equals(type)) {
                    select.isNull(condition.getCode());
                }

            }

        }
        if (orderList != null && orderList.size() > 0) {
            for (Order order : orderList) {
                String type = order.getType();
                if ("asc".equals(type)) {
                    select.orderByAsc(order.getCode());
                }
                if ("desc".equals(type)) {
                    select.orderByDesc(order.getCode());
                }
            }
        }
        List<DynaBean> list = metaService.select("JE_CORE_QUERYSTRATEGY", select);
        return dynaBeanToMap(list);
    }

    private List<Map<String, Object>> dynaBeanToMap(List<DynaBean> list) {
        List<Map<String, Object>> listMap = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (DynaBean dynaBean : list) {
                listMap.add(dynaBean.getValues());
            }
        }
        return listMap;
    }
}
