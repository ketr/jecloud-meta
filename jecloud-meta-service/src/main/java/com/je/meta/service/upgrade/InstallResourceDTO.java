package com.je.meta.service.upgrade;

import com.je.common.base.DynaBean;

import java.util.List;

public class InstallResourceDTO {
    /**
     * 升级包数据主键
     */
    private String upgradeInstallId;
    /**
     * 导入数据list
     */
    private List<DynaBean> list;
    /**
     * 产品code
     */
    private String productCode;
    /**
     * 确认唯一性字段编码 用于判读是新增还是修改
     */
    private String identifier;
    /**
     * 导入数据表code
     */
    private String tableCode;
    /**
     * 是否需要记录日志
     */
    private boolean logAdd;
    /**
     * 记录日志类型
     */
    private String logType;
    /**
     * 记录日志类型名称
     */
    private String logName;
    /**
     * 记录日志 资源名称code
     */
    private String logContentName;
    /**
     * 记录日志 资源编码code
     */
    private String logContentCode;


    // 私有构造函数，防止直接实例化
    private InstallResourceDTO(Builder builder) {
        this.upgradeInstallId = builder.upgradeInstallId;
        this.list = builder.list;
        this.productCode = builder.productCode;
        this.identifier = builder.identifier;
        this.tableCode = builder.tableCode;
        this.logType = builder.logType;
        this.logName = builder.logName;
        this.logContentName = builder.logContentName;
        this.logContentCode = builder.logContentCode;
        this.logAdd = builder.logAdd;
    }

    // 静态内部类 Builder
    public static class Builder {
        private String upgradeInstallId;
        private List<DynaBean> list;
        private String productCode;
        private String identifier;
        private String tableCode;
        private String logType;
        private String logName;
        private String logContentName;
        private String logContentCode;
        private boolean logAdd;

        public Builder() {
        }

        public Builder upgradeInstallId(String upgradeInstallId) {
            this.upgradeInstallId = upgradeInstallId;
            return this;
        }

        public Builder list(List<DynaBean> list) {
            this.list = list;
            return this;
        }

        public Builder productCode(String productCode) {
            this.productCode = productCode;
            return this;
        }

        public Builder identifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Builder tableCode(String tableCode) {
            this.tableCode = tableCode;
            return this;
        }

        public Builder logType(String logType) {
            this.logType = logType;
            return this;
        }

        public Builder logName(String logName) {
            this.logName = logName;
            return this;
        }

        public Builder logContentName(String logContentName) {
            this.logContentName = logContentName;
            return this;
        }

        public Builder logContentCode(String logContentCode) {
            this.logContentCode = logContentCode;
            return this;

        }

        public Builder logAdd(boolean logAdd) {
            this.logAdd = logAdd;
            return this;
        }

        public InstallResourceDTO build() {
            return new InstallResourceDTO(this);
        }
    }

    // Getter方法
    public String getUpgradeInstallId() {
        return upgradeInstallId;
    }

    public List<DynaBean> getList() {
        return list;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getTableCode() {
        return tableCode;
    }

    public String getLogType() {
        return logType;
    }

    public String getLogName() {
        return logName;
    }

    public String getLogContentName() {
        return logContentName;
    }

    public String getLogContentCode() {
        return logContentCode;
    }

    public boolean isLogAdd() {
        return logAdd;
    }

    // 重写toString方法方便调试
    @Override
    public String toString() {
        return "InstallResourceDTO{" +
                "upgradeInstallId='" + upgradeInstallId + '\'' +
                ", list=" + list +
                ", productCode='" + productCode + '\'' +
                ", identifier='" + identifier + '\'' +
                ", tableCode='" + tableCode + '\'' +
                '}';
    }
}
