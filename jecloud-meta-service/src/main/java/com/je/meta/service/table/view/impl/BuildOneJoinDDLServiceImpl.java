/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.view.impl;

import com.je.common.base.util.StringUtil;
import com.je.meta.model.view.ViewColumn;
import com.je.meta.service.table.view.BuildAssociationDDLService;
import com.je.meta.service.table.Entry;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("oneJoinDDLService")
public class BuildOneJoinDDLServiceImpl implements BuildAssociationDDLService {
    @Override
    public String createDdl(String template, List<ViewColumn> associationList) {
        ViewColumn viewColumn = associationList.get(0);
        String mainTableCode = viewColumn.getMainTableCode();
        String targetTableCode = viewColumn.getTargetTableCode();
        String mainColumn = viewColumn.getMainColumn();
        String targetColumn = viewColumn.getTargetColumn();
        String formula = viewColumn.getFormula();
        List<Entry> list = new ArrayList<>();
        list.add(Entry.build("tableCodes",mainTableCode));
        list.add(Entry.build("targetColumn",targetColumn));
        list.add(Entry.build("targetTableCode",targetTableCode));
        list.add(Entry.build("mainTableCode",mainTableCode));
        if(StringUtil.isNotEmpty(mainTableCode)&&StringUtil.isNotEmpty(mainColumn)){
            list.add(Entry.build("leftDot","."));
        }
        list.add(Entry.build("mainColumn",mainColumn));
        if(StringUtil.isNotEmpty(targetTableCode)&&StringUtil.isNotEmpty(targetColumn)){
            list.add(Entry.build("rightDot","."));
        }
        list.add(Entry.build("formula",formula));
        Entry[] entries = list.toArray(new Entry[list.size()]);
        //模板赋值
        return formatTemplate(template,
                entries);
    }
}
