package com.je.meta.service.table;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractTableColumnService {

    protected List<String> workList = Arrays.asList(
            "SY_STARTEDUSER",
            "SY_STARTEDUSERNAME",
            "SY_APPROVEDUSERS",
            "SY_APPROVEDUSERNAMES",
            "SY_PREAPPROVUSERS",
            "SY_LASTFLOWINFO",
            "SY_PREAPPROVUSERNAMES",
            "SY_LASTFLOWUSER",
            "SY_LASTFLOWUSERID",
            "SY_WFWARN",
            "SY_WARNFLAG",
            "SY_CURRENTTASK"
    );

    protected List<String> createList = Arrays.asList(
            "SY_CREATEORGID",
            "SY_CREATEORGNAME",
            "SY_CREATETIME",
            "SY_CREATEUSERID",
            "SY_CREATEUSERNAME"
    );

    protected List<String> updateList = Arrays.asList(
            "SY_MODIFYORGID",
            "SY_MODIFYORGNAME",
            "SY_MODIFYUSERID",
            "SY_MODIFYUSERNAME",
            "SY_MODIFYTIME"
    );

    /**
     * 辅助函数初始化列(传址操作)
     *
     * @param tableColumn
     * @param code
     * @param name
     * @param orderIndex
     */
    protected void buildColumn(DynaBean tableColumn, String code, String name, int orderIndex) {
        tableColumn.set("TABLECOLUMN_CODE", code);
        tableColumn.set("TABLECOLUMN_NAME", name);
        tableColumn.set("TABLECOLUMN_TREETYPE", "NORMAL");
        if ("SY_ORDERINDEX".equals(code)) {
            tableColumn.set("TABLECOLUMN_TYPE", "NUMBER");
            tableColumn.set("TABLECOLUMN_LENGTH", "20");
        } else {
            tableColumn.set("TABLECOLUMN_TYPE", "VARCHAR255");
        }
        tableColumn.set("SY_ORDERINDEX", orderIndex);
        tableColumn.set("TABLECOLUMN_ISNULL", "1");
        tableColumn.set("TABLECOLUMN_CLASSIFY", "SYS");
    }

    /**
     * 获取删除表字段，对应视图中存在此字段的ID列表
     * 此逻辑是判断资源表视图列中字段值TABLECOLUMN_VIEWCONFIG
     *
     * @param tableCode   资源表编码
     * @param columns     需要删除的列
     * @param viewColumns 视图中存在的列
     * @return
     */
    protected List<DynaBean> acquireEqualColumns(String tableCode, List<DynaBean> columns, List<DynaBean> viewColumns) {
        List<DynaBean> columnIds = Lists.newArrayList();
        for (DynaBean eachDelColumnBean : columns) {
            for (DynaBean eachViewColumnBean : viewColumns) {
                String viewColumnConfigStr = eachViewColumnBean.getStr("TABLECOLUMN_VIEWCONFIG");
                if (Strings.isNullOrEmpty(viewColumnConfigStr)) {
                    continue;
                }
                JSONObject viewColumnObj = JSON.parseObject(viewColumnConfigStr);
                if (tableCode.equals(viewColumnObj.getString("table"))
                        && viewColumnObj.getString("code").equals(eachDelColumnBean.getStr("TABLECOLUMN_CODE"))) {
                    columnIds.add(eachViewColumnBean);
                }
            }
        }
        return columnIds;
    }

}
