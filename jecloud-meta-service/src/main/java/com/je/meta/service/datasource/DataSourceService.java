/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.datasource;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.extjs.Model;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.meta.model.view.ViewDdlVo;
import java.util.List;
import java.util.Map;

public interface DataSourceService {

    void doCopy(DynaBean dynaBean);

    List<Model> getSelectModel(String sql);

    JSONTreeNode getDsTree(String chartId);

    String getSelectDdl(String resourceId);

    String getSelectDdlByConfig(String type,ViewDdlVo viewDdlVo);

    List<Map<String, Object>> execute(String config,String id,String parameterStr, String limit,String j_query);

    JSONObject batchExecute(String strData);

    void doUpdate(DynaBean dynaBean);

    String getFieldsBySql(String je_core_datasource_id,String tableCode);

    List<JSONTreeNode> getPreAddDataSource(String dataSourceId);

    JSONObject getDataSourceParameter(String ids);

    List<DynaBean> executeForRpc(String dataSourceCode,Map<String,Object> map, int limit);
}
