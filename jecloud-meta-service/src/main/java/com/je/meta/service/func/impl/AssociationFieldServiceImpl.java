/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.AssociationFieldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AssociationFieldServiceImpl implements AssociationFieldService {

    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean checkCode(String strData) {
        //JE_CORE_ASSOCIATIONFIELD_ID,ASSOCIATIONFIELD_PRIFIELDCODE,ASSOCIATIONFIELD_CHIFIELDCODE
        DynaBean db = new DynaBean();
        String errorStr ="";
        JSONArray array = JSON.parseArray(strData);
        //Iterator<Object> it = array.iterator();

        for(int i = 0;i<array.size();i++){
            JSONObject jsonObject =array.getJSONObject(i);
            String ASSOCIATIONFIELD_PRIFIELDCODE = jsonObject.getString("ASSOCIATIONFIELD_PRIFIELDCODE");
            String ASSOCIATIONFIELD_CHIFIELDCODE = jsonObject.getString("ASSOCIATIONFIELD_CHIFIELDCODE");
            String JE_CORE_ASSOCIATIONFIELD_ID = jsonObject.getString("JE_CORE_ASSOCIATIONFIELD_ID");
            DynaBean dynaBean = metaService.selectOneByPk("JE_CORE_ASSOCIATIONFIELD", JE_CORE_ASSOCIATIONFIELD_ID);
            String ASSOCIATIONFIELD_FUNCRELAT_ID = dynaBean.getStr("ASSOCIATIONFIELD_FUNCRELAT_ID");
            List<DynaBean> listPcode = metaService.select("JE_CORE_ASSOCIATIONFIELD", ConditionsWrapper.builder().eq("ASSOCIATIONFIELD_PRIFIELDCODE",ASSOCIATIONFIELD_PRIFIELDCODE).eq("ASSOCIATIONFIELD_FUNCRELAT_ID",ASSOCIATIONFIELD_FUNCRELAT_ID));
            if(listPcode!=null&&listPcode.size()>0){
                String pId = listPcode.get(0).getStr("JE_CORE_ASSOCIATIONFIELD_ID");
                if(!pId.equals(JE_CORE_ASSOCIATIONFIELD_ID)){
                    errorStr+=ASSOCIATIONFIELD_PRIFIELDCODE+",";
                }
            }

            List<DynaBean> listCcode = metaService.select("JE_CORE_ASSOCIATIONFIELD", ConditionsWrapper.builder().eq("ASSOCIATIONFIELD_CHIFIELDCODE",ASSOCIATIONFIELD_CHIFIELDCODE).eq("ASSOCIATIONFIELD_FUNCRELAT_ID",ASSOCIATIONFIELD_FUNCRELAT_ID));
            if(listCcode!=null&&listCcode.size()>0){
                String cId = listCcode.get(0).getStr("JE_CORE_ASSOCIATIONFIELD_ID");
                if(!cId.equals(JE_CORE_ASSOCIATIONFIELD_ID)){
                    errorStr+=ASSOCIATIONFIELD_CHIFIELDCODE+",";
                }
            }
        }
        if(!StringUtil.isEmpty(errorStr)){
            db.setStr("errorSrr",errorStr);
        }
        /*if(array!=null&&array.size()>0){
            db.setStr("strData",array.toJSONString());
        }*/
        return db;
    }

    private boolean findRepetitionFromList(List<DynaBean> listPcode, String code) {
        if(listPcode!=null&&listPcode.size()>0){
            for(DynaBean db:listPcode){
                String ASSOCIATIONFIELD_PRIFIELDCODE = db.getStr("ASSOCIATIONFIELD_PRIFIELDCODE");
                if(code.equals(ASSOCIATIONFIELD_PRIFIELDCODE)){
                    return true;

                }
            }
        }
        return false;
    }

}
