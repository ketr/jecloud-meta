/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.index.impl;

import com.alibaba.fastjson2.JSON;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.meta.service.table.AbstractTableTrace;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("tableIndexTrace")
public class TableIndexTraceImpl extends AbstractTableTrace {

    @Override
    public DynaBean save(List<DynaBean> columns, DynaBean trace, DynaBean oldBean, DynaBean newBean, String oper) {
        if ("INSERT".equals(oper)) {
            return null;
        }
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        if ("UPDATE".equals(oper)) {
            if (Strings.isNullOrEmpty(oldBean.getStr("TABLEINDEX_FIELDCODE"))) {
                oper = "INSERT";
                trace.set("TABLETRACE_OPER", "添加");
            } else {
                trace.set("TABLETRACE_OPER", "修改");
            }
            for (DynaBean column : columns) {
                if (column.getStr("TABLECOLUMN_CODE").startsWith("SY_")) {
                    continue;
                }
                if (!oper.equals("INSERT")) {
                    if (!newBean.containsKey(column.getStr("TABLECOLUMN_CODE")) || oldBean.getStr(column.getStr("TABLECOLUMN_CODE"), "").equals(newBean.getStr(column.getStr("TABLECOLUMN_CODE"), ""))) {
                        continue;
                    }
                }
                Map<String, Object> map = buildJsonData(oper, column, newBean, oldBean);
                dataList.add(map);
            }
            trace.set("TABLETRACE_FIELDCODE", newBean.get("TABLEINDEX_NAME"));
        } else if ("DELETE".equals(oper)) {
            newBean = oldBean;
            trace.set("TABLETRACE_OPER", "删除");
            for (DynaBean column : columns) {
                if (column.getStr("TABLECOLUMN_CODE").startsWith("SY_")) {
                    continue;
                }
                Map<String, Object> map = buildJsonData(oper, column, newBean, oldBean);
                dataList.add(map);
            }
            trace.set("TABLETRACE_FIELDCODE", oldBean.get("TABLEINDEX_NAME"));
        }
        String jsonStr = "";
        if (dataList.size() > 0) {
            jsonStr = JSON.toJSONString(dataList);
        }
        if (!Strings.isNullOrEmpty(newBean.getStr("TABLEINDEX_ISCREATE")) && newBean.getStr("TABLEINDEX_ISCREATE").equals("1")) {
            trace.set("TABLETRACE_SFYY", "1");
        } else {
            trace.set("TABLETRACE_SFYY", "0");
        }
        trace.set("TABLETRACE_FIELDNAME", newBean.getStr("TABLEINDEX_FIELDNAME"));
        trace.set("TABLETRACE_FIELDTYPE", getUniqueTypeName(newBean.getStr("TABLEINDEX_UNIQUE", "")));
        trace.set("TABLETRACE_TABLENAME", "索引");
        trace.set("TABLETRACE_XGNRJSON", jsonStr);
        metaService.insert(trace);
        return trace;
    }

    private String getUniqueTypeName(String value) {
        if (value.equals("1")) {
            return "唯一索引";
        } else {
            return "普通索引";
        }
    }


    private Map<String, Object> buildJsonData(String oper, DynaBean column, DynaBean newBean, DynaBean oldBean) {
        Map<String, Object> map = new HashMap<>();
        map.put("tableColumnName", column.getStr("TABLECOLUMN_NAME"));
        map.put("tableColumnCode", column.getStr("TABLECOLUMN_CODE"));
        if ("INSERT".equals(oper)) {
            map.put("type", "insert");
            map.put("tableColumnValue", newBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        if ("UPDATE".equals(oper)) {
            map.put("type", "update");
            map.put("tableColumnOldValue", oldBean.getStr(column.getStr("TABLECOLUMN_CODE")));
            map.put("tableColumnValue", newBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        if ("DELETE".equals(oper)) {
            map.put("type", "delete");
            map.put("tableColumnValue", oldBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        return map;
    }
}
