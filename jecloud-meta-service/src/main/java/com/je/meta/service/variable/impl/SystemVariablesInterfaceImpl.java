package com.je.meta.service.variable.impl;

import com.je.auth.check.SystemVariablesInterface;
import com.je.auth.check.context.model.AuthRequest;
import com.je.common.base.entity.func.FuncInfo;
import com.je.common.base.service.rpc.FunInfoRpcService;
import com.je.meta.cache.variable.BackCache;
import org.apache.servicecomb.core.Invocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SystemVariablesInterfaceImpl implements SystemVariablesInterface {

    @Autowired
    private BackCache backCache;
    @Autowired
    private FunInfoRpcService funInfoRpcService;

    @Override
    public Map<String, String> getSystemVariablesByCodes(String[] codes) {
        Map<String, String> resultMap = new HashMap<>();
        Map<String, String> backVar = backCache.getCacheValues();
        for (String code : codes) {
            resultMap.put(code, backVar.get(code) == null ? "" : backVar.get(code));
        }
        return resultMap;
    }

    @Override
    public String getRequestFormData(AuthRequest authRequest, String code) {
        if (authRequest.getSource() instanceof Invocation) {
            Invocation invocation = (Invocation) authRequest.getSource();
            return invocation.getRequestEx().getParameter(code);
        }
        return "";
    }

    @Override
    public String getRequestHeaderData(AuthRequest authRequest, String code) {
        if (authRequest.getSource() instanceof Invocation) {
            Invocation invocation = (Invocation) authRequest.getSource();
            if (code.equals("pd")) {
                return invocation.getSchemaMeta().getMicroserviceName();
            }
            return invocation.getRequestEx().getHeader(code);
        }
        return "";
    }

    @Override
    public List<String> getChildFuncCodesByFuncCode(String funcCode) {
        FuncInfo funcInfo = funInfoRpcService.getFuncInfo(funcCode);
        if (funcInfo == null) {
            return new ArrayList<>();
        }
        if (funcInfo.getFuncSubsetIdList() == null) {
            return new ArrayList<>();
        }
        return funcInfo.getFuncSubsetIdList();
    }
}
