/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.index.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.table.ColumnType;
import com.je.common.base.constants.table.TableType;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.table.BuildingSqlFactory;
import com.je.common.base.util.*;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.table.DynaCache;
import com.je.meta.cache.table.TableCache;
import com.je.meta.service.common.MetaBeanService;
import com.je.meta.service.table.index.MetaTableIndexService;
import com.je.meta.service.table.MetaTableTraceService;
import com.je.meta.util.enumUtil.TableTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 11:22
 * @description: 资源表索引服务实现
 */
@Service
public class MetaTableIndexServiceImpl implements MetaTableIndexService {

    @Autowired
    private MetaService metaService;
    @Lazy
    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaBeanService metaBeanService;
    @Autowired
    private TableCache tableCache;
    @Autowired
    private DynaCache dynaCache;
    @Autowired
    private MetaTableTraceService metaTableTraceService;

    @Override
    public String requireDeletePhysicalIndexDDL(String tableCode, List<DynaBean> indexs) {
        String delSql = BuildingSqlFactory.getDdlService().getDeleteIndexSql(tableCode, indexs);
        return delSql;
    }

    /**
     * 修改保存表格索引
     *
     * @param dynaBean
     * @param strData
     * @return
     */
    @Override
    public int doUpdateList(DynaBean dynaBean, String strData) {
        String tableCode = "JE_CORE_TABLEINDEX";
        String pkName = "JE_CORE_TABLEINDEX_ID";
        Map<String, DynaBean> oldMaps = new HashMap<String, DynaBean>();
        dynaBean.table(tableCode);
        String[] ids = JsonUtil.jsonSqlToIdsStr(dynaBean, strData);
        List<Map> sqlMapList = JsonUtil.fromJsonArray(strData);
        String[] updateSqls = new String[sqlMapList.size()];
        for (int i = 0; i < sqlMapList.size(); i++) {
            Map sqlMap = sqlMapList.get(i);
            String sql = BuildingSqlFactory.getSqlService().getUpdateSql(tableCode, pkName, sqlMap);
            updateSqls[i] = sql;
            DynaBean oldBean = metaService.selectOneByPk("JE_CORE_TABLEINDEX", sqlMap.get(pkName) + "");
            oldMaps.put(sqlMap.get(pkName) + "", oldBean);
        }
        int rows = updateSqls.length;
        for (String sql : updateSqls) {
            metaService.executeSql(sql);
        }

//				int rows = serviceTemplate.listUpdate(updateSqls);
        List<DynaBean> newBeans = metaService.select(ConditionsWrapper.builder()
                .table("JE_CORE_TABLEINDEX")
                .in("JE_CORE_TABLEINDEX_ID", ids));
        for (DynaBean bean : newBeans) {
            String pk = bean.getStr(pkName);
            DynaBean oldBean = oldMaps.get(pk);
            metaTableTraceService.saveTableTrace("JE_CORE_TABLEINDEX", oldBean, bean, "UPDATE", oldBean.getStr("TABLEINDEX_RESOURCETABLE_ID"), oldBean.getStr("TABLEINDEX_ISCREATE"));
        }
        return rows;
    }

    @Override
    public String checkParameter(String strData) {
        JSONArray data = JSONArray.parseArray(strData);
        for (int i = 0; i < data.size(); i++) {
            JSONObject jsonObject = data.getJSONObject(i);
            String TABLEINDEX_FIELDCODE = jsonObject.getString("TABLEINDEX_FIELDCODE");
            if (StringUtil.isEmpty(TABLEINDEX_FIELDCODE)) {
                return MessageUtils.getMessage("index.canNotEmpty");
            }
        }
        return null;
    }

    @Override
    public void deleteIndexMeta(String tableCode, List<DynaBean> indexs) {
        String resourcetableId = indexs.get(0).getStr("TABLEINDEX_RESOURCETABLE_ID");
        DynaBean dynaBean = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourcetableId);
        List<DynaBean> funcList = metaService.select("JE_CORE_FUNCINFO", ConditionsWrapper.builder().eq("FUNCINFO_TABLENAME", dynaBean.getStr("RESOURCETABLE_TABLECODE")));
        String[] funcIds = new String[funcList.size()];
        if (funcList != null && funcList.size() > 0) {
            for (int i = 0; i < funcList.size(); i++) {
                DynaBean func = funcList.get(i);
                funcIds[i] = func.getStr("JE_CORE_FUNCINFO_ID");
            }
        }
        String[] delIds = new String[indexs.size()];
        String[] codes = new String[indexs.size()];
        for (int i = 0; i < indexs.size(); i++) {
            DynaBean tc = indexs.get(i);
            delIds[i] = tc.getStr("JE_CORE_TABLEINDEX_ID");
            codes[i] = tc.getStr("TABLEINDEX_FIELDCODE");
        }
        if (funcIds.length > 0) {
            metaService.executeSql("UPDATE JE_CORE_RESOURCECOLUMN SET RESOURCECOLUMN_INDEX='0' WHERE RESOURCECOLUMN_CODE IN ({0})  AND RESOURCECOLUMN_FUNCINFO_ID IN ({1})", Arrays.asList(codes), Arrays.asList(funcIds));
        }
        if (delIds.length > 0) {
            metaService.executeSql("DELETE FROM JE_CORE_TABLEINDEX WHERE JE_CORE_TABLEINDEX_ID IN ({0})", Arrays.asList(delIds));
        }
        for (DynaBean delBean : indexs) {
            metaTableTraceService.saveTableTrace("JE_CORE_TABLEINDEX", delBean, delBean, "DELETE", resourcetableId, "0");
        }
    }

    @Override
    public void deleteIndex(String tableCode, List<DynaBean> indexs) {
        if (indexs == null || indexs.size() == 0) {
            return;
        }
        //删除物理索引
        String delSql = BuildingSqlFactory.getDdlService().getDeleteIndexSql(tableCode, indexs);
        if (StringUtil.isNotEmpty(delSql)) {
            try {
                delSql = delSql.replaceAll("\n", "");
                String[] delSqlArray = delSql.split(";");
                for (String sql : delSqlArray) {
                    metaService.executeSql(sql);
                }
            } catch (UncategorizedSQLException e) {
                throw new PlatformException(MessageUtils.getMessage("index.delete.index.error") + e.getCause().getMessage(),
                        PlatformExceptionEnum.JE_CORE_TABLEINDEX_DELETE_ERROR, new Object[]{tableCode, delSql}, e);
            } catch (Exception e) {
                throw new PlatformException(MessageUtils.getMessage("index.delete.wrong") + "," + e.getCause().getMessage(),
                        PlatformExceptionEnum.JE_CORE_TABLEINDEX_DELETE_ERROR, new Object[]{tableCode, delSql}, e);
            }
        }
        //删除元数据
        deleteIndexMeta(tableCode, indexs);
    }

    @Override
    public void initIndexs(DynaBean resourceTable, Boolean isTree) {
        /**ID的键设置*/
        List<DynaBean> indexs = new ArrayList<DynaBean>();
        DynaBean pkIndex = new DynaBean("JE_CORE_TABLEINDEX", false);
        pkIndex.set("TABLEINDEX_NAME", "JE_" + DateUtils.getUniqueTime() + "_ID");
        pkIndex.set("TABLEINDEX_FIELDCODE", resourceTable.get("RESOURCETABLE_TABLECODE") + "_ID");
        pkIndex.set("TABLEINDEX_FIELDNAME", "主键ID");
        pkIndex.set("TABLEINDEX_ISCREATE", "1");
        pkIndex.set("TABLEINDEX_UNIQUE", "1");
        pkIndex.set("TABLEINDEX_CLASSIFY", "SYS");
        pkIndex.set("SY_ORDERINDEX", 0);
        indexs.add(pkIndex);
        DynaBean orderIndex = new DynaBean("JE_CORE_TABLEINDEX", false);
        orderIndex.set("TABLEINDEX_NAME", "JE_" + DateUtils.getUniqueTime() + "_ORDER");
        orderIndex.set("TABLEINDEX_FIELDCODE", "SY_ORDERINDEX");
        orderIndex.set("TABLEINDEX_FIELDNAME", "排序字段");
        orderIndex.set("TABLEINDEX_ISCREATE", "0");
        orderIndex.set("TABLEINDEX_UNIQUE", "0");
        orderIndex.set("TABLEINDEX_CLASSIFY", "SYS");
        orderIndex.set("SY_ORDERINDEX", 1);
        indexs.add(orderIndex);

        String nowDate = DateUtils.formatDateTime(new Date());
        for (DynaBean index : indexs) {
            index.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            index.set("SY_CREATETIME", nowDate);
            index.set("SY_CREATEUSERID", SecurityUserHolder.getCurrentAccountRealUserId());
            index.set("SY_CREATEUSERNAME", SecurityUserHolder.getCurrentAccountRealUserName());
            index.set("SY_CREATEORGID", SecurityUserHolder.getCurrentAccountRealOrgId());
            index.set("SY_CREATEORGNAME", SecurityUserHolder.getCurrentAccountRealOrgName());
            index.set("TABLEINDEX_ISCREATE", "0");
            index.set("TABLEINDEX_TABLECODE", resourceTable.get("RESOURCETABLE_TABLECODE"));
            index.set("TABLEINDEX_RESOURCETABLE_ID", resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"));
            index.set("SY_PRODUCT_ID", resourceTable.get("SY_PRODUCT_ID"));
            index.set("SY_PRODUCT_NAME", resourceTable.getStr("SY_PRODUCT_NAME"));
            metaService.insert(index);
        }
    }

    @Override
    public DynaBean createIndexByColumn(DynaBean funcInfo, DynaBean resourceTable, String columnCode, String columnId) {
        DynaBean index = saveLogicData(funcInfo, resourceTable, columnCode, columnId);
        String indexSql = getDDL4AddIndex(index, resourceTable);
        metaService.executeSql(indexSql);
        return index;
    }

    @Override
    public DynaBean saveLogicData(DynaBean funcInfo, DynaBean resourceTable, String columnCode, String columnId) {
        String tableCode = funcInfo.getStr("FUNCINFO_TABLENAME");
        if (TableType.VIEWTABLE.equals(resourceTable.getStr("RESOURCETABLE_TYPE"))) {
            throw new PlatformException(MessageUtils.getMessage("index.view.canSave"), PlatformExceptionEnum.JE_CORE_DB_COLUMN_TABLE_ERROR);
        }
        DynaBean column = metaService.selectOne("JE_CORE_TABLECOLUMN",
                ConditionsWrapper.builder().eq("TABLECOLUMN_CODE", columnCode)
                        .eq("TABLECOLUMN_RESOURCETABLE_ID", resourceTable.getStr("JE_CORE_RESOURCETABLE_ID")));
        String type = column.getStr("TABLECOLUMN_TYPE");
        if (ColumnType.CLOB.equals(type) || ColumnType.BIGCLOB.equals(type)) {
            throw new PlatformException(MessageUtils.getMessage("index.text.canSave"), PlatformExceptionEnum.JE_CORE_DB_COLUMN_TABLE_ERROR);
        }
        //验证数据类型是否支持索引
        if (JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_MYSQL) || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_TIDB)) {
            if (ColumnType.VARCHAR1000.equals(type) || ColumnType.VARCHAR2000.equals(type) || ColumnType.VARCHAR4000.equals(type) || (ColumnType.VARCHAR.equals(type) && Integer.parseInt(column.getStr("TABLECOLUMN_LENGTH")) > 383)) {
                throw new PlatformException(MessageUtils.getMessage("index.mysql.length"), PlatformExceptionEnum.JE_CORE_DB_COLUMN_TABLE_ERROR);
            }
        } else if (JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SQLSERVER)) {
            if (ColumnType.VARCHAR1000.equals(type) || ColumnType.VARCHAR2000.equals(type) || ColumnType.VARCHAR4000.equals(type) || (ColumnType.VARCHAR.equals(type) && Integer.parseInt(column.getStr("TABLECOLUMN_LENGTH")) > 900)) {
                throw new PlatformException(MessageUtils.getMessage("index.sqlservice.length"), PlatformExceptionEnum.JE_CORE_DB_COLUMN_TABLE_ERROR);
            }
        }

        DynaBean index = new DynaBean("JE_CORE_TABLEINDEX", false);
        index.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLEINDEX_ID");
        index.set("TABLEINDEX_ISCREATE", "1");
        index.set("TABLEINDEX_UNIQUE", "0");
        index.set("TABLEINDEX_RESOURCETABLE_ID", resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"));
        index.set("TABLEINDEX_CLASSIFY", "PRO");
        index.set("TABLEINDEX_FIELDNAME", column.getStr("TABLECOLUMN_NAME"));
        index.set("TABLEINDEX_TABLECODE", tableCode);
        index.set("TABLEINDEX_FIELDCODE", columnCode);
        index.set("SY_ORDERINDEX", 0);
        commonService.buildModelCreateInfo(index);
        String nowDateTime = DateUtils.formatDateTime(new Date());
        nowDateTime = nowDateTime.replaceAll("-", "");
        nowDateTime = nowDateTime.replaceAll(" ", "");
        nowDateTime = nowDateTime.replaceAll(":", "");
        index.set("TABLEINDEX_NAME", "JE_" + nowDateTime);
        metaService.insert(index);
        metaTableTraceService.saveTableTrace("JE_CORE_TABLEINDEX", null, index, "INSERT", resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"), "1");
        metaService.executeSql("UPDATE JE_CORE_RESOURCECOLUMN SET RESOURCECOLUMN_INDEX='1' WHERE JE_CORE_RESOURCECOLUMN_ID={0}", columnId);
        tableCache.removeCache(tableCode);
        dynaCache.removeCache(tableCode);
        return index;
    }

    @Override
    public String getDDL4AddIndex(DynaBean index, DynaBean resourceTable) {
        return BuildingSqlFactory.getDdlService().getDDL4AddIndex(index, resourceTable);
    }

    @Override
    public String removeIndexByColumn(String funcId, String columnCode, String columnId) {
        DynaBean funcInfo = metaService.selectOneByPk("JE_CORE_FUNCINFO", funcId);
        String tableCode = funcInfo.getStr("FUNCINFO_TABLENAME");
        DynaBean resourceTable = metaBeanService.getResourceTable(tableCode);
        DynaBean index = metaService.selectOne("JE_CORE_TABLEINDEX",
                ConditionsWrapper.builder()
                        .eq("TABLEINDEX_FIELDCODE", columnCode)
                        .eq("TABLEINDEX_RESOURCETABLE_ID", resourceTable.getStr("JE_CORE_RESOURCETABLE_ID")));
        if ("SYS".equals(index.getStr("TABLEINDEX_CLASSIFY"))) {
            return MessageUtils.getMessage("index.delete.sys");
        }
        metaTableTraceService.saveTableTrace("JE_CORE_TABLEINDEX", index, null, "DELETE", index.getStr("TABLEINDEX_RESOURCETABLE_ID"), "1");
        List<DynaBean> indexs = new ArrayList<DynaBean>();
        indexs.add(index);
        deleteIndex(tableCode, indexs);
        metaService.executeSql("UPDATE JE_CORE_RESOURCECOLUMN SET RESOURCECOLUMN_INDEX='0' WHERE JE_CORE_RESOURCECOLUMN_ID={0}", columnId);
        tableCache.removeCache(tableCode);
        dynaCache.removeCache(tableCode);
        return null;
    }

    @Override
    public String checkIndexs(List<DynaBean> indexs) {
        String errors = "";
        Set<String> indexCodes = new HashSet<String>();
        for (DynaBean index : indexs) {
            String columnCode = index.getStr("TABLEINDEX_FIELDCODE");
            //检测传入空的code
            if (StringUtil.isEmpty(columnCode)) {
                errors = MessageUtils.getMessage("index.canNotEmpty");
                break;
            }
            //检测传入重复的字段code
            if (indexCodes.contains(columnCode)) {
                errors = MessageUtils.getMessage("index.multi", index.getStr("TABLEINDEX_FIELDCODE"));
                break;
            }
            //检测传入的code是否属于视图
            String RESOURCETABLE_TABLECODE = index.getStr("TABLEINDEX_TABLECODE");
            DynaBean table = metaService.selectOne("JE_CORE_RESOURCETABLE", ConditionsWrapper.builder()
                    .eq("RESOURCETABLE_TABLECODE", RESOURCETABLE_TABLECODE)
                    .selectColumns("RESOURCETABLE_TABLECODE,RESOURCETABLE_TYPE"));
            if (TableTypeEnum.VIEW.getValue().equals(table.getStr("RESOURCETABLE_TYPE"))) {
                errors = MessageUtils.getMessage("index.view.canSave");
                break;
            }
            //检测创建索引的字段长度是否大于767
            DynaBean column = metaService.selectOne("JE_CORE_TABLECOLUMN",
                    ConditionsWrapper.builder().eq("TABLECOLUMN_TABLECODE",RESOURCETABLE_TABLECODE).eq("TABLECOLUMN_CODE",columnCode));
            String[] arr = {"VARCHAR1000","VARCHAR2000","VARCHAR4000"};
            String type = column.getStr("TABLECOLUMN_TYPE");
            if(Arrays.asList(arr).contains(type)){
                errors = MessageUtils.getMessage("index.column.length.tooBig");
                break;
            }
            if(ColumnType.CUSTOM.equals(type)){
                String lengthStr = column.getStr("TABLECOLUMN_LENGTH");
                if(lengthStr!=null){
                    lengthStr = lengthStr.toUpperCase();
                    if(lengthStr.indexOf(ColumnType.VARCHAR)!=-1 &&
                            lengthStr.indexOf("(")!=-1 && lengthStr.indexOf(")")!=-1){
                        int a = lengthStr.indexOf("(");
                        int b = lengthStr.indexOf(")");
                        String length = lengthStr.substring(a+1,b);
                        if(StringUtil.isNotEmpty(length) && Long.parseLong(length)>767){
                            errors = MessageUtils.getMessage("index.column.length.tooBig");
                            break;
                        }
                    }
                }
            }else if(ColumnType.VARCHAR.equals(type)){
                Long length = column.getLong("TABLECOLUMN_LENGTH");
                if(length!=null && length>767){
                    errors = MessageUtils.getMessage("index.column.length.tooBig");
                    break;
                }
            }
            indexCodes.add(columnCode);
        }
        return errors;
    }

    /**
     * 删除索引
     *
     * @param dynaBean 自定义动态类
     * @param ids      索引ID列表
     * @return
     */
    @Override
    public Integer removeIndex(DynaBean dynaBean, String ids) {
        String[] split = ids.split(ArrayUtils.SPLIT);
        List<DynaBean> indexs = metaService.select(ConditionsWrapper.builder()
                .table("JE_CORE_TABLEINDEX")
                .in("JE_CORE_TABLEINDEX_ID", split));
        for (DynaBean index : indexs) {
            metaTableTraceService.saveTableTrace("JE_CORE_TABLEINDEX", index, null, "DELETE", index.getStr("TABLEINDEX_RESOURCETABLE_ID"), index.getStr("TABLEINDEX_ISCREATE"));
        }
        String tableCode = "JE_CORE_TABLEINDEX";
        if (indexs.size() > 0) {
            DynaBean index = indexs.get(0);
            tableCode = index.getStr("TABLEINDEX_TABLECODE");
        }
        if (indexs.size() != 0) {
            indexs.get(0).getStr("TABLEINDEX_TABLECODE");
        }
        deleteIndex(tableCode, indexs);
        tableCache.removeCache(tableCode);
        dynaCache.removeCache(tableCode);
        return indexs.size();
    }


}
