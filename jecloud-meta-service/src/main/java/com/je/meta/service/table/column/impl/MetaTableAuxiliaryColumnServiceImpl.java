package com.je.meta.service.table.column.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.JsonBuilder;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.table.ColumnType;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.ArrayUtils;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.table.column.MetaTableAuxiliaryColumnService;
import com.je.meta.service.table.column.MetaTableColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class MetaTableAuxiliaryColumnServiceImpl implements MetaTableAuxiliaryColumnService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaTableColumnService metaTableColumnService;

    /**
     * 字典辅助添加列
     *
     * @param dynaBean
     */
    @Override
    @Transactional
    public String addColumnByDD(DynaBean dynaBean) {
        String ids = dynaBean.getStr("addColumnCodes");
        String ddCode = dynaBean.getStr("ddCode");
        DynaBean table = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode)
                .apply("and (SY_STATUS = '' or SY_STATUS = '1' or SY_STATUS is NULL)"));
        String pkValue = dynaBean.getStr("JE_CORE_RESOURCETABLE_ID");
        String result = metaTableColumnService.checkUnique(ids, pkValue);
        if (StringUtil.isNotEmpty(result)) {
            return MessageUtils.getMessage("column.exitsParam", result);
        }
        String ddName = dynaBean.getStr("ddName");
        String ddType = "";
        if (table != null) {
            ddType = table.getStr("DICTIONARY_DDTYPE");
        }
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        String[] codeArray = ids.split(ArrayUtils.SPLIT);
        String queryField = "";
        Boolean idField = false;
        for (String code : codeArray) {
            if ("CODE".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                queryField = code;
            }
        }
        if (StringUtil.isEmpty(queryField)) {
            for (String code : codeArray) {
                if ("ID".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                    queryField = code;
                    idField = true;
                }
            }
        }
        String[] configInfoArray = new String[codeArray.length];
        for (int i = 0; i < codeArray.length; i++) {
            String code = codeArray[i];
            String type = code.substring(code.lastIndexOf("_") + 1);
            if ("name".equalsIgnoreCase(type)) {
                configInfoArray[i] = "text";
            } else if ("code".equalsIgnoreCase(type)) {
                configInfoArray[i] = "code";
            } else if ("id".equalsIgnoreCase(type)) {
                configInfoArray[i] = "id";
            }
        }
        for (String code : codeArray) {
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", false);
            column.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("TABLECOLUMN_CODE", code);
            String type = code.substring(code.lastIndexOf("_") + 1);

            if (ArrayUtils.contains(new String[]{"DYNA_TREE", "SQL_TREE", "TREE"}, ddType)) {
                if ("name".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName);
                } else if ("code".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName + "_CODE");
                } else if ("id".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName + "_ID");
                }
            } else if ("JE_RBAC_DEPTTREE".equals(ddCode)) {
                if ("name".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName);
                } else if ("code".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName + "_CODE");
                } else if ("id".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName + "_ID");
                }
            } else {
                if ("name".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName + "_NAME");
                } else if ("code".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName);
                } else if ("id".equalsIgnoreCase(type)) {
                    column.set("TABLECOLUMN_NAME", ddName + "_ID");
                }
            }
            column.set("TABLECOLUMN_TYPE", "VARCHAR30");
            if ("id".equalsIgnoreCase(type)) {
                column.set("TABLECOLUMN_TYPE", "VARCHAR50");
            }
            column.set("TABLECOLUMN_UNIQUE", "0");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_LENGTH", "");
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            column.setStr("TABLECOLUMN_SOURCE", "dictionary");
            String configInfo = StringUtil.buildSplitString(configInfoArray, "~");
            //树形字典
            if (ArrayUtils.contains(new String[]{"DYNA_TREE", "SQL_TREE", "TREE", "CUSTOM"}, ddType)) {
                if ("NAME".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                    column.set("TABLECOLUMN_DICCONFIG", ddCode + "," + ids.replace(",", "~") + "," + configInfo + ",S");
                    column.set("TABLECOLUMN_DICQUERYFIELD", queryField);
                }
            } else {
                if ("CODE".equalsIgnoreCase(code.substring(code.lastIndexOf("_") + 1))) {
                    column.set("TABLECOLUMN_DICCONFIG", ddCode + "," + ids.replace(",", "~") + "," + configInfo + ",S");
                }
            }
            commonService.buildModelCreateInfo(column);
            column.set("SY_ORDERINDEX", count);
            count++;
            metaService.insert(column);
        }
        return null;
    }

    /**
     * 字典辅助添加列
     *
     * @param dynaBean
     */
    @Override
    @Transactional
    public String addColumnByDDList(DynaBean dynaBean) {
        String strData = dynaBean.getStr("strData");
        String result = checkStrData(strData, "name", dynaBean.getStr("JE_CORE_RESOURCETABLE_ID"));
        if (StringUtil.isNotEmpty(result)) {
            return MessageUtils.getMessage("column.exitsParam", result);
        }
        String pkValue = dynaBean.getStr("JE_CORE_RESOURCETABLE_ID");
        String whereSql = dynaBean.getStr("whereSql");
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        JSONArray fields = JSONArray.parseArray(strData);
        for (int i = 0; i < fields.size(); i++) {
            JSONObject field = fields.getJSONObject(i);
            String type = field.getString("type");
            String name = field.getString("text");
            String code = field.getString("name");
            String configInfo = field.getString("configInfo");
            String otherConfig = field.getString("otherConfig");
            JSONObject config = new JSONObject();
            config.put("other", otherConfig);
            config.put("where", whereSql);
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", true);
            column.set("TABLECOLUMN_NAME", name);
            column.set("TABLECOLUMN_CODE", code);
            column.set("TABLECOLUMN_TYPE", "VARCHAR30");
            column.set("TABLECOLUMN_UNIQUE", "0");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_LENGTH", "");
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            column.set("TABLECOLUMN_DICCONFIG", configInfo);
            column.set("TABLECOLUMN_DICQUERYFIELD", config.toString());
            commonService.buildModelCreateInfo(column);
            column.set("SY_ORDERINDEX", count);
            count++;
            metaService.insert(column);
        }
        return null;
    }

    /**
     * 表辅助添加列
     *
     * @param dynaBean
     * @return
     */
    @Override
    @Transactional
    public String addColumnByTable(DynaBean dynaBean) {
        String strData = dynaBean.getStr("strData");
        String pkValue = dynaBean.getStr("JE_CORE_RESOURCETABLE_ID");
        String result = checkStrData(strData, "TABLECOLUMN_NEWCODE", pkValue);
        String FUNCCODE = dynaBean.getStr("FUNCCODE");
        String queryStr = dynaBean.getStr("queryStr");
        if (StringUtil.isNotEmpty(result)) {
            return MessageUtils.getMessage("column.exitsParam", result);
        }
        DynaBean oldBean = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", pkValue);
        String tableCode = oldBean.getStr("RESOURCETABLE_TABLECODE");
        List<Map> sqlMapList = JsonBuilder.getInstance().fromJsonArray(strData);
        String targerTableCode = dynaBean.getStr("fromTableId");
        String createChild = dynaBean.getStr("CREATECHILD");
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }

        for (Map map : sqlMapList) {
            //插入表字段
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", false);
            column.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("TABLECOLUMN_NAME", map.get("TABLECOLUMN_NAME"));
            column.set("TABLECOLUMN_CODE", map.get("TABLECOLUMN_NEWCODE").toString().toUpperCase());
            column.set("TABLECOLUMN_UNIQUE", "0");
            if ("ID".equals(map.get("TABLECOLUMN_TYPE") + "")) {
                column.set("TABLECOLUMN_TYPE", "VARCHAR");
                column.set("TABLECOLUMN_LENGTH", "50");
            } else {
                column.set("TABLECOLUMN_TYPE", map.get("TABLECOLUMN_TYPE"));
                column.set("TABLECOLUMN_LENGTH", map.get("TABLECOLUMN_LENGTH"));
            }
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_TABLECODE", tableCode);
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            column.setStr("TABLECOLUMN_SOURCE", "table");
            if (map.containsKey("TABLECOLUMN_DICCONFIG")) {
                String dicConfig = map.get("TABLECOLUMN_DICCONFIG") + "";
                if (StringUtil.isNotEmpty(dicConfig)) {
                    for (Map vals : sqlMapList) {
                        dicConfig = dicConfig.replace(vals.get("TABLECOLUMN_CODE") + "", vals.get("TABLECOLUMN_NEWCODE") + "");
                    }
                    column.setStr("TABLECOLUMN_DICCONFIG", dicConfig);
                }
            }
            if (map.containsKey("TABLECOLUMN_DICQUERYFIELD")) {
                String dicQueryConfig = map.get("TABLECOLUMN_DICQUERYFIELD") + "";
                if (StringUtil.isNotEmpty(dicQueryConfig)) {
                    for (Map vals : sqlMapList) {
                        dicQueryConfig = dicQueryConfig.replace(vals.get("TABLECOLUMN_CODE") + "", vals.get("TABLECOLUMN_NEWCODE") + "");
                    }
                    column.setStr("TABLECOLUMN_DICQUERYFIELD", dicQueryConfig);
                }
            }

            if ("1".equals(createChild)) {
                column.set("TABLECOLUMN_CHILDCONFIG", targerTableCode + "," + map.get("TABLECOLUMN_CODE"));
            }
            commonService.buildModelCreateInfo(column);
            if ("1".equals(map.get("TABLECOLUMN_ISNULL") + "")) {
                //查询选择配置
                if (StringUtil.isNotEmpty(FUNCCODE)) {
                    column.set("TABLECOLUMN_QUERYCONFIG", FUNCCODE + "," + queryStr);
                }
            }
            //使用外键
            String targetTablecolumnType = map.get("TABLECOLUMN_TYPE") + "";
            if ("1".equals(createChild) && "ID".equals(map.get("TABLECOLUMN_TYPE") + "")) {
                column.set("TABLECOLUMN_TYPE", "CUSTOMFOREIGNKEY");
                if (ColumnType.CUSTOMID.equals(targetTablecolumnType)) {
                    column.set("TABLECOLUMN_LENGTH", map.get("TABLECOLUMN_LENGTH") + "");
                }
                if (ColumnType.ID.equals(targetTablecolumnType)) {
                    if (JEDatabase.getCurrentDatabase().equalsIgnoreCase(ConstantVars.STR_ORACLE)) {
                        column.set("TABLECOLUMN_LENGTH", "VARCHAR2(50)");
                    } else {
                        column.set("TABLECOLUMN_LENGTH", "VARCHAR(50)");
                    }
                }
                //生成外键
                DynaBean forenignKey = new DynaBean("JE_CORE_TABLEKEY", false);
                forenignKey.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLEKEY_ID");
                String nowDateTime = DateUtils.formatDateTime(new Date());
                nowDateTime = nowDateTime.replaceAll("-", "");
                nowDateTime = nowDateTime.replaceAll(" ", "");
                nowDateTime = nowDateTime.replaceAll(":", "");
                forenignKey.set("TABLEKEY_CODE", "JE_" + nowDateTime);
                forenignKey.set("TABLEKEY_COLUMNCODE", column.get("TABLECOLUMN_CODE"));
                forenignKey.set("TABLEKEY_TYPE", "Foreign");
                forenignKey.set("TABLEKEY_CHECKED", "1");
                forenignKey.set("TABLEKEY_LINKTABLE", map.get("TABLECOLUMN_TABLECODE"));
                forenignKey.set("TABLEKEY_LINECOLUMNCODE", map.get("TABLECOLUMN_OLDCODE"));
                forenignKey.set("TABLEKEY_LINETYLE", "Cascade");
                forenignKey.set("TABLEKEY_ISRESTRAINT", "1");
                forenignKey.set("TABLEKEY_RESOURCETABLE_ID", pkValue);
                forenignKey.set("TABLEKEY_TABLECODE", tableCode);
                forenignKey.set("TABLEKEY_ISCREATE", "0");
                forenignKey.set("TABLEKEY_CLASSIFY", "PRO");
                forenignKey.set("SY_ORDERINDEX", 1);
                commonService.buildModelCreateInfo(forenignKey);
                metaService.insert(forenignKey);
            }
            column.set("SY_ORDERINDEX", count);
            count++;
            metaService.insert(column);
        }
        return null;
    }

    /**
     * 原子辅助添加列
     *
     * @param strData   数据主体
     * @param tableCode 表编码
     * @param pkValue   主键
     * @return
     */
    @Override
    @Transactional
    public Integer addColumnByAtom(String strData, String tableCode, String pkValue) {
        List<Map> sqlMapList = JsonBuilder.getInstance().fromJsonArray(strData);
        int count = 1;
        List<Map<String, Object>> countInfos = metaService.selectSql("SELECT MAX(SY_ORDERINDEX) ORDERINDEX FROM JE_CORE_TABLECOLUMN WHERE TABLECOLUMN_CLASSIFY='PRO' AND TABLECOLUMN_RESOURCETABLE_ID={0}", pkValue);
        if (countInfos != null && countInfos.size() > 0 && countInfos.get(0) != null) {
            String countStr = countInfos.get(0).get("ORDERINDEX") + "";
            if (StringUtil.isNotEmpty(countStr)) {
                count = Integer.parseInt(countStr) + 1;
            }
        }
        for (Map map : sqlMapList) {
            //插入表字段
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", false);
            column.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("TABLECOLUMN_NAME", map.get("ATOMCOLUMN_NAME"));
            column.set("TABLECOLUMN_CODE", map.get("ATOMCOLUMN_CONTEXT"));
            column.set("TABLECOLUMN_UNIQUE", "0");
            column.set("TABLECOLUMN_TYPE", map.get("ATOMCOLUMN_TYPE"));
            column.set("TABLECOLUMN_LENGTH", map.get("ATOMCOLUMN_LENGTH"));
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_ISNULL", "1");
            column.set("TABLECOLUMN_CLASSIFY", "PRO");
            column.set("TABLECOLUMN_TABLECODE", tableCode);
            column.set("TABLECOLUMN_RESOURCETABLE_ID", pkValue);
            column.set("TABLECOLUMN_TREETYPE", "NORMAL");
            column.set("SY_ORDERINDEX", count);
            count++;
            commonService.buildModelCreateInfo(column);
            metaService.insert(column);
        }
        return sqlMapList.size();
    }

    /**
     * 存为原子
     * @param strData
     * @param pkValue 主键
     * @return
     */
    @Override
    @Transactional
    public Integer addAtomByColumn(String strData, String pkValue) {
        List<Map> sqlMapList = JsonBuilder.getInstance().fromJsonArray(strData);
        for (Map map : sqlMapList) {
            DynaBean atomColumn = new DynaBean("JE_CORE_ATOMCOLUMN", false);
            atomColumn.set(BeanService.KEY_TABLE_CODE, "JE_CORE_ATOMCOLUMN");
            atomColumn.set("ATOMCOLUMN_CODE", map.get("ATOMCOLUMN_CONTEXT"));
            atomColumn.set("ATOMCOLUMN_ISNULL", map.get("ATOMCOLUMN_ISNULL"));
            atomColumn.set("ATOMCOLUMN_NAME", map.get("ATOMCOLUMN_NAME"));
            atomColumn.set("ATOMCOLUMN_ATOM_ID", pkValue);
            atomColumn.set("ATOMCOLUMN_LENGTH", map.get("ATOMCOLUMN_LENGTH"));
            atomColumn.set("ATOMCOLUMN_TYPE", map.get("ATOMCOLUMN_TYPE"));
            commonService.buildModelCreateInfo(atomColumn);
            metaService.insert(atomColumn);
        }
        return sqlMapList.size();
    }

    private String checkStrData(String strData, String codekey, String tableId) {
        JSONArray jsonArray = JSONArray.parseArray(strData);
        String codes = "";
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            codes += jsonObject.getString(codekey) + ",";
        }
        codes = codes.substring(0, codes.length() - 1);
        return metaTableColumnService.checkUnique(codes, tableId);
    }

}
