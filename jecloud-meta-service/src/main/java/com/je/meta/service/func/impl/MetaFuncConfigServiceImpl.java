/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.MetaFuncConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
public class MetaFuncConfigServiceImpl implements MetaFuncConfigService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;

    @Override
    @Transactional
    public void save(DynaBean config) {
        String id = config.getStr("JE_CORE_FUNC_CONFIG_ID");
        if (!Strings.isNullOrEmpty(id)) {
            // 修改数据
            metaService.update(config);
            return;
        }

        // 校验是否重复
//            long count = serviceTemplate.selectCount("JE_CORE_FUNC_CONFIG", " AND JE_CORE_FUNCINFO_ID = ?  AND CONFIG_FUNCCODE = ?  AND CONFIG_TYPE = ? ",
//                    new Object[]{config.getStr("JE_CORE_FUNCINFO_ID"), config.getStr("CONFIG_FUNCCODE"), config.getStr("CONFIG_TYPE")});
//            if (count > 0) {
//                throw new PlatformException("该类型已存在，请勿重复添加！", PlatformExceptionEnum.JE_FUNCINFO_ERROR);
//            }
        // 新增数据
        commonService.buildModelCreateInfo(config);
        metaService.insert(config);
    }

    @Override
    public List<DynaBean> load(String funcId, String funcCode, String type, String status) {
        if (Strings.isNullOrEmpty(funcId) && Strings.isNullOrEmpty(funcCode)) {
            throw new PlatformException("查询条件 funcId|funcCode 至少包含一个！", PlatformExceptionEnum.JE_FUNCINFO_ERROR);
        }
        String whereSql = "";
        if (!Strings.isNullOrEmpty(funcId)) {
            whereSql += " AND JE_CORE_FUNCINFO_ID = '" + funcId + "'";
        }
        if (!Strings.isNullOrEmpty(funcCode)) {
            whereSql += " AND CONFIG_FUNCCODE = '" + funcCode + "'";
        }
        if (!Strings.isNullOrEmpty(type)) {
            whereSql += " AND CONFIG_TYPE = '" + type + "'";
        }
        if (!Strings.isNullOrEmpty(status)) {
            whereSql += " AND SY_STATUS = '" + status + "'";
        }
        whereSql += " order by SY_CREATETIME desc";
        //查询列表s
        return metaService.select("JE_CORE_FUNC_CONFIG", ConditionsWrapper.builder().apply(whereSql));
    }
}
