/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dictionary.impl.typed;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.dd.DDType;
import com.je.common.base.constants.tree.NodeType;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mapper.query.Condition;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.service.QueryBuilderService;
import com.je.common.base.util.ArrayUtils;
import com.je.common.base.util.StringUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.dd.DicInfoCache;
import com.je.meta.model.SQLTreeNodeType;
import com.je.meta.service.dictionary.AbstractMetaDictionaryTreeServiceImpl;
import com.je.meta.service.dictionary.MetaDictionaryTreeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("sqlTreeDictionaryTreeService")
public class SqlTreeDictionaryTreeServiceImpl extends AbstractMetaDictionaryTreeServiceImpl implements MetaDictionaryTreeService {

    @Autowired
    private DicInfoCache dicInfoCache;
    @Autowired
    private QueryBuilderService queryBuilder;

    @Override
    public List<JSONTreeNode> loadAsynTree(JSONObject obj, String product, boolean en, boolean onlyItem) {
        String ddCode = obj.getString("ddCode");
        //查找字典主表记录
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
        }
        if (dictionary == null) {
            throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //字典类型，本方法支持[外部树形字典，列表字典，树形字典]
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        throw new PlatformException("不支持的异步字典类型：" + ddType + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddType});
    }

    @Override
    public List<JSONTreeNode> loadSyncTree(JSONObject obj, String product, boolean en, boolean onlyItem) {
        //构建返回结果
        List<JSONTreeNode> array = new ArrayList<>();

        //根节点ID,默认为ROOT
        String rootId = obj.getString("rootId");
        if (StringUtil.isEmpty(rootId)) {
            rootId = ConstantVars.TREE_ROOT;
        }

        //字典编码
        String ddCode = obj.getString("ddCode");
        //字典名称，非必需字段
        String ddName = obj.getString("ddName");
        //字典标识，非必需字段 创建根节点对象ID使用,一次加载多个字典时使用
        String nodeInfo = obj.getString("nodeInfo");

        //构建根节点
        JSONTreeNode emptyRoot = new JSONTreeNode();
        emptyRoot.setText(ddName);
        emptyRoot.setParent("ROOT");
        emptyRoot.setId("ROOT");
        emptyRoot.setNodeInfo(nodeInfo);

        //查找字典主表记录
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
            if (dictionary == null) {
                throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
            }
        }

        //字典类型
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        if (!DDType.SQL_TREE.equals(ddType)) {
            throw new PlatformException("错误的字典类型：" + ddType + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //字典配置sql
        String ddWhereSql = dictionary.getStr("DICTIONARY_WHERESQL_SQL", "");
        String ddOrderSql = dictionary.getStr("DICTIONARY_ORDERSQL_SQL", "");

        //前端sql条件转换
        Query query;
        if (StringUtils.isNotBlank(obj.getString("j_query"))) {
            query = Query.build(obj.getString("j_query"));
        } else {
            query = new Query();
        }

        List<Condition> customQuery = (List<Condition>) obj.get("customQuery");
        query.addCustoms(JSONArray.toJSONString(customQuery));

        //排序条件
        String orderSql = "";
        if (!query.getOrder().isEmpty()) {
            orderSql = " ORDER BY " + query.buildOrder();
        } else if (StringUtils.isNotBlank(ddOrderSql)) {
            orderSql = ddOrderSql;
        }

        //字典sql语句
        String sql = dictionary.getStr("DICTIONARY_SQL").toUpperCase();
        if (sql != null && !sql.contains("WHERE")) {
            sql = sql + " WHERE 1=1 ";
        }
        //sql查询结果字段与树形字段配置
        String sqlConfig = dictionary.getStr("DICTIONARY_SQLCONFIG");
        if (StringUtil.isEmpty(sql) || StringUtil.isEmpty(sqlConfig)) {
            throw new PlatformException("SQL动态字段信息错误,字典:【" + ddCode + "】参数值:【" + sql + "】,配置：【" + sqlConfig + "】!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //树形字段配置
//        JSONArray config = JSON.parseArray(sqlConfig);
        JSONObject config = (JSONObject) JSON.parse(sqlConfig);
        //参数集合
        JSONObject varObj = null;
        if (obj.containsKey("params")) {
            varObj = obj.getJSONObject("params");
        }
        //构建查询器
        ConditionsWrapper sqlSelect = ConditionsWrapper.builder();
        //添加字典语句和条件
        sqlSelect.apply(queryBuilder.formatSqlParameter(sql))
                .apply(StringUtil.isNotEmpty(ddWhereSql), queryBuilder.formatSqlParameter(varObj, ddWhereSql));
        //拼接query条件
        query.buildWrapper(sqlSelect);

        //添加字典排序条件
        if (StringUtil.isEmpty(orderSql) && StringUtil.isNotEmpty(ddOrderSql)) {
            orderSql = ddOrderSql;
        }
        sqlSelect.apply(queryBuilder.formatSqlParameter(varObj, orderSql));
        //执行查询
        List<Map<String, Object>> list = executeRemoteQuery(product, buildSqlByCustomVariables(sqlSelect.getParameterSql(), getCustomerVariables(obj)));

        //构建树形数据
        List<JSONTreeNode> jsonTreeNodeList = new ArrayList<>();
        for (Map<String, Object> map : list) {
            //初始化树形对象
            JSONTreeNode node = new JSONTreeNode();
            //遍历树形字段配置
            for (String key : config.keySet()) {
                //树形字段类型
                String fieldType = key;
                //树形字段对应结果集中字段名
                String fieldName = config.getString(key);
                if (StringUtil.isNotEmpty(fieldName)) {
                    //字段值
                    String value = map.get(fieldName) + "";
                    //根据不同fieldType放入对应字段
                    if (StringUtil.isNotEmpty(fieldName)) {
                        if (SQLTreeNodeType.ID.equals(fieldType)) {
                            node.setId(value);
                        } else if (SQLTreeNodeType.NAME.equals(fieldType)) {
                            node.setText(value);
                        } else if (SQLTreeNodeType.CODE.equals(fieldType)) {
                            node.setCode(value);
                        } else if (SQLTreeNodeType.PARENT.equals(fieldType)) {
                            node.setParent(value);
                        } else if (SQLTreeNodeType.NODETYPE.equals(fieldType)) {
                            node.setNodeType(value);
                            node.setLeaf(NodeType.LEAF.equals(value));
                            node.setNodeInfoType(value);
                        } else if (SQLTreeNodeType.PATH.equals(fieldType)) {
                            node.setNodePath(value);
                        } else if (SQLTreeNodeType.DISABLED.equals(fieldType)) {
                            node.setDisabled(value);
                        } else if (SQLTreeNodeType.TREEORDERINDEX.equals(fieldType)) {
                            if (StringUtil.isNotEmpty(value)) {
                                node.setOrderIndex(value);
                            } else {
                                node.setOrderIndex("0");
                            }
                        } else if (SQLTreeNodeType.ICONCLS.equals(fieldType)) {
                            node.setIcon(value);
//                        } else if (SQLTreeNodeType.FONTCLS.equals(fieldType)) {
//                            node.set(value);
//                        } else if (SQLTreeNodeType.FONTBACKGROUNDCLS.equals(fieldType)) {
//                            node.setDescription(value);
                        }
                    }
                }
            }
            node.setBean(map);
            //加入树形结果集
            jsonTreeNodeList.add(node);
        }

        //补充树形字段值
        for (JSONTreeNode node : jsonTreeNodeList) {
            if (node.getNodeType().equals(NodeType.ROOT)) {
                rootId = node.getId();
                node.setDisabled("1");
                if (StringUtil.isNotEmpty(nodeInfo)) {
//                    node.setId(node.getId() + "_" + nodeInfo);
                    node.setNodeInfo(nodeInfo);
                }
            } else {
                node.setDisabled("0");
                if (StringUtil.isNotEmpty(nodeInfo)) {
                    node.setNodeInfo(nodeInfo);
//                    node.setId(node.getId() + "_" + nodeInfo);
//                    node.setParent(node.getParent() + "_" + nodeInfo);
                }
            }
        }

//        if (StringUtil.isNotEmpty(nodeInfo)) {
//            rootId = rootId + "_" + nodeInfo;
//        }

        JSONTreeNode rootNode = buildJSONNewTree(jsonTreeNodeList, rootId);
        rootNode.setText(ddName);
        rootNode.setParent("ROOT");
        if (onlyItem) {
            array.addAll(rootNode.getChildren());
        } else {
            array.add(rootNode);
        }

        return array;
    }

    @Override
    public List<JSONTreeNode> loadLinkTree(String ddCode, String parentId, String parentCode, String rootId, String paramStr, boolean en, Query query) {
        if (StringUtil.isEmpty(rootId)) {
            rootId = ConstantVars.TREE_ROOT;
        }
        List<JSONTreeNode> array = new ArrayList<>();
        //查询字典主表信息
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
        }
        if (dictionary == null) {
            throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR);
        }

        //字典配置项
        String ddWhereSql = dictionary.getStr("DICTIONARY_WHERESQL", "");
        String ddOrderSql = dictionary.getStr("DICTIONARY_ORDERSQL", "");

        //前端sql条件转换
        //排序条件
        String orderSql = "";
        if (!query.getOrder().isEmpty()) {
            orderSql = " ORDER BY " + query.buildOrder();
        } else if (StringUtils.isNotBlank(ddOrderSql)) {
            orderSql = ddOrderSql;
        }

        //字典sql语句
        String sql = dictionary.getStr("DICTIONARY_SQL");
        //sql查询结果字段与树形字段配置
        String sqlConfig = dictionary.getStr("DICTIONARY_SQLCONFIG");
        if (Strings.isNullOrEmpty(sql) || Strings.isNullOrEmpty(sqlConfig)) {
            throw new PlatformException("SQL动态字段信息错误,字典:【" + ddCode + "】参数值:【" + sql + "】,配置：【" + sqlConfig + "】!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR);
        }

        //树形字段配置
//        JSONArray config = JSON.parseArray(sqlConfig);
        JSONObject config = (JSONObject) JSON.parse(sqlConfig);
        //参数集合
        JSONObject varObj = null;
        if (StringUtil.isNotEmpty(paramStr)) {
            varObj = JSON.parseObject(paramStr);
        }

        //构建查询器
        ConditionsWrapper sqlSelect = ConditionsWrapper.builder();
        //添加字典语句和条件
        sqlSelect.apply(queryBuilder.formatSqlParameter(sql))
                .apply(StringUtil.isNotEmpty(ddWhereSql), queryBuilder.formatSqlParameter(varObj, ddWhereSql));

        //级联条件
        String parentField = "";
        String codeField = "";
        String idField = "";
        //遍历树形字段配置
        for (String key : config.keySet()) {
            //树形字段类型
            String fieldType = key;
            //树形字段对应结果集中字段名
            String fieldName = config.getString(key);
            if (StringUtil.isNotEmpty(fieldName)) {
                if (SQLTreeNodeType.ID.equals(fieldType)) {
                    idField = fieldName;
                } else if (SQLTreeNodeType.CODE.equals(fieldType)) {
                    codeField = fieldName;
                } else if (SQLTreeNodeType.PARENT.equals(fieldType)) {
                    parentField = fieldName;
                }
            }
        }
        if (StringUtil.isNotEmpty(parentId)) {
            //父节点条件
            sqlSelect.eq(parentField, parentId);
        } else if (StringUtil.isNotEmpty(parentCode)) {
            // 为了支持客户允许地区可以多选 yangzhi改于20191111
            String[] split = parentCode.split(ArrayUtils.SPLIT);
            String parentCodes = StringUtil.buildArrayToString(split);
            List<Map<String, Object>> parents = metaService.selectSql(sqlSelect.clone().apply(" AND " + codeField + " in(" + parentCodes + ")"));
            if (!parents.isEmpty()) {
                ArrayList<String> ids = Lists.newArrayList();
                for (Map<String, Object> bean : parents) {
                    ids.add(bean.getOrDefault(idField, "").toString());
                }
                //父节点条件
                sqlSelect.in(parentField, ids);
//                        querySql += " AND " + parentField + " in(" + StringUtil.buildArrayToString(list) + ")";
            } else {
                throw new PlatformException("数据错误，根据CODE【" + parentCode + "】未找到父项值!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWNPARENT_ERROR);
            }
        } else {
            //父节点条件
            sqlSelect.eq(parentField, rootId);
        }

        //添加字典排序条件
        if (StringUtil.isEmpty(orderSql) && StringUtil.isNotEmpty(ddOrderSql)) {
            orderSql = ddOrderSql;
        }
        sqlSelect.apply(queryBuilder.formatSqlParameter(varObj, orderSql));

        //执行查询
        List<Map<String, Object>> list = metaService.selectSql(sqlSelect);

        //构建树形数据
        for (Map<String, Object> map : list) {
            //初始化树形对象
            JSONTreeNode node = new JSONTreeNode();
            //遍历树形字段配置
            for (String key : config.keySet()) {
                //树形字段类型
                String fieldType = key;
                //树形字段对应结果集中字段名
                String fieldName = config.getString(key);
                if (StringUtil.isNotEmpty(fieldName)) {
                    //字段值
                    String value = map.get(fieldName) + "";
                    //根据不同fieldType放入对应字段
                    if (SQLTreeNodeType.ID.equals(fieldType)) {
                        node.setId(value);
                    } else if (SQLTreeNodeType.NAME.equals(fieldType)) {
                        node.setText(value);
                    } else if (SQLTreeNodeType.CODE.equals(fieldType)) {
                        node.setCode(value);
                    } else if (SQLTreeNodeType.PARENT.equals(fieldType)) {
                        node.setParent(value);
                    } else if (SQLTreeNodeType.NODETYPE.equals(fieldType)) {
                        node.setNodeType(value);
                        node.setLeaf(NodeType.LEAF.equals(value));
//                } else if (SQLTreeNodeType.NODEINFO.equals(fieldType)) {
//                    node.setNodeInfo(value);
//                } else if (SQLTreeNodeType.NODETYPE.toString().equals(fieldType)) {
//                    node.setNodeInfoType(value);
                    } else if (SQLTreeNodeType.PATH.equals(fieldType)) {
                        node.setNodePath(value);
                    } else if (SQLTreeNodeType.DISABLED.equals(fieldType)) {
                        node.setDisabled(value);
                    } else if (SQLTreeNodeType.TREEORDERINDEX.equals(fieldType)) {
                        if (StringUtil.isNotEmpty(value)) {
                            node.setOrderIndex(value);
                        } else {
                            node.setOrderIndex("0");
                        }
                    } else if (SQLTreeNodeType.ICONCLS.equals(fieldType)) {
                        node.setIcon(value);
//                } else if (SQLTreeNodeType.ICONCOLOR.toString().equals(fieldType)) {
//                    node.setIconColor(value);
//                } else if (SQLTreeNodeType.DESCRIPTION.toString().equals(fieldType)) {
//                    node.setDescription(value);
                    }
                }
            }
            //加入树形结果集
            array.add(node);
        }

        return array;
    }

}
