package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.upgrade.InstallResourceDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 打包门户
 */
@Service("upgradePortalDate")
public class UpgradePortalDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradePortalDate() {
        super(UpgradeResourcesEnum.PORTAL);
    }

    //子数据
    private static final String PORTAL_INFO_CHILD_CODE = "portalInfo";

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> list = getSourceIds(upgradeBean);
        if (list == null || list.size() == 0) {
            return;
        }
        packageResult.setProductPortals(packageResources(packageResult, list));
    }

    @Override
    public void installUpgradeMetaDate(String upgradeInstallId, List<DynaBean> list, String productCode, Map<String, Object> customerParameter) {
        //主数据
        installResources(buildAbstractDto(upgradeInstallId, list, productCode));
        //子数据
        List<DynaBean> allChildList = getChildList(list, PORTAL_INFO_CHILD_CODE);
        for (DynaBean dynaBean : allChildList) {
            String data = dynaBean.getStr("INFO_DATA");
            if (data == null) {
                continue;
            }
            dynaBean.set("INFO_DATA", data.getBytes());
        }
        installResources(new InstallResourceDTO.Builder()
                .upgradeInstallId(upgradeInstallId)
                .list(allChildList)
                .productCode(productCode)
                .identifier("JE_CORE_PORTAL_INFO_ID")
                .tableCode("JE_CORE_PORTAL_INFO")
                .logAdd(false)
                .build());
    }

    @Override
    public List<DynaBean> packageResources(PackageResult packageResult, List<String> resourceIds) {
        List<DynaBean> beanList = metaService.select(upgradeResourcesEnum.getTableCode(), ConditionsWrapper.builder()
                .in(upgradeResourcesEnum.getContentCode(), resourceIds));

        List<DynaBean> childList = metaService.select("JE_CORE_PORTAL_INFO",
                ConditionsWrapper.builder().in("JE_CORE_PORTAL_ENGINE_ID", resourceIds));
        Map<String, List<DynaBean>> map = new HashMap<>();
        for (DynaBean dynaBean : childList) {
            String INFO_DATA = new String((byte[]) dynaBean.get("INFO_DATA"));
            dynaBean.setStr("INFO_DATA", INFO_DATA);
            String dataId = dynaBean.getStr("JE_CORE_PORTAL_ENGINE_ID");
            if (map.get(dataId) == null) {
                map.put(dataId, new ArrayList<>());
            }
            map.get(dataId).add(dynaBean);
        }

        for (DynaBean dynaBean : beanList) {
            dynaBean.set(PORTAL_INFO_CHILD_CODE, map.get(dynaBean.getPkValue()));
        }
        return beanList;
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductPortals();
    }


}
