/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.column;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.APIWarnException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * 资源表列服务
 */
public interface MetaTableColumnService {

    /**
     * 检查字段是否属于某个视图
     * @param resourceId
     * @param columnCode
     * @return
     */
    String checkColumnBelongtoView(String resourceId,String columnCode);

    Map<String,Object> addFieldToLogicTable(DynaBean dynaBean);

    /**
     * 添加列
     * @param dynaBean
     */
    String addField(DynaBean dynaBean);

    /**
     * 删除列
     * @param dynaBean 自定义动态类
     * @param ids 要删除的列id对象，逗号分隔
     * @return
     */
    Integer removeColumn(DynaBean dynaBean, String ids);

    /**
     * 删除列
     * @param dynaBean 自定义动态类
     * @param ids 删除列id
     * @param isDll 是否执行ddl
     * @return
     */
    Integer removeColumn(DynaBean dynaBean, String ids, Boolean isDll);

    /**
     * 删除资源表时，如果有视图使用此字段，则也删除
     * @param tableCode 资源表编码
     * @param columns 删除列集合
     */
    void deleteCascadeViewColumn(String tableCode, List<DynaBean> columns);

    /**
     * 增量导入
     * @param dynaBean
     */
    void impNewCols(DynaBean dynaBean);

    /**
     * 初始化表格列信息
     *
     * @param resourceTable 修改表的信息
     * @param isTree        是否根节点
     */
    void initColumns(DynaBean resourceTable, Boolean isTree);

    /**
     * 对列进行检测。。 主要检测字段为空和字段重复
     *
     * @param columns 检查得列信息
     * @param jeCore  是否系统
     * @return
     */
    String checkColumns(List<DynaBean> columns, Boolean jeCore);

    /**
     * 获取删除指定表的字段的DDL
     *
     * @param tableCode 暂不明确
     * @param columns   暂不明确
     */
    List<String> requireDeletePhysicalColumnDDL(String tableCode, List<DynaBean> columns);

    /**
     * 物理删除指定表的字段
     *
     * @param tableCode 资源表
     * @param columns   表列
     */
    void deleteColumnMeta(String tableCode, List<DynaBean> columns);

    /**
     * 物理删除指定表的字段
     *
     * @param tableCode 暂不明确
     * @param columns   暂不明确
     * @param isDdl     暂不明确
     */
    void deleteColumn(String tableCode, List<DynaBean> columns, Boolean isDdl);

    /**
     * 表格键保存修改操作
     * @param dynaBean
     * @param strData
     * @return
     */
    int doUpdateList(DynaBean dynaBean, String strData);

    /**
     * 字段列校验
     * @param ids
     * @param pkValue
     * @return
     */
    String checkUnique(String ids, String pkValue);

    /**
     * 保存更新列标错
     * @param dynaBean
     * @param strData
     * @return
     */
    int doUpdateListByColumn(DynaBean dynaBean, String strData) throws APIWarnException;

    /**
     * 根据resourceIds查询实体表信息
     * @param tableCode
     * @param split
     * @return
     */
    List<DynaBean> selectTableByPks(String tableCode, String[] split);

    /**
     * 如果是ID不能删除列
     * @param ids
     * @return
     */
    String checkTypeById(String ids);

    String checkStrData(String strData);

    InputStream exportColumns(String title,String  type, String tableCodes);

    boolean checkIsExistForeignKeyByColumnIds(String ids);
}
