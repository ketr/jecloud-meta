/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.index;

import com.je.common.base.DynaBean;

import java.util.List;

/**
 * 资源表索引服务
 */
public interface MetaTableIndexService {
    /**
     * 保存创建索引的逻辑数据
     * @param funcInfo
     * @param resourceTable
     * @param columnCode
     * @param columnId
     * @return
     */
    DynaBean saveLogicData(DynaBean funcInfo, DynaBean resourceTable, String columnCode, String columnId);

    /**
     * 获取创建索引的接口
     * @param index
     * @param resourceTable
     * @return
     */
    String getDDL4AddIndex(DynaBean index,DynaBean resourceTable);

    /**
     * 初始化表格键信息
     *
     * @param resourceTable 修改表的地方
     * @param isTree        是否根节点
     */
    void initIndexs(DynaBean resourceTable, Boolean isTree);

    /**
     * 创建索引
     *
     * @param funcInfo   功能
     * @param columnCode 创建索引的列编码
     * @param columnId   创建索引的列id
     * @return
     */
    DynaBean createIndexByColumn(DynaBean funcInfo, DynaBean resourceTable, String columnCode, String columnId);

    /**
     * 删除索引
     *
     * @param funcId     功能id
     * @param columnCode 删除索引的列编码
     * @param columnId   删除索引的列id
     * @return
     */
    String removeIndexByColumn(String funcId, String columnCode, String columnId);

    /**
     * 对索引进行检测，主要检测索引重复和索引为空
     *
     * @param indexs 检索的索引
     * @return
     */
    String checkIndexs(List<DynaBean> indexs);

    /**
     * 删除索引
     * @param dynaBean 自定义动态类
     * @param ids 索引id
     * @return
     */
    Integer removeIndex(DynaBean dynaBean, String ids);

    /**
     * 物理删除指定表的索引
     *
     * @param tableCode 表名
     * @param indexs    索引
     */
    void deleteIndex(String tableCode, List<DynaBean> indexs);

    /**
     * 删除指定表的索引元数据
     * @param tableCode
     * @param indexs
     */
    void deleteIndexMeta(String tableCode, List<DynaBean> indexs);

    /**
     * 删除指定表的索引元数据
     * @param tableCode
     * @param indexs
     */
    String requireDeletePhysicalIndexDDL(String tableCode, List<DynaBean> indexs);

    /**
     * 修改保存列表索引
     * @param dynaBean
     * @param strData
     * @return
     */
    int doUpdateList(DynaBean dynaBean, String strData);

    String checkParameter(String strData);
}
