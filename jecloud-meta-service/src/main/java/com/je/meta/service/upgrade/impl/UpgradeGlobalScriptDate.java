package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打包全局脚本
 */
@Service("upgradeGlobalScriptDate")
public class UpgradeGlobalScriptDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeGlobalScriptDate() {
        super(UpgradeResourcesEnum.GLOBAL_SCRIPT);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> list = getSourceIds(upgradeBean);
        if (list == null || list.size() == 0) {
            return;
        }
        packageResult.setProductGlobalScript(packageResources(packageResult, list));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductGlobalScript();
    }


}
