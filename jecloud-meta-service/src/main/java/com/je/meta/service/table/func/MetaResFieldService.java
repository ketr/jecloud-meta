/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.func;

import com.je.common.base.DynaBean;

import java.util.List;

public interface MetaResFieldService {

    /***
     * 构建保存字段的默认信息
     * @param dynaBean 传入信息
     */
    void buildDefault(DynaBean dynaBean);

    /**
     * 与列表字段同步
     * @param dynaBean 传入信息
     */
    void doSync(DynaBean dynaBean);

    /**
     * 更新字段
     * @param dynaBean 传入信息
     * @return
     */
    DynaBean updateField(DynaBean dynaBean);

    /**
     * 删除分组字段  将所在这个组的字段所属组清空
     * @param fields
     */
    String clearGroupFields(List<DynaBean> fields);

    /**
     * 设置租户自定义字典表单按钮样式
     */
    int doDicConfigSync();

    /**
     * 设置租户自定义字典表单按钮样式
     */
    int doFieldMaxLengthSync();

    /**
     * 同步流程所有KEY
     */
    void doSyncWfInfo();


    int doRemove(String ids);
}
