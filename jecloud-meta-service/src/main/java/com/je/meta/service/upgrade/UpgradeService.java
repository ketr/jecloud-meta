/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.upgrade;

import com.je.common.base.DynaBean;
import com.je.common.base.document.InternalFileBO;
import com.je.common.base.upgrade.PackageResult;

import java.io.InputStream;
import java.util.List;

public interface UpgradeService {

    /**
     * 写入升级包功能
     *
     * @param upgradeId
     * @param funcIds
     */
    void insertPackageFuncs(String upgradeId, String funcIds);

    /**
     * 写入升级包表
     *
     * @param upgradeId
     * @param tableIds
     */
    void insertPackageTables(String upgradeId, String tableIds);

    /**
     * 写入数据字典
     *
     * @param upgradeId
     * @param dictionaryIds
     */
    void insertPackageDictionarys(String upgradeId, String dictionaryIds);

    /**
     * 写入全局程序
     *
     * @param upgradeId
     * @param scriptIds
     */
    void insertPackageGlobalScripts(String upgradeId, String scriptIds);

    /**
     * 写入变量
     *
     * @param upgradeId
     * @param variableIds
     */
    void insertPackageSystemVariables(String upgradeId, String variableIds);

    /**
     * 写入升级包表
     *
     * @param upgradeId
     * @param tableIds
     */
    void insertPackageBusinessData(String upgradeId, String tableIds);

    /**
     * 打包
     *
     * @param upgradeBean
     * @return
     */
    PackageResult doPackage(DynaBean upgradeBean);

    /**
     * 执行安装
     *
     * @param pkValue
     * @return
     */
    String doInstall(String pkValue, String fileKey);

    /**
     * 安装前校验
     *
     * @param pkValue
     */
    String doCheck(String pkValue);

    void doSave(DynaBean dynaBean);

    String importDictionaryFromFuncinfo(String upgradeId);

    String saveResources(String oldId, String newId);

    String getResourceIds(String upgradeId, String upgraderesourceType);

    String importTableByFuncinfo(String upgradeId);

    String insertPackageResource(String upgradeId, String sourceIds, String type);

    void updateState(InternalFileBO fileBO, String fileName, String upgradeId);

    InputStream buildZip(PackageResult packageResult, List<DynaBean> functionBeanList);

    String generateIncrementalUpgradePackage(DynaBean dynaBean, String productCode, String version);

    String generateFullUpgradePackage(DynaBean dynaBean, String productCode, String version);

    void processResourceBeans(DynaBean upgradeBean, List<DynaBean> functionBeanList);

    List<DynaBean> buildPluginResources(String jsonStr, String SY_PRODUCT_ID, String SY_PRODUCT_NAME, String SY_PRODUCT_CODE);

}
