/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func;

public interface MetaCopyFuncService {

    /**
     * 复制列
     * @param newFuncId
     * @param oldFuncId
     */
    void copyColumn(String newFuncId,String oldFuncId);


    /**
     * 复制字段
     * @param newFuncId
     * @param oldFuncId
     */
    void copyField(String newFuncId,String oldFuncId);

    /**
     * 复制流转策略
     * @param newFuncId
     * @param oldFuncId
     */
    void copyDataFlow(String newFuncId,String oldFuncId);

    /**
     * 复制按钮
     * @param newFuncId
     * @param oldFuncId
     */
    void copyButton(String newFuncId,String oldFuncId);

    /**
     * 复制高级查询
     * @param newFuncId
     * @param oldFuncId
     */
    void copyQuerys(String newFuncId,String oldFuncId);


    /**
     * 复制查询策略
     * @param newFuncId
     * @param oldFuncId
     */
    void copyStrategies(String newFuncId,String funcCode,String oldFuncId);
}
