package com.je.meta.service.upgrade.impl;

import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.upgrade.UpgradeResourcesEnum;

public class UpgradeMeteDateFactory {

    public static AbstractUpgradeMetaDate getUpgradeMeteDateServiceByType(String type) {
        String serviceName = UpgradeResourcesEnum.getUpgradeResourcesEnumByType(type).getPackServiceName();
        return SpringContextHolder.getBean(serviceName);
    }

}
