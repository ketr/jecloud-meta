package com.je.meta.service.table.column;

import com.je.common.base.DynaBean;

/**
 * 辅助列操作
 */
public interface MetaTableAuxiliaryColumnService {

    /**
     * 字典辅助添加列
     */
    String addColumnByDD(DynaBean dynaBean);

    /**
     * 字典辅助添加列
     */
    String addColumnByDDList(DynaBean dynaBean);

    /**
     * 表辅助添加列
     */
    String addColumnByTable(DynaBean dynaBean);

    /**
     * 原子辅助添加列
     * @param strData 添加得数据
     * @param tableCode 表编码
     * @param pkValue 主键
     * @return
     */
    Integer addColumnByAtom(String strData, String tableCode, String pkValue);

    /**
     * 存为原子
     * @param strData 添加得数据
     * @param pkValue 主键
     * @return
     */
    Integer addAtomByColumn(String strData, String pkValue);
}
