/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dictionary.impl.typed;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.dd.DDSQLListType;
import com.je.common.base.constants.dd.DDType;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mapper.query.Condition;
import com.je.common.base.mapper.query.Query;
import com.je.common.base.service.QueryBuilderService;
import com.je.common.base.util.StringUtil;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.dd.DicInfoCache;
import com.je.meta.service.dictionary.AbstractMetaDictionaryTreeServiceImpl;
import com.je.meta.service.dictionary.MetaDictionaryTreeService;
import org.apache.commons.lang.StringUtils;
import org.bouncycastle.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("sqlDictionaryTreeService")
public class SqlDictionaryTreeServiceImpl extends AbstractMetaDictionaryTreeServiceImpl implements MetaDictionaryTreeService {

    private static final Logger logger = LoggerFactory.getLogger(SqlDictionaryTreeServiceImpl.class);

    @Autowired
    private DicInfoCache dicInfoCache;
    @Autowired
    private QueryBuilderService queryBuilder;

    @Override
    public List<JSONTreeNode> loadAsynTree(JSONObject obj, String product, boolean en, boolean onlyItem) {
        String ddCode = obj.getString("ddCode");
        //查找字典主表记录
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
        }
        if (dictionary == null) {
            throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //字典类型，本方法支持[外部树形字典，列表字典，树形字典]
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        throw new PlatformException("不支持的异步字典类型：" + ddType + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddType});
    }

    @Override
    public List<JSONTreeNode> loadSyncTree(JSONObject obj, String product, boolean en, boolean onlyItem) {
        //构建返回结果
        List<JSONTreeNode> array = new ArrayList<>();

        //字典编码
        String ddCode = obj.getString("ddCode");
        //字典名称，非必需字段
        String ddName = obj.getString("ddName");
        //字典标识，非必需字段 创建根节点对象ID使用,一次加载多个字典时使用
        String nodeInfo = obj.getString("nodeInfo");

        //构建根节点
        JSONTreeNode emptyRoot = new JSONTreeNode();
        emptyRoot.setText(ddName);
        emptyRoot.setParent("ROOT");
        emptyRoot.setId("ROOT_" + nodeInfo);
        emptyRoot.setNodeInfo(nodeInfo);

        //查找字典主表记录
        DynaBean dictionary = dicInfoCache.getCacheValue(ddCode);
        if (dictionary == null) {
            dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", ddCode));
            if (dictionary == null) {
                throw new PlatformException("未找到数据字典：" + ddCode + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
            }
        }

        //字典类型
        String ddType = dictionary.getStr("DICTIONARY_DDTYPE");
        if (!DDType.SQL.equals(ddType)) {
            throw new PlatformException("错误的字典类型：" + ddType + "!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }

        //字典配置sql
        String ddWhereSql = dictionary.getStr("DICTIONARY_WHERESQL_SQL", "");
        String ddOrderSql = dictionary.getStr("DICTIONARY_ORDERSQL_SQL", "");

        //前端sql条件转换
        Query query;
        if (StringUtils.isNotBlank(obj.getString("j_query"))) {
            query = Query.build(obj.getString("j_query"));
        } else {
            query = new Query();
        }

        List<Condition> customQuery = (List<Condition>) obj.get("customQuery");
        query.addCustoms(JSONArray.toJSONString(customQuery));

        //排序条件
        String orderSql = "";
        if (!query.getOrder().isEmpty()) {
            orderSql = " ORDER BY " + query.buildOrder();
        } else if (StringUtils.isNotBlank(ddOrderSql)) {
            orderSql = ddOrderSql;
        }

        //字典sql语句
        String sql = dictionary.getStr("DICTIONARY_SQL");
        if (Strings.toLowerCase(sql).indexOf("where") == -1) {
            sql += " WHERE 1=1 ";
        }

        //sql查询结果字段与树形字段配置
        String sqlConfig = dictionary.getStr("DICTIONARY_SQLPZXXLB");

        if (StringUtil.isEmpty(sql) || StringUtil.isEmpty(sqlConfig)) {
            array.add(emptyRoot);
            logger.error("SQL动态字段信息错误,字典:【" + ddCode + "】参数值:【" + sql + "】,配置：【" + sqlConfig + "】!");
            return array;
        }

        //树形字段配置
        JSONObject config = (JSONObject) JSON.parse(sqlConfig);
        //参数集合
        JSONObject varObj = null;
        if (obj.containsKey("params")) {
            varObj = obj.getJSONObject("params");
        }
        //构建查询器
        ConditionsWrapper sqlSelect = ConditionsWrapper.builder();
        //添加字典语句和条件
        sqlSelect.apply(queryBuilder.formatSqlParameter(varObj, sql))
                .apply(StringUtil.isNotEmpty(ddWhereSql), queryBuilder.formatSqlParameter(varObj, ddWhereSql));
        //拼接query条件
        query.buildWrapper(sqlSelect);

        //添加字典排序条件
        if (StringUtil.isEmpty(orderSql) && StringUtil.isNotEmpty(ddOrderSql)) {
            orderSql = ddOrderSql;
        }
        sqlSelect.apply(queryBuilder.formatSqlParameter(varObj, orderSql));
        //数据结构Code 0 单条  1 多条
        String sjjgCode = dictionary.getStr("DICTIONARY_SJJG_CODE");

        //执行查询
        List<Map<String, Object>> list = executeRemoteQuery(product, buildSqlByCustomVariables(sqlSelect.getParameterSql(), getCustomerVariables(obj)));
        //是否包含多值字段
        boolean isMore = false;
        if (!com.google.common.base.Strings.isNullOrEmpty(sjjgCode) && "0".equals(sjjgCode)) {
            isMore = true;
        }
        if (isMore && list.size() > 1){
            throw new PlatformException("sql字典，数据类型为单条，查询结果为多条！", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR, new Object[]{ddCode});
        }
        //分隔符（字段标准，按照顺序的标准规则，示例： 名称->张,李）
        String fieldStandard = dictionary.getStr("DICTIONARY_FGF");
        Map<String, String> fieldSplitName = new HashMap<>();
        Iterator<String> iterator = config.keySet().iterator();
        //循环并得到key列表
        while (iterator.hasNext()) {
            //获得key
            String fieldType = iterator.next();
            if ("codeField".equals(fieldType)) {

                //获得key值对应的value
                String fieldName = config.getString(fieldType);
                fieldSplitName.put(fieldType, fieldStandard);
            }
        }

        //构建树形数据
        List<JSONTreeNode> jsonTreeNodeList = buildJsonTreeNodeList(obj, list, config, isMore, fieldStandard, fieldSplitName, nodeInfo);

        //配置根节点
        JSONTreeNode rootNode = new JSONTreeNode();
        rootNode.setText(ddName);
        rootNode.setParent("ROOT");
        if (StringUtil.isNotEmpty(nodeInfo)) {
            rootNode.setNodeInfo(nodeInfo);
            // + rootNode.getId());
            rootNode.setId("ROOT_" + nodeInfo);
        } else {
            // + rootNode.getId());
            rootNode.setId("ROOT");
        }

        if (onlyItem) {
            array.addAll(jsonTreeNodeList);
        } else {
            rootNode.getChildren().addAll(jsonTreeNodeList);
            array.add(rootNode);
        }

        return array;
    }

    /**
     * 构建JSONTreeNode集合
     *
     * @param list
     * @param config
     * @param isMore
     * @param fieldStandard
     * @param fieldSplitName
     * @param nodeInfo
     * @return
     */
    public List<JSONTreeNode> buildJsonTreeNodeList(JSONObject obj, List<Map<String, Object>> list, JSONObject config, boolean isMore, String fieldStandard, Map<String, String> fieldSplitName, String nodeInfo) {
        List<JSONTreeNode> jsonTreeNodeList = Lists.newArrayList();
        for (Map<String, Object> map : list) {
            //初始化树形对象
            JSONTreeNode ddValue = new JSONTreeNode();
            //字典值
            List<JSONTreeNode> ddValues = new ArrayList<>();
            //TODO 未知
            Integer valueLength = -1;
            //构建字典值   多数据则值获取拆分后值的长度

            //字段类型
            Iterator<String> iterator = config.keySet().iterator();
            //循环并得到key列表
            while (iterator.hasNext()) {
                //获得key
                String fieldType = iterator.next();
                //获得key值对应的value
                String fieldName = config.getString(fieldType);
                //字段值
                if (map.get(fieldName) == null) {
                    continue;
                }
                String value = map.get(fieldName) + "";
                //将值拼接操作
                if (isMore) {
                    if (!com.google.common.base.Strings.isNullOrEmpty(fieldStandard) && DDSQLListType.ID.equals(fieldType)) {
                        valueLength = value.split(fieldStandard).length;
                    } else if (!com.google.common.base.Strings.isNullOrEmpty(fieldStandard) && DDSQLListType.NAME.equals(fieldType)) {
                        valueLength = value.split(fieldStandard).length;
                    } else if (!com.google.common.base.Strings.isNullOrEmpty(fieldStandard) && DDSQLListType.CODE.equals(fieldType)) {
                        valueLength = value.split(fieldStandard).length;
                    } else if (!com.google.common.base.Strings.isNullOrEmpty(fieldStandard) && DDSQLListType.ICONCLS.equals(fieldType)) {
                        valueLength = value.split(fieldStandard).length;
                    }
                    if (valueLength > 0) {
                        break;
                    }
                } else {
                    if (DDSQLListType.ID.equals(fieldType)) {
                        ddValue.setId(value);
                    } else if (DDSQLListType.NAME.equals(fieldType)) {
                        ddValue.setText(value);
                    } else if (DDSQLListType.CODE.equals(fieldType)) {
                        ddValue.setCode(value);
                    } else if (DDSQLListType.ICONCLS.equals(fieldType)) {
                        ddValue.setIcon(value);
                    }
                }
            }

            if (isMore && valueLength > 0) {
                for (Integer i = 0; i < valueLength; i++) {
                    JSONTreeNode splitValue = new JSONTreeNode();
                    Iterator<String> iterators = config.keySet().iterator();
                    //循环并得到key列表
                    while (iterators.hasNext()) {
                        //获得key
                        String fieldType = iterators.next();
                        //获得key值对应的value
                        String fieldName = config.getString(fieldType);
                        if (map.get(fieldName) == null) {
                            continue;
                        }
                        //字段值
                        String value = map.get(fieldName) + "";
                        String[] valueArray = new String[]{};
                        if (StringUtil.isNotEmpty(fieldStandard)) {
                            valueArray = value.split(fieldStandard);
                            if (valueArray.length != valueLength) {
                                logger.error("发现字典中字段【" + fieldType + "】值拆分长度不一致,请检查!");
                                throw new PlatformException("发现字典中字段【" + fieldType + "】值拆分长度不一致,请检查!", PlatformExceptionEnum.JE_CORE_DIC_CHECKITEM_ERROR, new Object[]{obj});
                            }
                            //没有定义分割
                        }

                        if (DDSQLListType.ID.equals(fieldType)) {
                            splitValue.setId(valueArray[i]);
                        } else if (DDSQLListType.NAME.equals(fieldType)) {
                            splitValue.setText(valueArray[i]);
                        } else if (DDSQLListType.CODE.equals(fieldType)) {
                            splitValue.setCode(valueArray[i]);
                        } else if (DDSQLListType.ICONCLS.equals(fieldType)) {
                            splitValue.setIcon(valueArray[i]);
                        } else if (DDSQLListType.FONTCLS.equals(fieldType)) {
                            splitValue.setIcon(valueArray[i]);
                        }else if (DDSQLListType.FONTBACKGROUNDCLS.equals(fieldType)) {
                            splitValue.setIcon(valueArray[i]);
                        }

                        if (StringUtil.isNotEmpty(nodeInfo)) {
                            splitValue.setNodeInfo(nodeInfo);
                            splitValue.setParent("ROOT_" + nodeInfo);
                        } else {
                            splitValue.setParent("ROOT");
                        }

                        splitValue.setLeaf(true);
                        splitValue.setBean(map);
                    }
                    ddValues.add(splitValue);
                }
                jsonTreeNodeList.addAll(ddValues);
                continue;
            }

            if (StringUtil.isNotEmpty(nodeInfo)) {
                ddValue.setNodeInfo(nodeInfo);
                ddValue.setParent("ROOT_" + nodeInfo);
            } else {
                ddValue.setParent("ROOT");
            }
            ddValue.setBean(map);
            ddValue.setLeaf(true);
            jsonTreeNodeList.add(ddValue);
        }
        return jsonTreeNodeList;
    }

    @Override
    public List<JSONTreeNode> loadLinkTree(String ddCode, String parentId, String parentCode, String rootId, String paramStr, boolean en, Query query) {
        throw new PlatformException("该字典类型不支持联动!", PlatformExceptionEnum.JE_CORE_DIC_UNKOWN_ERROR);
    }
}
