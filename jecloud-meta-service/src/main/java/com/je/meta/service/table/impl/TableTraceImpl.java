/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.impl;

import com.alibaba.fastjson2.JSON;
import com.je.common.base.DynaBean;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.table.AbstractTableTrace;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("tableTrace")
public class TableTraceImpl extends AbstractTableTrace {

    @Override
    public DynaBean save(List<DynaBean> columns, DynaBean trace, DynaBean oldBean, DynaBean newBean, String oper) {
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        String tableColumnCode = "";
        String tableColumnName="";
        if ("INSERT".equals(oper)) {
            trace.set("TABLETRACE_OPER", "添加");
            for (DynaBean column : columns) {
                if (column.getStr("TABLECOLUMN_CODE").startsWith("SY_")) {
                    continue;
                }
                if (!StringUtil.isNotEmpty(newBean.getStr(column.getStr("TABLECOLUMN_CODE")))) {
                    continue;
                }
                Map<String, Object> map = buildJsonData(oper, column, newBean, oldBean);
                dataList.add(map);
            }
            tableColumnCode = newBean.getStr("RESOURCETABLE_TABLECODE");
            tableColumnName = newBean.getStr("RESOURCETABLE_TABLENAME");
        } else if ("UPDATE".equals(oper)) {
            trace.set("TABLETRACE_OPER", "修改");
            for (DynaBean column : columns) {
                if (column.getStr("TABLECOLUMN_CODE").startsWith("SY_")) {
                    continue;
                }
                if (!newBean.containsKey(column.getStr("TABLECOLUMN_CODE")) || oldBean.getStr(column.getStr("TABLECOLUMN_CODE"), "").equals(newBean.getStr(column.getStr("TABLECOLUMN_CODE"), ""))) {
                    continue;
                }
                Map<String, Object> map = buildJsonData(oper, column, newBean, oldBean);
                dataList.add(map);
            }
            DynaBean db = metaService.selectOne("je_core_resourcetable", ConditionsWrapper.builder().eq("je_core_resourcetable_id", newBean.getStr("JE_CORE_RESOURCETABLE_ID")));
            trace.set("TABLETRACE_SFYY", oldBean.getStr("RESOURCETABLE_ISCREATE"));
            if (db != null) {
                if ("VIEW".equals(db.getStr("RESOURCETABLE_TYPE"))) {
                    trace.set("TABLETRACE_SFYY", "1");
                }
            }
            tableColumnCode = oldBean.getStr("RESOURCETABLE_TABLECODE");
            tableColumnName = oldBean.getStr("RESOURCETABLE_TABLENAME");
        } else if ("DELETE".equals(oper)) {
            trace.set("TABLETRACE_OPER", "删除");
            for (DynaBean column : columns) {
                if (column.getStr("TABLECOLUMN_CODE").startsWith("SY_")) {
                    continue;
                }
                if (StringUtil.isEmpty(oldBean.getStr(column.getStr("TABLECOLUMN_CODE")))) {
                    continue;
                }
                Map<String, Object> map = buildJsonData(oper, column, newBean, oldBean);
                dataList.add(map);
            }
            tableColumnCode = oldBean.getStr("TABLEINDEX_FIELDCODE ");
        }
        String jsonStr = "";
        if (dataList.size() > 0) {
            jsonStr = JSON.toJSONString(dataList);
        }
        trace.set("TABLETRACE_TABLENAME", "资源表");
        trace.set("TABLETRACE_FIELDCODE", tableColumnCode);
        trace.set("TABLETRACE_FIELDNAME", tableColumnName);
        trace.set("TABLETRACE_XGNRJSON", jsonStr);
        metaService.insert(trace);
        return trace;
    }

    private Map<String, Object> buildJsonData(String oper, DynaBean column, DynaBean newBean, DynaBean oldBean) {
        Map<String, Object> map = new HashMap<>();
        map.put("tableColumnName", column.getStr("TABLECOLUMN_NAME"));
        map.put("tableColumnCode", column.getStr("TABLECOLUMN_CODE"));
        if ("INSERT".equals(oper)) {
            map.put("type", "insert");
            map.put("tableColumnValue", newBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        if ("UPDATE".equals(oper)) {
            map.put("type", "update");
            map.put("tableColumnOldValue", oldBean.getStr(column.getStr("TABLECOLUMN_CODE")));
            map.put("tableColumnValue", newBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        if ("DELETE".equals(oper)) {
            map.put("type", "delete");
            map.put("tableColumnValue", oldBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        return map;
    }
}
