/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.develop.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaRbacService;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.MetaWorkflowService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.develop.DevelopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("developService")
public class DevelopServiceImpl implements DevelopService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private MetaWorkflowService metaWorkflowService;
    @Autowired
    MetaRbacService metaRbacService;

    @Override
    public DynaBean getCoreResourceCount() {
        DynaBean result = new DynaBean();
        //产品
        result.put("product",metaService.countBySql(ConditionsWrapper.builder().table("JE_PRODUCT_MANAGE")));
        //资源表
        result.put("table",metaService.countBySql(ConditionsWrapper.builder().table("JE_CORE_RESOURCETABLE")
                            .eq("SY_NODETYPE","LEAF")));
        //菜单
        result.put("menu",metaRbacService.countByNativeQuery(NativeQuery.build().tableCode("JE_CORE_MENU")
                            .eq("SY_NODETYPE","LEAF")));
        //功能
        result.put("func",metaService.countBySql(ConditionsWrapper.builder().table("JE_CORE_FUNCINFO")
                .in("FUNCINFO_NODEINFOTYPE","FUNC","FUNCFIELD")));
        //数据字典
        result.put("dictionary",metaService.countBySql(ConditionsWrapper.builder().table("JE_CORE_DICTIONARY")));
        //工作流
        result.put("workflow",metaWorkflowService.countBySql("SELECT * FROM  JE_WORKFLOW_PROCESSINFO"));
        return result;
    }


}
