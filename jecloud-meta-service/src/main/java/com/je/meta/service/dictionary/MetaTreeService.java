/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.dictionary;


import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.mapper.query.Query;
import com.je.core.entity.extjs.JSONTreeNode;
import java.util.List;

public interface MetaTreeService {

    /**
     * 读取异步树
     *
     * @param obj      字典查询条件
     * @param en       国际化
     * @param onlyItem 只包含子项
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> loadAsynTree(JSONObject obj, boolean en, boolean onlyItem);

    /**
     * 异步树时查找接口
     *
     * @param obj       字典查询条件
     * @param queryType 查找类型
     * @param value     查找值
     * @param en        国际化
     * @param onlyItem  只包含子项
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> findAsyncNodes(JSONObject obj, String queryType, String value, boolean en, boolean onlyItem);

    /**
     * 读取同步树
     *
     * @param obj      字典查询条件
     * @param en       国际化
     * @param onlyItem 只包含子项
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> loadSyncTree(JSONObject obj, boolean en, boolean onlyItem);

    /**
     * 级联下拉字典
     *
     * @param ddCode 字典编码
     * @param parentId 父级id
     * @param parentCode 多个父级code
     * @param rootId 跟节点ID
     * @param paramStr 参数数组
     * @param en 国际化
     * @param query 查询条件
     * @return java.util.List<com.je.core.entity.extjs.JSONTreeNode>
     */
    List<JSONTreeNode> loadLinkTree(String ddCode, String parentId, String parentCode, String rootId, String paramStr, boolean en, Query query);


}
