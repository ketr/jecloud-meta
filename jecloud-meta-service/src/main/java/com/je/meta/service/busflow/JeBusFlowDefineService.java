package com.je.meta.service.busflow;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;

import java.util.HashMap;

public interface JeBusFlowDefineService {
    /**
     * 新增节点
     * @param param
     * @param returnObj
     */
    public DynaBean doAddNode(HashMap<String,String> param, JSONObject returnObj);
    /**
     * 删除节点
     * @param param
     * @param returnObj
     */
    public void doDelNode(HashMap<String,String> param, JSONObject returnObj);
    /**
     * 初始化节点信息
     * @param param
     * @param returnObj
     */
    public DynaBean doInitDrawNode(HashMap<String,String> param, JSONObject returnObj);
}
