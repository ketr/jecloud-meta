/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.JEUUID;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.ResColumnPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;

@Service
public class ResColumnPlanServiceImpl implements ResColumnPlanService {

    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean doSave(DynaBean dynaBean) {
        String planId = JEUUID.uuid();
        dynaBean.setStr("JE_CORE_RESOURCECOLUMN_PLAN_ID", planId);
        dynaBean.setStr("SY_STATUS", "1");
        dynaBean.setStr("PLAN_TYPE", "1");
        dynaBean.setStr("PLAN_SY", "1");
        commonService.buildModelCreateInfo(dynaBean);
        String wheresql = " PLAN_FUNCINFO_ID = " + dynaBean.getStr("PLAN_FUNCINFO_ID");
        commonService.generateOrderIndex(dynaBean, wheresql);
        List<DynaBean> list = metaService.select("JE_CORE_RESOURCECOLUMN_PLAN", ConditionsWrapper.builder().eq("PLAN_FUNCINFO_ID", dynaBean.getStr("PLAN_FUNCINFO_ID")));
        dynaBean.set("SY_ORDERINDEX", list.size() + 1);
        metaService.insert(dynaBean);
        String columnStr = dynaBean.getStr("columnId");
        String fromPlanId = dynaBean.getStr("fromPlanId");
        if (StringUtil.isNotEmpty(fromPlanId)) {
            copyColumnConfig(fromPlanId, planId, columnStr);
        }

        return dynaBean;
    }

    @Override
    public void doUpdate(DynaBean dynaBean) {
       /* commonService.buildModelModifyInfo(dynaBean);
        metaService.update(dynaBean);
        String columnStr = dynaBean.getStr("columnStr");*/
    }

    private void copyColumnConfig(String fromPlanId, String planId, String ids) {
        ConditionsWrapper wheresql = ConditionsWrapper.builder().eq("RESOURCECOLUMN_PLAN_ID", fromPlanId);
        if (StringUtil.isNotEmpty(ids)) {
            wheresql.in("RESOURCECOLUMN_CODE", Arrays.asList(ids.split(",")));
        }
        List<DynaBean> list = metaService.select("JE_CORE_RESOURCECOLUMN", wheresql);
        for (DynaBean item : list) {
            DynaBean dynaBean = item;
            dynaBean.setStr("JE_CORE_RESOURCECOLUMN_ID", JEUUID.uuid());
            dynaBean.setStr("RESOURCECOLUMN_PLAN_ID", planId);
            commonService.buildModelCreateInfo(dynaBean);
            metaService.insert(dynaBean);
        }
    }

    @Transactional
    @Override
    public void doRemove(DynaBean dynaBean) {
        /**
         * 1.删除方案
         * 2.删除该方案的字段
         */
        String childTableCode = "";
        String childTableFk = "";
        if ("1".equals(dynaBean.getStr("PLAN_TYPE"))) {
            childTableCode = "JE_CORE_RESOURCECOLUMN";
            childTableFk = "RESOURCECOLUMN_PLAN_ID";
        }
        if ("2".equals(dynaBean.getStr("PLAN_TYPE"))) {
            childTableCode = "JE_CORE_USERINFO_COLUMN";
            childTableFk = "COLUMN_PLAN_ID";
        }
        metaService.delete(dynaBean.getTableCode(), ConditionsWrapper.builder()
                .eq("JE_CORE_RESOURCECOLUMN_PLAN_ID", dynaBean.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID")));

        metaService.delete(childTableCode, ConditionsWrapper.builder()
                .eq(childTableFk, dynaBean.getStr(childTableFk)));

    }

    @Override
    public DynaBean saveUserPlan(DynaBean dynaBean) {
        String planId = JEUUID.uuid();
        dynaBean.setStr("PLAN_DEFAULT", dynaBean.getStr("PLAN_DEFAULT") == null ? "0" : dynaBean.getStr("PLAN_DEFAULT"));
        dynaBean.setStr("JE_CORE_RESOURCECOLUMN_PLAN_ID", planId);
        dynaBean.setStr("PLAN_TYPE", "2");
        dynaBean.setStr("PLAN_SY", "0");//是否系统方案
        commonService.buildModelCreateInfo(dynaBean);
        String wheresql = " PLAN_PARENT_ID = " + "'" + dynaBean.getStr("PLAN_PARENT_ID") + "'";
        commonService.generateOrderIndex(dynaBean, wheresql);
        metaService.insert(dynaBean);
        String columnIds = dynaBean.getStr("columnIds");
        if (StringUtil.isNotEmpty(columnIds)) {
            List<DynaBean> columnList = metaService.select("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder()
                    .selectColumns("SY_ORDERINDEX,RESOURCECOLUMN_FUNCINFO_ID,RESOURCECOLUMN_CODE,RESOURCECOLUMN_NAME,JE_CORE_RESOURCECOLUMN_ID")
                    .in("JE_CORE_RESOURCECOLUMN_ID", Arrays.asList(columnIds.split(","))));
            for (int i = 0; i < columnList.size(); i++) {
                DynaBean item = columnList.get(i);
                DynaBean userPlan = new DynaBean("JE_CORE_USERINFO_COLUMN", true);
                userPlan.setStr("COLUMN_FUNCID", item.getStr("RESOURCECOLUMN_FUNCINFO_ID"));
                userPlan.setStr("COLUMN_PLAN_ID", planId);
                userPlan.setStr("COLUMN_CODE", item.getStr("RESOURCECOLUMN_CODE"));
                userPlan.setStr("COLUMN_NAME", item.getStr("RESOURCECOLUMN_NAME"));
                userPlan.setStr("COLUMN_RESOURCECOLUMN_ID", item.getStr("JE_CORE_RESOURCECOLUMN_ID"));
                userPlan.set("SY_ORDERINDEX", item.get("SY_ORDERINDEX"));
                commonService.buildModelCreateInfo(userPlan);
                metaService.insert(userPlan);
            }
        }
        return dynaBean;
    }

    @Override
    @Transactional
    public List<DynaBean> loadUserPlan(DynaBean dynaBean) {
        /**
         *查找列表字段是否有增量数据
         */
        List<DynaBean> columnList = metaService.select("JE_CORE_RESOURCECOLUMN", ConditionsWrapper.builder()
                .selectColumns("JE_CORE_RESOURCECOLUMN_ID,RESOURCECOLUMN_CODE,RESOURCECOLUMN_NAME,RESOURCECOLUMN_BULKING_TYPE")
                .eq("RESOURCECOLUMN_PLAN_ID", dynaBean.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID"))
                .isNotNull("RESOURCECOLUMN_BULKING_TYPE")
                .ne("RESOURCECOLUMN_BULKING_TYPE", ""));
        /**
         * 有则更新‘列表方案表’，表明有增量数据
         */
        if (columnList != null && columnList.size() > 0) {
            List<DynaBean> childPlanList = metaService.select("JE_CORE_RESOURCECOLUMN_PLAN", ConditionsWrapper.builder()
                    .selectColumns("JE_CORE_RESOURCECOLUMN_PLAN_ID")
                    .eq("PLAN_PARENT_ID", dynaBean.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID")));
            for (DynaBean plan : childPlanList) {
                String JE_CORE_RESOURCECOLUMN_PLAN_ID = plan.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID");
                for (DynaBean columnItem : columnList) {
                    String RESOURCECOLUMN_BULKING_TYPE = columnItem.getStr("RESOURCECOLUMN_BULKING_TYPE");
                    if ("1".equals(RESOURCECOLUMN_BULKING_TYPE)) {
                        DynaBean userColumnInsert = new DynaBean("JE_CORE_USERINFO_COLUMN", true);
                        userColumnInsert.setStr("COLUMN_RESOURCECOLUMN_ID", columnItem.getStr("JE_CORE_RESOURCECOLUMN_ID"));
                        userColumnInsert.setStr("COLUMN_BULKING_TYPE", "1");
                        userColumnInsert.setStr("COLUMN_PLAN_ID", JE_CORE_RESOURCECOLUMN_PLAN_ID);
                        userColumnInsert.setStr("COLUMN_CODE", columnItem.getStr("RESOURCECOLUMN_CODE"));
                        userColumnInsert.setStr("COLUMN_NAME", columnItem.getStr("RESOURCECOLUMN_NAME"));
                        commonService.buildModelCreateInfo(userColumnInsert);
                        String wheresql = " COLUMN_PLAN_ID=" + "'" + JE_CORE_RESOURCECOLUMN_PLAN_ID + "'";
                        commonService.generateOrderIndex(userColumnInsert, wheresql);
                        metaService.insert(userColumnInsert);
                    }
                    if ("2".equals(RESOURCECOLUMN_BULKING_TYPE)) {
                        DynaBean userColumnUpdate = new DynaBean("JE_CORE_USERINFO_COLUMN", true);
                        userColumnUpdate.setStr("COLUMN_BULKING_TYPE", "2");
                        commonService.buildModelModifyInfo(userColumnUpdate);
                        metaService.update(userColumnUpdate, ConditionsWrapper.builder()
                                .eq("COLUMN_PLAN_ID", JE_CORE_RESOURCECOLUMN_PLAN_ID)
                                .eq("COLUMN_CODE", columnItem.getStr("RESOURCECOLUMN_CODE")));
                    }
                }
                /**
                 * 更新方案的提示类型 PLAN_TIP_TYPE
                 */
                DynaBean column = new DynaBean("JE_CORE_RESOURCECOLUMN_PLAN", true);
                column.setStr("JE_CORE_RESOURCECOLUMN_PLAN_ID", plan.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID"));
                column.setStr("PLAN_TIP_TYPE", "1");
                metaService.update(column);
            }
            /**
             * 将列表字段的RESOURCECOLUMN_BULKING_TYPE更新为空
             */
            for (DynaBean item : columnList) {
                DynaBean columndb = new DynaBean("JE_CORE_RESOURCECOLUMN", true);
                columndb.setStr("JE_CORE_RESOURCECOLUMN_ID", item.getStr("JE_CORE_RESOURCECOLUMN_ID"));
                columndb.set("RESOURCECOLUMN_BULKING_TYPE", null);
                metaService.update(columndb);
            }

        }

        List<DynaBean> list = metaService.select("JE_CORE_RESOURCECOLUMN_PLAN", ConditionsWrapper.builder()
                .eq("JE_CORE_RESOURCECOLUMN_PLAN_ID", dynaBean.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID"))
                .or().eq("PLAN_PARENT_ID", dynaBean.getStr("JE_CORE_RESOURCECOLUMN_PLAN_ID")));

        return list;
    }

}
