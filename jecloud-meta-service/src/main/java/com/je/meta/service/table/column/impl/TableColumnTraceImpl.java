/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.column.impl;

import com.alibaba.fastjson2.JSON;
import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.table.AbstractTableTrace;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("tableColumnTrace")
public class TableColumnTraceImpl extends AbstractTableTrace {

    @Override
    public DynaBean save(List<DynaBean> columns, DynaBean trace, DynaBean oldBean, DynaBean newBean, String oper) {
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
        String tableColumnCode = "";
        if ("INSERT".equals(oper)) {
            return trace;
        } else if ("UPDATE".equals(oper)) {
            tableColumnCode = oldBean.getStr("TABLECOLUMN_CODE");
            if (Strings.isNullOrEmpty(tableColumnCode)) {
                tableColumnCode = newBean.getStr("TABLECOLUMN_CODE");
            }
            updateFiled(trace, columns, newBean, oldBean, dataList, oper);
        } else if ("DELETE".equals(oper)) {
            tableColumnCode = newBean.getStr("TABLECOLUMN_CODE");
            if (Strings.isNullOrEmpty(tableColumnCode)) {
                return trace;
            }
            deleteFiled(trace, columns, newBean, dataList, oper);
            oldBean = newBean;
        }
        String jsonStr = "";
        if (dataList.size() > 0) {
            jsonStr = JSON.toJSONString(dataList);
        }
        trace.set("TABLETRACE_TABLENAME", "表格列");
        trace.set("TABLETRACE_FIELDCODE", tableColumnCode);
        trace.set("TABLETRACE_FIELDTYPE", getFieldTypeNameByTypeCode(oldBean.getStr("TABLECOLUMN_TYPE")));
        if(Strings.isNullOrEmpty(oldBean.getStr("TABLECOLUMN_NAME"))){
            trace.set("TABLETRACE_FIELDNAME", newBean.getStr("TABLECOLUMN_NAME"));
        }else {
            trace.set("TABLETRACE_FIELDNAME", oldBean.getStr("TABLECOLUMN_NAME"));
        }
        trace.set("TABLETRACE_XGNRJSON", jsonStr);
        metaService.insert(trace);
        return trace;
    }

    private void updateFiled(DynaBean trace, List<DynaBean> columns, DynaBean newBean, DynaBean oldBean, List<Map<String, Object>> dataList, String oper) {
        trace.set("TABLETRACE_OPER", "修改");
        if (Strings.isNullOrEmpty(oldBean.getStr("TABLECOLUMN_CODE"))) {
            trace.set("TABLETRACE_OPER", "添加");
        }
        for (DynaBean column : columns) {
            if (column.getStr("TABLECOLUMN_CODE").startsWith("SY_")) {
                continue;
            }
            if (!newBean.containsKey(column.getStr("TABLECOLUMN_CODE")) || oldBean.getStr(column.getStr("TABLECOLUMN_CODE"), "").equals(newBean.getStr(column.getStr("TABLECOLUMN_CODE"), ""))) {
                continue;
            }
            Map<String, Object> map = buildJsonData(oper, column, newBean, oldBean);
            dataList.add(map);
        }
        if (!Strings.isNullOrEmpty(newBean.getStr("TABLECOLUMN_ISCREATE")) && newBean.getStr("TABLECOLUMN_ISCREATE").equals("1")) {
            trace.set("TABLETRACE_SFYY", "1");
        } else {
            trace.set("TABLETRACE_SFYY", "0");
        }
    }

    private void deleteFiled(DynaBean trace, List<DynaBean> columns, DynaBean newBean, List<Map<String, Object>> dataList, String oper) {
        trace.set("TABLETRACE_OPER", "删除");
        for (DynaBean column : columns) {
            if (Strings.isNullOrEmpty(column.getStr("TABLECOLUMN_CODE"))) {
                continue;
            }
            if (column.getStr("TABLECOLUMN_CODE").startsWith("SY_")) {
                continue;
            }
            if (StringUtil.isEmpty(newBean.getStr(column.getStr("TABLECOLUMN_CODE")))) {
                continue;
            }
            Map<String, Object> map = buildJsonData(oper, column, newBean, newBean);
            dataList.add(map);
        }
    }

    private Map<String, Object> buildJsonData(String oper, DynaBean column, DynaBean newBean, DynaBean oldBean) {
        Map<String, Object> map = new HashMap<>();
        map.put("tableColumnName", column.getStr("TABLECOLUMN_NAME"));
        map.put("tableColumnCode", column.getStr("TABLECOLUMN_CODE"));
        if ("UPDATE".equals(oper)) {
            map.put("type", "update");
            map.put("tableColumnOldValue", oldBean.getStr(column.getStr("TABLECOLUMN_CODE")));
            map.put("tableColumnValue", newBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        if ("DELETE".equals(oper)) {
            map.put("type", "delete");
            map.put("tableColumnValue", oldBean.getStr(column.getStr("TABLECOLUMN_CODE")));
        }
        return map;
    }


}
