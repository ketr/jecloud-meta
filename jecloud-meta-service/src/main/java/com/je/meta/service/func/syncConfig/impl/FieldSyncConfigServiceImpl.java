/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.syncConfig.impl;

import com.google.common.collect.Lists;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.syncConfig.SyncConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service("fieldSyncConfigService")
public class FieldSyncConfigServiceImpl implements SyncConfigService {

    @Autowired
    private MetaService metaService;

    @Override
    public void selectResources(List<DynaBean> targetResourcesList, String way, String columnCodes) {
        if(!"1".equals(way) &&StringUtil.isEmpty(columnCodes)){
            return;
        }
        List<String> columnCodeList = Arrays.asList(columnCodes.split(","));

        Iterator<DynaBean> iterator =targetResourcesList.iterator();
        //除需过滤的以外全部同步
       /* if("1".equals(way)){
            while(iterator.hasNext()){
                DynaBean dynaBean = iterator.next();
                if(SYS_CODES.contains(dynaBean.getStr("RESOURCEFIELD_CODE"))){
                    iterator.remove();
                }
            }
        }*/
        //同步选中的
        if("2".equals(way)){
            while(iterator.hasNext()){
                DynaBean dynaBean = iterator.next();
                if(!columnCodeList.contains(dynaBean.getStr("RESOURCEFIELD_CODE"))){
                    iterator.remove();
                }
            }
        }
        //同步未选中的
        if("3".equals(way)){
            while(iterator.hasNext()){
                DynaBean dynaBean = iterator.next();
                if(columnCodeList.contains(dynaBean.getStr("RESOURCEFIELD_CODE"))){
                    iterator.remove();
                }
            }
        }
    }

    /**
     * 构建分组框，规则如下
     * 1.如果目标字段的所属分组框也存在于当前字段集合中，以目标列为准
     * 2.如果目标字段的所属分组框为空，以目标字段为准
     * 3.如果目标字段的所属分组框不存在于当前字段集合中，以当前字段为准
     * @param targetResourcesList
     * @param currentResourcesList
     * @return
     */
    @Override
    public List<DynaBean> buildResources(List<DynaBean> targetResourcesList, List<DynaBean> currentResourcesList) {
        Map<String, DynaBean> currentResourcesMap = currentResourcesList.stream().
                collect(Collectors.toMap(p -> p.getStr("RESOURCEFIELD_CODE"), p -> p));
        for(DynaBean targetResources:targetResourcesList){
            String columnCode = targetResources.getStr("RESOURCEFIELD_CODE");
            DynaBean currentResources = currentResourcesMap.get(columnCode);
            if(currentResources==null || StringUtil.isEmpty(columnCode)){
                continue;
            }
            String targetGroup =targetResources.getStr("RESOURCEFIELD_GROUPNAME");
            String currentGroup = currentResources.getStr("RESOURCEFIELD_GROUPNAME");
            if(StringUtil.isEmpty(targetGroup)){
                //如果值为null,mybatise不会更新，所以置为""
                targetResources.setStr("RESOURCEFIELD_GROUPNAME","");
            }
            if(StringUtil.isNotEmpty(targetGroup) && !currentResourcesMap.containsKey(targetGroup)){
                //沿用当前字段分组信息
                targetResources.setStr("RESOURCEFIELD_GROUPNAME",currentGroup);
            }
            targetResources.remove("RESOURCEFIELD_FUNCINFO_ID");
            targetResources.remove("RESOURCEFIELD_CODE");
            //将JE_CORE_RESOURCECOLUMN_ID修改为当前列的JE_CORE_RESOURCECOLUMN_ID
            targetResources.setStr("JE_CORE_RESOURCEFIELD_ID",currentResources.getStr("JE_CORE_RESOURCEFIELD_ID"));
        }
        return targetResourcesList;
    }

    @Override
    public void excuteSync(List<DynaBean> resourcesList) {
        for(DynaBean dynaBean:resourcesList ){
            metaService.update(dynaBean, ConditionsWrapper.builder().
                    eq("JE_CORE_RESOURCEFIELD_ID",dynaBean.getStr("JE_CORE_RESOURCEFIELD_ID")));
        }
    }
}
