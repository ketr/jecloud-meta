/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.datasource.impl;

import com.alibaba.fastjson2.JSON;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.service.datasource.DataSourceExecuteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.je.servicecomb.JECloud.PRODUCT_CORE_META;

@Service("dataSourceSqlExecuteService")
public class DataSourceSqlExecuteServiceImpl implements DataSourceExecuteService {
    @Autowired
    private MetaService metaService;

    @Override
    public List<Map<String, Object>> execute(DynaBean dataSource, String parameterStr, String limit) {
        if (StringUtil.isEmpty(dataSource.getStr("DATASOURCE_DATA"))) {
            throw new PlatformException(MessageUtils.getMessage("dataSource.datasourceData.isEmpty"), PlatformExceptionEnum.UNKOWN_ERROR);
        }
        String productCode = dataSource.getStr("SY_PRODUCT_CODE");
        String sql = dataSource.getStr("DATASOURCE_DATA");
        //sql = buildSql(sql,parameterJson);
        if (!PRODUCT_CORE_META.equals(productCode)) {
            if (StringUtil.isNotEmpty(limit) && Integer.parseInt(limit) > 0) {
                return routeExecuteSql(productCode, sql, Integer.parseInt(limit));
            } else {
                return routeExecuteSql(productCode, sql);
            }
        }

        if (StringUtil.isNotEmpty(limit) && Integer.parseInt(limit) > 0) {
            return metaService.selectSql(0, Integer.parseInt(limit), sql);
        } else {
            return metaService.selectSql(sql);
        }


    }

    @Override
    public List<DynaBean> executeForRpc(DynaBean dataSource, Map<String, Object> map, int limit) {
        List<Map<String, Object>> list = execute(dataSource, JSON.toJSONString(map), String.valueOf(limit));
        List<DynaBean> listD = new ArrayList<>();
        for (Map<String, Object> item : list) {
            DynaBean dynaBean = new DynaBean();
            dynaBean.setValues(item);
            listD.add(dynaBean);
        }
        return listD;
    }
}
