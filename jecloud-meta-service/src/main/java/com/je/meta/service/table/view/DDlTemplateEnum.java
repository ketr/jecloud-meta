/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.view;

public enum DDlTemplateEnum {

    ONE_JOIN("外连接","FROM ${#tableCodes} ${#formula} ${#targetTableCode} ON ${#mainTableCode}${#leftDot}${#mainColumn}=${#targetTableCode}${#rightDot}${#targetColumn}","oneJoinDDLService"),

    CONVENTION_AND_JOIN("常规和外连接","FROM ${#tableCodes} ${#where} ${#whereSql}","conventionAndJoinDDLService"),

    CONVENTION("常规","FROM ${#tableCodes}  ${#where} ${#whereSql}","conventionDDLService"),

    MORE_JOIN("多个外连接","FROM ${#tableCodes} ${#joinDDL}","moreJoinDDLService");

    private String name;
    private String template;
    private String beanName;

    public String getBeanName() {
        return beanName;
    }

    public String getName() {
        return name;
    }

    public String getTemplate() {
        return template;
    }

    DDlTemplateEnum(String name, String template,String beanName){
        this.name = name;
        this.template = template;
        this.beanName =beanName;
    }

}
