/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table;

import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.common.MetaBeanService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractTableTrace {
    @Autowired
    protected MetaBeanService metaBeanService;
    @Autowired
    protected MetaService metaService;

    private static final Map<String, String> ddCache = new HashMap<>();

    public abstract DynaBean save(List<DynaBean> columns, DynaBean trace, DynaBean oldBean, DynaBean newBean, String oper);

    protected Boolean isSys(String code) {
        if (code.startsWith("SY_")) {
            return true;
        }
        return false;
    }



    public String getFieldTypeNameByTypeCode(String code) {
        if (ddCache.size() == 0) {
            List<DynaBean> list = metaService.select("JE_CORE_DICTIONARYITEM", ConditionsWrapper.builder()
                    .eq("DICTIONARYITEM_DICTIONARY_ID",
                    "28b3af46-4164-4d59-a504-3afadc979ff5")
                    .ne("SY_NODETYPE","ROOT")
                    , "DICTIONARYITEM_ITEMCODE,DICTIONARYITEM_ITEMNAME");
            for (DynaBean dynaBean : list) {
                ddCache.put(dynaBean.getStr("DICTIONARYITEM_ITEMCODE"), dynaBean.getStr("DICTIONARYITEM_ITEMNAME"));
            }
        }
        return ddCache.get(code);
    }

}
