package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.func.funcPerm.FuncDataPermVo;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.rpc.func.MetaFuncPermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 打包功能
 */
@Service("upgradeFuncDate")
public class UpgradeFuncDate extends AbstractUpgradeMetaDate<PackageResult> {

    @Autowired
    private MetaFuncPermService metaFuncPermService;

    public UpgradeFuncDate() {
        super(UpgradeResourcesEnum.FUNC);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> funcIdList = getSourceIds(upgradeBean);
        if (funcIdList == null || funcIdList.size() == 0) {
            return;
        }
        packageResult.setProductFunctions(packageFunction(funcIdList));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductFunctions();
    }

    private List<DynaBean> packageFunction(List<String> funcIds) {
        List<DynaBean> funcBeanList = metaService.select("JE_CORE_FUNCINFO", ConditionsWrapper.builder()
                .in("JE_CORE_FUNCINFO_ID", funcIds));
        List<DynaBean> funcList = new ArrayList<>();
        List<String> allParentIds = new ArrayList<>();
        //以后多方案改造
        for (DynaBean func : funcBeanList) {
            DynaBean funcInfo = getFuncInfo(func);
            //目录
            String SY_PARENTPATH = func.getStr("SY_PARENTPATH");
            List<String> currentParentIds = new ArrayList<>();
            for (String id : SY_PARENTPATH.split("/")) {
                if (StringUtil.isNotEmpty(id)) {
                    if (!"ROOT".equals(id) && !allParentIds.contains(id)) {
                        allParentIds.add(id);
                        currentParentIds.add(id);
                    }
                }
            }
            funcInfo.set("parentList", metaService.select("JE_CORE_FUNCINFO", ConditionsWrapper.builder().in("JE_CORE_FUNCINFO_ID", currentParentIds)));
            funcList.add(funcInfo);
        }
        return sort(funcList);
    }

    private DynaBean getFuncInfo(DynaBean func) {
        String funcId = func.getStr("JE_CORE_FUNCINFO_ID");
        //按钮
        List<DynaBean> buttons = metaService.select("JE_CORE_RESOURCEBUTTON",
                ConditionsWrapper.builder().eq("RESOURCEBUTTON_FUNCINFO_ID", funcId));
        func.set("buttons", buttons);
        //子功能关联关系
        List<DynaBean> childs = metaService.select("JE_CORE_FUNCRELATION",
                ConditionsWrapper.builder().eq("FUNCRELATION_FUNCINFO_ID", funcId));
        //子功能关联字段配置
        for (DynaBean child : childs) {
            String JE_CORE_FUNCRELATION_ID = child.getStr("JE_CORE_FUNCRELATION_ID");
            List<DynaBean> associations = metaService.select("JE_CORE_ASSOCIATIONFIELD",
                    ConditionsWrapper.builder().eq("ASSOCIATIONFIELD_FUNCRELAT_ID", JE_CORE_FUNCRELATION_ID));
            child.set("associations", associations);
        }
        func.set("childs", childs);
        //列表
        List<DynaBean> columns = metaService.select("JE_CORE_RESOURCECOLUMN",
                ConditionsWrapper.builder().eq("RESOURCECOLUMN_FUNCINFO_ID", funcId));
        func.set("columns", columns);
        //表单
        List<DynaBean> fields = metaService.select("JE_CORE_RESOURCEFIELD",
                ConditionsWrapper.builder().eq("RESOURCEFIELD_FUNCINFO_ID", funcId));
        func.set("fields", fields);
        //数据权限配置
        func.set("funcPerm", getFuncPrem(func.getStr("FUNCINFO_FUNCCODE")));
        //流程
        //高级查询
        List<DynaBean> groupquerys = metaService.select("JE_CORE_GROUPQUERY",
                ConditionsWrapper.builder().eq("GROUPQUERY_GNID", funcId));
        func.set("groupquerys", groupquerys);
        //查询策略
        List<DynaBean> queryStrategys = metaService.select("JE_CORE_QUERYSTRATEGY",
                ConditionsWrapper.builder().eq("QUERYSTRATEGY_FUNCINFO_ID", funcId));
        func.set("queryStrategys", queryStrategys);
        return func;
    }

    private FuncDataPermVo getFuncPrem(String funcCode) {
        FuncDataPermVo funcDataPermVo = new FuncDataPermVo();
        funcDataPermVo.setFastAuthVo(metaFuncPermService.getFastAuth(funcCode));
        funcDataPermVo.setFieldAuthVo(metaFuncPermService.getFiledAuth(funcCode));
        funcDataPermVo.setDictionaryAuthVo(metaFuncPermService.getDictionaryAuth(funcCode));
        funcDataPermVo.setRoleSqlAuthVo(metaFuncPermService.getRoleSqlAuth(funcCode));
        funcDataPermVo.setSql(metaFuncPermService.getSqlAuth(funcCode));
        funcDataPermVo.setControlFieldAuthVo(metaFuncPermService.controlField(funcCode));
        return funcDataPermVo;
    }
}
