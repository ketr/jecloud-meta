package com.je.meta.service.table.column.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.table.TableType;
import com.je.common.base.constants.tree.TreeNodeType;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.exception.APIWarnException;
import com.je.common.base.service.MetaService;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.SecurityUserHolder;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.develop.DevelopLogRpcService;
import com.je.meta.service.SystemColumnTypeEnum;
import com.je.meta.service.table.AbstractTableColumnService;
import com.je.meta.service.table.column.MetaTableSystemColumnService;
import com.je.servicecomb.CommonRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class MetaTableSystemColumnServiceImpl extends AbstractTableColumnService implements MetaTableSystemColumnService, CommonRequestService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private DevelopLogRpcService developLogRpcService;

    /**
     * 添加系统字段
     *
     * @param request
     */
    @Override
    public boolean addSystemColumn(HttpServletRequest request) throws APIWarnException {
        String type = getStringParameter(request, "type");
        if (SystemColumnTypeEnum.CREATE.getName().equals(type)) {
            return generateCreateInfo(request);
        } else if (SystemColumnTypeEnum.UPDATE.getName().equals(type)) {
            return generateUpdateInfo(request);
        } else if (SystemColumnTypeEnum.WORKFLOW_EXTEND.getName().equals(type)) {
            return generateWfExtendColumn(request);
        } else if (SystemColumnTypeEnum.WORKFLOW_BASE.getName().equals(type)) {
            return generateWfBaseColumn(request);
        } else if (SystemColumnTypeEnum.EXTEND.getName().equals(type)) {
            return generateExtendColumn(request);
        } else if (SystemColumnTypeEnum.PRODUCT.getName().equals(type)) {
            return generateProductColumn(request);
        } else if (SystemColumnTypeEnum.SECRET.getName().equals(type)) {
            return generateSecretColumn(request);
        } else if (SystemColumnTypeEnum.PROJECT.getName().equals(type)) {
            return generateProjectColumn(request);
        }
        return false;
    }

    /**
     * 新增相关字段
     */
    private boolean generateCreateInfo(HttpServletRequest request) throws APIWarnException {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        if (StringUtil.isNotEmpty(resourceId)) {
            DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
            return initCreateColumns(table);
        } else {
            return false;
        }
    }

    /**
     * 修改相关字段
     */
    private boolean generateUpdateInfo(HttpServletRequest request) throws APIWarnException {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        if (StringUtil.isNotEmpty(resourceId)) {
            DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
            return initUpdateColumns(table);
        } else {
            return false;
        }
    }

    /**
     * 产品相关字段
     */
    private boolean generateSecretColumn(HttpServletRequest request) {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (table != null) {
            initSecretColumns(table);
        }
        return true;
    }

    /**
     * 产品相关字段
     */
    private boolean generateProductColumn(HttpServletRequest request) {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (table != null) {
            initProductColumns(table);
        }
        return true;
    }

    /**
     * 产品相关字段
     */
    private boolean generateProjectColumn(HttpServletRequest request) {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (table != null) {
            initProjectColumns(table);
        }
        return true;
    }

    /**
     * 拓展相关字段
     */
    private boolean generateExtendColumn(HttpServletRequest request) throws APIWarnException {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (table != null) {
            return initExtendColumns(table);
        }
        return true;
    }

    /**
     * saas相关字段
     */
    private boolean generateSaasColumn(HttpServletRequest request) {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (table != null) {
            initSaasColumns(table);
        }
        return true;
    }

    /**
     * 流程相关字段
     */
    private boolean generateWfBaseColumn(HttpServletRequest request) throws APIWarnException {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (table != null) {
            return initWfBaseColumns(table);
        }
        return true;
    }

    /**
     * 流程相关字段
     */
    private boolean generateWfExtendColumn(HttpServletRequest request) throws APIWarnException {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (table != null) {
            return initProcessExtendColumns(table);
        }
        return true;
    }

    @Override
    @Transactional
    public boolean initCreateColumns(DynaBean resourceTable) throws APIWarnException {
        int p = 0;
        List<String> works = createList;
        List<String> tableList = new ArrayList<>();
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<Map<String, Object>> maps = metaService.selectSql("select TABLECOLUMN_CODE from JE_CORE_TABLECOLUMN where TABLECOLUMN_TABLECODE={0}", tableCode);
        Iterator<Map<String, Object>> it = maps.iterator();
        while (it.hasNext()) {
            Map<String, Object> data = (Map<String, Object>) it.next();
            String field = data.get("TABLECOLUMN_CODE").toString();
            tableList.add(field);
        }
        Boolean isTree = false;
        if (TableType.TREETABLE.equals(resourceTable.getStr("RESOURCETABLE_TYPE"))) {
            isTree = true;
        }
        List<DynaBean> columns = new ArrayList<DynaBean>();
        if (!tableList.contains("SY_CREATEORGID")) {
            /**登记者所在部门主键*/
            DynaBean createOrgId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(createOrgId, "SY_CREATEORGID", "登记部门主键", 312);
            createOrgId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            createOrgId.set("TABLECOLUMN_LENGTH", "");
            createOrgId.set("TABLECOLUMN_NAME_EN", "Dept ID");
            columns.add(createOrgId);
            p++;
        }

        if (!tableList.contains("SY_CREATEORGNAME")) {
            /**登记者所在部门名称*/
            DynaBean createOrgName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(createOrgName, "SY_CREATEORGNAME", "登记部门", 314);
            createOrgName.set("TABLECOLUMN_TYPE", "VARCHAR50");
            createOrgName.set("TABLECOLUMN_LENGTH", "");
            createOrgName.set("TABLECOLUMN_NAME_EN", "Dept");
            columns.add(createOrgName);
            p++;
        }

        if (!tableList.contains("SY_CREATETIME")) {
            /**登记时间*/
            DynaBean createTime = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(createTime, "SY_CREATETIME", "登记时间", 315);
            createTime.set("TABLECOLUMN_TYPE", "DATETIME");
            createTime.set("TABLECOLUMN_NAME_EN", "Create Time");
            columns.add(createTime);
            p++;
        }

        if (!tableList.contains("SY_CREATEUSERID")) {
            /**登记人主键*/
            DynaBean createUserId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(createUserId, "SY_CREATEUSERID", "登记人主键", 316);
            createUserId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            createUserId.set("TABLECOLUMN_LENGTH", "");
            createUserId.set("TABLECOLUMN_NAME_EN", "User ID");
            columns.add(createUserId);
            p++;
        }

        if (!tableList.contains("SY_CREATEUSERNAME")) {
            /**登记人姓名*/
            DynaBean createUserName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(createUserName, "SY_CREATEUSERNAME", "登记人", 318);
            createUserName.set("TABLECOLUMN_TYPE", "VARCHAR50");
            createUserName.set("TABLECOLUMN_LENGTH", "");
            createUserName.set("TABLECOLUMN_NAME_EN", "User");
            columns.add(createUserName);
            p++;
        }

        if (!tableList.contains("SY_STATUS")) {
            /**数据状态*/
            DynaBean status = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(status, "SY_STATUS", "数据状态", 319);
            status.set("TABLECOLUMN_TYPE", "" + "YESORNO");
            status.set("TABLECOLUMN_NAME_EN", "Data Status");
            columns.add(status);
            p++;
        }

        if (!tableList.contains("SY_ORDERINDEX")) {
            /**排序字段*/
            DynaBean orderIndex = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(orderIndex, "SY_ORDERINDEX", "排序字段", 320);
            orderIndex.set("TABLECOLUMN_NAME_EN", "Sort Field");
            if (isTree) {
                orderIndex.set("TABLECOLUMN_TREETYPE", TreeNodeType.ORDERINDEX.toString());
            }
            columns.add(orderIndex);
            p++;
        }

        if (!tableList.contains("SY_COMPANY_ID")) {
            /**公司ID*/
            DynaBean companyId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(companyId, "SY_COMPANY_ID", "所属公司ID", 332);
            companyId.set("TABLECOLUMN_NAME_EN", "Company Id");
            companyId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            companyId.set("TABLECOLUMN_LENGTH", "");
            columns.add(companyId);
            p++;
        }

        if (!tableList.contains("SY_COMPANY_NAME")) {
            /**公司名称*/
            DynaBean companyName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(companyName, "SY_COMPANY_NAME", "所属公司名称", 333);
            companyName.set("TABLECOLUMN_NAME_EN", "Company Name");
            companyName.set("TABLECOLUMN_TYPE", "VARCHAR255");
            companyName.set("TABLECOLUMN_LENGTH", "");
            columns.add(companyName);
            p++;
        }

        if (!tableList.contains("SY_GROUP_COMPANY_ID")) {
            /**集团公司ID*/
            DynaBean groupCompanyId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(groupCompanyId, "SY_GROUP_COMPANY_ID", "所属集团公司ID", 334);
            groupCompanyId.set("TABLECOLUMN_NAME_EN", "Group Company Id");
            groupCompanyId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            groupCompanyId.set("TABLECOLUMN_LENGTH", "");
            columns.add(groupCompanyId);
            p++;
        }

        if (!tableList.contains("SY_GROUP_COMPANY_NAME")) {
            /**集团公司名称*/
            DynaBean groupCompanyName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(groupCompanyName, "SY_GROUP_COMPANY_NAME", "所属集团公司名称", 335);
            groupCompanyName.set("TABLECOLUMN_NAME_EN", "Group Company Name");
            groupCompanyName.set("TABLECOLUMN_TYPE", "VARCHAR255");
            groupCompanyName.set("TABLECOLUMN_LENGTH", "");
            columns.add(groupCompanyName);
            p++;
        }

        if (!tableList.contains("SY_ORG_ID")) {
            /**机构ID*/
            DynaBean orgId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(orgId, "SY_ORG_ID", "所属机构ID", 336);
            orgId.set("TABLECOLUMN_NAME_EN", "Org Id");
            orgId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            orgId.set("TABLECOLUMN_LENGTH", "");
            columns.add(orgId);
            p++;
        }

        if (!tableList.contains("SY_TENANT_ID")) {
            /**租户ID*/
            DynaBean tenantId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(tenantId, "SY_TENANT_ID", "租户ID", 337);
            tenantId.set("TABLECOLUMN_NAME_EN", "Tenant Id");
            tenantId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            tenantId.set("TABLECOLUMN_LENGTH", "");
            columns.add(tenantId);
            p++;
        }

        if (!tableList.contains("SY_TENANT_NAME")) {
            /**租户名称*/
            DynaBean tenantName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(tenantName, "SY_TENANT_NAME", "租户名称", 338);
            tenantName.set("TABLECOLUMN_NAME_EN", "Tenant Name");
            tenantName.set("TABLECOLUMN_TYPE", "VARCHAR255");
            tenantName.set("TABLECOLUMN_LENGTH", "");
            columns.add(tenantName);
            p++;
        }

        /**树形字段设置*/
        if (isTree) {
            String prefix = tableCode.substring(tableCode.lastIndexOf("_") + 1);
            if (!tableList.contains("SY_PARENT")) {
                //父节点
                DynaBean parent = new DynaBean("JE_CORE_TABLECOLUMN", false);
                parent.set("SY_ORDERINDEX", 301);
                parent.set("TABLECOLUMN_TREETYPE", TreeNodeType.PARENT.toString());
                parent.set("TABLECOLUMN_CODE", "SY_PARENT");
                parent.set("TABLECOLUMN_TYPE", "VARCHAR50");
                parent.set("TABLECOLUMN_NAME", "父节点ID");
                parent.set("TABLECOLUMN_LENGTH", "");
                parent.set("TABLECOLUMN_ISNULL", "1");
                parent.set("TABLECOLUMN_CLASSIFY", "SYS");
                parent.set("TABLECOLUMN_NAME_EN", "Parent Node ID");
                columns.add(parent);
                p++;
            }
            if (!tableList.contains("SY_NODETYPE")) {
                //节点类型， 根、叶子、普通  区分
                DynaBean leaf = new DynaBean("JE_CORE_TABLECOLUMN", false);
                leaf.set("SY_ORDERINDEX", 302);
                leaf.set("TABLECOLUMN_TREETYPE", TreeNodeType.NODETYPE.toString());
                leaf.set("TABLECOLUMN_CODE", "SY_NODETYPE");
                leaf.set("TABLECOLUMN_TYPE", "VARCHAR50");
                leaf.set("TABLECOLUMN_NAME", "节点类型");
                leaf.set("TABLECOLUMN_LENGTH", "");
                leaf.set("TABLECOLUMN_ISNULL", "1");
                leaf.set("TABLECOLUMN_CLASSIFY", "SYS");
                leaf.set("TABLECOLUMN_NAME_EN", "Node Type");
                columns.add(leaf);
                p++;
            }
            if (!tableList.contains(prefix + "_TEXT")) {
                //节点名称
                DynaBean text = new DynaBean("JE_CORE_TABLECOLUMN", false);
                text.set("SY_ORDERINDEX", 1);
                text.set("TABLECOLUMN_TREETYPE", TreeNodeType.TEXT.toString());
                text.set("TABLECOLUMN_CODE", prefix + "_TEXT");
                text.set("TABLECOLUMN_TYPE", "VARCHAR255");
                text.set("TABLECOLUMN_NAME", "节点名称");
                text.set("TABLECOLUMN_ISNULL", "1");
                text.set("TABLECOLUMN_CLASSIFY", "PRO");
                columns.add(text);
                p++;
            }
            if (!tableList.contains(prefix + "_CODE")) {
                //节点编码
                DynaBean code = new DynaBean("JE_CORE_TABLECOLUMN", false);
                code.set("SY_ORDERINDEX", 2);
                code.set("TABLECOLUMN_TREETYPE", TreeNodeType.CODE.toString());
                code.set("TABLECOLUMN_CODE", prefix + "_CODE");
                code.set("TABLECOLUMN_TYPE", "VARCHAR255");
                code.set("TABLECOLUMN_NAME", "节点编码");
                code.set("TABLECOLUMN_ISNULL", "1");
                code.set("TABLECOLUMN_CLASSIFY", "PRO");
                columns.add(code);
                p++;
            }
            if (!tableList.contains("SY_LAYER")) {
                //层次
                DynaBean layer = new DynaBean("JE_CORE_TABLECOLUMN", false);
                layer.set("SY_ORDERINDEX", 306);
                layer.set("TABLECOLUMN_TREETYPE", TreeNodeType.LAYER.toString());
                layer.set("TABLECOLUMN_CODE", "SY_LAYER");
                layer.set("TABLECOLUMN_NAME", "层次");
                layer.set("TABLECOLUMN_TYPE", "NUMBER");
                layer.set("TABLECOLUMN_LENGTH", "");
                layer.set("TABLECOLUMN_ISNULL", "1");
                layer.set("TABLECOLUMN_CLASSIFY", "SYS");
                layer.set("TABLECOLUMN_NAME_EN", "Layer");
                columns.add(layer);
                p++;
            }
            if (!tableList.contains("SY_PATH")) {
                //树形路径
                DynaBean path = new DynaBean("JE_CORE_TABLECOLUMN", false);
                path.set("SY_ORDERINDEX", 307);
                path.set("TABLECOLUMN_TREETYPE", TreeNodeType.NODEPATH.toString());
                path.set("TABLECOLUMN_CODE", "SY_PATH");
                path.set("TABLECOLUMN_TYPE", "VARCHAR1000");
                path.set("TABLECOLUMN_LENGTH", "");
                path.set("TABLECOLUMN_NAME", "树形路径");
                path.set("TABLECOLUMN_ISNULL", "1");
                path.set("TABLECOLUMN_CLASSIFY", "SYS");
                path.set("TABLECOLUMN_NAME_EN", "Tree Path");
                columns.add(path);
                p++;
            }
            if (!tableList.contains("SY_DISABLED")) {
                //是否启用
                DynaBean disabled = new DynaBean("JE_CORE_TABLECOLUMN", false);
                disabled.set("SY_ORDERINDEX", 308);
                disabled.set("TABLECOLUMN_TREETYPE", TreeNodeType.DISABLED.toString());
                disabled.set("TABLECOLUMN_CODE", "SY_DISABLED");
                disabled.set("TABLECOLUMN_TYPE", "YESORNO");
                disabled.set("TABLECOLUMN_LENGTH", "");
                disabled.set("TABLECOLUMN_NAME", "是否启用");
                disabled.set("TABLECOLUMN_ISNULL", "1");
                disabled.set("TABLECOLUMN_CLASSIFY", "SYS");
                disabled.set("TABLECOLUMN_NAME_EN", "Enable");
                columns.add(disabled);
                p++;
            }
            if (!tableList.contains("SY_TREEORDERINDEX")) {
                //树形排序
                DynaBean treeOrderIndex = new DynaBean("JE_CORE_TABLECOLUMN", false);
                treeOrderIndex.set("SY_ORDERINDEX", 309);
                treeOrderIndex.set("TABLECOLUMN_TREETYPE", TreeNodeType.TREEORDERINDEX.toString());
                treeOrderIndex.set("TABLECOLUMN_CODE", "SY_TREEORDERINDEX");
                treeOrderIndex.set("TABLECOLUMN_TYPE", "VARCHAR");
                if (ConstantVars.STR_MYSQL.equals(JEDatabase.getCurrentDatabase()) || ConstantVars.STR_TIDB.equals(JEDatabase.getCurrentDatabase())) {
                    treeOrderIndex.set("TABLECOLUMN_LENGTH", "383");
                } else {
                    treeOrderIndex.set("TABLECOLUMN_LENGTH", "900");
                }
                treeOrderIndex.set("TABLECOLUMN_NAME", "树形排序字段");
                treeOrderIndex.set("TABLECOLUMN_ISNULL", "1");
                treeOrderIndex.set("TABLECOLUMN_CLASSIFY", "SYS");
                treeOrderIndex.set("TABLECOLUMN_NAME_EN", "Tree Sort Field");
                columns.add(treeOrderIndex);
                p++;
            }

        }

        if (0 == p) {
            throw new APIWarnException(MessageUtils.getMessage("table.generate.saveMulti"), "201", new Object[]{resourceTable});
        }

        insertColumns(resourceTable, tableCode, columns);
        return true;
    }

    @Override
    @Transactional
    public boolean initUpdateColumns(DynaBean resourceTable) throws APIWarnException {
        int p = 0;
        List<String> works = updateList;
        List<String> tableList = new ArrayList<>();
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<Map<String, Object>> maps = metaService.selectSql("select TABLECOLUMN_CODE from JE_CORE_TABLECOLUMN where TABLECOLUMN_TABLECODE={0}", tableCode);
        Iterator<Map<String, Object>> it = maps.iterator();
        while (it.hasNext()) {
            Map<String, Object> data = (Map<String, Object>) it.next();
            String field = data.get("TABLECOLUMN_CODE").toString();
            tableList.add(field);
        }

        List<DynaBean> columns = new ArrayList<DynaBean>();
        if (!tableList.contains("SY_MODIFYORGID")) {
            /**修改人部门编码*/
            DynaBean modifyOrgId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(modifyOrgId, "SY_MODIFYORGID", "修改人部门主键", 325);
            modifyOrgId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            modifyOrgId.set("TABLECOLUMN_LENGTH", "");
            modifyOrgId.set("TABLECOLUMN_NAME_EN", "Modifier Dept ID");
            columns.add(modifyOrgId);
            p++;
        }

        if (!tableList.contains("SY_MODIFYORGNAME")) {
            /**修改人部门*/
            DynaBean modifyOrgName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(modifyOrgName, "SY_MODIFYORGNAME", "修改人部门", 327);
            modifyOrgName.set("TABLECOLUMN_TYPE", "VARCHAR50");
            modifyOrgName.set("TABLECOLUMN_LENGTH", "");
            modifyOrgName.set("TABLECOLUMN_NAME_EN", "Modifier Dept");
            columns.add(modifyOrgName);
            p++;
        }

        if (!tableList.contains("SY_MODIFYUSERID")) {
            /**修改人主键*/
            DynaBean modifyUserId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(modifyUserId, "SY_MODIFYUSERID", "修改人主键", 328);
            modifyUserId.set("TABLECOLUMN_TYPE", "VARCHAR50");
            modifyUserId.set("TABLECOLUMN_LENGTH", "");
            modifyUserId.set("TABLECOLUMN_NAME_EN", "Modifier User ID");
            columns.add(modifyUserId);
            p++;
        }

        if (!tableList.contains("SY_MODIFYUSERNAME")) {
            /**修改人*/
            DynaBean modifyUserName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(modifyUserName, "SY_MODIFYUSERNAME", "修改人", 330);
            modifyUserName.set("TABLECOLUMN_TYPE", "VARCHAR50");
            modifyUserName.set("TABLECOLUMN_LENGTH", "");
            modifyUserName.set("TABLECOLUMN_NAME_EN", "Modifier User");
            columns.add(modifyUserName);
            p++;
        }

        if (!tableList.contains("SY_MODIFYTIME")) {
            /**修改时间*/
            DynaBean modifyTime = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(modifyTime, "SY_MODIFYTIME", "修改时间", 331);
            modifyTime.set("TABLECOLUMN_TYPE", "DATETIME");
            modifyTime.set("TABLECOLUMN_NAME_EN", "Modify Time");
            columns.add(modifyTime);
            p++;
        }
        if (0 == p) {
            throw new APIWarnException(MessageUtils.getMessage("table.generate.updateMulti"), "201", new Object[]{resourceTable});
        }

        insertColumns(resourceTable, tableCode, columns);
        return true;
    }

    @Override
    @Transactional
    public void initShColumns(DynaBean resourceTable) {
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<DynaBean> columns = new ArrayList<>();
        /**审核标识*/
        DynaBean shFlag = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(shFlag, "SY_ACKFLAG", "审核标识", 325);
        shFlag.set("TABLECOLUMN_TYPE", "YESORNO");
        shFlag.set("TABLECOLUMN_LENGTH", "");
        shFlag.set("TABLECOLUMN_NAME_EN", "");
        columns.add(shFlag);
        /**审核人*/
        DynaBean shUserName = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(shUserName, "SY_ACKUSERNAME", "审核人", 326);
        shUserName.set("TABLECOLUMN_TYPE", "VARCHAR255");
        shUserName.set("TABLECOLUMN_LENGTH", "");
        shUserName.set("TABLECOLUMN_NAME_EN", "");
        columns.add(shUserName);
        /**审核人ID*/
        DynaBean shUserId = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(shUserId, "SY_ACKUSERID", "审核人ID", 327);
        shUserId.set("TABLECOLUMN_TYPE", "VARCHAR255");
        shUserId.set("TABLECOLUMN_LENGTH", "");
        shUserId.set("TABLECOLUMN_NAME_EN", "");
        columns.add(shUserId);
        /**审核时间*/
        DynaBean shTime = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(shTime, "SY_ACKTIME", "审核时间", 328);
        shTime.set("TABLECOLUMN_TYPE", "DATETIME");
        shTime.set("TABLECOLUMN_LENGTH", "");
        shTime.set("TABLECOLUMN_NAME_EN", "");
        columns.add(shTime);

        insertColumns(resourceTable, tableCode, columns);
    }

    /**
     * 初始化产品字段
     *
     * @param table
     */
    @Override
    @Transactional
    public void initProductColumns(DynaBean table) {
        String tableCode = table.getStr("RESOURCETABLE_TABLECODE");
        List<DynaBean> columns = new ArrayList<DynaBean>();
        /**产品id*/
        DynaBean productId = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(productId, "SY_PRODUCT_ID", "产品ID", 325);
        productId.set("TABLECOLUMN_TYPE", "VARCHAR255");
        productId.set("TABLECOLUMN_LENGTH", "");
        productId.set("TABLECOLUMN_NAME_EN", "Product ID");
        columns.add(productId);
        /**产品编码*/
        DynaBean productCode = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(productCode, "SY_PRODUCT_CODE", "产品编码", 325);
        productCode.set("TABLECOLUMN_TYPE", "VARCHAR255");
        productCode.set("TABLECOLUMN_LENGTH", "");
        productCode.set("TABLECOLUMN_NAME_EN", "Product Code");
        columns.add(productCode);
        /**产品名称*/
        DynaBean productName = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(productName, "SY_PRODUCT_NAME", "产品名称", 325);
        productCode.set("TABLECOLUMN_TYPE", "VARCHAR255");
        productCode.set("TABLECOLUMN_LENGTH", "");
        productCode.set("TABLECOLUMN_NAME_EN", "Product Name");
        columns.add(productName);

        insertColumns(table, tableCode, columns);
        developLogRpcService.doDevelopLog("INIT_PRODUCT_COLUMNS", "初始化产品字段", "TABLE", "资源表", table.getStr("RESOURCETABLE_TABLENAME"), table.getStr("RESOURCETABLE_TABLECODE"), table.getStr("JE_CORE_RESOURCETABLE_ID"), table.getStr("SY_PRODUCT_ID"));
    }

    @Override
    @Transactional
    public void initProjectColumns(DynaBean table) {
        String tableCode = table.getStr("RESOURCETABLE_TABLECODE");
        List<DynaBean> columns = new ArrayList<DynaBean>();
        /**项目id*/
        DynaBean productId = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(productId, "SY_PROJECT_ID", "项目ID", 325);
        productId.set("TABLECOLUMN_TYPE", "VARCHAR255");
        productId.set("TABLECOLUMN_LENGTH", "");
        productId.set("TABLECOLUMN_NAME_EN", "Project ID");
        columns.add(productId);
        /**项目名称*/
        DynaBean productName = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(productName, "SY_PROJECT_NAME", "项目名称", 325);
        productName.set("TABLECOLUMN_TYPE", "VARCHAR255");
        productName.set("TABLECOLUMN_LENGTH", "");
        productName.set("TABLECOLUMN_NAME_EN", "Project Name");
        columns.add(productName);
        /**组织id*/
        DynaBean productOrgId = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(productOrgId, "SY_PROJECT_ORG_ID", "项目组织ID", 325);
        productOrgId.set("TABLECOLUMN_TYPE", "VARCHAR255");
        productOrgId.set("TABLECOLUMN_LENGTH", "");
        productOrgId.set("TABLECOLUMN_NAME_EN", "Project Org ID");
        columns.add(productOrgId);
        /**组织名称*/
        DynaBean productOrgName = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(productOrgName, "SY_PROJECT_ORG_NAME", "项目组织名称", 325);
        productOrgName.set("TABLECOLUMN_TYPE", "VARCHAR255");
        productOrgName.set("TABLECOLUMN_LENGTH", "");
        productOrgName.set("TABLECOLUMN_NAME_EN", "Project Org Name");
        columns.add(productOrgName);

        insertColumns(table, tableCode, columns);
        developLogRpcService.doDevelopLog("INIT_PRODUCT_COLUMNS", "初始化产品字段", "TABLE", "资源表", table.getStr("RESOURCETABLE_TABLENAME"), table.getStr("RESOURCETABLE_TABLECODE"), table.getStr("JE_CORE_RESOURCETABLE_ID"), table.getStr("SY_PRODUCT_ID"));
    }

    @Override
    @Transactional
    public boolean initProcessExtendColumns(DynaBean resourceTable) throws APIWarnException {
        int p = 0;
        List<String> works = workList;
        List<String> tableList = new ArrayList<>();
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<Map<String, Object>> maps = metaService.selectSql("select TABLECOLUMN_CODE from JE_CORE_TABLECOLUMN where TABLECOLUMN_TABLECODE={0}", tableCode);
        Iterator<Map<String, Object>> it = maps.iterator();
        while (it.hasNext()) {
            Map<String, Object> data = (Map<String, Object>) it.next();
            String field = data.get("TABLECOLUMN_CODE").toString();
            tableList.add(field);
        }
        List<DynaBean> columns = new ArrayList<DynaBean>();
        if (!tableList.contains("SY_STARTEDUSER")) {
            /**流程启动人*/
            DynaBean startUser = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(startUser, "SY_STARTEDUSER", "流程启动人ID", 332);
            startUser.set("TABLECOLUMN_NAME_EN", "Starter ID");
            columns.add(startUser);
            p++;
        }

        if (!tableList.contains("SY_STARTEDUSERNAME")) {
            /**流程启动人*/
            DynaBean startUserName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(startUserName, "SY_STARTEDUSERNAME", "流程启动人", 333);
            startUserName.set("TABLECOLUMN_NAME_EN", "Starter");
            columns.add(startUserName);
            p++;
        }

        if (!tableList.contains("SY_APPROVEDUSERS")) {
            /**流程已执行人*/
            DynaBean approvedUser = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(approvedUser, "SY_APPROVEDUSERS", "流程已执行人ID", 334);
            approvedUser.set("TABLECOLUMN_NAME_EN", "Executor ID");
            approvedUser.set("TABLECOLUMN_TYPE", "CLOB");
            approvedUser.set("TABLECOLUMN_LENGTH", "");
            columns.add(approvedUser);
            p++;
        }

        if (!tableList.contains("SY_APPROVEDUSERNAMES")) {
            /**流程已执行人*/
            DynaBean approvedUserName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(approvedUserName, "SY_APPROVEDUSERNAMES", "流程已执行人", 335);
            approvedUserName.set("TABLECOLUMN_NAME_EN", "Executor");
            approvedUserName.set("TABLECOLUMN_TYPE", "CLOB");
            approvedUserName.set("TABLECOLUMN_LENGTH", "");
            columns.add(approvedUserName);
            p++;
        }

        if (!tableList.contains("SY_PREAPPROVUSERS")) {
            /**流程已执行人*/
            DynaBean preapprovUser = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(preapprovUser, "SY_PREAPPROVUSERS", "流程待执行人ID", 336);
            preapprovUser.set("TABLECOLUMN_NAME_EN", "Waiting ID");
            preapprovUser.set("TABLECOLUMN_TYPE", "VARCHAR");
            preapprovUser.set("TABLECOLUMN_LENGTH", "1000");
            columns.add(preapprovUser);
            p++;
        }

        if (!tableList.contains("SY_PREAPPROVUSERNAMES")) {
            /**流程已执行人*/
            DynaBean preapprovUserName = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(preapprovUserName, "SY_PREAPPROVUSERNAMES", "流程待执行人", 337);
            preapprovUserName.set("TABLECOLUMN_NAME_EN", "Waiting");
            preapprovUserName.set("TABLECOLUMN_TYPE", "VARCHAR");
            preapprovUserName.set("TABLECOLUMN_LENGTH", "1000");
            columns.add(preapprovUserName);
            p++;
        }

        if (!tableList.contains("SY_LASTFLOWUSER")) {
            /**流程任务指派人*/
            DynaBean lastFlowUser = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(lastFlowUser, "SY_LASTFLOWUSER", "流程任务指派人", 338);
            lastFlowUser.set("TABLECOLUMN_NAME_EN", "Waiting");
            lastFlowUser.set("TABLECOLUMN_TYPE", "VARCHAR");
            lastFlowUser.set("TABLECOLUMN_LENGTH", "1000");
            columns.add(lastFlowUser);
            p++;
        }

        if (!tableList.contains("SY_LASTFLOWUSERID")) {
            DynaBean lastFlowUserId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(lastFlowUserId, "SY_LASTFLOWUSERID", "流程任务指派人ID", 338);
            lastFlowUserId.set("TABLECOLUMN_NAME_EN", "Waiting");
            lastFlowUserId.set("TABLECOLUMN_TYPE", "VARCHAR");
            lastFlowUserId.set("TABLECOLUMN_LENGTH", "1000");
            columns.add(lastFlowUserId);
            p++;
        }

        /*if (!tableList.contains("SY_WFWARN")) {
         *//**流程预警延期信息*//*
            DynaBean warnFlowUser = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(warnFlowUser, "SY_WFWARN", "流程预警延期信息", 339);
            warnFlowUser.set("TABLECOLUMN_NAME_EN", "Warning");
            warnFlowUser.set("TABLECOLUMN_TYPE", "CLOB");
            warnFlowUser.set("TABLECOLUMN_LENGTH", "");
            columns.add(warnFlowUser);
            p++;
        }*/

        /* if (!tableList.contains("SY_WARNFLAG")) {
         *//**流程预警延期标识*//*
            DynaBean warnFlag = new DynaBean("JE_CORE_TABLECOLUMN", false);
            //0正常，1预警，2延期
            buildColumn(warnFlag, "SY_WARNFLAG", "流程预警延期标识", 340);
            warnFlag.set("TABLECOLUMN_NAME_EN", "Warning Flag");
            warnFlag.set("TABLECOLUMN_TYPE", "YESORNO");
            warnFlag.set("TABLECOLUMN_LENGTH", "");
            columns.add(warnFlag);
            p++;
        }*/

        if (!tableList.contains("SY_CURRENTTASK")) {
            /**流程预警延期标识*/
            DynaBean currentTask = new DynaBean("JE_CORE_TABLECOLUMN", false);
            //0正常，1预警，2延期
            buildColumn(currentTask, "SY_CURRENTTASK", "当前执行节点", 340);
            currentTask.set("TABLECOLUMN_NAME_EN", "Warning Flag");
            currentTask.set("TABLECOLUMN_TYPE", "VARCHAR255");
            currentTask.set("TABLECOLUMN_LENGTH", "");
            columns.add(currentTask);
            p++;
        }

        /* if (!tableList.contains("SY_LASTFLOWINFO")) {
         *//**流程最后任务指派信息*//*
            DynaBean lastFlowInfo = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(lastFlowInfo, "SY_LASTFLOWINFO", "流程最后任务指派信息", 336);
            lastFlowInfo.set("TABLECOLUMN_NAME_EN", "Waiting ID");
            lastFlowInfo.set("TABLECOLUMN_TYPE", "VARCHAR");
            lastFlowInfo.set("TABLECOLUMN_LENGTH", "1000");
            columns.add(lastFlowInfo);
            p++;
        }*/
        if (p == 0) {
            throw new APIWarnException(MessageUtils.getMessage("table.generate.workMulti"), "201", new Object[]{resourceTable});
        }

        insertColumns(resourceTable, tableCode, columns);
        resourceTable.set("RESOURCETABLE_IMPLWF", "1");
        metaService.update(resourceTable);
        return true;
    }

    @Override
    @Transactional
    public void initSaasColumns(DynaBean resourceTable) {
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<DynaBean> columns = new ArrayList<DynaBean>();
        /**流程启动人*/
        DynaBean zhId = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(zhId, "SY_TENANT_ID", "租户ID", 23);
        zhId.set("TABLECOLUMN_NAME_EN", "SaasUser ID");
        zhId.set("TABLECOLUMN_TYPE", "VARCHAR50");
        zhId.set("TABLECOLUMN_LENGTH", "");
        zhId.set("TABLECOLUMN_NAME_EN", "SaasUser ID");
        columns.add(zhId);
        DynaBean zhMc = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(zhMc, "SY_ZHMC", "租户名称", 23);
        zhMc.set("TABLECOLUMN_NAME_EN", "SaasUser Name");
        zhMc.set("TABLECOLUMN_TYPE", "VARCHAR100");
        zhMc.set("TABLECOLUMN_LENGTH", "");
        columns.add(zhMc);
        insertColumns(resourceTable, tableCode, columns);
        developLogRpcService.doDevelopLog("INIT_SAAS_COLUMNS", "初始化SAAS基础字段", "TABLE", "资源表", resourceTable.getStr("RESOURCETABLE_TABLENAME"), resourceTable.getStr("RESOURCETABLE_TABLECODE"), resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"), resourceTable.getStr("SY_PRODUCT_ID"));
    }


    @Override
    @Transactional
    public boolean initExtendColumns(DynaBean resourceTable) throws APIWarnException {
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<String> tableList = new ArrayList<>();
        List<Map<String, Object>> maps = metaService.selectSql("select TABLECOLUMN_CODE from JE_CORE_TABLECOLUMN where TABLECOLUMN_TABLECODE={0}", tableCode);
        Iterator<Map<String, Object>> it = maps.iterator();
        while (it.hasNext()) {
            Map<String, Object> data = (Map<String, Object>) it.next();
            String field = data.get("TABLECOLUMN_CODE").toString();
            tableList.add(field);
        }

        String nowDate = DateUtils.formatDateTime(new Date());
        //默十个自定义字段
        int p = 0;
        for (int i = 1, num = 10; i <= num; i++) {
            String rowNum = String.format("%02d", i);
            String code = "SY_EXTEND" + rowNum;
            if (tableList.contains(code)) {
                continue;
            }
            DynaBean column = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(column, code, "扩展字段" + rowNum, 200 + i);
            column.set("TABLECOLUMN_TYPE", "VARCHAR100");
            column.set("TABLECOLUMN_LENGTH", "");
            column.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("SY_CREATETIME", nowDate);
            column.set("SY_CREATEUSERID", SecurityUserHolder.getCurrentAccountRealUserId());
            column.set("SY_CREATEUSERNAME", SecurityUserHolder.getCurrentAccountRealUserName());
            column.set("SY_CREATEORGID", SecurityUserHolder.getCurrentAccountRealOrgId());
            column.set("SY_CREATEORGNAME", SecurityUserHolder.getCurrentAccountRealOrgName());
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_TABLECODE", tableCode);
            if (!"1".equals(column.getStr("TABLECOLUMN_UNIQUE", ""))) {
                column.set("TABLECOLUMN_UNIQUE", "0");
            }
            column.set("TABLECOLUMN_RESOURCETABLE_ID", resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"));
            metaService.insert(column);
            p++;
        }
        if (p == 0) {
            throw new APIWarnException(MessageUtils.getMessage("table.generate.extendMulti"), "201", new Object[]{resourceTable});
        }
        //return BaseRespResult.successResult(MessageUtils.getMessage("table.generate.extendFiled"));
        developLogRpcService.doDevelopLog("INIT_EXTEND_COLUMNS", "初始化扩展字段", "TABLE", "资源表", resourceTable.getStr("RESOURCETABLE_TABLENAME"), resourceTable.getStr("RESOURCETABLE_TABLECODE"), resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"), resourceTable.getStr("SY_PRODUCT_ID"));
        return true;
    }

    @Override
    public boolean initWfBaseColumns(DynaBean resourceTable) throws APIWarnException {
        int p = 0;
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<String> columsCode = new ArrayList<>();
        List<Map<String, Object>> maps = metaService.selectSql("select TABLECOLUMN_CODE from JE_CORE_TABLECOLUMN where TABLECOLUMN_TABLECODE={0}", tableCode);
        Iterator<Map<String, Object>> it = maps.iterator();
        while (it.hasNext()) {
            Map<String, Object> data = (Map<String, Object>) it.next();
            String field = data.get("TABLECOLUMN_CODE").toString();
            columsCode.add(field);
        }
        List<DynaBean> columns = new ArrayList();
        /**审核标记*/
        if (!columsCode.contains("SY_AUDFLAG")) {
            DynaBean audFlag = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(audFlag, "SY_AUDFLAG", "审核标记", 311);
            audFlag.set("TABLECOLUMN_TYPE", "VARCHAR");
            audFlag.set("TABLECOLUMN_LENGTH", "20");
            audFlag.set("TABLECOLUMN_NAME_EN", "Audit Flag");
            columns.add(audFlag);
            p++;
        }

        /**流程实例ID*/
        if (!columsCode.contains("SY_PIID")) {
            DynaBean piId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(piId, "SY_PIID", "流程实例ID", 321);
            piId.set("TABLECOLUMN_NAME_EN", "Process Case ID");
            columns.add(piId);
            p++;
        }

        /**流程定义ID*/
        if (!columsCode.contains("SY_PDID")) {
            DynaBean pdId = new DynaBean("JE_CORE_TABLECOLUMN", false);
            buildColumn(pdId, "SY_PDID", "流程定义ID", 322);
            pdId.set("TABLECOLUMN_NAME_EN", "Process Definition ID");
            columns.add(pdId);
            p++;
        }
        if (p == 0) {
            throw new APIWarnException(MessageUtils.getMessage("table.generate.workMulti"), "201", new Object[]{resourceTable});
        }

        insertColumns(resourceTable, tableCode, columns);
        developLogRpcService.doDevelopLog("INIT_WFBASE_COLUMNS", "初始化流程基础字段", "TABLE", "资源表", resourceTable.getStr("RESOURCETABLE_TABLENAME"), resourceTable.getStr("RESOURCETABLE_TABLECODE"), resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"), resourceTable.getStr("SY_PRODUCT_ID"));
        return true;
    }

    @Override
    public boolean initSecretColumns(DynaBean resourceTable) {
        String tableCode = resourceTable.getStr("RESOURCETABLE_TABLECODE");
        List<DynaBean> columns = new ArrayList<DynaBean>();
        /**密级编码*/
        DynaBean secretCode = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(secretCode, "SY_SECRET_CODE", "密级编码", 325);
        secretCode.set("TABLECOLUMN_TYPE", "VARCHAR30");
        secretCode.set("TABLECOLUMN_LENGTH", "");
        secretCode.set("TABLECOLUMN_NAME_EN", "Secret Code");
        columns.add(secretCode);
        /**密级名称*/
        DynaBean secretName = new DynaBean("JE_CORE_TABLECOLUMN", false);
        buildColumn(secretName, "SY_SECRET_NAME", "密级名称", 325);
        secretName.set("TABLECOLUMN_TYPE", "VARCHAR30");
        secretName.set("TABLECOLUMN_LENGTH", "");
        secretName.set("TABLECOLUMN_NAME_EN", "Secret Name");
        columns.add(secretName);

        insertColumns(resourceTable, tableCode, columns);
        developLogRpcService.doDevelopLog("INIT_PRODUCT_COLUMNS", "初始化密级字段", "TABLE", "资源表", resourceTable.getStr("RESOURCETABLE_TABLENAME"), resourceTable.getStr("RESOURCETABLE_TABLECODE"), resourceTable.getStr("JE_CORE_RESOURCETABLE_ID"), resourceTable.getStr("SY_PRODUCT_ID"));
        return true;
    }

    private void insertColumns(DynaBean table, String tableCode, List<DynaBean> columns) {
        String nowDate = DateUtils.formatDateTime(new Date());
        for (DynaBean column : columns) {
            column.set(BeanService.KEY_PK_CODE, "JE_CORE_TABLECOLUMN_ID");
            column.set("SY_CREATETIME", nowDate);
            column.set("SY_CREATEUSER", SecurityUserHolder.getCurrentAccountRealUserId());
            column.set("SY_CREATEUSERNAME", SecurityUserHolder.getCurrentAccountRealUserName());
            column.set("SY_CREATEORG", SecurityUserHolder.getCurrentAccountRealOrgId());
            column.set("SY_CREATEORGNAME", SecurityUserHolder.getCurrentAccountRealOrgName());
            column.set("TABLECOLUMN_ISCREATE", "0");
            column.set("TABLECOLUMN_TABLECODE", tableCode);
            if (!"1".equals(column.getStr("TABLECOLUMN_UNIQUE", ""))) {
                column.set("TABLECOLUMN_UNIQUE", "0");
            }
            column.set("TABLECOLUMN_RESOURCETABLE_ID", table.getStr("JE_CORE_RESOURCETABLE_ID"));
            metaService.insert(column);
        }
    }

}
