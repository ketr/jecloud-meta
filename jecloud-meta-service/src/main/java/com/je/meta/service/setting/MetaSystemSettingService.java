/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.setting;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.document.DocumentPreviewWayEnum;
import com.je.message.vo.EmailInfo;
import com.je.message.vo.EmailMsg;
import com.je.meta.setting.SettingException;
import com.je.meta.setting.SystemSetting;

import java.util.List;
import java.util.Map;

public interface MetaSystemSettingService {

    SystemSetting findAllSetting();

    /**
     * 加载系统设置
     */
    void reloadLoadSystemSetting();

    /**
     * 加载系统设置(系统设置页面回显使用)
     * @return
     */
    SystemSetting findSystemConfig(String attribute);

    /**
     * 更新系统设置(系统设置页面保存使用)
     * @param attribute
     * @param map
     * @param type
     * @return
     */
    void doWriteSysConfigVariables(String attribute, JSONObject map,String type) throws SettingException;

    /**
     * 写入设置
     * @param attribute
     * @param code
     * @param value
     */
    void doWriteSettingValue(String attribute,String code,String value);

    /**
     *
     * @return
     */
    EmailInfo getEmailInfo();

    /**
     * 发送邮件
     * @param msgVo
     * @param emailInfo
     */
    void sendeEmail(EmailMsg msgVo, EmailInfo emailInfo) throws Exception;

    /**
     * 获取平台设置-》消息服务中的所有消息类型
     * @throws Exception
     */
    List<Map<String,String>> getMessageType() throws Exception;

}
