/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.JEUUID;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import com.je.meta.service.func.MetaFuncRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MetaFuncRelationServiceImpl implements MetaFuncRelationService {

    @Autowired
    protected CommonService commonService;
    @Autowired
    protected MetaService metaService;

    @Override
    public DynaBean saveRelationSet(DynaBean dynaBean) {
        String id = JEUUID.uuid();
        dynaBean.setStr("JE_CORE_ASSOCIATIONFIELD_ID", id);
        commonService.buildModelCreateInfo(dynaBean);
        metaService.insert(dynaBean);
        return metaService.selectOneByPk("JE_CORE_ASSOCIATIONFIELD", id);
    }

    @Override
    public List<Map<String, Object>> getChildList(Page page, String funcId) {
        List<Map<String,Object>> childList = metaService.load("JE_CORE_FUNCINFO",page,
                ConditionsWrapper.builder().table("JE_CORE_FUNCINFO").like("SY_PARENTPATH",funcId)
                        .eq("FUNCINFO_NODEINFOTYPE","FUNCFIELD"));
        if(childList==null || childList.size()==0){
            return new ArrayList<>();
        }
        String productId = childList.get(0).get("SY_PRODUCT_ID").toString();
        //当前服务的所有功能的表单字段类型为'childfuncfield'的RESOURCEFIELD_CONFIGINFO信息
        List<Map<String,Object>> fildsList = metaService.selectSql("SELECT r.RESOURCEFIELD_CONFIGINFO from je_core_funcinfo f,JE_CORE_RESOURCEFIELD r WHERE f.je_core_funcinfo_id=r.RESOURCEFIELD_FUNCINFO_ID AND f.SY_PRODUCT_ID='"+productId+"' AND r.RESOURCEFIELD_XTYPE='childfuncfield'");
        if(fildsList==null || fildsList.size()==0){
            return childList;
        }
        List<String> configs = getChildFuncCode(fildsList);
        Iterator item =childList.iterator();
        while (item.hasNext()){
            Map<String,Object> map = (Map<String, Object>) item.next();
            String funcCode = map.get("FUNCINFO_FUNCCODE").toString();
            if(configs.contains(funcCode)){
                item.remove();
            }
        }
        return childList;
    }

    @Override
    public Map<String, Object> getParentAndChildInfo(String funcId) {
        DynaBean currentRelation = metaService.selectOne("JE_CORE_FUNCRELATION",
                ConditionsWrapper.builder().eq("FUNCRELATION_FUNCID",funcId));

        Map<String,Object> map = new HashMap<>();
        DynaBean parent=null;
        if(currentRelation!=null){
            String parentFuncId = currentRelation.getStr("FUNCRELATION_FUNCINFO_ID");
            if(StringUtil.isNotEmpty(parentFuncId)){
                parent = metaService.selectOneByPk("JE_CORE_FUNCINFO",parentFuncId);
            }
        }
        map.put("parent",parent);

        DynaBean current = metaService.selectOneByPk("JE_CORE_FUNCINFO",funcId);
        map.put("current",current);


        List<DynaBean> childsRelation = metaService.select("JE_CORE_FUNCRELATION",
                ConditionsWrapper.builder().eq("FUNCRELATION_FUNCINFO_ID",funcId)
                        .eq("FUNCRELATION_RELYONTYPE","func"));

        List<DynaBean> childs =new ArrayList<>();
        for(DynaBean dynaBean:childsRelation){
            String FUNCRELATION_FUNCID = dynaBean.getStr("FUNCRELATION_FUNCID");
            if(StringUtil.isNotEmpty(FUNCRELATION_FUNCID)){
                DynaBean child = metaService.selectOneByPk("JE_CORE_FUNCINFO",FUNCRELATION_FUNCID);
                if(child!=null){
                    childs.add(child);
                }

            }
        }
        map.put("childs",childs);
        return map;
    }

    private List<String> getChildFuncCode(List<Map<String, Object>> fildsList) {
        List<String> list = new ArrayList<>();
        for(Map<String,Object> map: fildsList){
            String config = map.get("RESOURCEFIELD_CONFIGINFO")==null?"":map.get("RESOURCEFIELD_CONFIGINFO").toString();
            if(StringUtil.isEmpty(config)){
                continue;
            }
            config = config.trim();
            list.add(config.split(",")[0]);
        }
        return list;
    }

}
