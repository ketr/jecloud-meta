package com.je.meta.service.table.column;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.APIWarnException;

import javax.servlet.http.HttpServletRequest;

public interface MetaTableSystemColumnService {

    /**
     * 添加系统字段
     * @param request
     */
    boolean addSystemColumn(HttpServletRequest request) throws APIWarnException;

    /**
     * 初始化产品字段
     * @param table
     */
    void initProjectColumns(DynaBean table);

    /**
     * 初始化产品字段
     * @param table
     */
    void initProductColumns(DynaBean table);

    /**
     * 初始化修改信息
     * @param resourceTable 修改的表信息
     */
    boolean initUpdateColumns(DynaBean resourceTable) throws APIWarnException;

    /**
     * 初始化创建信息
     * @param resourceTable 修改的表信息
     */
    boolean initCreateColumns(DynaBean resourceTable) throws APIWarnException;

    /**
     * 初始化审核信息
     *
     * @param resourceTable 修改表的信息
     */
    void initShColumns(DynaBean resourceTable);

    /**
     * 初始化工作流信息
     * @param resourceTable 修改表的信息
     */
    boolean initProcessExtendColumns(DynaBean resourceTable) throws APIWarnException;

    /**
     * 初始化租户信息
     *
     * @param resourceTable 修改表的信息
     */
    void initSaasColumns(DynaBean resourceTable);

    /**
     * 初始化拓展字段
     *
     * @param resourceTable 修改表的信息
     */
    boolean initExtendColumns(DynaBean resourceTable) throws APIWarnException;

    /**
     * 初始化工作流基础字段
     * @param resourceTable
     * @return
     */
    boolean initWfBaseColumns(DynaBean resourceTable) throws APIWarnException;

    /**
     * 初始化密级字段
     * @param resourceTable
     * @return
     */
    boolean initSecretColumns(DynaBean resourceTable);

}
