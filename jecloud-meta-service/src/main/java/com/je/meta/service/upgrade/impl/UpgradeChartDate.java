package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打包图表
 */
@Service("upgradeChartDate")
public class UpgradeChartDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeChartDate() {
        super(UpgradeResourcesEnum.CHART);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> chartList = getSourceIds(upgradeBean);
        if (chartList == null || chartList.size() == 0) {
            return;
        }
        packageResult.setProductCharts(packageResourcesOrderAsc(packageResult, chartList, "SY_TREEORDERINDEX"));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductCharts();
    }


}
