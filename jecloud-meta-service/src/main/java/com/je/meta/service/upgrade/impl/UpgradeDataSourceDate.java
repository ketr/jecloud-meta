package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打包数据源
 */
@Service("upgradeDataSourceDate")
public class UpgradeDataSourceDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeDataSourceDate() {
        super(UpgradeResourcesEnum.DATASOURCE);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> list = getSourceIds(upgradeBean);
        if (list == null || list.size() == 0) {
            return;
        }
        packageResult.setProductDataSource(packageResourcesOrderAsc(packageResult, list, "SY_TREEORDERINDEX"));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductDataSource();
    }

}
