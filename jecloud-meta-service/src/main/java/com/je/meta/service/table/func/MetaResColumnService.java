/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.func;

import com.je.common.base.DynaBean;

import java.util.List;
import java.util.Map;

public interface MetaResColumnService {

    /**
     * 批量调整功能列信息
     * @param funcId
     */
    void batchSortOutColumnInfoByFuncId(String funcId);

    /**
     * 构建列表操作默认信息
     * @param dynaBean 数据源
     */
    void buildDefault(DynaBean dynaBean);

    /**
     * 实体类方式导入字段
     * @param funId 类型
     * @param c 类
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    void impl(String funId, Class c) throws InstantiationException, IllegalAccessException;

    /**
     * DynaBean方式导入字段
     * @param funId TODO未处理
     * @param tableCode
     */
    void implDyanBean(String funId, String tableCode);

    /**
     * 存储过程导入字段
     * @param funcInfo TODO未处理
     */
    void implProcedure(DynaBean funcInfo);

    /**
     * 动态列存储过程导入字段
     * @param funcInfo TODO未处理
     */
    void implIditProcedure(DynaBean funcInfo);

    /**
     * 存储过程导入字段
     * @param funcInfo TODO未处理
     */
    void implSql(DynaBean funcInfo);

    /**
     * 与表单字段同步
     * @param dynaBean 数据源
     */
    void doSync(DynaBean dynaBean);

    /**
     * 导入列和字段
     * @param funcInfo 数据源
     */
    String impl(DynaBean funcInfo);

    /**
     * 保存查询字典
     * @param name 名称
     * @param code 编码
     * @param funId TODO未处理
     */
    void saveQueryField(String name, String code, String funId);

    /**
     * 获取指定功能树形表字段的名称集合
     * @param funcInfo TODO未处理
     * @return
     */
    List<String> getTreeTableCodes(DynaBean funcInfo);

    int doRemove(String ids);

    Map<String, Object> getRelevanceField(DynaBean dynaBean);

    int doUpdateList(String strData);
}
