/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.MessageUtils;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.ResFieldPrintPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ResFieldPrintPlanServiceImpl implements ResFieldPrintPlanService {

    @Autowired
    private CommonService commonService;
    @Autowired
    private MetaService metaService;

    @Override
    public DynaBean doCopy(DynaBean dynaBean) {
        String fromPlanId = dynaBean.getStr("fromPlanId");
        String PLAN_FUNCINFO_ID = dynaBean.getStr("PLAN_FUNCINFO_ID");
        DynaBean fromPlan = metaService.selectOneByPk(dynaBean.getTableCode(), fromPlanId);
        if (fromPlan == null) {
            throw new PlatformException(MessageUtils.getMessage("function.field.print plan.notExits"), PlatformExceptionEnum.UNKOWN_ERROR);
        }
        dynaBean.setStr("PLAN_PRINT_CONFIG", fromPlan.getStr("PLAN_PRINT_CONFIG"));
        commonService.buildModelCreateInfo(dynaBean);
        List<DynaBean> list = metaService.select(dynaBean.getTableCode(), ConditionsWrapper.builder().eq("PLAN_FUNCINFO_ID", PLAN_FUNCINFO_ID));
        dynaBean.set("SY_ORDERINDEX", list.size() + 1);
        dynaBean.setStr("PLAN_DEFAULT", "0");
        dynaBean.setStr("SY_STATUS", "1");
        metaService.insert(dynaBean);
        return dynaBean;
    }

}
