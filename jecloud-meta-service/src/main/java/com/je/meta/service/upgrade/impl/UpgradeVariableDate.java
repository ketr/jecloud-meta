package com.je.meta.service.upgrade.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.upgrade.PackageResult;
import com.je.common.base.upgrade.UpgradeResourcesEnum;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 打包系统变量
 */
@Service("upgradeVariableDate")
public class UpgradeVariableDate extends AbstractUpgradeMetaDate<PackageResult> {

    public UpgradeVariableDate() {
        super(UpgradeResourcesEnum.VARIABLE);
    }

    @Override
    public void packUpgradeMetaDate(PackageResult packageResult, DynaBean upgradeBean) {
        List<String> variableList = getSourceIds(upgradeBean);
        if (variableList == null || variableList.size() == 0) {
            return;
        }
        packageResult.setProductSystemVariables(packageResources(packageResult, variableList));
    }

    @Override
    public List<DynaBean> extractNecessaryData(PackageResult packageResult) {
        return packageResult.getProductSystemVariables();
    }

}
