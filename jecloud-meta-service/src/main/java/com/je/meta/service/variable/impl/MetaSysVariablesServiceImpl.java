/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.variable.impl;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.variable.BackCache;
import com.je.meta.cache.variable.FrontCache;
import com.je.meta.service.variable.MetaSysVariablesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MetaSysVariablesServiceImpl implements MetaSysVariablesService {

    /**
     * 前端类型
     */
    public static final String FRONT_TYPE = "FRONT";
    /**
     * 公共类型
     */
    public static final String SHARE_TYPE = "SHARE";
    /**
     * 后端类型
     */
    public static final String BACK_TYPE = "BACK";

    public static final String TABLE_CODE_CONFIG = "JE_CORE_CONFIG";

    @Autowired
    private BackCache backCache;
    @Autowired
    private FrontCache frontCache;
    @Autowired
    private MetaService metaService;
    @Lazy
    @Autowired
    private CommonService commonService;

    /**
     * 加载用户变量
     */
    @Override
    public void reloadSystemVariables() {
        frontCache.clear();
        backCache.clear();

        // Retrieve the configuration from the service
        List<DynaBean> configList = metaService.select(TABLE_CODE_CONFIG,
                ConditionsWrapper.builder().eq("CONFIG_ENABLED", "1"));

        Map<String, String> frontVars = new HashMap<>();
        Map<String, String> backVars = new HashMap<>();

        for (DynaBean config : configList) {
            String configCode = config.getStr("CONFIG_CODE");
            String configValue = config.getStr("CONFIG_VALUE");
            String configTypeCode = config.getStr("CONFIG_TYPE_CODE");

            boolean isPlatform = "1".equals(config.getStr("CONFIG_PTBL", "1"));

            if (SHARE_TYPE.equals(configTypeCode)) {
                if (!frontVars.containsKey(configCode) || !isPlatform) {
                    frontVars.put(configCode, configValue);
                    backVars.put(configCode, configValue);
                }
            } else if (FRONT_TYPE.equals(configTypeCode)) {
                if (!frontVars.containsKey(configCode) || !isPlatform) {
                    frontVars.put(configCode, configValue);
                }
            } else if (BACK_TYPE.equals(configTypeCode)) {
                if (!backVars.containsKey(configCode) || !isPlatform) {
                    backVars.put(configCode, configValue);
                }
            }
        }

        String currentTenantId = SecurityUserHolder.getCurrentAccountTenantId();
        if (Strings.isNullOrEmpty(currentTenantId)) {
            frontCache.putMapCache(frontVars);
            backCache.putMapCache(backVars);
        } else {
            frontCache.putMapCache(currentTenantId, frontVars);
            backCache.putMapCache(currentTenantId, backVars);
        }
    }

    @Override
    public Map<String, String> findNativeAllFrontVariables() {
        List<DynaBean> lists = metaService.select(TABLE_CODE_CONFIG, ConditionsWrapper.builder()
                .eq("CONFIG_ENABLED", "1")
                .eq("CONFIG_TYPE_CODE", FRONT_TYPE));
        Map<String, String> frontVars = new HashMap<>();
        for (DynaBean var : lists) {
            boolean isPlatform = "1".equals(var.getStr("CONFIG_PTBL", "1"));

            String configCode = var.getStr("CONFIG_CODE");
            String configValue = var.getStr("CONFIG_VALUE");

            if (!frontVars.containsKey(configCode) || !isPlatform) {
                frontVars.put(configCode, configValue);
            }

        }
        return frontVars;
    }

    @Override
    public Map<String, String> findNativeAllBackVariables() {
        List<DynaBean> lists = metaService.select(TABLE_CODE_CONFIG, ConditionsWrapper.builder()
                .eq("CONFIG_ENABLED", "1")
                .eq("CONFIG_TYPE_CODE", BACK_TYPE));
        Map<String, String> backVars = new HashMap<>();
        for (DynaBean var : lists) {
            boolean isPlatform = "1".equals(var.getStr("CONFIG_PTBL", "1"));

            String configCode = var.getStr("CONFIG_CODE");
            String configValue = var.getStr("CONFIG_VALUE");

            if (!backVars.containsKey(configCode) || !isPlatform) {
                backVars.put(configCode, configValue);
            }
        }
        return backVars;
    }

    @Override
    public String findNativeBackVariable(String code) {
        List<DynaBean> variableBeans = metaService.select(TABLE_CODE_CONFIG, ConditionsWrapper.builder()
                .eq("CONFIG_ENABLED", "1")
                .eq("CONFIG_TYPE_CODE", BACK_TYPE)
                .eq("CONFIG_CODE", code));
        if (variableBeans == null || variableBeans.size() == 0) {
            return null;
        }
        if (variableBeans.size() == 1) {
            return variableBeans.get(0).getStr("CONFIG_VALUE");
        }
        DynaBean variableBean = null;
        for (DynaBean dynaBean : variableBeans) {
            boolean isPlatform = "1".equals(dynaBean.getStr("CONFIG_PTBL", "1"));
            if (variableBean == null || !isPlatform) {
                variableBean = dynaBean;
            }
        }
        return variableBean.getStr("CONFIG_VALUE");
    }

    @Override
    public String findNativeFrontVariable(String code) {
        List<DynaBean> variableBeans = metaService.select(TABLE_CODE_CONFIG, ConditionsWrapper.builder()
                .eq("CONFIG_ENABLED", "1")
                .eq("CONFIG_TYPE_CODE", FRONT_TYPE)
                .eq("CONFIG_CODE", code));
        if (variableBeans == null || variableBeans.size() == 0) {
            return null;
        }
        if (variableBeans.size() == 1) {
            return variableBeans.get(0).getStr("CONFIG_VALUE");
        }
        DynaBean variableBean = null;
        for (DynaBean dynaBean : variableBeans) {
            boolean isPlatform = "1".equals(dynaBean.getStr("CONFIG_PTBL", "1"));
            if (variableBean == null || !isPlatform) {
                variableBean = dynaBean;
            }
        }
        return variableBean.getStr("CONFIG_VALUE");
    }

    /**
     * 根据Code获取系统变量
     *
     * @param code
     * @return
     */
    @Override
    public String findNativeSystemVariable(String code) {
        List<DynaBean> variableBeans = metaService.select(TABLE_CODE_CONFIG, ConditionsWrapper.builder()
                .eq("CONFIG_ENABLED", "1")
                .eq("CONFIG_CODE", code));
        if (variableBeans == null || variableBeans.size() == 0) {
            return null;
        }
        if (variableBeans.size() == 1) {
            return variableBeans.get(0).getStr("CONFIG_VALUE");
        }
        DynaBean variableBean = null;
        for (DynaBean dynaBean : variableBeans) {
            boolean isPlatform = "1".equals(dynaBean.getStr("CONFIG_PTBL", "1"));
            if (variableBean == null || !isPlatform) {
                variableBean = dynaBean;
            }
        }
        return variableBean.getStr("CONFIG_VALUE");
    }

    @Override
    public void writeBackVariable(String key, String value) {
        List<DynaBean> variableBeans = metaService.select(TABLE_CODE_CONFIG, ConditionsWrapper.builder()
                .eq("CONFIG_ENABLED", "1")
                .eq("CONFIG_TYPE_CODE", BACK_TYPE)
                .eq("CONFIG_CODE", key));
        if (variableBeans == null || variableBeans.size() == 0) {
            DynaBean variableBean = new DynaBean(TABLE_CODE_CONFIG, true);
            variableBean.set("CONFIG_ENABLED", "1");
            variableBean.set("CONFIG_TYPE_CODE", BACK_TYPE);
            variableBean.set("CONFIG_CODE", key);
            variableBean.set("CONFIG_VALUE", value);
            variableBean.set("CONFIG_PTBL", "0");
            commonService.buildModelCreateInfo(variableBean);
            metaService.insert(variableBean);
        } else {
            for (DynaBean variableBean : variableBeans) {
                variableBean.set("CONFIG_VALUE", value);
                commonService.buildModelModifyInfo(variableBean);
                metaService.update(variableBean);
            }
        }
    }

    @Override
    public void writeFrontVariable(String key, String value) {
        List<DynaBean> variableBeans = metaService.select(TABLE_CODE_CONFIG, ConditionsWrapper.builder()
                .eq("CONFIG_ENABLED", "1")
                .eq("CONFIG_TYPE_CODE", FRONT_TYPE)
                .eq("CONFIG_CODE", key));
        if (variableBeans == null || variableBeans.size() == 0) {
            DynaBean variableBean = new DynaBean(TABLE_CODE_CONFIG, true);
            variableBean.set("CONFIG_ENABLED", "1");
            variableBean.set("CONFIG_TYPE_CODE", BACK_TYPE);
            variableBean.set("CONFIG_CODE", key);
            variableBean.set("CONFIG_VALUE", value);
            variableBean.set("CONFIG_PTBL", "0");
            commonService.buildModelCreateInfo(variableBean);
            metaService.insert(variableBean);
        } else {
            for (DynaBean variableBean : variableBeans) {
                variableBean.set("CONFIG_VALUE", value);
                commonService.buildModelModifyInfo(variableBean);
                metaService.update(variableBean);
            }
        }
    }


}
