package com.je.meta.service.busflow;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.JEUUID;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.cache.busflow.JeBusFlowCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component("jeBusFlowDefineService")
public class JeBusFlowDefineServiceImpl implements JeBusFlowDefineService {
    @Autowired
    private MetaService metaService;
    @Autowired
    private CommonService commonService;
    @Autowired
    private JeBusFlowCache jeBusFlowCache;
    @Override
    public DynaBean doAddNode(HashMap<String, String> param, JSONObject returnObj) {
        String mainId=param.get("mainId");
        String nodeName=param.get("nodeName");
        String nodeCode=param.get("nodeCode");
        String parentId=param.get("parentId");
        long count=metaService.selectCount("JE_META_BUSFLOW_NODE"," AND JE_META_BUSFLOW_ID='"+mainId+"' AND NODE_CODE='"+nodeCode+"'");
        if(count>0){
            //节点已存在
            returnObj.put("success",false);
            returnObj.put("obj","节点编码【"+nodeCode+"】已存在!");
            return null;
//            return BaseRespResult.errorResult("节点编码【"+nodeCode+"】已存在!");
        }
        DynaBean parentNode=null;
        if(StringUtil.isNotEmpty(parentId)) {
            parentNode=metaService.selectOneByPk("JE_META_BUSFLOW_NODE", parentId);
        }else{
            parentNode=metaService.selectOneByPk("JE_META_BUSFLOW_NODE", "ROOT");
        }
        long index=metaService.selectCount("JE_META_BUSFLOW_NODE"," AND JE_META_BUSFLOW_ID='"+mainId+"' AND SY_PARENT='"+parentNode.get("JE_META_BUSFLOW_NODE_ID")+"'");
        index++;
        DynaBean node=new DynaBean("JE_META_BUSFLOW_NODE",true);
        node.set("JE_META_BUSFLOW_ID",mainId);
        node.set("NODE_NAME",nodeName);
        node.set("NODE_CODE",nodeCode);
        //拼接树形的属性信息
        node.set("JE_META_BUSFLOW_NODE_ID", JEUUID.uuid());
        node.set("SY_PARENT",parentNode.get("JE_META_BUSFLOW_NODE_ID"));
        node.set("SY_NODETYPE","LEAF");
        node.set("SY_LAYER",parentNode.getInt("SY_LAYER") + 1);
        node.set("SY_PATH",parentNode.get("SY_PATH")+"/"+node.get("JE_META_BUSFLOW_NODE_ID"));
        node.set("SY_DISABLED","0");
        node.set("SY_TREEORDERINDEX",parentNode.getStr("SY_TREEORDERINDEX") + String.format("%06d", index));
        commonService.buildFuncDefaultValues("JE_META_BUSFLOW_NODE",node);
        commonService.buildModelCreateInfo(node);
        node=metaService.insertAndReturn(node);
        jeBusFlowCache.clear();
        returnObj.put("success",true);
        returnObj.put("obj","操作成功");
        return node;
    }

    @Override
    public void doDelNode(HashMap<String, String> param, JSONObject returnObj) {
        String mainId=param.get("mainId");
        String nodeId=param.get("nodeId");
        if(StringUtil.isNotEmpty(nodeId)) {
            List<DynaBean> nodes = metaService.selectBeanSql("JE_META_BUSFLOW_NODE", " AND SY_PATH LIKE '%" + nodeId + "%' ORDER BY SY_LAYER DESC");
            for(DynaBean node:nodes){
                String id=node.getStr("JE_META_BUSFLOW_NODE_ID");
                metaService.delete("JE_META_BUSFLOW_NODE", ConditionsWrapper.builder().eq("JE_META_BUSFLOW_NODE_ID",id));
            }
            jeBusFlowCache.clear();
            returnObj.put("success",true);
            returnObj.put("obj","删除成功!");
        }else{
            returnObj.put("success",false);
            returnObj.put("obj","传入节点id为空!");
        }
    }

    @Override
    public DynaBean doInitDrawNode(HashMap<String, String> param, JSONObject returnObj) {
        String mainId=param.get("mainId");
        String nodeName="默认节点";
        String nodeCode="DEFAULT";
        String parentId="";
        long count=metaService.selectCount("JE_META_BUSFLOW_NODE"," AND JE_META_BUSFLOW_ID='"+mainId+"' AND NODE_CODE='"+nodeCode+"'");
        if(count>0){
            //节点已存在
            returnObj.put("success",false);
            returnObj.put("obj","节点编码【"+nodeCode+"】已存在!");
            return null;
//            return BaseRespResult.errorResult("节点编码【"+nodeCode+"】已存在!");
        }
        DynaBean parentNode=null;
        if(StringUtil.isNotEmpty(parentId)) {
            parentNode=metaService.selectOneByPk("JE_META_BUSFLOW_NODE", parentId);
        }else{
            parentNode=metaService.selectOneByPk("JE_META_BUSFLOW_NODE", "ROOT");
        }
        long index=metaService.selectCount("JE_META_BUSFLOW_NODE"," AND JE_META_BUSFLOW_ID='"+mainId+"' AND SY_PARENT='"+parentNode.get("JE_META_BUSFLOW_NODE_ID")+"'");
        index++;
        DynaBean node=new DynaBean("JE_META_BUSFLOW_NODE",true);
        node.set("JE_META_BUSFLOW_ID",mainId);
        node.set("NODE_NAME",nodeName);
        node.set("NODE_CODE",nodeCode);
        //拼接树形的属性信息
        node.set("JE_META_BUSFLOW_NODE_ID", JEUUID.uuid());
        node.set("SY_PARENT",parentNode.get("JE_META_BUSFLOW_NODE_ID"));
        node.set("SY_NODETYPE","LEAF");
        node.set("SY_LAYER",parentNode.getInt("SY_LAYER") + 1);
        node.set("SY_PATH",parentNode.get("SY_PATH")+"/"+node.get("JE_META_BUSFLOW_NODE_ID"));
        node.set("SY_DISABLED","0");
        node.set("SY_TREEORDERINDEX",parentNode.getStr("SY_TREEORDERINDEX") + String.format("%06d", index));
        commonService.buildFuncDefaultValues("JE_META_BUSFLOW_NODE",node);
        commonService.buildModelCreateInfo(node);
        node=metaService.insertAndReturn(node);
        jeBusFlowCache.clear();
        returnObj.put("success",true);
        returnObj.put("obj","操作成功");
        return node;
    }
}
