/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.func.impl;

import com.je.common.base.DynaBean;
import com.je.common.base.service.CommonService;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.meta.service.func.AdvancedQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdvancedQueryServiceImpl implements AdvancedQueryService {

    @Autowired
    protected MetaService metaService;
    @Autowired
    protected CommonService commonService;
    @Override
    public void doUpdateList(DynaBean dynaBean) {

        DynaBean funcinfo =new DynaBean("JE_CORE_FUNCINFO",false);
        funcinfo.setStr("JE_CORE_FUNCINFO_ID",dynaBean.getStr("funcId"));
        funcinfo.setStr("FUNCINFO_LAYOUT_TYPE",dynaBean.getStr("FUNCINFO_LAYOUT_TYPE"));
        funcinfo.setStr("FUNCINFO_ADD_QUERY_STRATEGY",dynaBean.getStr("FUNCINFO_ADD_QUERY_STRATEGY"));
        funcinfo.setStr("FUNCINFO_LAYOUT_COLUMN_COUNT",dynaBean.getStr("FUNCINFO_LAYOUT_COLUMN_COUNT"));
        funcinfo.setStr("FUNCINFO_GROUPQUERY_LABEL_WIDTH",dynaBean.getStr("FUNCINFO_GROUPQUERY_LABEL_WIDTH"));
        funcinfo.setStr("FUNCINFO_DISABLE_CHANGE",dynaBean.getStr("FUNCINFO_DISABLE_CHANGE"));
        funcinfo.setStr("FUNCINFO_GROUPQUERY_ENABEL_QUERY",dynaBean.getStr("FUNCINFO_GROUPQUERY_ENABEL_QUERY"));
        funcinfo.setStr("FUNCINFO_GROUPQUERY_START_EXPORT",dynaBean.getStr("FUNCINFO_GROUPQUERY_START_EXPORT"));
        funcinfo.setStr("FUNCINFO_GROUPQUERY_BJS",dynaBean.getStr("FUNCINFO_GROUPQUERY_BJS"));
        metaService.update(funcinfo);

        String strData = dynaBean.getStr("strData");
        commonService.doUpdateList("JE_CORE_GROUPQUERY",strData,null,null,null);
    }

    @Override
    public Map<String, Object> getData(DynaBean dynaBean) {
        Map<String,Object> map = new HashMap<>();
        String funcId = dynaBean.getStr("funcId");
        DynaBean funcinfo = metaService.selectOneByPk("JE_CORE_FUNCINFO",funcId);
        map.put("FUNCINFO_LAYOUT_TYPE",funcinfo.getStr("FUNCINFO_LAYOUT_TYPE"));
        map.put("FUNCINFO_ADD_QUERY_STRATEGY",funcinfo.getStr("FUNCINFO_ADD_QUERY_STRATEGY"));
        map.put("FUNCINFO_LAYOUT_COLUMN_COUNT",funcinfo.getStr("FUNCINFO_LAYOUT_COLUMN_COUNT"));
        map.put("FUNCINFO_GROUPQUERY_LABEL_WIDTH",funcinfo.getStr("FUNCINFO_GROUPQUERY_LABEL_WIDTH"));
        map.put("FUNCINFO_DISABLE_CHANGE",funcinfo.getStr("FUNCINFO_DISABLE_CHANGE"));
        map.put("FUNCINFO_GROUPQUERY_ENABEL_QUERY",funcinfo.getStr("FUNCINFO_GROUPQUERY_ENABEL_QUERY"));
        map.put("FUNCINFO_GROUPQUERY_START_EXPORT",funcinfo.getStr("FUNCINFO_GROUPQUERY_START_EXPORT"));
        map.put("FUNCINFO_GROUPQUERY_BJS",funcinfo.getStr("FUNCINFO_GROUPQUERY_BJS"));
        //高级查询数据
        String queryStr = dynaBean.getStr("queryStr");
        String whereSql ="";
        if(StringUtil.isNotEmpty(queryStr)){
            whereSql = " AND (GROUPQUERY_MC like '%"+queryStr+"%'" +" OR GROUPQUERY_BM like '%"+queryStr+"%')";
        }
        String sort =" ORDER BY SY_ORDERINDEX ASC";
       List<Map<String,Object>> list = metaService.selectSql("SELECT * FROM JE_CORE_GROUPQUERY WHERE GROUPQUERY_GNID={0} "+whereSql+""+" "+sort,funcId);
       for(Map<String,Object> mapData:list){
           if(mapData.containsKey("$TABLE_CODE$")){
               mapData.remove("$TABLE_CODE$");
           }
       }
        map.put("rows",list);
        return map;
    }
}
