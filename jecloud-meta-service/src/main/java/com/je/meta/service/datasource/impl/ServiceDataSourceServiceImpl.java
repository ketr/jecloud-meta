/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.datasource.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.ServiceDataSourceVo;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.MessageUtils;
import com.je.meta.rpc.dataSource.ServiceDataSourceRpcService;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

public class ServiceDataSourceServiceImpl implements ServiceDataSourceRpcService {
    @Override
    public List<DynaBean> queryData(ServiceDataSourceVo serviceDataSourceVo) {
        DynaBean dynaBean =serviceDataSourceVo.getDataSourceBean();
        JSONObject configJson = JSON.parseObject(dynaBean.getStr("DATASOURCE_CONFIG"));
        String serviceName = configJson.getString("apiUrl");
        String methodName = configJson.getString("methodName");
        Object object = SpringContextHolder.getBean(serviceName);
        try {
            Method method = object.getClass().getMethod(methodName);
            return (List<DynaBean>) method.invoke(object,serviceDataSourceVo);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String check(ServiceDataSourceVo serviceDataSourceVo) {
        DynaBean dynaBean =serviceDataSourceVo.getDataSourceBean();
        JSONObject configJson = JSON.parseObject(dynaBean.getStr("DATASOURCE_CONFIG"));
        String serviceName = configJson.getString("apiUrl");
        String methodName = configJson.getString("methodName");
        Object object=null;
        try{
            object = SpringContextHolder.getBean(serviceName);
        }catch (NoSuchBeanDefinitionException e){
            return MessageUtils.getMessage("dataSource.serviceName.notFound",serviceName);
        }
        Method[] arr = object.getClass().getMethods();
        boolean isExist = false;
        for(Method method:arr){
            String name = method.getName();
            //如果能匹配说明存在此方法，直接返回null
            if(methodName.equals(name)){
                isExist=true;
                break;
            }
        }
        if(!isExist){
            return MessageUtils.getMessage("dataSource.method.notFound",methodName);
        }
        return null;
    }
}
