/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.datasource.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.entity.ServiceDataSourceVo;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.dataSource.ServiceDataSourceRpcService;
import com.je.meta.service.datasource.DataSourceExecuteService;
import com.je.servicecomb.RpcSchemaFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.je.servicecomb.JECloud.PRODUCT_CORE_META;

@Service("dataSourceServiceExecuteService")
public class DataSourceServiceExecuteServiceImpl implements DataSourceExecuteService {
    @Override
    public List<Map<String, Object>> execute(DynaBean datasource, String parameterStr, String limit) {
        String productCode = datasource.getStr("SY_PRODUCT_CODE");
        ServiceDataSourceVo serviceDataSourceVo = new ServiceDataSourceVo();
        serviceDataSourceVo.setParameterStr(parameterStr);
        serviceDataSourceVo.setLimit(StringUtil.isNotEmpty(limit)?Integer.parseInt(limit):0);
        serviceDataSourceVo.setDataSourceBean(datasource);
        if(StringUtil.isNotEmpty(parameterStr)){
            JSONObject jsonObject = JSONObject.parseObject(parameterStr);
            serviceDataSourceVo.setParameterMap(jsonObject);
        }
        List<DynaBean> list = null;
        if(!PRODUCT_CORE_META.equals(productCode)){
            ServiceDataSourceRpcService dataSourceActionService   = RpcSchemaFactory.getRemoteProvierClazz(productCode,
                    "serviceDataSourceRpcService", ServiceDataSourceRpcService.class);
            String result = dataSourceActionService.check(serviceDataSourceVo);
            if (StringUtil.isNotEmpty(result)) {
                throw new PlatformException(result, PlatformExceptionEnum.UNKOWN_ERROR);
            }
            list =dataSourceActionService.queryData(serviceDataSourceVo);
        }else{
            ServiceDataSourceServiceImpl serviceDataSourceServiceImpl = new ServiceDataSourceServiceImpl();
            String result =serviceDataSourceServiceImpl.check(serviceDataSourceVo);
            if (StringUtil.isNotEmpty(result)) {
                throw new PlatformException(result, PlatformExceptionEnum.UNKOWN_ERROR);
            }
            list =serviceDataSourceServiceImpl.queryData(serviceDataSourceVo);
        }
        List<Map<String,Object>> listData = new ArrayList<>();
        if(list!=null){
            for(DynaBean dynaBean:list){
                listData.add(dynaBean.getValues());
            }
        }
        return listData;
    }

    @Override
    public List<DynaBean> executeForRpc(DynaBean dataSource, Map<String, Object> map, int limit) {
        String productCode = dataSource.getStr("SY_PRODUCT_CODE");
        ServiceDataSourceVo serviceDataSourceVo = new ServiceDataSourceVo();
        serviceDataSourceVo.setLimit(limit);
        serviceDataSourceVo.setDataSourceBean(dataSource);
        serviceDataSourceVo.setParameterMap(map);
        List<DynaBean> list = null;
        if(!PRODUCT_CORE_META.equals(productCode)){
            ServiceDataSourceRpcService dataSourceActionService   = RpcSchemaFactory.getRemoteProvierClazz(productCode,
                    "serviceDataSourceRpcService", ServiceDataSourceRpcService.class);
            list =dataSourceActionService.queryData(serviceDataSourceVo);
        }else{
            ServiceDataSourceServiceImpl serviceDataSourceServiceImpl = new ServiceDataSourceServiceImpl();
            list =serviceDataSourceServiceImpl.queryData(serviceDataSourceVo);
        }
        return list;
    }
}
