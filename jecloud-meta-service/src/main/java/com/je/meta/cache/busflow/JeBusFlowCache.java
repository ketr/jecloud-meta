package com.je.meta.cache.busflow;

import com.je.common.base.DynaBean;
import com.je.common.base.cache.AbstractHashCache;
import com.je.meta.cache.dd.DicCache;
import org.springframework.stereotype.Service;

@Service(JeBusFlowCache.CACHE_KEY)
public class JeBusFlowCache extends AbstractHashCache<DynaBean> {
    public static final String CACHE_KEY = "jeBusFlowCache";

    @Override
    public String getCacheKey() {
        return CACHE_KEY;
    }

    @Override
    public String getName() {
        return " 业务流缓存";
    }

    @Override
    public String getDesc() {
        return "缓存业务流结构，包括业务流节点！";
    }

    @Override
    public boolean enableFristLevel(){
        return false;
    }
}
