/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.licence;

import cn.hutool.core.util.HexUtil;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class MxContents {

    public static final String CT = "{'a':'您的证书已经过期！','b':'您的产品数量已超出授权限制！','c':'您的微服务实例数量已超出授权限制！','d':'您的用户数已超出授权限制！','e':'解密异常！','f':'您的证书不存在或无效，请联系官方！','g':'license/je.license','h':'user.home','i':'http://suanbanyun.com/jecloud/control/valid','j':'rbac','k':'请在架构管理中创建方案产品！','l':'servicecomb.service.name','m':'meta','n':'plugins.lock','o':'license','p':'.tmp','q':'server.port','r':'connector'}";
    /**
     * 角色表
     */
    public static final String R_T = "JE_RBAC_ROLE";
    /**
     * 角色表主键
     */
    public static final String R_T_PK = "JE_RBAC_ROLE_ID";
    /**
     * 默认平台开发者角色
     */
    public static final String R_T_P = "platformRoleId";
    /**
     * 系统设置表
     */
//    public static final String S_T = "je_core_setting";
    /**
     * 角色表主键
     */
//    public static final String S_T_C = "code";
    /**
     * 默认平台开发者角色
     */
//    public static final String S_T_V = "PLATFORM_DEVELOP";

    /**
     * 提示秘钥
     */
    public static String l = "6c34574e4554495a656a3850514e5a4158444f364a413d3d";
    /**
     * 偏移量
     */
    public static String m = "57384f6b61336c75513532746e4c55356b6c545042773d3d";

    private static final String CA = "Z2pmM5hA18EzGvxZojac5VBnEUbNSQtxMqjz26HwTssiYlcmTRCRVYW7+MT7InmdN3v8APUCo1hwuIX9TifKKzy0JDE30Qusw2sGoXvNTMXGO1SH7YDVDfRzifZPw47on8b/ZkS1Sgk4gIa1WOwUfu7ZI6kbSuvhuOwhfl3iPI8dy0k0YdnDEu1sk1Whf4VGQdj4fVLOfRToHgWG8lyPxQ9yk681TzFZtPKStXjdND5Q38+KaYFW+W6Pef1LBxNCdOXVPSX9eHnPpKSw3o5q6xPLCRkv3aZsDmByki/JPtqndWKTybhYq5aTXXLR7w25YSYTHAQmFAGUAfP6fFQDkg7k7QvQ5E+6S9BUgHkcfbMjZ0fDJvuGfWw04nbqn2rRNbr3H5JTpu2WwnqWfixrb+pO11gJ4Ra156bmUglR6UcQ+S7JNhVmC8cHkzxcH+HUy1blXiDc8/o0tCynj6KrJFGIhGc3LyU04/2RtXuP38vqK8EtrBs/Qf4xf21w6ysT+TDEKJxKln+lXYsn6YkRuAQ/K8nRipOImMSlcQNdNraiVkfI1JDyJhiXHbuVEl8ssSqQxqniM0u52cl11caSRr1Oi/9lVxGZj7DlZGiqo4QmET7oMXT3/e+U72hCfsnAHuZQhKNZFVpPu3b4qgaLy4Uo4mB5ma0/Jv4BRl9ZzOlIG1DJhrN2599ClQvF+LSyiNzEj9YstY8EaHjfeclCZQ==";

    private static final String CE = "e8a7a3e5af86e5bc82e5b8b8efbc81";

//    private static final String R_T = "4a455f524241435f524f4c45";

//    private static final String R_T_PK = "4a455f524241435f524f4c455f4944";

    private static final String R_T_R_PK = "53595f50524f445543545f4944";

    private static final String RE_R_K = "706c6174666f726d50726f647563744361636865";

    private static final String R_T_Y = "50524f445543545f54595045";

    private static final String P_PK = "4a455f50524f445543545f4d414e4147455f4944";

    //    private static final String R_T_P = "706c6174666f726d526f6c654964";
    public static final String S_T = "6a655f636f72655f73657474696e67";
    public static final String S_T_C = "636f6465";
    public static final String S_T_V = "504c4154464f524d5f444556454c4f50";
    private static final String D_R_P = "64656c6574652066726f6d204a455f524241435f4143434f554e54524f4c45205748455245204143434f554e54524f4c455f524f4c455f4944206e6f7420696e2873656c656374204a455f524241435f524f4c455f49442066726f6d206a655f726261635f726f6c6529";

    public static void main(String[] args) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
//        String key = RsaOperor.genRandomAesSecretKey();
//        String iv = RsaOperor.genRandomIV();
//        System.out.println("key:" + HexUtil.encodeHexStr(key));
//        System.out.println("iv:" + HexUtil.encodeHexStr(iv));
//        String encrptStr = RsaOperor.encryptAES(key,iv,CT);
//        System.out.println(encrptStr);
//        String decryptAES = RsaOperor.decryptAES(key,iv,encrptStr);
//        System.out.println(decryptAES);
//        System.out.println(HexUtil.encodeHexStr("jecloud license"));
//        System.out.println(HexUtil.encodeHexStr(S_T));
//        System.out.println(HexUtil.encodeHexStr(S_T_C));
//        System.out.println(HexUtil.encodeHexStr(S_T_V));
        System.out.println(HexUtil.decodeHexStr(S_T, Charset.forName("UTF-8")));
        System.out.println(HexUtil.decodeHexStr(S_T_C, Charset.forName("UTF-8")));
        System.out.println(HexUtil.decodeHexStr(S_T_V, Charset.forName("UTF-8")));
//        System.out.println(HexUtil.decodeHexStr(HexUtil.encodeHexStr(R_T), Charset.forName("UTF-8")));
//        System.out.println(HexUtil.encodeHexStr(R_T_PK));
//        System.out.println(HexUtil.encodeHexStr(R_T_P));
//        System.out.println(HexUtil.encodeHexStr("解密异常！"));
//        System.out.println(HexUtil.decodeHexStr("", Charset.forName("UTF-8")));
//        String s = RsaDecoder.decryptAES(HexUtil.decodeHexStr(l), HexUtil.decodeHexStr(m), CA);
//        System.out.println("CA："+s);
//        System.out.println("CE："+HexUtil.decodeHexStr(CE, Charset.forName("UTF-8")));
//        System.out.println("R_T_R_PK："+HexUtil.decodeHexStr(R_T_R_PK, Charset.forName("UTF-8")));
//        System.out.println("RE_R_K："+HexUtil.decodeHexStr(RE_R_K, Charset.forName("UTF-8")));
//        System.out.println("R_T_Y："+HexUtil.decodeHexStr(R_T_Y, Charset.forName("UTF-8")));
//        System.out.println("P_PK："+HexUtil.decodeHexStr(P_PK, Charset.forName("UTF-8")));
//        System.out.println("D_R_P："+HexUtil.decodeHexStr(D_R_P, Charset.forName("UTF-8")));

    }

}
