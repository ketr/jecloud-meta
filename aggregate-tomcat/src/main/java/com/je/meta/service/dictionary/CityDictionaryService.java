package com.je.meta.service.dictionary;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.JEUUID;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CityDictionaryService {

    @Autowired
    private MetaService metaService;

    @Transactional
    public void importCityDictionary(JSONArray cityArray) {
        DynaBean dictionary = metaService.selectOne("JE_CORE_DICTIONARY", ConditionsWrapper.builder().eq("DICTIONARY_DDCODE", "JE_COMM_SSQX"));
        String dictionaryId = dictionary.getStr("JE_CORE_DICTIONARY_ID");
        DynaBean rootItem = metaService.selectOne("JE_CORE_DICTIONARYITEM", ConditionsWrapper.builder()
                .eq("SY_NODETYPE", "ROOT")
                .eq("DICTIONARYITEM_DICTIONARY_ID", dictionaryId));
        metaService.delete("JE_CORE_DICTIONARYITEM", ConditionsWrapper.builder()
                .ne("JE_CORE_DICTIONARYITEM_ID", rootItem.getStr("JE_CORE_DICTIONARYITEM_ID"))
                .eq("DICTIONARYITEM_DICTIONARY_ID", dictionaryId));
        List<DynaBean> list = new ArrayList<>();
        for (int i = 0; i < cityArray.size(); i++) {
            recursiveBuildDynaBean(dictionaryId, rootItem, cityArray.getJSONObject(i), i, list);
        }
        for (DynaBean eachBean : list) {
            metaService.insert(eachBean);
        }
    }


    private void recursiveBuildDynaBean(String dictionaryId, DynaBean parentBean, JSONObject cityObject, int index, List<DynaBean> list) {
        DynaBean city = new DynaBean("JE_CORE_DICTIONARYITEM", true);
        city.setStr("JE_CORE_DICTIONARYITEM_ID", JEUUID.uuid());
        city.setStr("SY_PARENT", parentBean.getStr("JE_CORE_DICTIONARYITEM_ID"));
        city.setStr("DICTIONARYITEM_DICTIONARY_ID", dictionaryId);
        city.setStr("DICTIONARYITEM_ITEMCODE", cityObject.getString("code"));
        city.setStr("SY_ORDERINDEX", cityObject.getString("code"));
        city.setStr("DICTIONARYITEM_ITEMNAME", cityObject.getString("value"));
        city.set("SY_LAYER", parentBean.getInt("SY_LAYER") + 1);
        city.set("SY_PATH", parentBean.getStr("SY_PATH") + "/" + city.getStr("JE_CORE_DICTIONARYITEM_ID"));
        city.set("SY_PARENTPATH", parentBean.getStr("SY_PATH"));
        city.set("SY_TREEORDERINDEX", parentBean.getStr("SY_TREEORDERINDEX") + String.format("%06d", index));
        city.set("DICTIONARY_CLASSIFY", parentBean.getInt("DICTIONARY_CLASSIFY") + 1);
        if (cityObject.containsKey("children") && cityObject.getJSONArray("children").size() > 0) {
            city.setStr("SY_NODETYPE", "GENERAL");
        } else {
            city.setStr("SY_NODETYPE", "LEAF");
        }
        city.setStr("DICTIONARYITEM_NAME", cityObject.getString("name"));
        city.setInt("DICTIONARYITEM_ORDER", 0);
        city.setStr("SY_FLAG", "1");
        city.setStr("SY_CREATETIME", DateUtil.formatDateTime(new Date()));
        list.add(city);

        if (!cityObject.containsKey("children") || cityObject.getJSONArray("children").size() <= 0) {
            return;
        }

        JSONArray children = cityObject.getJSONArray("children");
        for (int i = 0; i < children.size(); i++) {
            recursiveBuildDynaBean(dictionaryId, city, children.getJSONObject(i), i, list);
        }
    }


}
