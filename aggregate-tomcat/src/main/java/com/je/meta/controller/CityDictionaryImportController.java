package com.je.meta.controller;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson2.JSONArray;
import com.google.common.base.Strings;
import com.je.common.base.result.BaseRespResult;
import com.je.meta.service.dictionary.CityDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.io.File;

@RestController
@RequestMapping("/je/meta/dictionary/test")
public class CityDictionaryImportController {

    @Lazy
    @Autowired
    private CityDictionaryService cityDictionaryService;
    @Lazy
    @Autowired
    private Environment environment;

    @RequestMapping(value = "/city", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult city(HttpServletRequest request) {
        String path = request.getParameter("path");
        if (Strings.isNullOrEmpty(path)) {
            path = environment.getProperty("servicecomb.uploads.directory");
        }
        path = path + File.separator + "city.json";
        String content = FileUtil.readString(path, "utf-8");
        JSONArray contentArray = JSONArray.parseArray(content);
        cityDictionaryService.importCityDictionary(contentArray);
        return BaseRespResult.successResult("导入成功");
    }

}
