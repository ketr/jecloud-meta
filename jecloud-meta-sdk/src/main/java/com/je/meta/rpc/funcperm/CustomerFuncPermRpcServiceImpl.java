package com.je.meta.rpc.funcperm;

import com.je.common.base.func.funcPerm.fastAuth.CustomerFuncPerm;
import com.je.common.base.func.funcPerm.fastAuth.FuncQuickPermission;
import com.je.common.base.func.funcPerm.roleSqlAuth.RoleSqlAuthVo;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

@Service
public class CustomerFuncPermRpcServiceImpl implements CustomerFuncPermRpcService {

    @RpcReference(microserviceName = "meta", schemaId = "customerFuncPermRpcService")
    private CustomerFuncPermRpcService customerFuncPermRpcService;

    @Override
    public FuncQuickPermission getCustomerFuncQuickPermission(String code) {
        return customerFuncPermRpcService.getCustomerFuncQuickPermission(code);
    }

    @Override
    public RoleSqlAuthVo getCustomerFuncRoleSqlPermission(String code) {
        return customerFuncPermRpcService.getCustomerFuncRoleSqlPermission(code);
    }

    @Override
    public String getCustomerFuncSqlPermission(String code) {
        return customerFuncPermRpcService.getCustomerFuncSqlPermission(code);
    }

    @Override
    public CustomerFuncPerm getCustomerFuncAllPermByCode(String code) {
        return customerFuncPermRpcService.getCustomerFuncAllPermByCode(code);
    }
}
