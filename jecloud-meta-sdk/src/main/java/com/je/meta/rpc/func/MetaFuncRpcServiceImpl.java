/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.func;

import com.je.common.base.DynaBean;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MetaFuncRpcServiceImpl implements MetaFuncRpcService {

    @RpcReference(microserviceName = "meta", schemaId = "metaFuncRpcService")
    private MetaFuncRpcService metaFuncRpcService;

    @Override
    public List<DynaBean> findFuncAnsButtonBaseInformationByCode(List<String> funcCodeList) {
        return metaFuncRpcService.findFuncAnsButtonBaseInformationByCode(funcCodeList);
    }

    @Override
    public Map<String, List<DynaBean>> findPermedFuncsAndChildFuncsWithButtonsByFuncCodes(List<String> funcCodeList) {
        return metaFuncRpcService.findPermedFuncsAndChildFuncsWithButtonsByFuncCodes(funcCodeList);
    }

    @Override
    public Map<String, List<DynaBean>> findMicroAppButtonsByMicroAppCodes(List<String> microAppCodeList) {
        return metaFuncRpcService.findMicroAppButtonsByMicroAppCodes(microAppCodeList);
    }

    @Override
    public Map<String, DynaBean> findMicroAppByMicroAppCodes(List<String> microAppCodeList) {
        return metaFuncRpcService.findMicroAppByMicroAppCodes(microAppCodeList);
    }

    @Override
    public Map<String, List<DynaBean>> findPermedFuncsAndChildFuncsWithButtonsByFuncIds(List<String> funcIdList) {
        return metaFuncRpcService.findPermedFuncsAndChildFuncsWithButtonsByFuncIds(funcIdList);
    }

    @Override
    public Map<String, DynaBean> findSubFuncParentFuncInfos(List<String> subFuncIdList) {
        return metaFuncRpcService.findSubFuncParentFuncInfos(subFuncIdList);
    }

    @Override
    public Map<String, String> findFuncProductMap(List<String> funcCodeList) {
        return metaFuncRpcService.findFuncProductMap(funcCodeList);
    }

    @Override
    public void disableFormField(String funcCode, String columnCode) {
        metaFuncRpcService.disableFormField(funcCode,columnCode);
    }

    @Override
    public void enableFormField(String funcCode, String columnCode) {
        metaFuncRpcService.enableFormField(funcCode,columnCode);
    }

}
