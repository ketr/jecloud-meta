package com.je.meta.rpc.busflow;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.DynaBean;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.apache.servicecomb.provider.pojo.RpcSchema;

import java.util.List;

@RpcSchema(schemaId = "jeBusFlowRpcService")
public class JeBusFlowRpcServiceImpl implements JeBusFlowRpcService{
    @RpcReference(microserviceName = "meta",schemaId = "jeBusFlowRpcService")
    private JeBusFlowRpcService jeBusFlowRpcService;

    @Override
    public DynaBean getBusFlow(String busFlowCode, String refresh) {
        return jeBusFlowRpcService.getBusFlow(busFlowCode,refresh);
    }

    @Override
    public List<DynaBean> loadFlowNodes(String busFlowCode, String mainId) {
        return jeBusFlowRpcService.loadFlowNodes(busFlowCode,mainId);
    }

    @Override
    public void doInitFlowNodes(String busFlowCode, String mainId) {
        jeBusFlowRpcService.doInitFlowNodes(busFlowCode,mainId);
    }

    @Override
    public void doRenewFlowNodes(String busFlowCode, String mainId) {
        jeBusFlowRpcService.doRenewFlowNodes(busFlowCode,mainId);
    }

    @Override
    public void doRenewNode(String busFlowCode, String mainId, String nodeCode) {
        jeBusFlowRpcService.doRenewNode(busFlowCode,mainId,nodeCode);
    }

    @Override
    public void doRenewNodeInfo(String busFlowCode, String mainId, String nodeCode, JSONObject infoObj) {
        jeBusFlowRpcService.doRenewNodeInfo(busFlowCode,mainId,nodeCode,infoObj);
    }

    @Override
    public void doClearCache(String busFlowCode) {
        jeBusFlowRpcService.doClearCache(busFlowCode);
    }
}
