/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc;

import com.je.common.base.DynaBean;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.service.MetaResourceService;
import com.je.ibatis.extension.plugins.pagination.Page;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class MetaResourceRpcServiceImpl implements MetaResourceService {

    @RpcReference(microserviceName = "meta",schemaId = "metaResourceService")
    private MetaResourceService metaResourceService;

    @Override
    public void clearMyBatisCache() {
        metaResourceService.clearMyBatisCache();
    }

    @Override
    public void clearMyBatisTableCache(String tableCode) {
        metaResourceService.clearMyBatisTableCache(tableCode);
    }

    @Override
    public void clearAllMyBatisFuncCache() {
        metaResourceService.clearAllMyBatisFuncCache();
    }

    @Override
    public void clearMyBatisFuncCache(String funcCode) {
        metaResourceService.clearMyBatisFuncCache(funcCode);
    }

    @Override
    public DynaBean insertByTableCode(String tableCode, DynaBean beanMap) {
        return metaResourceService.insertByTableCode(tableCode,beanMap);
    }

    @Override
    public List<DynaBean> insertBatchByTableCode(String tableCode, List<DynaBean> list) {
        return metaResourceService.insertBatchByTableCode(tableCode,list);
    }

    @Override
    public DynaBean update(String tableCode, String pkValue, DynaBean beanMap, NativeQuery nativeQuery) {
        return metaResourceService.update(tableCode,pkValue,beanMap,nativeQuery);
    }

    @Override
    public int deleteByTableCodeAndNativeQuery(String tableCode, NativeQuery nativeQuery) {
        return metaResourceService.deleteByTableCodeAndNativeQuery(tableCode,nativeQuery);
    }

    @Override
    public List<Map<String, Object>> selectMapByPageAndNativeQuery(Page page, NativeQuery nativeQuery) {
        return metaResourceService.selectMapByPageAndNativeQuery(page,nativeQuery);
    }

    @Override
    public List<DynaBean> selectPageWithColumns(String tableCode, Page page, NativeQuery nativeQuery, String columns) {
        return metaResourceService.selectPageWithColumns(tableCode,page,nativeQuery,columns);
    }

    @Override
    public DynaBean selectOne(String tableCode, NativeQuery nativeQuery, String columns) {
        return metaResourceService.selectOne(tableCode,nativeQuery,columns);
    }

    @Override
    public List<Map<String, Object>> loadByNativeQuery(String funcCode, Page page, NativeQuery nativeQuery) {
        return metaResourceService.loadByNativeQuery(funcCode,page,nativeQuery);
    }

    @Override
    public DynaBean selectOneByPkWithColumns(String tableCode, String pkValue, String columns) {
        return metaResourceService.selectOneByPkWithColumns(tableCode,pkValue,columns);
    }

    @Override
    public int executeSqlByNativeQuery(NativeQuery nativeQuery) {
        return metaResourceService.executeSqlByNativeQuery(nativeQuery);
    }

    @Override
    public long countByNativeQuery(NativeQuery nativeQuery) {
        return metaResourceService.countByNativeQuery(nativeQuery);
    }

    @Override
    public Boolean selectDataExistsByPkValue(String tableCode, String pkValue) {
        return metaResourceService.selectDataExistsByPkValue(tableCode,pkValue);
    }

}
