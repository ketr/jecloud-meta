/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc.table;

import com.google.common.base.Strings;
import com.je.common.base.DynaBean;
import com.je.common.base.constants.table.TableType;
import com.je.common.base.exception.APIWarnException;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.MetaService;
import com.je.common.base.table.BuildingSqlFactory;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: jecloud-meta
 * @author: LIULJ
 * @create: 2021-09-18 13:25
 * @description:
 */
@Service
public class MetaTableKeyRpcServiceImpl implements MetaTableKeyRpcService {

    @RpcReference(microserviceName = "meta", schemaId = "metaTableKeyRpcService")
    private MetaTableKeyRpcService metaTableKeyRpcService;
    @Autowired
    private MetaTableRpcService metaTableRpcService;
    @Autowired
    private MetaService metaService;

    @Override
    public void initKeys(DynaBean resourceTable, Boolean isTree) {
        metaTableKeyRpcService.initKeys(resourceTable, isTree);
    }

    @Override
    public String checkKeys(List<DynaBean> keys) {
        return metaTableKeyRpcService.checkKeys(keys);
    }

    @Override
    public Integer removeKey(DynaBean dynaBean, String ids, String ddl) {
        return metaTableKeyRpcService.removeKey(dynaBean, ids, ddl);
    }

    @Override
    public void deleteKey(String tableCode, List<DynaBean> keys, String ddl) {
        metaTableKeyRpcService.deleteKey(tableCode, keys, ddl);
    }

    @Override
    public void deleteKeyMeta(String tableCode, List<DynaBean> keys) {
        metaTableKeyRpcService.deleteKeyMeta(tableCode, keys);
    }

    @Override
    public String requireDeletePhysicalKeyDDL(String tableCode, List<DynaBean> keys) {
        return metaTableKeyRpcService.requireDeletePhysicalKeyDDL(tableCode, keys);
    }

    @Override
    public int doUpdateList(DynaBean dynaBean, String strData) throws APIWarnException {
        return metaTableKeyRpcService.doUpdateList(dynaBean, strData);
    }

    @Override
    public String generateDeleteKeyDDL(DynaBean dynaBean, List<DynaBean> keys, String isDeleteDdl) {
        return metaTableKeyRpcService.generateDeleteKeyDDL(dynaBean, keys, isDeleteDdl);
    }

    @Override
    public List<DynaBean> selectTableKeyByKeyId(String ids) {
        return metaTableKeyRpcService.selectTableKeyByKeyId(ids);
    }

    @Override
    public List<DynaBean> selectTableIndexByIndexId(String ids) {
        return metaTableKeyRpcService.selectTableIndexByIndexId(ids);
    }

    @Override
    public boolean doRemoveKey(DynaBean dynaBean, List<DynaBean> keys, String isDeleteDdl, String tableCode) {
        DynaBean table = metaTableRpcService.selectOneByCodeToType(tableCode);
        //删除表字段
        String delSql = BuildingSqlFactory.getDdlService().getDeleteKeySql(tableCode, keys);
        if (StringUtil.isNotEmpty(delSql) && table != null && !TableType.VIEWTABLE.equals(table.getStr("RESOURCETABLE_TYPE"))) {
            delSql = delSql.replaceAll("\n", "");
            for(String sql:delSql.split(";")){
                metaService.executeSql(sql);
            }
            return true;
        }
        return false;
    }

    /*@Override
    public String doRemoveIndex(String ids) {
        String ddlSql = metaTableKeyRpcService.doRemoveIndex(ids);
        int i = 0;
        if (!Strings.isNullOrEmpty(ddlSql)) {
            for (String sql : ddlSql.split(";")) {
                if (!Strings.isNullOrEmpty(sql)) {
                    try {
                        sql = sql.replaceAll("\n", "");
                        i = i + metaService.executeSql(sql);
                    } catch (UncategorizedSQLException e) {
                        throw new PlatformException(MessageUtils.getMessage("index.delete.index.error") + e.getCause().getMessage(),
                                PlatformExceptionEnum.JE_CORE_TABLEINDEX_DELETE_ERROR, new Object[]{sql}, e);
                    } catch (Exception e) {
                        throw new PlatformException(MessageUtils.getMessage("index.delete.wrong") + "," + e.getCause().getMessage(),
                                PlatformExceptionEnum.JE_CORE_TABLEINDEX_DELETE_ERROR, new Object[]{sql}, e);
                    }
                }
            }
        }
        return String.valueOf(i);
    }*/

}
