/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.rpc;

import com.je.common.base.DynaBean;
import com.je.meta.rpc.table.ResourceTableService;
import org.apache.servicecomb.provider.pojo.RpcReference;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MetaResourceTableServiceImpl implements ResourceTableService {

    @RpcReference(microserviceName = "meta", schemaId = "metaResourceTableService")
    private ResourceTableService resourceTableService;

    @Override
    public List<DynaBean> findAllTables() {
        return resourceTableService.findAllTables();
    }

    @Override
    public DynaBean findTable(String tableCode) {
        return resourceTableService.findTable(tableCode);
    }

    @Override
    public List<DynaBean> findTables(List<String> tableCodes) {
        return resourceTableService.findTables(tableCodes);
    }

    @Override
    public DynaBean findTableById(String resourceTableId) {
        return resourceTableService.findTableById(resourceTableId);
    }

    @Override
    public List<DynaBean> findTableColumns(String resourceTableId) {
        return resourceTableService.findTableColumns(resourceTableId);
    }

    @Override
    public List<DynaBean> findTableKeys(String resourceTableId) {
        return resourceTableService.findTableKeys(resourceTableId);
    }
}
