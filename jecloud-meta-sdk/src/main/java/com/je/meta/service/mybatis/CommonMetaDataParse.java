/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.mybatis;

import com.je.common.base.DynaBean;
import com.je.common.base.service.rpc.FunctionService;
import com.je.ibatis.extension.metadata.IdType;
import com.je.ibatis.extension.metadata.model.Column;
import com.je.ibatis.extension.metadata.model.Function;
import com.je.ibatis.extension.metadata.model.Id;
import com.je.ibatis.extension.metadata.model.Table;
import com.je.ibatis.extension.parse.MetaDataParse;
import com.je.ibatis.extension.toolkit.DateUtils;
import com.je.meta.rpc.table.ResourceTableService;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.*;

public class CommonMetaDataParse implements MetaDataParse {

    private static final String[] NUMBER_FIELD_TYPE = {"NUMBER", "FLOAT2", "FLOAT"};

    private FunctionService functionService;

    private ResourceTableService resourceTableService;

    public CommonMetaDataParse(FunctionService functionService, ResourceTableService resourceTableService) {
        this.functionService = functionService;
        this.resourceTableService = resourceTableService;
    }

    @Override
    public List<Table> tables() {
        List<DynaBean> tableBeans = resourceTableService.findAllTables();
        //组装Table实体
        ArrayList<Table> tables = new ArrayList<>();
        tableBeans.forEach(p -> tables.add(parseTable(p)));
        return tables;
    }

    @Override
    public List<Table> tables(List<String> list) {
        List<DynaBean> tableBeans = resourceTableService.findTables(list);
        //组装Table实体
        ArrayList<Table> tables = new ArrayList<>();
        tableBeans.forEach(p -> tables.add(parseTable(p)));
        return tables;
    }

    @Override
    public Table table(String s) {
        DynaBean tableBean = resourceTableService.findTable(s);
        return parseTable(tableBean);
    }

    @Override
    public List<Function> functions() {
        List<DynaBean> functionBeans = functionService.findAllFunctions();
        //组装功能元数据对象
        ArrayList<Function> functions = new ArrayList<>();
        functionBeans.forEach(p -> functions.add(parseFunction(p)));
        return functions;
    }

    @Override
    public Function function(String s) {
        DynaBean functionBean = functionService.findFunction(s);
        return parseFunction(functionBean);
    }

    /**
     * Map转换Table实体
     *
     * @param map 表信息Map
     * @return com.je.ibatis.extension.metadata.model.Table
     */
    protected Table parseTable(DynaBean map) {
        //获取值
        String tableId = map.getStr("JE_CORE_RESOURCETABLE_ID");
        String tableName = map.getStr("RESOURCETABLE_TABLENAME");
        String tableCode = map.getStr("RESOURCETABLE_TABLECODE");
        String keyIgnoreCase = map.getStr("RESOURCETABLE_KEY_GENERATOR_TYPE");
        String incrementerName = map.getStr("RESOURCETABLE_INCREMENTER_NAME");
        String generatorSql = map.getStr("RESOURCETABLE_KEY_GENERATOR_SQL");
        //校验参数
        Assert.notNull(tableId, "field[JE_CORE_RESOURCETABLE_ID] is null!");
        Assert.notNull(tableCode, String.format("id[%s] field[RESOURCETABLE_TABLECODE] is null!", tableId));

        //初始化Table对象
        Table table = new Table();
        table.setCode(tableCode);
        table.setName(tableName == null ? null : tableName);

        //查询列信息
        List<DynaBean> columnList = resourceTableService.findTableColumns(tableId);
        //添加列信息到Table实体
        columnList.forEach(c -> {
            Column column = parseColumn(c);
            if ("ID".equalsIgnoreCase(column.getType())) {
                table.setId(new Id(column.getCode(), IdType.getIdType(keyIgnoreCase), incrementerName, generatorSql));
            }
            table.addColumn(column);
        });

        //查找主键
        if (table.getId() == null) {
            List<DynaBean> keys = resourceTableService.findTableKeys(tableId);
            if (keys != null && !keys.isEmpty()) {
                if (keys.size() > 1) {
                    // TODO 提示当前包含多个主键字段
                }
                //获取主键字段名
                String pkCode = keys.get(0).getStr("TABLEKEY_COLUMNCODE");
                table.setId(new Id(pkCode.toString(), IdType.getIdType(keyIgnoreCase), incrementerName, generatorSql));
            }
        }
        return table;
    }

    /**
     * Map转换Column实体
     *
     * @param map 列信息Map
     * @return com.je.ibatis.extension.metadata.model.Column
     */
    protected Column parseColumn(DynaBean map) {

        //获取值
        String columnCode = map.getStr("TABLECOLUMN_CODE");
        String columnName = map.getStr("TABLECOLUMN_NAME");
        String columnType = map.getStr("TABLECOLUMN_TYPE");
        String columnLength = map.getStr("TABLECOLUMN_LENGTH");
        Object isNull = map.get("TABLECOLUMN_ISNULL");

        //系统中的日期 其实是字符串
        if ("DATETIME".equalsIgnoreCase(String.valueOf(columnType)) || "DATE".equalsIgnoreCase(String.valueOf(columnType))) {
            columnType = "VARCHAR";
        }

        //如果类型为自定义，表示非系统支持类型，真实类型存在 TABLECOLUMN_LENGTH 中
        if ("CUSTOM".equalsIgnoreCase(String.valueOf(columnType)) && !StringUtils.isEmpty(columnLength)) {
            columnType = columnLength;
        }

        //校验参数
        Assert.notNull(columnCode, "field[TABLECOLUMN_CODE] is null!");
        // TODO 完善列类型
        Boolean isNullBoolean = false;
        if (String.valueOf(isNull) != null && String.valueOf(isNull).equals("1")) {
            isNullBoolean = true;
        }
        return new Column(columnCode, String.valueOf(columnName), String.valueOf(columnType), isNullBoolean);
    }

    protected Function parseFunction(DynaBean map) {

        //获取值
        String id = map.getStr("JE_CORE_FUNCINFO_ID");
        String code = map.getStr("FUNCINFO_FUNCCODE");
        String name = map.getStr("FUNCINFO_FUNCNAME");
        String tableCode = map.getStr("FUNCINFO_TABLENAME");
        String lazy = map.getStr("FUNCINFO_COLUMNLAZY");
        //校验参数
        Assert.notNull(code, "field[JE_CORE_FUNCINFO_ID] is null!");
//        Assert.notNull(tableCode, String.format("id[%s] field[FUNCINFO_TABLENAME] is null!", tableCode));

        //初始化功能元数据对象
        List<String> loadColumnNames = new ArrayList<>();
        //查询列信息
        List<DynaBean> columnList = functionService.findResourceColumnByFunctionId(id);
        //如果启用懒加载 设置查询列
        if ("1".equalsIgnoreCase(lazy.toString())) {
            columnList.forEach(c -> {
                loadColumnNames.add(c.get("RESOURCECOLUMN_CODE").toString());
            });
        }

        if (tableCode == null) {
            return new Function(code.toString(), null, loadColumnNames);
        }
        return new Function(code.toString(), tableCode.toString(), loadColumnNames);
    }

    @Override
    public void setDataSource(DataSource dataSource) {
//        throw new PlatformException("此元数据解析器通过Rpc方式访问，不支持数据源方式获取资源！", PlatformExceptionEnum.UNKOWN_ERROR);
    }

    @Override
    public boolean formData(Table table, Map<String, Object> bean) {
        table.getColumnList().forEach(column -> {
            //判断类型
            String type = column.getType();
            //日期类型特殊处理
            if ("DATE".equalsIgnoreCase(type) || "TIMESTAMP".equalsIgnoreCase(type)) {
                Object value = bean.get(column.getCode());
                if (value instanceof String) {
                    Date date = DateUtils.parse(value);
                    if ("TIMESTAMP".equalsIgnoreCase(type)) {
                        bean.put(column.getCode(), new Timestamp(date.getTime()));
                    } else {
                        bean.put(column.getCode(), date);
                    }

                }
            }
            if (column.getNull() && null == bean.get(column.getCode())) {
                bean.put(column.getCode(), null);
            }

            if (bean.keySet().contains(column.getCode()) && Arrays.asList(NUMBER_FIELD_TYPE).contains(type) && (bean.get(column.getCode()) == null || bean.get(column.getCode()).equals(""))) {
                bean.put(column.getCode(), 0);
            }

        });
        return true;
    }
}
