/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.service.table.impl;

import com.google.common.base.Strings;
import com.je.common.base.service.MetaService;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.table.MetaTableRpcService;
import com.je.meta.service.table.MetaTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MetaTableServiceImpl implements MetaTableService {
    @Autowired
    private MetaTableRpcService metaTableRpcService;
    @Autowired
    private MetaService metaService;
    @Override
    public String doHardDeleteTable(String ids) {
        //执行删除表元数据操作
        String result = metaTableRpcService.checkBeforeDeletion(ids);
        if(StringUtil.isNotEmpty(result)){
            return result;
        }
        //执行删除表DDL
        String DDl = metaTableRpcService.getDeleteTableDDlByIds(ids);
        if (!Strings.isNullOrEmpty(DDl)) {
            metaService.executeSql(DDl);
        }
        //删除元数据
        metaTableRpcService.removeTableMetaToMetaNoDDL(ids);
        return null;
    }
}
