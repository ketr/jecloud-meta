/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.je.common.base.DynaBean;
import com.je.common.base.datasource.runner.DynaSqlRunner;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.MetaResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.List;

/**
 * 第三方数据源
 */
@RestController
@RequestMapping(value = "/je/dataSource")
public class DataSourceController extends AbstractPlatformController {

    @Autowired
    private MetaResourceService metaResourceService;

    /**
     * 调用第三方数据源
     *
     * @param request
     * @throws SQLException
     */
    @RequestMapping(value = "/doTest", method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult doTest(HttpServletRequest request) throws SQLException {
        String dataSourceName = request.getParameter("name");
        List<DynaBean> dataSources = metaResourceService.selectByTableCodeAndNativeQuery("JE_CORE_THIRDSOURCE", NativeQuery.build().eq("THIRDSOURCE_MC", dataSourceName));
        if(dataSources.size() == 0){
            return BaseRespResult.successResult("未找到第三方数据源!");
        }
        if(dataSources.size() > 1){
            return BaseRespResult.errorResult("第三方数据源名称不唯一!");
        }
        String sql = dataSources.get(0).getStr("THIRDSOURCE_CSSQL");
        DynaSqlRunner sqlRunner = DynaSqlRunner.getInstance(dataSourceName);
        List<DynaBean> resultList = sqlRunner.queryDynaBeanList(sql);
        return BaseRespResult.successResult(resultList);
    }

}
