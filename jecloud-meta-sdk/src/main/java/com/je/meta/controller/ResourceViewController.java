/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.table.MetaTableRpcService;
import com.je.meta.rpc.table.MetaViewRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

/**
 * 资源表-操作视图
 */
@RestController
@RequestMapping(value = "/je/meta/resourceTable/view")
public class ResourceViewController extends AbstractPlatformController {

    @Autowired
    private MetaTableRpcService metaTableRpcService;
    @Autowired
    private MetaViewRpcService metaViewRpcService;

    /**
     * 修改视图
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/updateView", method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult updateView(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String resourcetableTablename = getStringParameter(request, "RESOURCETABLE_TABLENAME");
        String resourcetableTablecode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        String resourcetableSql = getStringParameter(request, "RESOURCETABLE_SQL");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        String strData = param.getStrData();

        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablename)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.name.canNotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablecode)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.code.canNotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableSql)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.ddl.canNotEmpty"));
        }
        if (StringUtil.isEmpty(strData)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.cascade.canNotEmpty"));
        }
        dynaBean = metaViewRpcService.updateTableByView(strData, dynaBean);
        return BaseRespResult.successResult(dynaBean,MessageUtils.getMessage("common.update.success"));
    }

    /**
     * 同步视图
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/syncView", method = RequestMethod.POST)
    @ResponseBody
    public BaseRespResult syncView(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        DynaBean table = metaService.selectOneByPk("JE_CORE_RESOURCETABLE", dynaBean.getStr("JE_CORE_RESOURCETABLE_ID"));
        table = metaViewRpcService.updateViewInfo(table, "[]", true);
        //清空mybatis缓存
        metaService.clearMyBatisCache();
        return BaseRespResult.successResult(table);
    }

    /**
     * 生成DDL操作——点击确定按钮,创建视图
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/saveTableByView", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult saveTableByView(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = new DynaBean();
        String resourcetableTablename = getStringParameter(request, "RESOURCETABLE_TABLENAME");
        String resourcetableTablecode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        String resourcetableSql = getStringParameter(request, "RESOURCETABLE_SQL");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        String syParent = getStringParameter(request, "SY_PARENT");
        String strData = param.getStrData();

        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablename)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.name.canotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablecode)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.code.canotEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableSql)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.ddl.canNotEmpty "));
        }
        if (StringUtil.isEmpty(syParent)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.canotempty"));
        }
        if (StringUtil.isEmpty(strData)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.view.cascade.canNotEmpty"));
        }

        dynaBean.set("RESOURCETABLE_TABLECODE", resourcetableTablecode);
        if (metaTableRpcService.checkTableCodeExcludeCode(dynaBean)) {
            throw new PlatformException(MessageUtils.getMessage("table.code.repeat"), PlatformExceptionEnum.JE_CORE_TABLE_CHECKCOLUMN_ERROR);
        }
        dynaBean.table("JE_CORE_RESOURCETABLE");
        dynaBean.set("RESOURCETABLE_TABLENAME", resourcetableTablename);
        dynaBean.set("RESOURCETABLE_SQL", resourcetableSql);
        dynaBean.set("RESOURCETABLE_COORDINATE", getStringParameter(request, "RESOURCETABLE_COORDINATE"));
        dynaBean.set("SY_PARENT", syParent);
        dynaBean = metaViewRpcService.selectTableByView(strData, dynaBean);
        return BaseRespResult.successResult(dynaBean);
    }


}
