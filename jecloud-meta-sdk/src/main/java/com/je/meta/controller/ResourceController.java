/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.util.DataBaseUtils;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.table.MetaTableRpcService;
import com.je.meta.service.table.MetaTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 资源表
 */

@RestController
@RequestMapping("/je/meta/resourceTable")
public class ResourceController extends AbstractPlatformController {

    @Autowired
    private MetaTableRpcService metaTableRpcService;

    @Autowired
    private MetaTableService metaTableService;

    /**
     * 执行物理删除
     *
     * @param param
     * @param request
     * @return
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/doHardDelete", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doHardDelete(BaseMethodArgument param, HttpServletRequest request) {
        String ids = param.getIds();
        if (StringUtil.isEmpty(ids)) {
            return BaseRespResult.errorResult("1000", "", MessageUtils.getMessage("common.params.illegal"));
        }
        String result = metaTableService.doHardDeleteTable(ids);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        return BaseRespResult.successResult("1000", "", MessageUtils.getMessage("common.delete.success"));
    }

    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/clearCache", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult clearCache(BaseMethodArgument param, HttpServletRequest request) {
        String resourceTableCode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        if (StringUtil.isEmpty("resourceTableCode")) {
            return BaseRespResult.errorResult("1000", "", MessageUtils.getMessage("table.code.canotEmpty"));
        }
        List<String> list = metaTableRpcService.clearTableCache(resourceTableCode);
        for (String str : list) {
            metaService.clearMyBatisTableCache(str);
        }
        return BaseRespResult.successResult("1000", "", MessageUtils.getMessage("common.clear.success"));
    }

    /**
     * 导入表
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/import", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult importTable(BaseMethodArgument param, HttpServletRequest request) {
        DynaBean dynaBean = (DynaBean) request.getAttribute("dynaBean");
        String resourcetableTablecode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        String syParent = getStringParameter(request, "SY_PARENT");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableTablecode)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.code.canotEmpty"));
        }
        if (StringUtil.isEmpty(syParent)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.module.canotempty"));
        }
        String result = DataBaseUtils.checkTableExists(dynaBean.getStr("RESOURCETABLE_TABLECODE"));
        if ("0".equals(result)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("tableOrView.notExits"));
        }

        if (metaTableRpcService.checkTableCodeExcludeCode(dynaBean)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.code.exits"));
        } else {
            dynaBean.set("RESOURCETABLE_IMPORT", 1);
            dynaBean.set("RESOURCETABLE_ICON", "jeicon jeicon-table");
            dynaBean.set("RESOURCETABLE_ISCREATE", 0);
            dynaBean.set("RESOURCETABLE_ISUSEFOREIGNKEY", 0);
            dynaBean.set("SY_DISABLED", 0);
            dynaBean.setStr(BeanService.KEY_TABLE_CODE, "JE_CORE_RESOURCETABLE");
            metaTableRpcService.impTable(dynaBean);
            return BaseRespResult.successResult("1000", "", MessageUtils.getMessage("common.import.success"));
        }
    }

}
