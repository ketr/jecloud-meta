/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.meta.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.je.auth.check.annotation.AuthCheckPermission;
import com.je.common.base.DynaBean;
import com.je.common.base.exception.APIWarnException;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.mvc.BaseMethodArgument;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.service.CommonCheckService;
import com.je.common.base.util.DataBaseUtils;
import com.je.common.base.util.DateUtils;
import com.je.common.base.util.MessageUtils;
import com.je.common.base.util.StringUtil;
import com.je.meta.rpc.table.MetaTableRpcService;
import com.je.meta.service.dbswitch.service.MetaDataService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 资源表-操作表
 */
@RestController
@RequestMapping("/je/meta/resourceTable/table")
public class ResourceTableController extends AbstractPlatformController {

    @Autowired
    private MetaTableRpcService metaTableRpcService;
    @Autowired
    private CommonCheckService commonCheckService;
    @Autowired
    private MetaDataService metaDataService;

    /**
     * 更改主键策略
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/changePrimaryKeyPolicy", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult changePrimaryKeyPolicy(BaseMethodArgument param, HttpServletRequest request) {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        String type = getStringParameter(request, "RESOURCETABLE_KEY_GENERATOR_TYPE");
        String incrementerName = getStringParameter(request, "RESOURCETABLE_INCREMENTER_NAME");
        String generatorSql = getStringParameter(request, "RESOURCETABLE_KEY_GENERATOR_SQL");
        String tableCode = getStringParameter(request, "RESOURCETABLE_TABLECODE");
        DynaBean dynaBean = metaTableRpcService.changePrimaryKeyPolicy(resourceId, tableCode, type, incrementerName, generatorSql);
        return BaseRespResult.successResult(dynaBean.getValues());
    }

    /**
     * 应用表
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/apply", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult apply(BaseMethodArgument param, HttpServletRequest request) {
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult("1000", "", MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        if (StringUtil.isEmpty(resourceId)) {
            return BaseRespResult.errorResult("1000", "", MessageUtils.getMessage("table.id.canotEmpty"));
        }

        DynaBean table = metaTableRpcService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (null == table) {
            return BaseRespResult.errorResult("1000", "", MessageUtils.getMessage("table.notExits"));
        }
        String result = DataBaseUtils.checkTableExists(table.getStr("RESOURCETABLE_TABLECODE"));
        if ("1".equals(table.getStr("RESOURCETABLE_ISCREATE")) && !"0".equals(result)) {
            boolean status = metaTableRpcService.updateTable(resourceId, true);
            if (status) {
                List<String> list = metaTableRpcService.clearTableCache(table.getStr("RESOURCETABLE_TABLECODE"));
                for (String str : list) {
                    metaService.clearMyBatisTableCache(str);
                }
                return BaseRespResult.successResult("1000", "", MessageUtils.getMessage("table.applyUpdate"));
            }
            return BaseRespResult.errorResult("1000", "", MessageUtils.getMessage("common.update.error"));
        }
        boolean status = metaTableRpcService.createTable(resourceId);
        if (status) {
            List<String> list = metaTableRpcService.clearTableCache(table.getStr("RESOURCETABLE_TABLECODE"));
            for (String str : list) {
                metaService.clearMyBatisTableCache(str);
            }
            return BaseRespResult.successResult("1000", "", MessageUtils.getMessage("table.applySave"));
        }
        return BaseRespResult.errorResult("1000", "", MessageUtils.getMessage("common.save.error"));
    }


    /**
     * 同步资源表字段
     *
     * @param param
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/syncTableField", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult syncTableField(BaseMethodArgument param, HttpServletRequest request) {
        String pkValue = param.getPkValue();
        String ckPkValue = getStringParameter(request, "ckPkValue");
        if (StringUtil.isEmpty(pkValue)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.sysn.sourceTable.notEmpty"));
        }
        if (StringUtil.isEmpty(ckPkValue)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.sync.targetTable.notEmpty"));
        }
        metaTableRpcService.syncTableField(pkValue, ckPkValue);
        return BaseRespResult.successResult(MessageUtils.getMessage("common.synchronization.success"));
    }

    /**
     * 设置主键
     *
     * @param param
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/changeTablePrimaryKey", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult doSetPkName(BaseMethodArgument param, HttpServletRequest request) {
        String pkValue = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        String newCode = getStringParameter(request, "newCode");
        if (StringUtil.isEmpty(pkValue)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.id.canotEmpty"));
        }
        if (StringUtil.isEmpty(newCode)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("column.code.notEmpty"));
        }
        try {
            String result = metaTableRpcService.changeTablePrimaryKey(pkValue, newCode);
            if (StringUtil.isNotEmpty(result)) {
                return BaseRespResult.errorResult(result);
            }
        } catch (APIWarnException e) {
            return BaseRespResult.errorResult(e.getCode(), e.getMessage());
        }
        return BaseRespResult.successResult("", MessageUtils.getMessage("table.change.primaryKey.succes"));
    }

    /**
     * 同步表
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/syncTable", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    public BaseRespResult syncTable(BaseMethodArgument param, HttpServletRequest request) {
        String resourcetableId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");
        if (StringUtil.isEmpty(productId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("framework.product.id.notEmpty"));
        }
        if (StringUtil.isEmpty(resourcetableId)) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.id.canotEmpty"));
        }
        DynaBean table = metaTableRpcService.selectOneByPk("JE_CORE_RESOURCETABLE", resourcetableId);
        if (table == null) {
            return BaseRespResult.errorResult(MessageUtils.getMessage("table.notExits"));
        }
        String tableCode = table.getStr("RESOURCETABLE_TABLECODE");
        //同步表的逻辑改成导入表，导入表支持增量修改
        metaDataService.importTable("", tableCode, "", productId);
        metaService.clearMyBatisTableCache(tableCode);
        table = metaTableRpcService.selectOneByPk("JE_CORE_RESOURCETABLE", resourcetableId);
        return BaseRespResult.successResult(table);
    }

    /**
     * SQL视图，只支持查询数据，不支持修改和删除，可导出txt文件
     */
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/selectByShowSqlView", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public BaseRespResult selectByShowSqlView(BaseMethodArgument param, HttpServletRequest request) {
        String column = getStringParameter(request, "columns");
        String where = getStringParameter(request, "whereSql");
        if(StringUtil.isNotEmpty(where)){
            //达梦的查询条件中，值不能用双引号，否则会被认为是字段
            where = where.replaceAll("\"","'");
        }
        String sort = getStringParameter(request, "orderSql");
        String page = getStringParameter(request, "limit");
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");
        String productId = getStringParameter(request, "SY_PRODUCT_ID");

        String pattern = "^[0-9]*[1-9][0-9]*$";
        if(!Pattern.compile(pattern).matcher(page).matches()){
            return BaseRespResult.errorResult("展示条数请输入大于0的正整数！");
        }

        String[] key = {"SY_PRODUCT_ID", "JE_CORE_RESOURCETABLE_ID"};
        String result = StringUtil.checkEmpty(key, request);
        if (StringUtil.isNotEmpty(result)) {
            return BaseRespResult.errorResult(result);
        }
        DynaBean dynaBean = metaTableRpcService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
        if (StringUtil.isNotEmpty(where) || StringUtil.isNotEmpty(sort)) {
            if (!commonCheckService.checkSql(dynaBean.getStr("RESOURCETABLE_TABLECODE"), where, sort)) {
                return BaseRespResult.errorResult(MessageUtils.getMessage("common.sql.error"));
            }
        }

        List<String> columnList = Arrays.asList(column.split(","));
        List<String> whereList = Arrays.asList(where.split(","));
        List<String> orderList = Arrays.asList(sort.split(","));
        if ("".equals(column)) {
            columnList = new ArrayList<>();
        }
        if ("".equals(where)) {
            whereList = new ArrayList<>();
        }
        if ("".equals(sort)) {
            orderList = new ArrayList<>();
        }
        JSONArray columnArr = JSONArray.parseArray(JSON.toJSONString(columnList));
        JSONArray whereArr = JSONArray.parseArray(JSON.toJSONString(whereList));
        JSONArray orderArr = JSONArray.parseArray(JSON.toJSONString(orderList));

        List<Map<String, Object>> list = metaTableRpcService.selectSqlTable(columnArr, whereArr, orderArr, dynaBean.getStr("RESOURCETABLE_TABLECODE"), page);
        return BaseRespResult.successResultPage(list, Long.valueOf(String.valueOf(list.size())));
    }

    /**
     * SQL视图，导出txt文件
     */
    @ApiResponses({
            @ApiResponse(code = 200, response = File.class, message = ""),
    })
    @AuthCheckPermission(value = "pc-plugin-JE_CORE_TABLE-show")
    @RequestMapping(value = "/exportByShowSqlView", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public ResponseEntity<InputStream> exportByShowSqlView(BaseMethodArgument param, HttpServletRequest request) {
        String column = getStringParameter(request, "columns");
        String where = getStringParameter(request, "whereSql");
        if(StringUtil.isNotEmpty(where)){
            //达梦的查询条件中，值不能用双引号，否则会被认为是字段
            where = where.replaceAll("\"","'");
        }
        String sort = getStringParameter(request, "orderSql");
        String resourceId = getStringParameter(request, "JE_CORE_RESOURCETABLE_ID");

        try {

            List<String> columnList = Arrays.asList(column.split(","));
            List<String> whereList = Arrays.asList(where.split(","));
            List<String> orderList = Arrays.asList(sort.split(","));
            if ("".equals(column)) {
                columnList = new ArrayList<>();
            }
            if ("".equals(where)) {
                whereList = new ArrayList<>();
            }
            if ("".equals(sort)) {
                orderList = new ArrayList<>();
            }
            if (StringUtil.isEmpty(resourceId)) {
                throw new PlatformException(MessageUtils.getMessage("table.id.canotEmpty"), PlatformExceptionEnum.UNKOWN_ERROR);
            }

            JSONArray columnArr = JSONArray.parseArray(JSON.toJSONString(columnList));
            JSONArray whereArr = JSONArray.parseArray(JSON.toJSONString(whereList));
            JSONArray orderArr = JSONArray.parseArray(JSON.toJSONString(orderList));
            boolean status = false;
            if (StringUtil.isEmpty(column)) {
                status = true;
            }

            DynaBean dynaBean = metaTableRpcService.selectOneByPk("JE_CORE_RESOURCETABLE", resourceId);
            String tableCode = dynaBean.getStr("RESOURCETABLE_TABLECODE");

            List<Map<String, Object>> list = metaTableRpcService.selectSqlTable(columnArr, whereArr, orderArr, tableCode, null);
            List<String> insertList = metaTableRpcService.generateInsertSQL(columnArr, list, tableCode, status);

            String fileName = "_" + tableCode + "_" + DateUtils.formatDate(new Date()) + ".sql";
            StringBuffer txtDetail = new StringBuffer();
            for (String insertStr : insertList) {
                txtDetail.append(insertStr);
                txtDetail.append("\n");
            }

            InputStream inputStream = new ByteArrayInputStream((txtDetail.toString()).getBytes(StandardCharsets.UTF_8));
            fileName = URLEncoder.encode(fileName, "UTF-8");

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_TYPE, "application/octet-stream")
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName)
                    .body(inputStream);
        } catch (Exception e) {
            throw new PlatformException(MessageUtils.getMessage("common.export.error"), PlatformExceptionEnum.UNKOWN_ERROR);
        }
    }
}

